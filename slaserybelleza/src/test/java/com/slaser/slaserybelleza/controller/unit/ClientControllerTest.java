package com.slaser.slaserybelleza.controller.unit;

import com.slaser.slaserybelleza.controller.ClientController;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.service.client.ClientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static com.slaser.slaserybelleza.Utils.Fixtures.validClientDto;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientControllerTest {

    @Mock
    private ClientService clientService;

    @InjectMocks
    private ClientController clientController;

    @Test
    void shouldCallSaveClient_whenSaveRequestIsReceived() {
        //Given
        final ClientDto givenClient = validClientDto().build();
        when(clientService.add(givenClient)).thenReturn(givenClient);

        //When
        final ResponseEntity<ClientDto> actualResponse = clientController.saveClient(givenClient);

        //Then
        assertEquals(HttpStatus.CREATED, actualResponse.getStatusCode());
        assertEquals(givenClient, actualResponse.getBody());
        verifyNoMoreInteractions(clientService);
    }

    @Test
    void shouldGetClientDetails_whenGetClientRequestIsReceived() {
        //Given
        final ClientDto givenClient = validClientDto().build();
        final long givenId = 1L;
        when(clientService.getById(givenId)).thenReturn(givenClient);

        //When
        final ResponseEntity<ClientDto> actualClientDetails = clientController.getClientDetails(1L);

        //Then
        assertEquals(HttpStatus.OK, actualClientDetails.getStatusCode());
        assertEquals(givenClient, actualClientDetails.getBody());
        verifyNoMoreInteractions(clientService);
    }

    @Test
    void shouldDeleteClient_whenIdIsReceived() {
        //Given
        final long givenId = 1L;
        when(clientService.delete(givenId)).thenReturn(true);

        //When
        final ResponseEntity<Boolean> actualResponseEntity = clientController.deleteClient(1L);

        //Then
        assertEquals(HttpStatus.ACCEPTED, actualResponseEntity.getStatusCode());
        assertEquals(true, actualResponseEntity.getBody());
        verifyNoMoreInteractions(clientService);
    }

    @Test
    void shouldGetCentres_whenRequested() {
        //Given
        final List<ClientDto> givenClients = List.of(
                validClientDto().clientType("profesional").build(),
                validClientDto().clientType("profesional").build()
        );
        when(clientService.findCentres()).thenReturn(givenClients);

        //When
        final ResponseEntity<List<ClientDto>> actualClients = clientController.getCentres();

        //Then
        assertEquals(HttpStatus.OK, actualClients.getStatusCode());
        assertEquals(givenClients, actualClients.getBody());
        verifyNoMoreInteractions(clientService);
    }


    @Test
    void shouldGetClientsByCentre_whenCentreIsReceived() {
        //Given
        final List<ClientDto> givenClients = List.of(
                validClientDto().build(),
                validClientDto().build()
        );
        final String givenAsociatedCentre = "centro";
        when(clientService.findByAsociatedCentre(givenAsociatedCentre))
                .thenReturn(givenClients);

        //When
        final ResponseEntity<List<ClientDto>> actualClients = clientController.getCentreClients(givenAsociatedCentre);

        //Then
        assertEquals(HttpStatus.OK, actualClients.getStatusCode());
        assertEquals(givenClients, actualClients.getBody());
        verifyNoMoreInteractions(clientService);
    }

    @Test
    void shouldGetClientsByFilter_whenRequestFilterIsReceived() {
        //Given
        final ClientDto givenFilter = validClientDto().build();
        final List<ClientDto> givenClients = List.of(
                validClientDto().build(),
                validClientDto().build()
        );
        when(clientService.findByFilter(givenFilter))
                .thenReturn(givenClients);

        //When
        final ResponseEntity<List<ClientDto>> actualClients = clientController.getClientsByFilter(givenFilter);

        //Then
        assertEquals(HttpStatus.OK, actualClients.getStatusCode());
        assertEquals(givenClients, actualClients.getBody());
        verifyNoMoreInteractions(clientService);
    }
}