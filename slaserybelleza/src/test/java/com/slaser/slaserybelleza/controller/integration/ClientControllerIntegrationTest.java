package com.slaser.slaserybelleza.controller.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.ClientRepository;
import com.slaser.slaserybelleza.component.persistance.repository.UserRepository;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.client.UserRole;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import com.slaser.slaserybelleza.spring.security.components.JwtTokenUtil;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientControllerIntegrationTest {

    private static final String URL_SAVE = "/private/client/save";
    private static final String URL_GET = "/private/client/{id}";
    private static final String URL_DEL = "/private/client/delete/{id}";
    private static final String URL_LIST_CENTRES = "/private/client/list/centres";
    private static final String URL_LIST_CLIENTS = "/private/client/list/{asociatedCentre}";
    private static final String URL_LIST_FILTER = "/private/client/list/search";
    private static final String AUTHORIZATION = "Authorization";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @BeforeEach
    void initialize() {
        final UserRegister user = Fixtures.validUserRegister().build();
        userRepository.deleteAll();
        userRepository.save(user);
        final ClientRegister client = Fixtures.validClientRegister().build();
        clientRepository.deleteAll();
        clientRepository.save(client);
    }

    //TODO Complete tests

    @Test
    @SneakyThrows
    void shouldReturnClient_whenSaveClientRequestIsProvided() throws Exception {
        //Given
        final ClientDto givenClient = Fixtures.validClientDto().build();

        //When
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_SAVE)
                        .header(AUTHORIZATION, getToken())
                        .content(objectMapper.writeValueAsString(givenClient))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists());
    }

    @Test
    @SneakyThrows
    void shouldReturnClient_whenDetailsIdRequestIsProvided() throws Exception {
        //Given
        final long givenId = clientRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        //Then
        mockMvc.perform(get(URL_GET, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @SneakyThrows
    void shouldThrow404_WhenInvalidDetailsIdIsProvided() throws Exception {
        //Given
        final long givenId = 33333L;

        //When
        //then
        mockMvc.perform(get(URL_GET, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON)).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    void shouldDeleteClient_whenDeleteIdRequestIsProvided() throws Exception {
        //Given
        final long givenId = clientRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        //Then
        mockMvc.perform(get(URL_DEL, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andReturn().getResponse();
    }

    @Test
    @SneakyThrows
    void shouldReturnAsociatedCentres_whenRequired() throws Exception {
        //Given
        List<ClientRegister> givenClients = clientRepository.findAll();

        //When
        //Then
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL_LIST_CENTRES)
                .header(AUTHORIZATION, getToken())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists());
    }


    @Test
    @SneakyThrows
    void shouldReturnListOfClientsByAsociatedCentre_whenAsociatedCentreIsProvided() throws Exception {
        //Given
        List<ClientRegister> givenClients = clientRepository.findAll();
        final ClientDto givenClient = Fixtures.validClientDto().build();

        //When
        //Then
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_LIST_CLIENTS, "Vernys")
                        .header(AUTHORIZATION, getToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists());
    }

    @ParameterizedTest(name = "[{0}]")
    @MethodSource("filter")
    @SneakyThrows
    void shouldReturnListOfClients_whenFilterIsProvided(String name, String asociatedCentre)  throws Exception{
        //Given
        final ClientRegister givenFilter = ClientRegister.builder().clientType("particular").name(name).asociatedCentre(asociatedCentre).build();
        final ClientRegister client3 = Fixtures.validClientRegister().clientType("particular").name("Ted").asociatedCentre("centre1").creationDate(null).build();
        final ClientRegister client4 = Fixtures.validClientRegister().clientType("particular").name("Dougal").asociatedCentre("centre1").creationDate(null).build();
        final ClientRegister client5 = Fixtures.validClientRegister().clientType("particular").name("Jack").asociatedCentre("centre2").creationDate(null).build();
        final ClientRegister client6 = Fixtures.validClientRegister().clientType("particular").name("Javier").asociatedCentre("centre2").creationDate(null).build();
        clientRepository.save(client3);
        clientRepository.save(client4);
        clientRepository.save(client5);
        clientRepository.save(client6);

        //When
        //Then
        final String actualResult = mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_LIST_FILTER)
                        .header(AUTHORIZATION, getToken())
                        .content(objectMapper.writeValueAsString(givenFilter))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        final List<ClientDto> ClientDtoResult = Arrays.asList(objectMapper.readValue(actualResult, ClientDto[].class));
        assertEquals(name, ClientDtoResult.get(0).getName());
        assertEquals(asociatedCentre, ClientDtoResult.get(0).getAsociatedCentre());
    }

    private String getToken() {
        User givenUser = new User("user", "password", List.of(UserRole.USER));
        return "Bearer " + jwtTokenUtil.generateToken(givenUser);
    }

    static private Stream<Arguments> filter() {
        return Stream.of(
                Arguments.of("Dougal", "centre1"),
                Arguments.of("Ted", "centre1"),
                Arguments.of("Javier", "centre2"),
                Arguments.of("Jack", "centre2")
        );
    }
}
