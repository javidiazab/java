package com.slaser.slaserybelleza.controller.unit;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.controller.LoginController;
import com.slaser.slaserybelleza.model.client.AuthenticationRequest;
import com.slaser.slaserybelleza.model.login.JwtResponse;
import com.slaser.slaserybelleza.service.LoginService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoginControllerTest {

    @Mock
    private LoginService loginService;

    @InjectMocks
    private LoginController loginController;

    @Test
    @SneakyThrows
    void shouldCallSaveAppointment_whenSaveRequestIsReceived() throws Exception {
        //Given
        final AuthenticationRequest givenAuthenticationRequest = Fixtures.validAuthenticationRequest().build();
        final JwtResponse givenJwtResponse = new JwtResponse(givenAuthenticationRequest.getUser(), "some token");
        when(loginService.createAuthenticationToken(givenAuthenticationRequest)).thenReturn(givenJwtResponse);

        //When
        final ResponseEntity<JwtResponse> actualResponse = loginController.login(givenAuthenticationRequest);

        //Then
        assertEquals("some token", actualResponse.getBody().getToken());
        assertEquals("user", actualResponse.getBody().getUserName());
        verifyNoMoreInteractions(loginService);
    }
}