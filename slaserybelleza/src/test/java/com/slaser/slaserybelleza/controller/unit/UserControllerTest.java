package com.slaser.slaserybelleza.controller.unit;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.controller.UserController;
import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.service.user.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @Test
    void shouldCallSaveUser_whenSaveRequestIsReceived() {
        //Given
        final UserDto givenUserDto = Fixtures.validUserDto().build();
        when(userService.save(givenUserDto)).thenReturn(givenUserDto);

        //When
        final ResponseEntity<UserDto> actualUserDto = userController.saveUser(givenUserDto);

        //Then
        assertEquals(HttpStatus.ACCEPTED, actualUserDto.getStatusCode());
        assertEquals(givenUserDto, actualUserDto.getBody());
        verifyNoMoreInteractions(userService);
    }

    @Test
    void shouldCallUserDetails_whenUserNameIsReceived() {
        //Given
        final String givenUserName = "user_name";
        final UserDto givenUserDto = Fixtures.validUserDto().build();
        when(userService.getById(givenUserName)).thenReturn(givenUserDto);

        //When
        final ResponseEntity<UserDto> actualUserDto = userController.getUserDetails(givenUserName);

        //Then
        assertEquals(HttpStatus.OK, actualUserDto.getStatusCode());
        assertEquals(givenUserDto, actualUserDto.getBody());
        verifyNoMoreInteractions(userService);
    }

    @Test
    void shouldCallUserDelete_whenUserNameIsReceived() {
        //Given
        final String givenUserName = "user_name";
        final UserDto givenUserDto = Fixtures.validUserDto().build();
        when(userService.delete(givenUserName)).thenReturn(true);

        //When
        final ResponseEntity<Boolean> actualResponse = userController.deleteUser(givenUserName);

        //Then
        assertEquals(HttpStatus.ACCEPTED, actualResponse.getStatusCode());
        assertTrue(actualResponse.getBody());
        verifyNoMoreInteractions(userService);
    }

    @Test
    void shouldReturn404_whenDeleteUserAndNotFound() {
        //Given
        final String givenUserName = "xxx";
        when(userService.delete(givenUserName)).thenReturn(false);

        //When
        final ResponseEntity<Boolean> actualResponse = userController.deleteUser(givenUserName);

        //Then
        assertEquals(HttpStatus.BAD_REQUEST, actualResponse.getStatusCode());
        assertFalse(actualResponse.getBody());
        verifyNoMoreInteractions(userService);
    }
}