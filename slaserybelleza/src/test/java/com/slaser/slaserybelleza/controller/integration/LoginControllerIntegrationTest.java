package com.slaser.slaserybelleza.controller.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.model.client.AuthenticationRequest;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import com.slaser.slaserybelleza.model.login.JwtResponse;
import com.slaser.slaserybelleza.service.LoginService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class LoginControllerIntegrationTest {

    private static final String URL = "/login";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LoginService loginService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    @SneakyThrows
    void shouldReturnTokenWhenValidRequestIsProvided() throws Exception {
        //Given
        UserRegister givenUser = Fixtures.validUserRegister().build();
        final AuthenticationRequest givenAuthenticationRequest = Fixtures.validAuthenticationRequest().build();
        when(loginService.createAuthenticationToken(givenAuthenticationRequest)).thenReturn(new JwtResponse(givenUser.getUserName(), "token"));

        //When
        //Then
        mockMvc.perform(post(URL)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(givenAuthenticationRequest))
                        .accept(APPLICATION_JSON))
                .andExpect(status().isAccepted());
//                .andExpect(ResultMatcher.matchAll(jsonPath("$.token", is(givenJwtResponse))));
    }
}
