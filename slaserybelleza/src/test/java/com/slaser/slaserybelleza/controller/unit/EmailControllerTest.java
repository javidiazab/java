package com.slaser.slaserybelleza.controller.unit;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.controller.EmailController;
import com.slaser.slaserybelleza.model.client.EmailBodyDto;
import com.slaser.slaserybelleza.service.email.EmailSenderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailControllerTest {

    @Mock
    private EmailSenderService emailSenderService;

    @InjectMocks
    private EmailController emailController;

    @Test
    void shouldCallSaveAppointment_whenSaveRequestIsReceived() {
        //Given
        final EmailBodyDto givenEmailBody = Fixtures.validEmailBody().build();
        when(emailSenderService.sendEmail(givenEmailBody)).thenReturn(true);

        //When
        emailController.SendEmail(givenEmailBody);

        //Then
        verifyNoMoreInteractions(emailSenderService);
    }

}