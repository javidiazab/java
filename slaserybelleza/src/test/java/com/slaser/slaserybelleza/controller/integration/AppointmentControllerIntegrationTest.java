package com.slaser.slaserybelleza.controller.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.AppointmentRepository;
import com.slaser.slaserybelleza.component.persistance.repository.UserRepository;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.client.UserRole;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import com.slaser.slaserybelleza.service.appointment.AppointmentService;
import com.slaser.slaserybelleza.spring.security.components.JwtTokenUtil;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AppointmentControllerIntegrationTest {

    private static final String URL_SAVE = "/private/appointment/save";
    private static final String URL_GET = "/private/appointment/{id}";
    private static final String URL_DEL = "/private/appointment/delete/{id}";
    private static final String URL_LIST = "/private/appointment/list?start={start}&end={end}";
    private static final String URL_LIST_BY_CLI = "/private/appointment/list/{idCli}?start={start}&end={end}";
    private static final String AUTHORIZATION = "Authorization";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @BeforeEach
    void initialize() {
        final UserRegister user = Fixtures.validUserRegister().build();
        final AppointmentRegister appointment = Fixtures.validAppointmentRegister().build();
        userRepository.deleteAll();
        userRepository.save(user);
        appointmentRepository.deleteAll();
        appointmentRepository.save(appointment);
    }

    //TODO Complete tests

    @Test
    @SneakyThrows
    void shouldReturnAppointment_whenSaveAppointmentRequestIsProvided() throws Exception {
        final AppointmentDto givenAppointment = Fixtures.validAppointmentDto().build();

        //When
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_SAVE)
                        .header(AUTHORIZATION, getToken())
                        .content(objectMapper.writeValueAsString(givenAppointment))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.subject").exists());
    }

    @Test
    @SneakyThrows
    void shouldReturnAppointment_whenDetailsIdRequestIsProvided() throws Exception {
        //Given
        final long givenId = appointmentRepository.findAll().stream().findFirst().orElseThrow().getId();
        //When
        //Then
        mockMvc.perform(get(URL_GET, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @SneakyThrows
    void shouldThrow404_WhenInvalidDetailsIdIsProvided() throws Exception {
        //Given
        final long givenId = 33333L;

        //When
        //then
        mockMvc.perform(get(URL_GET, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON)).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    void shouldDeteleAppointmenty_WhenDeleteIdIsProvided() throws Exception {
        //Given
        final long givenId = appointmentRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        //then
        mockMvc.perform(get(URL_DEL, givenId)
                        .header(AUTHORIZATION, getToken())
                        .accept(APPLICATION_JSON)).andDo(print())
                .andExpect(status().isAccepted())
                .andReturn().getResponse();
    }

    @Test
    @SneakyThrows
    void shouldReturnListOfAppointments_whenDatesAreProvided() throws Exception {
        //Given
        final LocalDateTime start = LocalDateTime.now().minusDays(1);
        final LocalDateTime end = LocalDateTime.now().plusDays(1);
        List<AppointmentRegister> givenAppointments = appointmentRepository.findAll();

        //When
        //then
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_LIST, start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                                end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                        .header(AUTHORIZATION, getToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(MockMvcResultMatchers.jsonPath("$.subject").exists());
    }

    @Test
    @SneakyThrows
    void shouldReturnListOfAppointments_whenIdAndDatesAreProvided() throws Exception {
        //Given
        final Long idCli = 1L;
        final LocalDateTime start = LocalDateTime.now().minusDays(1);
        final LocalDateTime end = LocalDateTime.now().plusDays(1);
        List<AppointmentRegister> givenAppointments = appointmentRepository.findAll();

        //When
        //then
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_LIST_BY_CLI, idCli,
                                start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                                end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                        .header(AUTHORIZATION, getToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(MockMvcResultMatchers.jsonPath("$.subject").exists());
    }

    private String getToken() {
        User givenUser = new User("user", "password", List.of(UserRole.USER));
        return "Bearer " + jwtTokenUtil.generateToken(givenUser);
    }
}
