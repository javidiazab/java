package com.slaser.slaserybelleza.service.appointment;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AppointmentMapperTest {

    @Test
    void shoudMapToAppointmentRegister_whengivenAppointmentDto() {
        //Given
        AppointmentRegister givenAppointmentRegister = Fixtures.validAppointmentRegister().build();

        //When
        AppointmentDto actualAppointmentDto = AppointmentMapper.buildAppointmentDto(givenAppointmentRegister);

        //Then
        assertEquals(givenAppointmentRegister.getId(), actualAppointmentDto.getId());
        assertEquals(givenAppointmentRegister.getSubject(), actualAppointmentDto.getSubject());
        assertEquals(givenAppointmentRegister.getPlace(), actualAppointmentDto.getPlace());
        assertEquals(givenAppointmentRegister.getDescription(), actualAppointmentDto.getDescription());
        assertEquals(givenAppointmentRegister.getDuration(), actualAppointmentDto.getDuration());
        assertEquals(givenAppointmentRegister.getType(), actualAppointmentDto.getType());
        assertEquals(givenAppointmentRegister.getCancelDate(), actualAppointmentDto.getCancelDate());
        assertEquals(givenAppointmentRegister.getCreationDate(), actualAppointmentDto.getCreationDate());
        assertEquals(givenAppointmentRegister.getEndDate(), actualAppointmentDto.getEndDate());
    }

    @Test
    void shoudMapToAppointmentDto_whengivenAppointmentRegister() {
        //Given
        AppointmentDto givenAppointmentDto = Fixtures.validAppointmentDto().build();

        //When
        AppointmentRegister actualAppointmentRegister = AppointmentMapper.buildAppointmentRegister(givenAppointmentDto);

        //Then
        assertEquals(givenAppointmentDto.getId(), actualAppointmentRegister.getId());
        assertEquals(givenAppointmentDto.getSubject(), actualAppointmentRegister.getSubject());
        assertEquals(givenAppointmentDto.getPlace(), actualAppointmentRegister.getPlace());
        assertEquals(givenAppointmentDto.getDescription(), actualAppointmentRegister.getDescription());
        assertEquals(givenAppointmentDto.getDuration(), actualAppointmentRegister.getDuration());
        assertEquals(givenAppointmentDto.getType(), actualAppointmentRegister.getType());
        assertEquals(givenAppointmentDto.getCancelDate(), actualAppointmentRegister.getCancelDate());
        assertEquals(givenAppointmentDto.getCreationDate(), actualAppointmentRegister.getCreationDate());
        assertEquals(givenAppointmentDto.getEndDate(), actualAppointmentRegister.getEndDate());
    }

}