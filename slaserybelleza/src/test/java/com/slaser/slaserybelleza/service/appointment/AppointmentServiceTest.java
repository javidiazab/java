package com.slaser.slaserybelleza.service.appointment;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.store.AppointmentStore;
import com.slaser.slaserybelleza.utils.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceTest {

    @Mock
    private AppointmentStore appointmentStore;

    @InjectMocks
    private  AppointmentService appointmentService;

    @Test
    void shouldAddAppointment_whenAppointmentDtoWithNoIdProvided() {
        //Given
        AppointmentDto givenAppointmentDto = Fixtures.validAppointmentDto().build();
        final AppointmentRegister appointmentRegister = AppointmentMapper.buildAppointmentRegister(givenAppointmentDto);
        when(appointmentStore.save(appointmentRegister)).thenReturn(appointmentRegister);

        //When
        AppointmentDto actualResult = appointmentService.add(givenAppointmentDto);

        //Then
        verifyNoMoreInteractions(appointmentStore);
    }

    @Test
    void shouldUpdateAppointment_whenAppointmentDtoWithIdProvided() {
        //Given
        AppointmentDto givenAppointmentDto = Fixtures.validAppointmentDto().id(2L).build();
        final AppointmentRegister appointmentRegister = AppointmentMapper.buildAppointmentRegister(givenAppointmentDto);
        when(appointmentStore.update(appointmentRegister)).thenReturn(appointmentRegister);

        //When
        appointmentService.add(givenAppointmentDto);

        //Then
        verifyNoMoreInteractions(appointmentStore);
    }

    @Test
    void shouldReturnNull_whenErrorStoringAppointment() {
        //Given
        AppointmentDto givenAppointmentDto = Fixtures.validAppointmentDto().id(2L).build();

        //When
        AppointmentDto actualResult = appointmentService.add(givenAppointmentDto);

        //Then
        assertTrue(Objects.isNull(actualResult));
    }

    @Test
    void shouldGetAppointment_whenIdProvided() {
        //Given
        final long givenId = 2L;
        AppointmentDto givenAppointmentDto = Fixtures.validAppointmentDto().id(2L).build();
        final AppointmentRegister appointmentRegister = AppointmentMapper.buildAppointmentRegister(givenAppointmentDto);
        when(appointmentStore.getById(givenId)).thenReturn(Optional.of(appointmentRegister));

        //When
        AppointmentDto actualResult = appointmentService.getById(givenId);

        //Then
        verifyNoMoreInteractions(appointmentStore);
        assertEquals(givenAppointmentDto.getSubject(), actualResult.getSubject());
        assertEquals(givenAppointmentDto.getPlace(), actualResult.getPlace());
        assertEquals(givenAppointmentDto.getDescription(), actualResult.getDescription());
        assertEquals(givenAppointmentDto.getDuration(), actualResult.getDuration());
        assertEquals(givenAppointmentDto.getType(), actualResult.getType());
        assertEquals(givenAppointmentDto.getCancelDate(), actualResult.getCancelDate());
        assertEquals(givenAppointmentDto.getStartingDate(), actualResult.getStartingDate());
        assertEquals(givenAppointmentDto.getEndDate(), actualResult.getEndDate());
    }

    @Test
    void shouldReturnNull_whenGetByIdNotFound() {
        //Given
        final long givenId = 2L;
        when(appointmentStore.getById(givenId)).thenReturn(Optional.empty());

        //When
        AppointmentDto actualResult = appointmentService.getById(givenId);

        //Then
        verifyNoMoreInteractions(appointmentStore);
        assertTrue(Objects.isNull(actualResult));
    }

    @Test
    void shouldDeleteAppointment_whenIdProvided() {
        //Given
        final long givenId = 2L;
        when(appointmentStore.delete(givenId)).thenReturn(true);

        //When
        boolean actualResult = appointmentService.delete(givenId);

        //Then
        verifyNoMoreInteractions(appointmentStore);
        assertTrue(actualResult);
    }

    @Test
    void shouldGetAppointments_whenIdIsReceived() {
        //Given
        final List<AppointmentRegister> givenAppointments = List.of(
                Objects.requireNonNull(AppointmentMapper.buildAppointmentRegister(Fixtures.validAppointmentDto().build())),
                Objects.requireNonNull(AppointmentMapper.buildAppointmentRegister(Fixtures.validAppointmentDto().build()))
        );
        final String givenStart = "2021-01-01";
        final String givenEnd = "2021-01-31";
        final LocalDateTime givenStartingDate = Utils.convertToDate(givenStart);
        final LocalDateTime givenEndDate = Utils.convertToDate(givenEnd).toLocalDate().atTime(LocalTime.MAX);
        when(appointmentStore.findByDates(givenStartingDate, givenEndDate))
                .thenReturn(givenAppointments);

        //When
        List<AppointmentDto> ActualAppointments = appointmentService.findByDates(givenStartingDate, givenEndDate);

        //Then
        assertEquals(2, ActualAppointments.size());
        verifyNoMoreInteractions(appointmentStore);
    }

    @Test
    void shouldGetAppointmentsByCli_whenRequestIsReceived() {
        //Given
        final List<AppointmentRegister> givenAppointments = List.of(
                Objects.requireNonNull(AppointmentMapper.buildAppointmentRegister(Fixtures.validAppointmentDto().build())),
                Objects.requireNonNull(AppointmentMapper.buildAppointmentRegister(Fixtures.validAppointmentDto().build()))
        );
        final long givenIdCli = 1L;
        final String givenStart = "2021-01-01";
        final String givenEnd = "2021-01-31";
        final LocalDateTime givenStartingDate = Utils.convertToDate(givenStart);
        final LocalDateTime givenEndDate = Utils.convertToDate(givenEnd).toLocalDate().atTime(LocalTime.MAX);
        when(appointmentStore.findByIdCliAndDates(givenIdCli, givenStartingDate, givenEndDate))
                .thenReturn(givenAppointments);

        //When
        List<AppointmentDto> ActualAppointments = appointmentService.findByIdCliAndDates(givenIdCli, givenStartingDate, givenEndDate);

        //Then
        assertEquals(2, ActualAppointments.size());
        verifyNoMoreInteractions(appointmentStore);
    }

}