package com.slaser.slaserybelleza.service.client;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClientMapperTest {

    @Test
    void shoudMapToClientRegister_whengivenClientDto() {
        //Given
        ClientRegister givenClientRegister = Fixtures.validClientRegister().build();

        //When
        ClientDto actualClientDto = ClientMapper.buildClientDto(givenClientRegister);

        //Then
        assertEquals(givenClientRegister.getId(), actualClientDto.getId());
        assertEquals(givenClientRegister.getClientType(), actualClientDto.getClientType());
        assertEquals(givenClientRegister.getName(), actualClientDto.getName());
        assertEquals(givenClientRegister.getSurname1(), actualClientDto.getSurname1());
        assertEquals(givenClientRegister.getSurname2(), actualClientDto.getSurname2());
        assertEquals(givenClientRegister.getAsociatedCentre(), actualClientDto.getAsociatedCentre());
        assertEquals(givenClientRegister.getTypeIdentification(), actualClientDto.getTypeIdentification());
        assertEquals(givenClientRegister.getIdentification(), actualClientDto.getIdentification());
        assertEquals(givenClientRegister.getStreetType(), actualClientDto.getStreetType());
        assertEquals(givenClientRegister.getStreet(), actualClientDto.getStreet());
        assertEquals(givenClientRegister.getStreetNo(), actualClientDto.getStreetNo());
        assertEquals(givenClientRegister.getFloor(), actualClientDto.getFloor());
        assertEquals(givenClientRegister.getLetter(), actualClientDto.getLetter());
        assertEquals(givenClientRegister.getCity(), actualClientDto.getCity());
        assertEquals(givenClientRegister.getCp(), actualClientDto.getCp());
        assertEquals(givenClientRegister.getProvince(), actualClientDto.getProvince());
        assertEquals(givenClientRegister.getCountry(), actualClientDto.getCountry());
        assertEquals(givenClientRegister.getTelephone(), actualClientDto.getTelephone());
        assertEquals(givenClientRegister.getLandline(), actualClientDto.getLandline());
        assertEquals(givenClientRegister.getEmail(), actualClientDto.getEmail());
        assertEquals(givenClientRegister.getWeb(), actualClientDto.getWeb());
        assertEquals(givenClientRegister.getOtherInfo(), actualClientDto.getOtherInfo());
        assertEquals(givenClientRegister.getGdpr(), actualClientDto.getGdpr());
        assertEquals(givenClientRegister.getCreationDate(), actualClientDto.getCreationDate());
    }

    @Test
    void shoudMapToClientDto_whengivenClientRegister() {
        //Given
        ClientDto givenClientDto = Fixtures.validClientDto().build();

        //When
        ClientRegister actualClientRegister = ClientMapper.buildClientRegister(givenClientDto);

        //Then
        assertEquals(givenClientDto.getId(), actualClientRegister.getId());
        assertEquals(givenClientDto.getClientType(), actualClientRegister.getClientType());
        assertEquals(givenClientDto.getName(), actualClientRegister.getName());
        assertEquals(givenClientDto.getSurname1(), actualClientRegister.getSurname1());
        assertEquals(givenClientDto.getSurname2(), actualClientRegister.getSurname2());
        assertEquals(givenClientDto.getAsociatedCentre(), actualClientRegister.getAsociatedCentre());
        assertEquals(givenClientDto.getTypeIdentification(), actualClientRegister.getTypeIdentification());
        assertEquals(givenClientDto.getIdentification(), actualClientRegister.getIdentification());
        assertEquals(givenClientDto.getStreetType(), actualClientRegister.getStreetType());
        assertEquals(givenClientDto.getStreet(), actualClientRegister.getStreet());
        assertEquals(givenClientDto.getStreetNo(), actualClientRegister.getStreetNo());
        assertEquals(givenClientDto.getFloor(), actualClientRegister.getFloor());
        assertEquals(givenClientDto.getLetter(), actualClientRegister.getLetter());
        assertEquals(givenClientDto.getCity(), actualClientRegister.getCity());
        assertEquals(givenClientDto.getCp(), actualClientRegister.getCp());
        assertEquals(givenClientDto.getProvince(), actualClientRegister.getProvince());
        assertEquals(givenClientDto.getCountry(), actualClientRegister.getCountry());
        assertEquals(givenClientDto.getTelephone(), actualClientRegister.getTelephone());
        assertEquals(givenClientDto.getLandline(), actualClientRegister.getLandline());
        assertEquals(givenClientDto.getEmail(), actualClientRegister.getEmail());
        assertEquals(givenClientDto.getWeb(), actualClientRegister.getWeb());
        assertEquals(givenClientDto.getOtherInfo(), actualClientRegister.getOtherInfo());
        assertEquals(givenClientDto.getGdpr(), actualClientRegister.getGdpr());
        assertEquals(givenClientDto.getCreationDate(), actualClientRegister.getCreationDate());
    }

}