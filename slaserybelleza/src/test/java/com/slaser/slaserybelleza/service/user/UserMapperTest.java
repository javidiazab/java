package com.slaser.slaserybelleza.service.user;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserMapperTest {


    @Test
    void shoudMapToUserRegister_whengivenUserDto() {
        //Given
        UserRegister givenUserRegister = Fixtures.validUserRegister().build();

        //When
        UserDto actualUserDto = UserMapper.buildUserDto(givenUserRegister);

        //Then
        assertEquals(givenUserRegister.getUserName(), actualUserDto.getUserName());
        assertEquals(givenUserRegister.getPassword(), givenUserRegister.getPassword());
        assertEquals(givenUserRegister.getUserFullName(), givenUserRegister.getUserFullName());
        Assertions.assertEquals(givenUserRegister.getRole(), givenUserRegister.getRole());
        assertEquals(givenUserRegister.getRetries(), givenUserRegister.getRetries());
        assertEquals(givenUserRegister.getCreationDate(), givenUserRegister.getCreationDate());
        assertEquals(givenUserRegister.getEndDate(), givenUserRegister.getEndDate());
    }

    @Test
    void shoudMapToUserDto_whengivenUserRegister() {
        //Given
        UserDto givenUserDto = Fixtures.validUserDto().build();

        //When
        UserRegister actualUserRegister = UserMapper.buildUserRegister(givenUserDto);

        //Then
        assertEquals(givenUserDto.getUserName(), actualUserRegister.getUserName());
        assertEquals(givenUserDto.getPassword(), actualUserRegister.getPassword());
        assertEquals(givenUserDto.getUserFullName(), actualUserRegister.getUserFullName());
        Assertions.assertEquals(givenUserDto.getRole(), actualUserRegister.getRole());
        assertEquals(givenUserDto.getRetries(), actualUserRegister.getRetries());
        assertEquals(givenUserDto.getCreationDate(), actualUserRegister.getCreationDate());
        assertEquals(givenUserDto.getEndDate(), actualUserRegister.getEndDate());
    }
}