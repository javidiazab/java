package com.slaser.slaserybelleza.service.client;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.model.store.ClientStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.slaser.slaserybelleza.Utils.Fixtures.validClientDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @Mock
    private ClientStore clientStore;

    @InjectMocks
    private ClientService clientService;

    @Test
    void shouldAddClient_whenClientDtoWithNoIdProvided() {
        //Given
        ClientDto givenClientDto = Fixtures.validClientDto().build();
        final ClientRegister clientRegister = ClientMapper.buildClientRegister(givenClientDto);
        when(clientStore.save(clientRegister)).thenReturn(clientRegister);

        //When
        ClientDto actualResult = clientService.add(givenClientDto);

        //Then
        verifyNoMoreInteractions(clientStore);
    }

    @Test
    void shouldUpdateClient_whenClientDtoWithIdProvided() {
        //Given
        ClientDto givenClientDto = Fixtures.validClientDto().id(2L).build();
        final ClientRegister clientRegister = ClientMapper.buildClientRegister(givenClientDto);
        when(clientStore.update(clientRegister)).thenReturn(clientRegister);

        //When
        clientService.add(givenClientDto);

        //Then
        verifyNoMoreInteractions(clientStore);
    }

    @Test
    void shouldReturnNull_whenErrorStoringClient() {
        //Given
        ClientDto givenClientDto = Fixtures.validClientDto().id(2L).build();

        //When
        ClientDto actualResult = clientService.add(givenClientDto);

        //Then
        assertTrue(Objects.isNull(actualResult));
    }

    @Test
    void shouldGetClient_whenIdProvided() {
        //Given
        final long givenId = 2L;
        ClientDto givenClientDto = Fixtures.validClientDto().id(2L).build();
        final ClientRegister clientRegister = ClientMapper.buildClientRegister(givenClientDto);
        when(clientStore.getById(givenId)).thenReturn(Optional.of(clientRegister));

        //When
        ClientDto actualResult = clientService.getById(givenId);

        //Then
        verifyNoMoreInteractions(clientStore);
        assertEquals(givenClientDto.getId(), actualResult.getId());
        assertEquals(givenClientDto.getClientType(), actualResult.getClientType());
        assertEquals(givenClientDto.getName(), actualResult.getName());
        assertEquals(givenClientDto.getSurname1(), actualResult.getSurname1());
        assertEquals(givenClientDto.getSurname2(), actualResult.getSurname2());
        assertEquals(givenClientDto.getAsociatedCentre(), actualResult.getAsociatedCentre());
        assertEquals(givenClientDto.getTypeIdentification(), actualResult.getTypeIdentification());
        assertEquals(givenClientDto.getIdentification(), actualResult.getIdentification());
        assertEquals(givenClientDto.getStreetType(), actualResult.getStreetType());
        assertEquals(givenClientDto.getStreet(), actualResult.getStreet());
        assertEquals(givenClientDto.getStreetNo(), actualResult.getStreetNo());
        assertEquals(givenClientDto.getFloor(), actualResult.getFloor());
        assertEquals(givenClientDto.getLetter(), actualResult.getLetter());
        assertEquals(givenClientDto.getCity(), actualResult.getCity());
        assertEquals(givenClientDto.getCp(), actualResult.getCp());
        assertEquals(givenClientDto.getProvince(), actualResult.getProvince());
        assertEquals(givenClientDto.getCountry(), actualResult.getCountry());
        assertEquals(givenClientDto.getTelephone(), actualResult.getTelephone());
        assertEquals(givenClientDto.getLandline(), actualResult.getLandline());
        assertEquals(givenClientDto.getEmail(), actualResult.getEmail());
        assertEquals(givenClientDto.getWeb(), actualResult.getWeb());
        assertEquals(givenClientDto.getOtherInfo(), actualResult.getOtherInfo());
        assertEquals(givenClientDto.getGdpr(), actualResult.getGdpr());
        assertEquals(givenClientDto.getCreationDate(), actualResult.getCreationDate());
    }

    @Test
    void shouldReturnNull_whenGetByIdNotFound() {
        //Given
        final long givenId = 2L;
        when(clientStore.getById(givenId)).thenReturn(Optional.empty());

        //When
        ClientDto actualResult = clientService.getById(givenId);

        //Then
        verifyNoMoreInteractions(clientStore);
        assertTrue(Objects.isNull(actualResult));
    }

    @Test
    void shouldDeleteClient_whenIdProvided() {
        //Given
        final long givenId = 2L;
        when(clientStore.delete(givenId)).thenReturn(true);

        //When
        boolean actualResult = clientService.delete(givenId);

        //Then
        verifyNoMoreInteractions(clientStore);
        assertTrue(actualResult);
    }

    @Test
    void shouldRetrieveCentres_whenRequestIsReceived() {
        //Given
        final String givenClientType = "profesional";
        final List<ClientRegister> givenClients = List.of(
                Fixtures.validClientRegister().asociatedCentre("centre1").build(),
                Fixtures.validClientRegister().asociatedCentre("centre1").build()
        );
        when(clientStore.findAllByClientType(givenClientType))
                .thenReturn(givenClients);

        //When
        List<ClientDto> ActualClients = clientService.findCentres();

        //Then
        assertEquals(2, ActualClients.size());
        verifyNoMoreInteractions(clientStore);
    }

    @Test
    void shouldRetrieveClientsByAsociatedCentre_whenRequestIsReceived() {
        //Given
        final String givenAsociatedCentre = "centre";
        final List<ClientRegister> givenClients = List.of(
                Fixtures.validClientRegister().build(),
                Fixtures.validClientRegister().build()
        );
        when(clientStore.findClientsByAsociatedCentre(givenAsociatedCentre))
                .thenReturn(givenClients);

        //When
        List<ClientDto> ActualClients = clientService.findByAsociatedCentre(givenAsociatedCentre);

        //Then
        assertEquals(2, ActualClients.size());
        verifyNoMoreInteractions(clientStore);
    }

    @Test
    void shouldRetrieveClientsByFilter_whenFilterIsReceived() {
        //Given
        final ClientDto givenFilter = ClientDto.builder()
                .clientType("particular")
                .build();
        final List<ClientRegister> givenClients = List.of(
                Fixtures.validClientRegister().build(),
                Fixtures.validClientRegister().build()
        );
        final ClientRegisterSpecification specification =
                new ClientRegisterSpecification(ClientMapper.buildClientRegister(givenFilter));
        when(clientStore.findAllByFilter(any(ClientRegisterSpecification.class))).thenReturn(givenClients);

        //When
        List<ClientDto> ActualClients = clientService.findByFilter(givenFilter);

        //Then
        assertEquals(2, ActualClients.size());
        verifyNoMoreInteractions(clientStore);
    }
}