package com.slaser.slaserybelleza.model.store.unit;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.ClientRepository;
import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.model.store.ClientStore;
import com.slaser.slaserybelleza.service.client.ClientRegisterSpecification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.slaser.slaserybelleza.Utils.Fixtures.validClientDto;
import static com.slaser.slaserybelleza.Utils.Fixtures.validClientRegister;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientStoreTest {

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientStore clientStore;

    @Test
    void shouldCallSaveClient_whenSaveRequestIsReceived() {
        //Given
        final ClientRegister givenClientRegister = Fixtures.validClientRegister().build();
        when(clientRepository.save(givenClientRegister)).thenReturn(givenClientRegister);

        //When
        final ClientRegister actualClient = clientStore.save(givenClientRegister);

        //Then
        assertEquals(givenClientRegister, actualClient);
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldThrowSLaserExecption_whenSaveRequestIdIsPresent() {
        //Given
        final ClientRegister givenClientRegister =
                Fixtures.validClientRegister().id(1L).build();

        //When
        //Then
        final SLaserException actualException = assertThrows(SLaserException.class, () -> clientStore.save(givenClientRegister));

        assertEquals("Client id is not null", actualException.getMessage());
        verifyNoInteractions(clientRepository);
    }

    @Test
    void shouldCallUpdateClient_whenUpdateRequestIsReceivedAndClientIsFound() {
        //Given
        final ClientRegister givenClientRegister =
                Fixtures.validClientRegister().id(1L).build();
        when(clientRepository.findById(givenClientRegister.getId())).thenReturn(Optional.of(givenClientRegister));
        when(clientRepository.save(givenClientRegister)).thenReturn(givenClientRegister);

        //When
        final ClientRegister actualClient = clientStore.update(givenClientRegister);

        //Then
        assertEquals(givenClientRegister, actualClient);
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldThrowSLaserException_whenUpdateRequestIdIsNOTPresent() {
        //Given
        final ClientRegister givenClientRegister = Fixtures.validClientRegister().build();

        //When
        //Then
        final SLaserException actualException = assertThrows(SLaserException.class, () -> clientStore.update(givenClientRegister));

        assertEquals("Client id is null", actualException.getMessage());
        verifyNoInteractions(clientRepository);
    }

    @Test
    void shouldThrowSLaserException_whenUpdateRequestIdIsPresentAndClientIsNotFound() {
        //Given
        final ClientRegister givenClientRegister = Fixtures.validClientRegister().id(1L).build();
        when(clientRepository.findById(givenClientRegister.getId())).thenReturn(Optional.empty());

        //When
        //Then
        final SLaserException actualException = assertThrows(SLaserException.class, () -> clientStore.update(givenClientRegister));

        assertEquals("Client id not found", actualException.getMessage());
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldCallGetById_whenGetByIdRequestIsReceivedAndClientIsFound() {
        //Given
        final long givenId = 1L;
        final ClientRegister givenClientRegister =
                Fixtures.validClientRegister().id(givenId).build();
        when(clientRepository.findById(givenId)).thenReturn(Optional.of(givenClientRegister));

        //When
        final Optional<ClientRegister> actualClient = clientStore.getById(givenId);

        //Then
        assertEquals(Optional.of(givenClientRegister), actualClient);
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldThrowSLaserException_whenGetByIdRequestIdIsPresentAndClientIsNotFound() {
        //Given
        final long givenId = 1L;
        when(clientRepository.findById(givenId)).thenReturn(Optional.empty());

        //When
        final Optional<ClientRegister> actualClient = clientStore.getById(givenId);

        //Then
        assertTrue(actualClient.isEmpty());
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldCallDelete_whenDeleteRequestIsReceivedAndClientIsFound() {
        //Given
        final long givenId = 1L;
        when(clientRepository.existsById(givenId)).thenReturn(true);

        //When
        boolean actualResponse = clientStore.delete(givenId);

        //Then
        verify(clientRepository).deleteById(givenId);
        assertTrue(actualResponse);
    }

    @Test
    void shouldReturnFalse_whenIdNotFound() {
        //Given
        final long givenId = 3333333L;
        when(clientRepository.existsById(givenId)).thenReturn(false);

        //When
        boolean actualResponse = clientStore.delete(givenId);

        //Then
        assertFalse(actualResponse);
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldRetrieveAsociatedCentres_whenRequestIsReceived() {
        //Given
        final String givenClientType = "profesional";
        final List<ClientRegister> givenClients = List.of(
                Fixtures.validClientRegister().asociatedCentre("centre1").build(),
                Fixtures.validClientRegister().asociatedCentre("centre1").build()
        );
        when(clientRepository.findAllByClientTypeOrderByName(givenClientType))
                .thenReturn(givenClients);

        //When
        List<ClientRegister> actualClients = clientStore.findAllByClientType(givenClientType);

        //Then
        assertEquals(givenClients, actualClients);
        verifyNoMoreInteractions(clientRepository);
    }


    @Test
    void shouldRetrieveClientsByAsociatedCentre_whenRequestIsReceived() {
        //Given
        final String givenAsociatedCentre = "centre";
        final List<ClientRegister> givenClients = List.of(
                Fixtures.validClientRegister().build(),
                Fixtures.validClientRegister().build()
        );
        when(clientRepository.findAllByAsociatedCentreOrderByAsociatedCentreAscNameAscSurname1AscSurname2Asc(givenAsociatedCentre))
                .thenReturn(givenClients);

        //When
        List<ClientRegister> actualClients = clientStore.findClientsByAsociatedCentre(givenAsociatedCentre);

        //Then
        assertEquals(givenClients, actualClients);
        verifyNoMoreInteractions(clientRepository);
    }

    @Test
    void shouldRetrieveClientsByFilter_whenFilterIsReceived() {
        //Given
        final ClientRegister givenFilter = validClientRegister().build();
        final List<ClientRegister> givenClients = List.of(
                Fixtures.validClientRegister().build(),
                Fixtures.validClientRegister().build()
        );
        final ClientRegisterSpecification specification = new ClientRegisterSpecification(givenFilter);
        when(clientRepository.findAll(specification))
                .thenReturn(givenClients);

        //When
        List<ClientRegister> actualClients = clientStore.findAllByFilter(specification);

        //Then
        assertEquals(givenClients, actualClients);
        verifyNoMoreInteractions(clientRepository);
    }
}