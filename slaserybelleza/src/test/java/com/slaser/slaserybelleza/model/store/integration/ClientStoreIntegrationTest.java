package com.slaser.slaserybelleza.model.store.integration;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.ClientRepository;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.model.store.ClientStore;
import com.slaser.slaserybelleza.service.client.ClientRegisterSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientStoreIntegrationTest {

    @Autowired
    private ClientStore clientStore;

    @Autowired
    private ClientRepository clientRepository;

    @BeforeEach
    void initialize() {
        final ClientRegister client = Fixtures.validClientRegister().name("Dougal1").build();
        final ClientRegister client2 = Fixtures.validClientRegister().name("Ted1").build();
        clientRepository.deleteAll();
        clientRepository.save(client);
        clientRepository.save(client2);
    }

    @Test
    void shouldReturnClient_whenSaveRequestIsProvided() {
        //Given
        final ClientRegister givenClientRegister = Fixtures.validClientRegister().build();
        //When
        final ClientRegister actualClient = clientStore.save(givenClientRegister);

        //then
        assertEquals(3, clientRepository.count());
        assertEquals(givenClientRegister.getClientType(), actualClient.getClientType());
        assertEquals(givenClientRegister.getName(), actualClient.getName());
        assertEquals(givenClientRegister.getSurname1(), actualClient.getSurname1());
        assertEquals(givenClientRegister.getSurname2(), actualClient.getSurname2());
        assertEquals(givenClientRegister.getAsociatedCentre(), actualClient.getAsociatedCentre());
        assertEquals(givenClientRegister.getTypeIdentification(), actualClient.getTypeIdentification());
        assertEquals(givenClientRegister.getIdentification(), actualClient.getIdentification());
        assertEquals(givenClientRegister.getStreetType(), actualClient.getStreetType());
        assertEquals(givenClientRegister.getStreet(), actualClient.getStreet());
        assertEquals(givenClientRegister.getStreetNo(), actualClient.getStreetNo());
        assertEquals(givenClientRegister.getFloor(), actualClient.getFloor());
        assertEquals(givenClientRegister.getLetter(), actualClient.getLetter());
        assertEquals(givenClientRegister.getCity(), actualClient.getCity());
        assertEquals(givenClientRegister.getCp(), actualClient.getCp());
        assertEquals(givenClientRegister.getProvince(), actualClient.getProvince());
        assertEquals(givenClientRegister.getCountry(), actualClient.getCountry());
        assertEquals(givenClientRegister.getTelephone(), actualClient.getTelephone());
        assertEquals(givenClientRegister.getLandline(), actualClient.getLandline());
        assertEquals(givenClientRegister.getEmail(), actualClient.getEmail());
        assertEquals(givenClientRegister.getWeb(), actualClient.getWeb());
        assertEquals(givenClientRegister.getOtherInfo(), actualClient.getOtherInfo());
        assertEquals(givenClientRegister.getGdpr(), actualClient.getGdpr());
        assertEquals(givenClientRegister.getCreationDate(), actualClient.getCreationDate());
    }

    @Test
    void shouldReturnClient_whenUpdateRequestIsProvided() {
        //Given
        final long givenId = clientRepository.findAll().stream().findFirst().orElseThrow().getId();
        final ClientRegister givenClientToUpdate = Fixtures.validClientRegister().id(givenId).name("name").build();

        //When
        final ClientRegister actualClient = clientStore.update(givenClientToUpdate);

        //then
        assertEquals(2, clientRepository.count());
        assertEquals(givenClientToUpdate.getId(), actualClient.getId());
        assertEquals(givenClientToUpdate.getClientType(), actualClient.getClientType());
        assertEquals(givenClientToUpdate.getName(), actualClient.getName());
        assertEquals(givenClientToUpdate.getSurname1(), actualClient.getSurname1());
        assertEquals(givenClientToUpdate.getSurname2(), actualClient.getSurname2());
        assertEquals(givenClientToUpdate.getAsociatedCentre(), actualClient.getAsociatedCentre());
        assertEquals(givenClientToUpdate.getTypeIdentification(), actualClient.getTypeIdentification());
        assertEquals(givenClientToUpdate.getIdentification(), actualClient.getIdentification());
        assertEquals(givenClientToUpdate.getStreetType(), actualClient.getStreetType());
        assertEquals(givenClientToUpdate.getStreet(), actualClient.getStreet());
        assertEquals(givenClientToUpdate.getStreetNo(), actualClient.getStreetNo());
        assertEquals(givenClientToUpdate.getFloor(), actualClient.getFloor());
        assertEquals(givenClientToUpdate.getLetter(), actualClient.getLetter());
        assertEquals(givenClientToUpdate.getCity(), actualClient.getCity());
        assertEquals(givenClientToUpdate.getCp(), actualClient.getCp());
        assertEquals(givenClientToUpdate.getProvince(), actualClient.getProvince());
        assertEquals(givenClientToUpdate.getCountry(), actualClient.getCountry());
        assertEquals(givenClientToUpdate.getTelephone(), actualClient.getTelephone());
        assertEquals(givenClientToUpdate.getLandline(), actualClient.getLandline());
        assertEquals(givenClientToUpdate.getEmail(), actualClient.getEmail());
        assertEquals(givenClientToUpdate.getWeb(), actualClient.getWeb());
        assertEquals(givenClientToUpdate.getOtherInfo(), actualClient.getOtherInfo());
        assertEquals(givenClientToUpdate.getGdpr(), actualClient.getGdpr());
        assertEquals(givenClientToUpdate.getCreationDate(), actualClient.getCreationDate());
    }

    @Test
    void shouldReturnClient_whenIdIsProvided() {
        //Given
        final long givenId = clientRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        final ClientRegister actualClient = clientStore.getById(givenId).orElseThrow();

        //Then
        assertEquals(givenId, actualClient.getId());
    }

    @Test
    void shouldDeleteClient_whenIdIsProvided() {
        //Given
        final long givenId = clientRepository.findAll().stream().findFirst().orElseThrow().getId();

        //When
        boolean actualResult = clientStore.delete(givenId);

        //then
        assertTrue(actualResult);
        assertEquals(1, clientRepository.count());
    }

    @Test
    void shouldReturnAsociatedCentres_whenRequired() {
        //Given
        final String givenClientType = "profesional";
        final ClientRegister client3 = Fixtures.validClientRegister().clientType("profesional").asociatedCentre("centre1").build();
        final ClientRegister client4 = Fixtures.validClientRegister().clientType("profesional").asociatedCentre("centre2").build();
        clientRepository.save(client3);
        clientRepository.save(client4);
        //When
        final List<ClientRegister> actualClients = clientStore.findAllByClientType(givenClientType);

        //then
        assertEquals(2, actualClients.size());
    }

    @Test
    void shouldReturnListOfClientsByAsociatedCentre_whenAsociatedCentreIsProvided()  {
        //Given
        final String asociatedCentre = "centre1";
        final ClientRegister client3 = Fixtures.validClientRegister().asociatedCentre("centre1").build();
        final ClientRegister client4 = Fixtures.validClientRegister().asociatedCentre("centre1").build();
        final ClientRegister client5 = Fixtures.validClientRegister().asociatedCentre("centre2").build();
        clientRepository.save(client3);
        clientRepository.save(client4);
        clientRepository.save(client5);
        //When
        final List<ClientRegister> actualClients = clientStore.findClientsByAsociatedCentre(asociatedCentre);

        //then
        assertEquals(2, actualClients.size());
    }

    @ParameterizedTest(name = "[{0}]")
    @MethodSource("filter")
    void shouldReturnListOfClients_whenFilterIsProvided(String name, String asociatedCentre){
        //Given
        final ClientRegister givenFilter = ClientRegister.builder().clientType("particular").name(name).asociatedCentre(asociatedCentre).build();
        final ClientRegister client3 = Fixtures.validClientRegister().clientType("particular").name("Ted").asociatedCentre("centre1").build();
        final ClientRegister client4 = Fixtures.validClientRegister().clientType("particular").name("Dougal").asociatedCentre("centre1").build();
        final ClientRegister client5 = Fixtures.validClientRegister().clientType("particular").name("Jack").asociatedCentre("centre2").build();
        final ClientRegister client6 = Fixtures.validClientRegister().clientType("particular").name("Javier").asociatedCentre("centre2").build();
        clientRepository.save(client3);
        clientRepository.save(client4);
        clientRepository.save(client5);
        clientRepository.save(client6);
        final ClientRegisterSpecification specification = new ClientRegisterSpecification(givenFilter);
        //When
        final List<ClientRegister> actualClients = clientStore.findAllByFilter(specification);

        //then
        assertEquals(1, actualClients.size());
        assertEquals(name, actualClients.get(0).getName());
        assertEquals(asociatedCentre, actualClients.get(0).getAsociatedCentre());
    }

    static private Stream<Arguments> filter() {
        return Stream.of(
                Arguments.of("Dougal", "centre1"),
                Arguments.of("Ted", "centre1"),
                Arguments.of("Javier", "centre2"),
                Arguments.of("Jack", "centre2")
        );
    }
}
