package com.slaser.slaserybelleza.model.store.unit;

import com.slaser.slaserybelleza.Utils.Fixtures;
import com.slaser.slaserybelleza.component.persistance.repository.AppointmentRepository;
import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.store.AppointmentStore;
import com.slaser.slaserybelleza.utils.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AppointmentStoreTest {
    @Mock
    private AppointmentRepository appointmentRepository;

    @InjectMocks
    private AppointmentStore appointmentStore;

    @Test
    void shouldCallSaveAppointment_whenSaveRequestIsReceived() {
        //Given
        final AppointmentRegister givenAppointmentRegister = Fixtures.validAppointmentRegister().build();
        when(appointmentRepository.save(givenAppointmentRegister)).thenReturn(givenAppointmentRegister);

        //When
        final AppointmentRegister actualAppointment = appointmentStore.save(givenAppointmentRegister);

        //Then
        assertEquals(givenAppointmentRegister, actualAppointment);
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldThrowSLaserExecption_whenSaveRequestIdIsPresent() {
        //Given
        final AppointmentRegister givenAppointmentRegister =
                Fixtures.validAppointmentRegister().id(1L).build();

        //When
        //Then
        final SLaserException actualException = assertThrows(SLaserException.class, () -> appointmentStore.save(givenAppointmentRegister));

        assertEquals("Appointment id is not null", actualException.getMessage());
        verifyNoInteractions(appointmentRepository);
    }

    @Test
    void shouldCallUpdateAppointment_whenUpdateRequestIsReceivedAndAppointmentIsFound() {
        //Given
        final AppointmentRegister givenAppointmentRegister =
                Fixtures.validAppointmentRegister().id(1L).build();
        when(appointmentRepository.findById(givenAppointmentRegister.getId())).thenReturn(Optional.of(givenAppointmentRegister));
        when(appointmentRepository.save(givenAppointmentRegister)).thenReturn(givenAppointmentRegister);

        //When
        final AppointmentRegister actualAppointment = appointmentStore.update(givenAppointmentRegister);

        //Then
        assertEquals(givenAppointmentRegister, actualAppointment);
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldThrowSLaserException_whenUpdateRequestIdIsNOTPresent() {
        //Given
        final AppointmentRegister givenAppointmentRegister = Fixtures.validAppointmentRegister().build();

        //When
        //Then
        final SLaserException actualException = assertThrows(SLaserException.class, () -> appointmentStore.update(givenAppointmentRegister));

        assertEquals("Appointment id is null", actualException.getMessage());
        verifyNoInteractions(appointmentRepository);
    }

    @Test
    void shouldThrowSLaserException_whenUpdateRequestIdIsPresentAndAppointmentIsNotFound() {
        //Given
        final AppointmentRegister givenAppointmentRegister = Fixtures.validAppointmentRegister().id(1L).build();
        when(appointmentRepository.findById(givenAppointmentRegister.getId())).thenReturn(Optional.empty());

        //When
        //Then
        final SLaserException actualException = assertThrows(SLaserException.class, () -> appointmentStore.update(givenAppointmentRegister));

        assertEquals("Appointment id not found", actualException.getMessage());
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldCallGetById_whenGetByIdRequestIsReceivedAndAppointmentIsFound() {
        //Given
        final long givenId = 1L;
        final AppointmentRegister givenAppointmentRegister =
                Fixtures.validAppointmentRegister().id(givenId).build();
        when(appointmentRepository.findById(givenId)).thenReturn(Optional.of(givenAppointmentRegister));

        //When
        final Optional<AppointmentRegister> actualAppointment = appointmentStore.getById(givenId);

        //Then
        assertEquals(Optional.of(givenAppointmentRegister), actualAppointment);
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldThrowSLaserException_whenGetByIdRequestIdIsPresentAndAppointmentIsNotFound() {
        //Given
        final long givenId = 1L;
        when(appointmentRepository.findById(givenId)).thenReturn(Optional.empty());

        //When
        final Optional<AppointmentRegister> actualAppointment = appointmentStore.getById(givenId);

        //Then
        assertTrue(actualAppointment.isEmpty());
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldCallDelete_whenDeleteRequestIsReceivedAndAppointmentIsFound() {
        //Given
        final long givenId = 1L;
        when(appointmentRepository.existsById(givenId)).thenReturn(true);

        //When
        boolean actualResponse = appointmentStore.delete(givenId);

        //Then
        verify(appointmentRepository).deleteById(givenId);
        assertTrue(actualResponse);
    }

    @Test
    void shouldReturnFalse_whenIdNotFound() {
        //Given
        final long givenId = 1L;
        when(appointmentRepository.existsById(givenId)).thenReturn(false);

        //When
        boolean actualResponse = appointmentStore.delete(givenId);

        //Then
        assertFalse(actualResponse);
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldRetrieveAppointmentsByDates_whenRequestIsReceived() {
        //Given
        final LocalDateTime givenStartingDate = Utils.convertToDate("2021-01-01");
        final LocalDateTime givenEndDate = Utils.convertToDate("2021-01-31").toLocalDate().atTime(LocalTime.MAX);
        final List<AppointmentRegister> givenAppointments = List.of(
                Fixtures.validAppointmentRegister().build(),
                Fixtures.validAppointmentRegister().build()
        );
        when(appointmentRepository.findAllByStartingDateGreaterThanEqualAndEndDateLessThanEqualOrderByStartingDate(givenStartingDate, givenEndDate))
                .thenReturn(givenAppointments);

        //When
        List<AppointmentRegister> actualAppointments = appointmentStore.findByDates(givenStartingDate, givenEndDate);

        //Then
        assertEquals(givenAppointments, actualAppointments);
        verifyNoMoreInteractions(appointmentRepository);
    }

    @Test
    void shouldRetrieveAppointmentsByDatesandIdCli_whenRequestIsReceived() {
        //Given
        final long idCli = 1L;
        final LocalDateTime givenStartingDate = Utils.convertToDate("2021-01-01");
        final LocalDateTime givenEndDate = Utils.convertToDate("2021-01-31").toLocalDate().atTime(LocalTime.MAX);
        final List<AppointmentRegister> givenAppointments = List.of(
                Fixtures.validAppointmentRegister().build(),
                Fixtures.validAppointmentRegister().build()
        );
        when(appointmentRepository.findAllByIdCliAndStartingDateGreaterThanEqualAndEndDateLessThanEqualOrderByStartingDateAsc(idCli, givenStartingDate, givenEndDate))
                .thenReturn(givenAppointments);

        //When
        List<AppointmentRegister> actualAppointments = appointmentStore.findByIdCliAndDates(idCli, givenStartingDate, givenEndDate);

        //Then
        assertEquals(givenAppointments, actualAppointments);
        verifyNoMoreInteractions(appointmentRepository);
    }
}