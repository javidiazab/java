package com.slaser.slaserybelleza.utils;

/**
 * Util class
 * Commond constacts
 */
public class Constants {
    public final static String USER_ROLE = "USER";
    public final static String ADMIN_ROLE = "ADMIN";
}
