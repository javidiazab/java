package com.slaser.slaserybelleza.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Util class
 * File utilities to read files
 */
public final class FileUtils {
    private FileUtils() {
    }

    public static String readString(String path) throws IOException {
        InputStream inputStream = FileUtils.class.getClassLoader().getResourceAsStream(path);
        return IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
    }
}
