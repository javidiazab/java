package com.slaser.slaserybelleza.service;

import com.slaser.slaserybelleza.model.client.AuthenticationRequest;
import com.slaser.slaserybelleza.model.login.JwtResponse;
import com.slaser.slaserybelleza.spring.security.components.JwtTokenUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.security.auth.login.LoginException;

/**
 * Manages authentication utilities for login access
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
  //  private final UserStore userStore;
    private final UserDetailsService userDetailsService;

    public JwtResponse createAuthenticationToken(AuthenticationRequest authenticationRequest) throws Exception {
        String token = authenticateUser(authenticationRequest.getUser(), authenticationRequest.getPassword());
        return new JwtResponse(authenticationRequest.getUser(), token);
    }

    private String authenticateUser(@NonNull String userName, @NonNull String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
        } catch (DisabledException exceptiin) {
            throw new LoginException("USER_DISABLED");
        } catch (BadCredentialsException exception) {
            throw new LoginException("INVALID_CREDENTIALS");
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

        return jwtTokenUtil.generateToken(userDetails);
    }

//    private UserDto findUser(AuthenticationRequest authenticationRequest) throws LoginException {
//        return userStore.findByIdAndPassword(
//                authenticationRequest.getUser(), authenticationRequest.getPassword());
//    }
//
//    private String getJWTToken(String username) {
//        String secretKey = "sLaseryBellezaSecretKey";
//        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
//                .commaSeparatedStringToAuthorityList("ROLE_USER");
//
//        String token = Jwts
//                .builder()
//                .setId("001")
//                .setSubject(username)
//                .claim("authorities",
//                        grantedAuthorities.stream()
//                                .map(GrantedAuthority::getAuthority)
//                                .collect(Collectors.toList()))
//                .setIssuedAt(new Date(System.currentTimeMillis()))
//                .setExpiration(new Date(System.currentTimeMillis() + 600000))
//                .signWith(SignatureAlgorithm.HS512,
//                        secretKey.getBytes()).compact();
//
//        return "Bearer " + token;
//    }
}
