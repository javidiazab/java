package com.slaser.slaserybelleza.service.user;

import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.store.UserStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * User Details access for JWT
 */
@RequiredArgsConstructor
@Service
@Primary
@Slf4j
public class JpaUserDetailsService implements UserDetailsService {

    final UserStore userStore;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto user = Optional.of(userStore.findById(username))
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        return new User(user.getUserName(), user.getPassword(), List.of(user.getRole()));
    }
}