package com.slaser.slaserybelleza.service.appointment;

import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.store.AppointmentStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * Handle Appointments and its store
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AppointmentService {

    private final AppointmentStore appointmentStore;

    /**
     * <p>Appointment to be added on DB</p>
     * @param appointmentDto Appointment data
     * @return The same appointment with an id
     * @since 1.0
     */
    public AppointmentDto add(AppointmentDto appointmentDto) {
        validateAppointmentDto(appointmentDto);

        return save(appointmentDto, isNull(appointmentDto.getId()));
    }

    /**
     * <p>Get an appointment by its id in DB</p>
     * @param id  Id of the appointment to be retrieved
     * @return Requested appointment
     * @since 1.0
     */
    public AppointmentDto getById(long id) {
        Optional<AppointmentRegister> appointmentRegister = appointmentStore.getById(id);
        return appointmentRegister.isEmpty()
                ? null
                : AppointmentMapper.buildAppointmentDto(appointmentRegister.get());
    }

    /**
     * <p>Delete an appointment by its id in DB</p>
     * @param id  Id of the appointment to be deleted
     * @return Boolean indicating if the deletion was successful
     * @since 1.0
     */
    public boolean delete(long id) {
        return appointmentStore.delete(id);
    }

    /**
     * <p>Retrieve a list of the appointments in a period of time</p>
     * @param starting  Starting Date of the period to be retrieved
     * @param end  end date of the period to be retrieved
     * @return Appointments in this period
     * @since 1.0
     */
    public List<AppointmentDto> findByDates(LocalDateTime starting, LocalDateTime end) {
        validateDates(starting, end);

        List<AppointmentRegister> results = appointmentStore.findByDates(starting, end);
        return results.stream()
                .map(AppointmentMapper::buildAppointmentDto)
                .collect(Collectors.toList());
    }

    /**
     * <p>Retrieve a list of the appointments by client in a period of time</p>
     * @param idCli  Id of the client of the period to be retrieved
     * @param starting  Starting Date of the period to be retrieved
     * @param end  end date of the period to be retrieved
     * @return Appointments by client id in this period
     * @since 1.0
     */
    public List<AppointmentDto> findByIdCliAndDates(Long idCli, LocalDateTime starting, LocalDateTime end) {
        validateDates(starting, end);

        List<AppointmentRegister> results = appointmentStore.findByIdCliAndDates(idCli, starting, end);
        return results.stream()
                .map(AppointmentMapper::buildAppointmentDto)
                .collect(Collectors.toList());
    }

    private AppointmentDto save(AppointmentDto appointmentDto, boolean isCreate) {
        final AppointmentRegister appointmentRegister = AppointmentMapper.buildAppointmentRegister(appointmentDto);
        try {
            final AppointmentRegister appointmentRegisterSaved = isCreate
                    ? appointmentStore.save(appointmentRegister)
                    : appointmentStore.update(appointmentRegister);
            return AppointmentMapper.buildAppointmentDto(appointmentRegisterSaved);
        } catch (Exception exception) {
            log.error("[AppointmentService] Unable to {} appointment register in DB: {}\n{}",
                    isCreate ? "create" : "update",
                    exception.getMessage(),
                    appointmentDto.getSubject());
        }
        return null;
    }

    private void validateAppointmentDto(AppointmentDto appointmentDto) {
        if (isNull(appointmentDto)) {
            throw new SLaserException("Appointment not informed");
        }
        if (isNull(appointmentDto.getSubject())) {
            throw new SLaserException("Subject not informed");
        }
        if (isNull(appointmentDto.getPlace())) {
            throw new SLaserException("Place not informed");
        }
        validateDates(appointmentDto.getStartingDate(), appointmentDto.getEndDate());
    }

    private void validateDates(LocalDateTime startingDate, LocalDateTime endDate) {
        if (isNull(startingDate) || isNull(endDate)) {
            throw new SLaserException("Starting date and ending time must be filled in");
        }
        if (startingDate.isAfter(endDate)) {
            throw new SLaserException("Starting time after ending time");
        }
    }
}
