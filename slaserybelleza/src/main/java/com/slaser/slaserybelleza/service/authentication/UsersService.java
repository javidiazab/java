package com.slaser.slaserybelleza.service.authentication;

/**
 * interface to authenticate users
 */
public interface UsersService {

    public String authenticateUser(String userName, String password) throws Exception;
}
