package com.slaser.slaserybelleza.service.email;

import com.slaser.slaserybelleza.model.client.EmailBodyDto;

/**
 * Set acctions with emails
 */
public interface EmailPort {
    boolean sendEmail(EmailBodyDto emailBody);
}
