package com.slaser.slaserybelleza.service.client;

import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.ClientRegister;

import static java.util.Objects.isNull;

/**
 * Mapper from/to Dto/Register
 */
public class ClientMapper {

    /**
     * <p>Generate a ClientRegister to be feed to the DB</p>
     * @param clientDto  Client information in Dto format
     * @return Client in Register format
     * @since 1.0
     */
    public static ClientRegister buildClientRegister(ClientDto clientDto) {

        if (isNull(clientDto)) {
            return null;
        }
        return ClientRegister.builder()
                .id(clientDto.getId())
                .clientType(clientDto.getClientType())
                .name(clientDto.getName())
                .surname1(clientDto.getSurname1())
                .surname2(clientDto.getSurname2())
                .asociatedCentre(clientDto.getAsociatedCentre())
                .typeIdentification(clientDto.getTypeIdentification())
                .identification(clientDto.getIdentification())
                .streetType(clientDto.getStreetType())
                .street(clientDto.getStreet())
                .streetNo(clientDto.getStreetNo())
                .floor(clientDto.getFloor())
                .letter(clientDto.getLetter())
                .city(clientDto.getCity())
                .cp(clientDto.getCp())
                .province(clientDto.getProvince())
                .country(clientDto.getCountry())
                .telephone(clientDto.getTelephone())
                .landline(clientDto.getLandline())
                .email(clientDto.getEmail())
                .web(clientDto.getWeb())
                .otherInfo(clientDto.getOtherInfo())
                .gdpr(clientDto.getGdpr())
                .creationDate(clientDto.getCreationDate())
                .build();
    }

    /**
     * <p>Generate a ClientDto to be returned to frontend of the application</p>
     * @param clientRegister  Client information in Register format
     * @return Client in Dto format
     * @since 1.0
     */
    public static ClientDto buildClientDto(ClientRegister clientRegister) {
        if (isNull(clientRegister)) {
            return null;
        }
        return ClientDto.builder()
                .id((clientRegister.getId()))
                .clientType((clientRegister.getClientType()))
                .name((clientRegister.getName()))
                .surname1((clientRegister.getSurname1()))
                .surname2((clientRegister.getSurname2()))
                .asociatedCentre((clientRegister.getAsociatedCentre()))
                .typeIdentification((clientRegister.getTypeIdentification()))
                .identification((clientRegister.getIdentification()))
                .streetType((clientRegister.getStreetType()))
                .street((clientRegister.getStreet()))
                .streetNo((clientRegister.getStreetNo()))
                .floor((clientRegister.getFloor()))
                .letter((clientRegister.getLetter()))
                .city((clientRegister.getCity()))
                .cp((clientRegister.getCp()))
                .province((clientRegister.getProvince()))
                .country((clientRegister.getCountry()))
                .telephone((clientRegister.getTelephone()))
                .landline((clientRegister.getLandline()))
                .email((clientRegister.getEmail()))
                .web((clientRegister.getWeb()))
                .otherInfo((clientRegister.getOtherInfo()))
                .gdpr((clientRegister.getGdpr()))
                .creationDate((clientRegister.getCreationDate()))
                .build();
    }
}

