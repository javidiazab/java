package com.slaser.slaserybelleza.service.client;

import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.model.store.ClientStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * Handle Clients and its store
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ClientService {

    private final ClientStore clientStore;

    /**
     * <p>Client to be added on DB</p>
     * @param clientDto Client data
     * @return The same client with an id
     * @since 1.0
     */
    public ClientDto add(ClientDto clientDto) {
        validateClientDto(clientDto);
        return save(clientDto, isNull(clientDto.getId()));
    }

    /**
     * <p>Get a client by its id in DB</p>
     * @param id  Id of the client to be retrieved
     * @return Requested client
     * @since 1.0
     */
    public ClientDto getById(long id) {
        Optional<ClientRegister> clientRegister = clientStore.getById(id);
        return clientRegister.isEmpty()
                ? null
                : ClientMapper.buildClientDto(clientRegister.get());
    }

    /**
     * <p>Delete a client by its id in DB</p>
     * @param id  Id of the client to be deleted
     * @return Boolean indicating if the deletion was successful
     * @since 1.0
     */
    public boolean delete(long id) {
        return clientStore.delete(id);
    }


    /**
     * <p>Retrieve a list of Asociated Centres (clients with clientType=profesional</p>
     * @return list of Asociated Centres
     * @since 1.0
     */
    public List<ClientDto> findCentres() {
        List<ClientRegister> results = clientStore.findAllByClientType("profesional");
        return results.stream()
                .map(ClientMapper::buildClientDto)
                .collect(Collectors.toList());
    }

    /**
     * <p>Retrieve a list of the clients</p>
     * @param asociatedCentre  Asociated Centre where the client belongs
     * @return Clients in this period
     * @since 1.0
     */
    public List<ClientDto> findByAsociatedCentre(String asociatedCentre) {
        List<ClientRegister> results = clientStore.findClientsByAsociatedCentre(asociatedCentre);
        return results.stream()
                .map(ClientMapper::buildClientDto)
                .collect(Collectors.toList());
    }

    /**
     * <p>Retrieve a list of the clients by filter</p>
     * @param clientDto  Asociated Centre where the client belongs
     * @return Clients in this period
     * @since 1.0
     */
    public List<ClientDto> findByFilter(ClientDto clientDto) {
        ClientRegisterSpecification specification =
                new ClientRegisterSpecification(ClientMapper.buildClientRegister(clientDto));
        List<ClientRegister> results = clientStore.findAllByFilter(specification);
        return results.stream()
                .map(ClientMapper::buildClientDto)
                .collect(Collectors.toList());
    }

    private ClientDto save(ClientDto clientDto, boolean isCreate) {
        final ClientRegister clientRegister = ClientMapper.buildClientRegister(clientDto);
        try {
            final ClientRegister clientRegisterSaved = isCreate
                    ? clientStore.save(clientRegister)
                    : clientStore.update(clientRegister);
            return ClientMapper.buildClientDto(clientRegisterSaved);
        } catch (Exception exception) {
            log.error("[ClientService] Unable to {} client register in DB: {}\n{}",
                    isCreate ? "create" : "update",
                    exception.getMessage(),
                    clientDto.getName());
        }
        return null;
    }

    private void validateClientDto(ClientDto clientDto) {
        if (isNull(clientDto)) {
            throw new SLaserException("Client not informed");
        }
        if (isNull(clientDto.getClientType())) {
            throw new SLaserException("ClientType not informed");
        }
        if (isNull(clientDto.getName())) {
            throw new SLaserException("Name not informed");
        }
    }
}
