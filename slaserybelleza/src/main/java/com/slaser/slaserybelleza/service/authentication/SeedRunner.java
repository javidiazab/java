package com.slaser.slaserybelleza.service.authentication;

import com.slaser.slaserybelleza.component.persistance.repository.UserRepository;
import com.slaser.slaserybelleza.model.client.UserRole;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Inserts default values in users DB
 * Used only in Local profile
 */
@Profile("!default")
@Component
@AllArgsConstructor
public class SeedRunner implements CommandLineRunner {

    UserRepository userRepository;
    PasswordEncoder pwdEncoder;

    @Override
    public void run(String... args) throws Exception {

        if (userRepository.count() > 0) {
            return;
        }

        userRepository.save(new UserRegister("javi", pwdEncoder.encode("javi"), "admin", UserRole.USER, 0, null, null));
        userRepository.save(new UserRegister("sonia", pwdEncoder.encode("sonia"), "user", UserRole.USER, 0, null, null));
        userRepository.save(new UserRegister("admin", pwdEncoder.encode("admin"), "admin", UserRole.ADMIN, 0, null, null));
    }
}
