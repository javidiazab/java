package com.slaser.slaserybelleza.service.scheduledTask;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.model.client.EmailBodyDto;
import com.slaser.slaserybelleza.service.appointment.AppointmentService;
import com.slaser.slaserybelleza.service.email.EmailSenderService;
import com.slaser.slaserybelleza.service.email.EmailSimplerService;
import com.slaser.slaserybelleza.utils.FileUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Schedule for tasks
 * Sends a daily email with the next day's appointments
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ScheduledTaskService {

    private final AppointmentService appointmentService;
    private final EmailSimplerService emailService;
    private final EmailSenderService emailSenderService;

    @Value("${application.send-emails-to}")
    private String sendEmailsTo;

    /**
     * <p>Send an email with the appointments information for the next day</p>
     * @throws IOException if an I/O error occurs
     * @since 1.0
     */
    @Scheduled(cron = "${application.cron-next-appointments}")
    public void sendAppointmentsByEmail() throws IOException {
        LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        final List<AppointmentDto> appointmentsList = appointmentService.findByDates(
                tomorrow.toLocalDate().atStartOfDay(), tomorrow.toLocalDate().atTime(LocalTime.MAX));

        final String dateTimeFormatter =
                DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).format(tomorrow);

        Map<String, Object> content = new HashMap<>();
        content.put("date", dateTimeFormatter);
        content.put("appointments", appointmentsList);
        content.put("emoji", getRandomEmojiCode());

        String processedTemplate = getProcessedTemplate(getTemplate("templates/NextAppointments.html"), content);
        processedTemplate.replaceAll("T\\d{2}:\\d{2}:\\d{2}", "");

        EmailBodyDto email = EmailBodyDto.builder()
                .email(sendEmailsTo)
                .subject("Citas " + dateTimeFormatter)
                .content(processedTemplate)
                .build();
        //emailService.sendEmail(email);
        emailSenderService.sendEmail(email);

        log.info("Scheduled task sent");
    }

    private String getTemplate(String template) throws IOException {
        return FileUtils.readString(template);
    }

    private String getProcessedTemplate(String template, Map<String, Object> parameters) throws IOException {
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new StringReader(template), "appointments.mustache");
        mustache.execute(writer, parameters).flush();

        return writer.toString();
    }

    private String getRandomEmojiCode() {
        return String.valueOf(new Random().nextInt(129510 - 127744) + 127744);
    }
}
