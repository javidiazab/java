package com.slaser.slaserybelleza.service.client;

import com.slaser.slaserybelleza.model.entity.ClientRegister;
import lombok.RequiredArgsConstructor;;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class ClientRegisterSpecification implements Specification<ClientRegister> {

    private static final long serialVersionUID = -4138314789361947954L;
    private final ClientRegister clientRegister;

    @Override
    public Predicate toPredicate(Root<ClientRegister> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        List<Predicate> predicates = new ArrayList<>();

        if (Objects.nonNull(clientRegister)) {
            generalPredicates(predicates, root, cb);
        }

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    private void generalPredicates(List<Predicate> predicates, Root<ClientRegister> root, CriteriaBuilder cb) {
        if (Objects.nonNull(clientRegister.getId())) {
            predicates.add(cb.equal(root.get("id"), clientRegister.getId()));
        }
        addLikePredicate(clientRegister.getClientType(), "clientType", root, cb, predicates);
        addLikePredicate(clientRegister.getName(), "name", root, cb, predicates);
        addLikePredicate(clientRegister.getSurname1(), "surname1", root, cb, predicates);
        addLikePredicate(clientRegister.getSurname2(), "surname2", root, cb, predicates);
        addLikePredicate(clientRegister.getAsociatedCentre(), "asociatedCentre", root, cb, predicates);
        addLikePredicate(clientRegister.getIdentification(), "identification", root, cb, predicates);
        addLikePredicate(clientRegister.getCity(), "city", root, cb, predicates);
        addLikePredicate(clientRegister.getCp(), "cp", root, cb, predicates);
        addLikePredicate(clientRegister.getProvince(), "province", root, cb, predicates);
        addLikePredicate(clientRegister.getTelephone(), "telephone", root, cb, predicates);
        addLikePredicate(clientRegister.getLandline(), "landline", root, cb, predicates);
        addLikePredicate(clientRegister.getEmail(), "email", root, cb, predicates);
        addLikePredicate(clientRegister.getWeb(), "web", root, cb, predicates);
        addLikePredicate(clientRegister.getOtherInfo(), "otherInfo", root, cb, predicates);
        addLikePredicate(clientRegister.getGdpr(), "gdpr", root, cb, predicates);
    }

    private void addLikePredicate(String value, String fieldName, Root<ClientRegister> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (StringUtils.isNotBlank(value)) {
            predicates.add(cb.like(cb.upper(root.get(fieldName)), "%" + value.trim().toUpperCase() + "%"));
        }
    }
}
