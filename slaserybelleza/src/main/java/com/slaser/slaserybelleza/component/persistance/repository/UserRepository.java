package com.slaser.slaserybelleza.component.persistance.repository;

import com.slaser.slaserybelleza.model.entity.UserRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Repository for users and not automatic accesses
 */
@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserRegister, String> {
    Optional<UserRegister> findByUserNameAndPassword(String userName, String password);
}
