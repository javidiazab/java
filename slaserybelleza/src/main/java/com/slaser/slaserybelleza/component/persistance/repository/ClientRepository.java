package com.slaser.slaserybelleza.component.persistance.repository;

import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Repository for clients and not automatic accesses
 */
@Repository
@Transactional
public interface ClientRepository extends JpaRepository<ClientRegister, Long> {

    List<ClientRegister> findAllByAsociatedCentreOrderByAsociatedCentreAscNameAscSurname1AscSurname2Asc(
            String asociatedCentre);

    List<ClientRegister> findAllByClientTypeOrderByName(String clientType);

    List<ClientRegister> findAll(Specification<ClientRegister> spec);
}

