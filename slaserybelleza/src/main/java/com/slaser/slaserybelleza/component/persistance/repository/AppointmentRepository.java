package com.slaser.slaserybelleza.component.persistance.repository;

import com.slaser.slaserybelleza.model.entity.AppointmentRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository for appointments and not automatic accesses
 */
@Repository
@Transactional
public interface AppointmentRepository extends JpaRepository<AppointmentRegister, Long> {

    List<AppointmentRegister> findAllByStartingDateGreaterThanEqualAndEndDateLessThanEqualOrderByStartingDate(
            LocalDateTime startingDate, LocalDateTime endDate);

    List<AppointmentRegister> findAllByIdCliAndStartingDateGreaterThanEqualAndEndDateLessThanEqualOrderByStartingDateAsc(
            Long idCli, LocalDateTime startingDate, LocalDateTime endDate);
}
