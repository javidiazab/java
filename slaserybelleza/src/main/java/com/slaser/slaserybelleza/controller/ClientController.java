package com.slaser.slaserybelleza.controller;

import com.slaser.slaserybelleza.model.client.ClientDto;
import com.slaser.slaserybelleza.service.client.ClientService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * Controller to manage clients
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/private/client")
@Slf4j
public class ClientController {

    private final ClientService clientService;

    /**
     * <p>Client to be saved on DB</p>
     * @param clientDto  Client data
     * @return The same client with an id
     * @since 1.0
     */
    @PostMapping("/save")
    public ResponseEntity<ClientDto> saveClient(@NonNull @RequestBody ClientDto clientDto) {
        log.info("Received call /save with: " + clientDto);
        ClientDto result = clientService.add(clientDto);
        log.info("     Response /save with: {}", result);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * <p>Get a client by its id in DB</p>
     * @param id  Id of the client to be retrieved
     * @return Requested client
     * @since 1.0
     */
    @GetMapping("/{id}")
    public ResponseEntity<ClientDto> getClientDetails(@NonNull @PathVariable Long id) {
        log.info("Received call /clients/id with id: " + id);
        ClientDto clientDto = clientService.getById(id);
        if (Objects.nonNull(clientDto)) {
            log.info("     Response /clients/id with: {}", clientDto);
            return ResponseEntity.ok(clientDto);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    /**
     * <p>Delete a client by its id in DB</p>
     * @param id  Id of the client to be deleted
     * @return Boolean indicating if the deletion was successful
     * @since 1.0
     */
    @GetMapping("/delete/{id}")
    public ResponseEntity<Boolean> deleteClient(@NonNull @PathVariable Long id) {
        log.info("Received call /delete with id: " + id);
        if (clientService.delete(id)) {
            log.info("     Response /delete with: {}", id);
            return ResponseEntity.accepted().body(true);
        }
        return ResponseEntity.badRequest().body(false);
    }

    /**
     * <p>Retrieve a list of the existing asociated centres</p>
     * @return Clients in this period
     * @since 1.0
     */
    //@PreAuthorize("hasAuthority('" + Constants.USER_ROLE + "')")
    @PostMapping("/list/centres")
    public ResponseEntity<List<ClientDto>> getCentres() {
        log.info("Received call /list/centres");
        List<ClientDto> responseSLaser = clientService.findCentres();
        log.info("     Response /list/centres with: {}", responseSLaser);
        return ResponseEntity.ok().body(responseSLaser);
    }

    /**
     * <p>Retrieve a list of the clients with the same asociated centre</p>
     * @param asociatedCentre Asociated center where the client belongs
     * @return Clients with the same asociated centre
     * @since 1.0
     */
    //@PreAuthorize("hasAuthority('" + Constants.USER_ROLE + "')")
    @PostMapping("/list/{asociatedCentre}")
    public ResponseEntity<List<ClientDto>> getCentreClients(@NonNull @PathVariable String asociatedCentre) {
        log.info("Received call /list/centre with: {}", asociatedCentre);
        List<ClientDto> responseSLaser = clientService.findByAsociatedCentre(asociatedCentre);
        log.info("     Response /list/centre with: {}", responseSLaser);
        return ResponseEntity.ok().body(responseSLaser);
    }

    /**
     * <p>Retrieve a list of the clients by filter</p>
     * @param client Client fields to apply filter
     * @return Clients in this period
     * @since 1.0
     */
    //@PreAuthorize("hasAuthority('" + Constants.USER_ROLE + "')")
    @PostMapping("/list/search")
    public ResponseEntity<List<ClientDto>> getClientsByFilter(@NonNull @RequestBody ClientDto client) {
        log.info("Received call /list/search with: {}", client);
        List<ClientDto> responseSLaser = clientService.findByFilter(client);
        log.info("     Response /list/search with: {}", responseSLaser);
        return ResponseEntity.ok().body(responseSLaser);
    }
}
