package com.slaser.slaserybelleza.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * Controller to test connectivity when deployed
 */
@RestController
@RequestMapping("/ping")
@Slf4j
public class ServerTestController {

    @Value("${application.name}")
    private String application;

    @Value("${application.version}")
    private String version;

    /**
     * <p>Utility to check if the access to the application is available</p>
     * Check whether the server is up and the application running
     * @return Version & Current date and time
     * @since 1.0
     */
    @GetMapping
    public ResponseEntity<String> pin(){
        log.info("Received call /ping");
        String result = "Conection stablished with " + application + " - V" + version + " at " + LocalDateTime.now();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
