package com.slaser.slaserybelleza.controller;

import com.slaser.slaserybelleza.exception.SLaserException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.security.auth.login.LoginException;
import java.util.List;

/**
 * Controller advice to manage all unhandled exceptions
 */
@RestControllerAdvice
public class ControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(LoginException.class)
    public ResponseEntity<com.slaser.slaserybelleza.model.ErrorResponse> userPasswordNotFoundExceptionHandling(LoginException exception) {
        com.slaser.slaserybelleza.model.ErrorResponse errorResponse = new com.slaser.slaserybelleza.model.ErrorResponse(HttpStatus.UNAUTHORIZED.value(), exception.getMessage(), null);
        return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(SLaserException.class)
    public ResponseEntity<ErrorResponse> bdpNotFoundExceptionHandling(SLaserException exception) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND.value(), exception.getMessage(), null);
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handle(Exception exception) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage(), null);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @AllArgsConstructor
    @Getter
    private static class ErrorResponse {
        private int status;
        private String message;
        private List<String> errors;
    }
}
