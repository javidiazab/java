package com.slaser.slaserybelleza.controller;

import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.service.user.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to manage users
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/private/user")
@Slf4j
public class UserController {

    private final UserService userService;

    /**
     * <p>User to be saved on DB</p>
     * @param userDto User data
     * @return The same user with an id
     * @since 1.0
     */
    @PostMapping("/save")
    public ResponseEntity<UserDto> saveUser(@NonNull @RequestBody UserDto userDto) {
        log.info("Received call /save with: " + userDto);
        UserDto result = userService.save(userDto);
        return new ResponseEntity<>(result, HttpStatus.ACCEPTED);
    }

    /**
     * <p>User to be retrieved from DB</p>
     * @param userName UserName of the user to be retrieved
     * @return Requested user information
     * @since 1.0
     */
    @GetMapping("/{userName}")
    public ResponseEntity<UserDto> getUserDetails(@NonNull @PathVariable String userName) {
        log.info("Received call /users/" + userName);
        UserDto userDto = userService.getById(userName);
        return ResponseEntity.ok(userDto);
    }

    /**
     * <p>User to be deleted from DB</p>
     * @param userName  UserName of the user to be deleted
     * @return boolean value indicating whether de deletion was successful
     * @since 1.0
     */
    @GetMapping("/delete/{userName}")
    public ResponseEntity<Boolean> deleteUser(@NonNull @PathVariable String userName) {
        log.info("Received call /delete/" + userName);
        if (userService.delete(userName)) {
            return ResponseEntity.accepted().body(true);
        }
        return ResponseEntity.badRequest().body(false);
    }
}
