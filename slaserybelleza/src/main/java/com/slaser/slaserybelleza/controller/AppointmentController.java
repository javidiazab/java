package com.slaser.slaserybelleza.controller;

import com.slaser.slaserybelleza.model.client.AppointmentDto;
import com.slaser.slaserybelleza.service.appointment.AppointmentService;
import com.slaser.slaserybelleza.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

/**
 * Controller to manage appointments
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/private/appointment")
@Slf4j
public class AppointmentController {

    private final AppointmentService appointmentService;

    /**
     * <p>Appointment to be saved on DB</p>
     * @param appointmentDto  Appointment data
     * @return The same appointment with an id
     * @since 1.0
     */
    @PostMapping("/save")
    public ResponseEntity<AppointmentDto> saveAppointment(@NonNull @RequestBody AppointmentDto appointmentDto) {
        log.info("Received call /save with: " + appointmentDto);
        AppointmentDto result = appointmentService.add(appointmentDto);
        log.info("     Response /save with: {}", result);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * <p>Get an appointment by its id in DB</p>
     * @param id  Id of the appointment to be retrieved
     * @return Requested appointment
     * @since 1.0
     */
    @GetMapping("/{id}")
    public ResponseEntity<AppointmentDto> getAppointmentDetails(@NonNull @PathVariable Long id) {
        log.info("Received call /appointments/id with id: " + id);
        AppointmentDto appointmentDto = appointmentService.getById(id);
        if (Objects.nonNull(appointmentDto)) {
            log.info("     Response /appointments/id with: {}", appointmentDto);
            return ResponseEntity.ok(appointmentDto);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    /**
     * <p>Delete an appointment by its id in DB</p>
     * @param id  Id of the appointment to be deleted
     * @return Boolean indicating if the deletion was successful
     * @since 1.0
     */
    @GetMapping("/delete/{id}")
    public ResponseEntity<Boolean> deleteAppointment(@NonNull @PathVariable Long id) {
        log.info("Received call /delete with id: " + id);
        if (appointmentService.delete(id)) {
            log.info("     Response /delete with: {}", id);
            return ResponseEntity.accepted().body(true);
        }
        return ResponseEntity.badRequest().body(false);
    }

    /**
     * <p>Retrieve a list of the appointments in a period of time</p>
     * @param start Starting Date of the period to be retrieved
     * @param end  Ending date of the period to be retrieved
     * @return Appointments in this period
     * @since 1.0
     */
    //@PreAuthorize("hasAuthority('" + Constants.USER_ROLE + "')")
    @PostMapping("/list")
    public ResponseEntity<List<AppointmentDto>> getAppointments(@NonNull @RequestParam String start,
                                                                @NonNull @RequestParam String end) {
        log.info("Received call /list with: {} {}", start, end);
        LocalDateTime startingDate = Utils.convertToDate(start);
        LocalDateTime endDate = Utils.convertToDate(end).toLocalDate().atTime(LocalTime.MAX);
        List<AppointmentDto> responseSLaser = appointmentService.findByDates(startingDate, endDate);
        log.info("     Response /list with: {}", responseSLaser);
        return ResponseEntity.ok().body(responseSLaser);
    }

    /**
     * <p>Retrieve a list of the appointments by client in a period of time</p>
     * @param idCli  Id of the client of the period to be retrieved
     * @param start Starting Date of the period to be retrieved
     * @param end  Ending date of the period to be retrieved
     * @return Appointments by client id in this period
     * @since 1.0
     */
    @PostMapping("/list/{idCli}")
    public ResponseEntity<List<AppointmentDto>> getAppointmentsByCli(@NonNull @PathVariable Long idCli,
                                                                @NonNull @RequestParam String start,
                                                                @NonNull @RequestParam String end) {
        log.info("Received call /list/idcli with: " + idCli + " " + start + " " + end);
        LocalDateTime startingDate = Utils.convertToDate(start);
        LocalDateTime endDate = Utils.convertToDate(end).toLocalDate().atTime(LocalTime.MAX);
        List<AppointmentDto> responseSLaser = appointmentService.findByIdCliAndDates(idCli, startingDate, endDate);
        log.info("     Response /list/idcli with: {}", responseSLaser);
        return ResponseEntity.ok().body(responseSLaser);
    }
}
