package com.slaser.slaserybelleza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
//@EnableMailSenderClient      //TBD
public class SlaserybellezaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlaserybellezaApplication.class, args);
	}
}
