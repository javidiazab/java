package com.slaser.slaserybelleza.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * I/O for clients DB
 * Client information as DB
 */
@Entity
@Table(name = "client")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class ClientRegister {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", length = 5)
    private Long id;
    private String clientType;
    private String name;
    private String surname1;
    private String surname2;
    @Column(name = "asoc_centre")
    private String asociatedCentre;
    @Column(name = "type_ident")
    private String typeIdentification;
    private String identification;
    @Column(name = "street_type")
    private String streetType;
    private String street;
    @Column(name = "street_no")
    private String streetNo;
    private String floor;
    private String letter;
    private String city;
    private String cp;
    private String province;
    private String country;
    private String telephone;
    private String landline;
    private String email;
    private String web;
    @Column(name = "other_info", length = 1000)
    private String otherInfo;
    private String gdpr;
    @CreatedDate
    @Column(name = "creation_date")
    private LocalDateTime creationDate;
}
