package com.slaser.slaserybelleza.model.store;

import com.slaser.slaserybelleza.component.persistance.repository.UserRepository;
import com.slaser.slaserybelleza.model.client.UserDto;
import com.slaser.slaserybelleza.model.entity.UserRegister;
import com.slaser.slaserybelleza.service.user.UserMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.security.auth.login.LoginException;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class UserStore {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserDto save(@NonNull UserDto userDto) {
            userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
            return userRepository.save(UserMapper.buildUserRegister(userDto)).toUserDto();
    }

    public UserDto findById(String userName) {
        Optional<UserRegister> user = userRepository.findById(userName);
        return user.map(UserRegister::toUserDto).orElse(null);
    }

    public boolean delete(String userName) {
        if (!userRepository.existsById(userName)) {
            return false;
        }
        userRepository.deleteById(userName);
        return true;
    }

    public UserDto findByIdAndPassword(String userName, String password) throws LoginException {
        String encodedPsw = passwordEncoder.encode(password);
        UserRegister userRegister = userRepository.findByUserNameAndPassword(userName, encodedPsw)
                .orElseThrow(() -> new LoginException("User/password not found"));

        return userRegister.toUserDto();
    }
}
