package com.slaser.slaserybelleza.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Output for LoginException
 */
@AllArgsConstructor
@Getter
public class ErrorResponse {
    private int status;
    private String message;
    private List<String> errors;
}
