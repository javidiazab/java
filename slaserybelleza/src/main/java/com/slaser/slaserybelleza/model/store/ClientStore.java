package com.slaser.slaserybelleza.model.store;

import com.slaser.slaserybelleza.component.persistance.repository.ClientRepository;
import com.slaser.slaserybelleza.exception.SLaserException;
import com.slaser.slaserybelleza.model.entity.ClientRegister;
import com.slaser.slaserybelleza.service.client.ClientRegisterSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ClientStore {

    private final ClientRepository clientRepository;

    public ClientRegister save(ClientRegister clientRegister) throws SLaserException {
        if (nonNull(clientRegister.getId())) {
            throw new SLaserException("Client id is not null");
        }
        return clientRepository.save(clientRegister);
    }

    public ClientRegister update(ClientRegister clientRegister) throws SLaserException {
        if (isNull(clientRegister.getId())) {
            throw new SLaserException("Client id is null");
        }
        final Optional<ClientRegister> clientRegisterById =
                clientRepository.findById(clientRegister.getId());
        if (clientRegisterById.isEmpty()) {
            throw new SLaserException("Client id not found");
        }
        return clientRepository.save(clientRegister);
    }

    public Optional<ClientRegister> getById(long id) throws SLaserException {
        if (isNull(id)) {
            throw new SLaserException("Client id is null");
        }
        return clientRepository.findById(id);
    }

    public boolean delete(long id) throws SLaserException {
        if (!clientRepository.existsById(id)) {
            return false;
        }
        clientRepository.deleteById(id);
        return true;
    }

    public List<ClientRegister> findAllByClientType(String clientType) {
        return clientRepository.findAllByClientTypeOrderByName(clientType);
    }

    public List<ClientRegister> findClientsByAsociatedCentre(String asociatedCentre) {
        return clientRepository
                      .findAllByAsociatedCentreOrderByAsociatedCentreAscNameAscSurname1AscSurname2Asc(asociatedCentre);
    }

    public List<ClientRegister> findAllByFilter(ClientRegisterSpecification specification) {
        return clientRepository.findAll(specification);
    }
}
