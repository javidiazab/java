package com.slaser.slaserybelleza.model.client;

public enum StreetType {
    STREET("Calle"),
    AVENUE("Avda."),
    PLACE("Plza."),
    ROUNDABOUT("Gta."),
    URBANIZATION("Urb."),
    INDUSTRIAL_ST("Pol."),
    ROAD("Ctra.");

    public final String streetType;

    private StreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getStreetType() {
        return this.streetType;
    }
}
