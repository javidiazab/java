package com.slaser.slaserybelleza.model.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Input for Json Web Tokens endpoints
 * Json Web Tokens information
 */
@Data
@AllArgsConstructor
@NoArgsConstructor

public class JwtRequest implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;
	
	private String username;
	private String password;
}