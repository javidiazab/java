package com.slaser.slaserybelleza.model.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * I/O for client endpoints
 * Client information
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientDto implements Serializable {
    private Long id;
    private String clientType;
    private String name;
    private String surname1;
    private String surname2;
    private String asociatedCentre;
    private String typeIdentification;
    private String identification;
    private String streetType;
    private String street;
    private String streetNo;
    private String floor;
    private String letter;
    private String city;
    private String cp;
    private String province;
    private String country;
    private String telephone;
    private String landline;
    private String email;
    private String web;
    private String otherInfo;
    private String gdpr;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime creationDate;
}
