package com.slaser.slaserybelleza.model.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * I/O for user endpoints
 * User information
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto implements Serializable {

    private String userName;
    private String password;
    private String userFullName;
    private UserRole role;
    private int retries;
    private LocalDateTime creationDate;
    private LocalDateTime endDate;
}
