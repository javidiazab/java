package com.slaser.slaserybelleza.model.login;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * Output for Json Web Tokens endpoints
 * Json Web Tokens information
 */
@AllArgsConstructor
@Getter
public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;

	private final String userName;
	private final String token;
}