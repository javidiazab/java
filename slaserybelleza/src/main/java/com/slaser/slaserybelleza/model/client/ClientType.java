package com.slaser.slaserybelleza.model.client;

public enum ClientType {
    PARTICULAR("particular"),
    PROFESSIONAL("profesional");

    public final String clientType;

    private ClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientType() {
        return this.clientType;
    }
}
