package com.slaser.slaserybelleza.model.client;

public enum DocumentType {
    PARTICULAR("particular"),
    PROFESSIONAL("profesional");

    public final String clientType;

    private DocumentType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientType() {
        return this.clientType;
    }
}
