package com.example.KafkaFuntional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaFuntionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaFuntionalApplication.class, args);
	}
}
