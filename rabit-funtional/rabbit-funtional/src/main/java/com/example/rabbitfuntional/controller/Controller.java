package com.example.rabbitfuntional.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class Controller {

    @Autowired
    private StreamBridge streamBridge;

    @RequestMapping("/")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void controller(@RequestBody String body) {
        log.info("Reciviendo desde controller: {}", body);
        streamBridge.send("data-in-0", body);
    }
}
