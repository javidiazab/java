package com.example.rabbitfuntional.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
@Configuration
public class RabbitMQConfig {
    //mySupplier-out-0
//    @Bean
//    public Supplier<String> supply(){
//        log.info("accion: enviando desde supplier " + count);
//        return () -> "enviando desde supplier " + count++ + " " + LocalDateTime.now();
//    }

    //data-in-0
    //data-out-0
    @Bean
    public Function<String, String> data() {
        log.info("accion: enviando desde funcion");
        return value -> value;
    }

    //toUpperCase-in-0
    //toUpperCase-out-0
    @Bean
    public Function<String, String> toUpperCase() {
        log.info("accion: to upper case");
        return String::toUpperCase;
    }

    //consume-in-0
    @Bean
    public Consumer<String> consume() {
        return value -> log.info("accion: consuming: {} {}", value, LocalDateTime.now());
    }
}
