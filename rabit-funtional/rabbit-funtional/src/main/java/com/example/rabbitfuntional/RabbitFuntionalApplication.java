package com.example.rabbitfuntional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitFuntionalApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitFuntionalApplication.class, args);
    }

}
