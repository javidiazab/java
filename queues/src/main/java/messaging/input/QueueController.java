package messaging.input;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import service.MessageProcessor;

import static util.Sanitizer.sanitizeString;

@Component
@Slf4j
@RequiredArgsConstructor
public class QueueController {

    private final MessageProcessor messageProcessor;

    @JmsListener(destination = "test-input-queue")
    public void processServiceMsg(Message<String> message) {
        final String payload = message.getPayload();
        log.info("Received a service message from SAA: {}", sanitizeString(payload));
        messageProcessor.processMessage(payload);
    }
}
