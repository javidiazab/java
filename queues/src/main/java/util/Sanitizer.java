package util;


import java.util.Objects;
import java.util.Optional;


public final class Sanitizer {

    public static String sanitizeString(String string) {
        return (String) Optional.ofNullable(string).map((elem) -> {
            return elem.replace("\r\n","");
        }).orElse("");
    }
}
