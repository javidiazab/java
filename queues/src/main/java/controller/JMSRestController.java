package controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.config.JmsListenerEndpointRegistry;

@RestController
@RequestMapping("/jms")
@RequiredArgsConstructor
@Slf4j
public class JMSRestController {
    private final JmsListenerEndpointRegistry jmsListenerEndpointRegistry;

    @GetMapping("/stop")
    public ResponseEntity<String> stop() {
        if (jmsListenerEndpointRegistry.isRunning()) {
            log.warn("Stopping JMS ...");
            jmsListenerEndpointRegistry.stop();
            return ResponseEntity.ok("All listeners was stopped");
        }
        return ResponseEntity.badRequest().body("All listeners was already stopped");
    }

    @GetMapping("/start")
    public ResponseEntity<String> start() {
        if (!jmsListenerEndpointRegistry.isRunning()) {
            log.warn("Starting JMS ...");
            jmsListenerEndpointRegistry.start();
            return ResponseEntity.ok("All listeners was started");
        }

        return ResponseEntity.badRequest().body("All listeners was already started");
    }

    @GetMapping("/status")
    public ResponseEntity<String> getStatus() {
        return jmsListenerEndpointRegistry.isRunning() ? ResponseEntity.ok("Running") : ResponseEntity.ok("Stopped");
    }

}