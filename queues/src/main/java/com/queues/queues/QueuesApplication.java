package com.queues.queues;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBinding({
		Pacs008Receiver.class,
		SwiftMsgReceiver.class,
		Pacs002Sender.class,
		EventsSource.class,
		SwiftSender.class
})
public class QueuesApplication {

	public static void main(String[] args) {
		SpringApplication.run(QueuesApplication.class, args);
	}

}
