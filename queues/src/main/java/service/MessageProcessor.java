package service;

import org.springframework.stereotype.Service;

@Service
public class MessageProcessor {

    public void processMessage(String payload){
        System.out.println("Payload: " + payload);
    }

}
