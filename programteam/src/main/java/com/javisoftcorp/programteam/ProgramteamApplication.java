package com.javisoftcorp.programteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramteamApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgramteamApplication.class, args);
	}

}
