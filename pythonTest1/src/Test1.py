import platform
import json

x = dir(platform)
print(x)
print(platform.processor())
print(platform.system())


#Json -----------
# a Python object (dict):
x = {
    "name": "John",
    "age": 30,
    "city": "New York"
}

# convert into JSON:
y = json.dumps(x)

# the result is a JSON string:
print(y)

username = input("Enter username:")
print("Username is: " + username)