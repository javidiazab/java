/*
 * DateTime.java
 *
 * Created on 20 de mayo de 2004, 0:31
 */
package App;
import java.util.Calendar;
/**
 *
 * @author  Javito Corleone
 */
public class DateTime {
    
    /** Creates a new instance of DateTime */
    public DateTime() {
    }
    
    public void getCurrentTime(){
        
        Calendar cal = Calendar.getInstance();
    
    // Get the components of the time
    int hour12 = cal.get(Calendar.HOUR);            // 0..11
    int hour24 = cal.get(Calendar.HOUR_OF_DAY);     // 0..23
    int min = cal.get(Calendar.MINUTE);             // 0..59
    int sec = cal.get(Calendar.SECOND);             // 0..59
    int ms = cal.get(Calendar.MILLISECOND);         // 0..999
    int ampm = cal.get(Calendar.AM_PM);             // 0=AM, 1=PM
    }
        
    public void getCurrentDate(){
        Calendar cal = Calendar.getInstance();
    
    // Get the components of the date
    int era = cal.get(Calendar.ERA);               // 0=BC, 1=AD
    int year = cal.get(Calendar.YEAR);             // 2002
    int month = cal.get(Calendar.MONTH);           // 0=Jan, 1=Feb, ...
    int day = cal.get(Calendar.DAY_OF_MONTH);      // 1...
    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK); // 1=Sunday, 2=Monday, ...

        
    }
    
    /*
    public void nose(){
        // Get the current time
            long curTime = System.currentTimeMillis();
    
            // If last key was typed less than 300 ms ago, append to current pattern
            if (curTime - lastKeyTime < 300) {
                pattern += ("" + aKey).toLowerCase();
            } else {
                pattern = ("" + aKey).toLowerCase();
            }
    
            // Save current time
            lastKeyTime = curTime;
    
            // Search forward from current selection
            for (int i=selIx+1; i<model.getSize(); i++) {
                String s = model.getElementAt(i).toString().toLowerCase();
                if (s.startsWith(pattern)) {
                    return i;
                }
            }
    
            // Search from top to current selection
            for (int i=0; i<selIx ; i++) {
                if (model.getElementAt(i) != null) {
                    String s = model.getElementAt(i).toString().toLowerCase();
                    if (s.startsWith(pattern)) {
                        return i;
                    }
                }
            }
            return -1;
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
