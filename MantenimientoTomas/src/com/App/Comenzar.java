/*
 * Comenzar.java
 *
 * Created on 5 de marzo de 2004, 22:38
 */
package App;
/**
 *
 * @author  Javito
 *Crea un formulario de comenzar con todos los paneles de mantenimiento de las tablas
 */
public class Comenzar extends javax.swing.JInternalFrame {
    
    /** Creates new form Comenzar */

   public static int estancias; 
   private javax.swing.JTabbedPane tabbedPaneComenzar;
   private javax.swing.JPanel [] tablaPaneles = new javax.swing.JPanel[100];
   private Mantenimiento [] tablaMantenimientos = new Mantenimiento [100];
    
    public Comenzar() {
        super();
        checkInstancias();
        initComponents();
	initialize();
}
/**
 * Comenzar constructor comment.
 * @param title java.lang.String
 */
public Comenzar(String title) {
	super(title);
        checkInstancias();
        initComponents();
	initialize();
}
/**
 * Comenzar constructor comment.
 * @param title java.lang.String
 * @param resizable boolean
 */
public Comenzar(String title, boolean resizable) {
	super(title, resizable);
        checkInstancias();
        initComponents();
	initialize();
}
/**
 * Comenzar constructor comment.
 * @param title java.lang.String
 * @param resizable boolean
 * @param closable boolean
 */
public Comenzar(String title, boolean resizable, boolean closable) {
	super(title, resizable, closable); //resizable, closable
        checkInstancias();
        initComponents();
	initialize();
}
/**
 * Comenzar constructor comment.
 * @param title java.lang.String
 * @param resizable boolean
 * @param closable boolean
 * @param maximizable boolean
 */
public Comenzar(String title, boolean resizable, boolean closable, boolean maximizable) {
	super(title, resizable, closable, maximizable);
        checkInstancias();
        initComponents();
	initialize();
}
/**
 * Comenzar constructor comment.
 * @param title java.lang.String
 * @param resizable boolean
 * @param closable boolean
 * @param maximizable boolean
 * @param iconifiable boolean
 */
public Comenzar(String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
	super(title, resizable, closable, maximizable, iconifiable);
        initComponents();
        checkInstancias();
	initialize();
}
  
private void checkInstancias() {
//    esta puta mierda del dispose no funciona aqui
//    if (estancias > 0) dispose();
    estancias++;
}

    private void initialize() {
        try {
        tabbedPaneComenzar = new javax.swing.JTabbedPane();
//	setResizable(true);setClosable(true);setIconifiable(true);setMaximizable(true);
        setName("Comenzar");
        setSelected(true);
        setSize(662, 423);
        setTitle("Titulo de Comenzar");
        setFrameIcon(new javax.swing.ImageIcon(Configurar.get("icono")));

//                setFrameIcon(new javax.swing.ImageIcon("error.gif")); 
//                Utils.setIcon(this);

//                Mantenimiento clientes = new Mantenimiento(this, "Clientes", "clientes");
                
        cargarMantenimientos();
        cargarOtrosPaneles();
        asignaListeners();
        
// para establecer la fuente del panel                
//        setFont(new java.awt.Font("Allegro BT", 0, 10));
//        panelClientes.setFont(new java.awt.Font("Comic Sans MS", 1, 11));
        
        
       // mira los que son mantenimientos de la tabla de paneles y los a�ade al tabbed pane
        for (int i = 0; i < tablaPaneles.length && tablaPaneles[i] != null; i++) {
            if (tablaPaneles[i] instanceof Mantenimiento) {
//                ((Mantenimiento)tablaPaneles[i]).llenarPanel();
                tabbedPaneComenzar.addTab(((Mantenimiento)tablaPaneles[i]).getTitle(),
                                           ((Mantenimiento)tablaPaneles[i]).getPanel());
            } else {
                tabbedPaneComenzar.addTab("", tablaPaneles[i]);
            }
        }
        
        getContentPane().add(tabbedPaneComenzar, java.awt.BorderLayout.CENTER);
        //ponemos aqui el tama�o porque si no se lo torea
        setSize(662, 423);
	} catch (java.lang.Throwable ivjExc) {
		handleException(ivjExc);
	}
    }

    public void asignaListeners()  {
        //Se asignan tambien otros listeners a los paneles segun se van creando
        tabbedPaneComenzar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabbedPaneComenzarMouseClicked(evt);
            }
        });
        tabbedPaneComenzar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt2) {
                tabbedPaneComenzarFocusGained(evt2);
                
            }
        });
        
//  PARECE QUE EL METODO DE ARRIBA MOLA MAS QUE ESTE        
//        java.awt.event.ActionListener bl = new java.awt.event.ActionListener() {
//             public void actionPerformed(java.awt.event.ActionEvent e) { //va a redestribuir las acciones
//                 String name = ((javax.swing.JButton)e.getSource()).getText();
//             }};
    }
    
    public void tabbedPaneComenzarMouseClicked(java.awt.event.MouseEvent evt) {
        int x = ((javax.swing.JTabbedPane)evt.getSource()).getSelectedIndex();
        this.setTitle("Mantenimiento de la tabla de " + 
                      ((javax.swing.JTabbedPane)evt.getSource()).getTitleAt(x));
//        System.out.println("Focus en panel clicked");
    }
    
     public void tabbedPaneComenzarFocusGained(java.awt.event.FocusEvent evt) {
        int x = ((javax.swing.JTabbedPane)evt.getSource()).getSelectedIndex();
                this.setTitle("Mantenimiento de la tabla de " + 
                      ((javax.swing.JTabbedPane)evt.getSource()).getTitleAt(x));
//                System.out.println("Focus en panel focus gained");
//              como coger el componente/tab que esta seleccionado (y es un mantenimiento)         
                java.awt.Component com = tabbedPaneComenzar.getComponentAt(x);
                if (com instanceof Mantenimiento) {
                    ((Mantenimiento)com).setFocusFirstEdit();
//                    System.out.println("Manteiniento " + com.getName());
                }
    }
     
    public void setFocus() {
        int index = tabbedPaneComenzar.getSelectedIndex();
        java.awt.Component com = tabbedPaneComenzar.getComponentAt(index);
        if (com instanceof Mantenimiento) {
            ((Mantenimiento)com).setFocusFirstEdit();
//                    System.out.println("Manteiniento " + com.getName());
        }
    }
    
    
    public void cargarMantenimientos(){
//        for (int i = 0; i < tablaMantenimientos.length; i++) {
//            tablaMantenimientos[i] = null;
//        }
        
        for (int i = 0; Configurar.get("mantenimiento" + (i+1)) != null ;i++) {
           tablaPaneles[i] = new Mantenimiento((Configurar.get("mantenimiento" + (i+1))).toLowerCase());
           //Asigna Listener al mismo tiempo que se crean
           //Otros Listeners tambien en asignaListeners()
//           tablaPaneles[i].addFocusListener(new java.awt.event.FocusAdapter() {
//                public void focusGained(java.awt.event.FocusEvent evt2) {
//                    System.out.println("Focus en mantenimiento");
//                }
//            });
        }
//        if (tablaPaneles[0] != null) {
//            System.out.println("Pasa por aqui?");
//            ((Mantenimiento)tablaPaneles[0]).setFocusFirstEdit();
//        }
    }
    
    public void cargarOtrosPaneles(){
        
    }
    
       
    private void handleException(java.lang.Throwable exception) {

	/* Uncomment the following lines to print uncaught exceptions to stdout */
	// System.out.println("--------- UNCAUGHT EXCEPTION ---------");
	// exception.printStackTrace(System.out);
}
    
    public javax.swing.JPanel getPanel(String panel) {
        for (int i = 0; i < tablaPaneles.length && tablaPaneles[i] != null; i++) {
            if (tablaPaneles[i] instanceof Mantenimiento) {
                if (panel == ((Mantenimiento)tablaPaneles[i]).getTitle())
                     return ((Mantenimiento)tablaPaneles[i]).getPanel();
            } else {
                if (tablaPaneles[i].equals(panel)) return tablaPaneles[i];
            }
        }
     // no ha encontrado ningun panel con el mismo nombre
        return null;
    }
      
     public void selectPanel(String aPanel) {
         int index;
         index = tabbedPaneComenzar.indexOfTab(aPanel);
         if (index > -1) tabbedPaneComenzar.setSelectedIndex(index);
         else javax.swing.JOptionPane.showMessageDialog
                (null,
                 "Panel : " + aPanel + " no existe.",
                 "Error",
                 javax.swing.JOptionPane.ERROR_MESSAGE); 
         
//        for (int i = 0; i < TabbedPaneComenzar.getTabCount(); i++) {
//            if (TabbedPaneComenzar.getTitleAt(i) == aPanel) TabbedPaneComenzar.setSelectedIndex(i);
//        }
     }
    
   
    private void initComponents() {//GEN-BEGIN:initComponents

        setClosable(true);
        setFont(new java.awt.Font("Allegro BT", 0, 10));
        setVisible(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosed(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

    }//GEN-END:initComponents

    private void formInternalFrameClosed(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosed
        // TODO add your handling code here:
        estancias--;
    }//GEN-LAST:event_formInternalFrameClosed

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_formInternalFrameClosing
    
    public static void main(java.lang.String[] args) {
       Utils.setJaviProperties();
       javax.swing.JFrame frame = new javax.swing.JFrame();
       frame.setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
//       frame.setSize(1000, 900);
       Comenzar newComenzar = new Comenzar("Nuevo Comenzar",true,true,true,true);
//     newComenzar.setVisible(true);  //no hace falta
              frame.setVisible(true);
       frame.getContentPane().add(newComenzar);
//       frame.show();
       newComenzar.show();

//          
       
//       Comenzar newComenzar2 = new Comenzar("Nuevo Comenzar",true,true,true,true);
//       if (newComenzar2 == null) {
//           Utils.dialog("mierfda");}
//           else {
//       
////     newComenzar.setVisible(true);  //no hace falta
//       frame.getContentPane().add(newComenzar2);
//       newComenzar2.show();
////          
//       frame.show();
//       }
    }
        
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
}
