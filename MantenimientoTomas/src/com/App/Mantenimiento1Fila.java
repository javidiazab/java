/*
 * IFMantenimiento.java
 *
 * Created on 16 de julio de 2004, 21:43
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
public class Mantenimiento1Fila extends Mantenimiento{
    /** Creates a new instance of IFMantenimiento */
        
    public Mantenimiento1Fila(String tabla) {
// tenemos que incluir el Comenaar para tener una referencia al llamador para cerrar
// la ventana cuando pulsamos Salir
        super(tabla);
        llenarPanelConDatos(this.tabla);
        
    } 
    
       
    public void setUpButtons (){
       tablaBotones[0] = new javax.swing.JButton("Aceptar");   // crea la tabla de botones
       tablaBotones[1] = new javax.swing.JButton("Salir");
       
//     class ButtonListener implements java.awt.event.ActionListener {
       java.awt.event.ActionListener bl = new java.awt.event.ActionListener() {
             public void actionPerformed(java.awt.event.ActionEvent e) { //va a redestribuir las acciones
                 String name = ((javax.swing.JButton)e.getSource()).getText();
                 if (name.equals("Aceptar")) {
                     update();
                     botonCerrar_ActionPerformed(e);
                     
                 }
                 if (name.equals("Salir")) {
                     botonCerrar_ActionPerformed(e);
                 }
             }
        };
              
        
       for (int i = 0; i < tablaBotones.length && tablaBotones[i] != null; i++){
           // asocia el listener a todos los botones de abajo
           tablaBotones[i].addActionListener(bl);  // asocia el listener a todos los botones de abajo
           add("Botones", tablaBotones[i]);
//           aJaviConf.getPanel("Botones").add(tablaBotones[i]);  // a�ade los botones al panel
       }
       
    }
    
    
     public static void main(java.lang.String[] args) {
//       javax.swing.JTabbedPane TabbedPaneComenzar = new javax.swing.JTabbedPane();
        Utils.setJaviProperties();
        javax.swing.JFrame frame2 = new javax.swing.JFrame();
        frame2.setSize(662, 423);
        Mantenimiento1Fila clientes2 = new Mantenimiento1Fila("configuracion");
        frame2.getContentPane().add(clientes2);
        frame2.setVisible(true);
     
    }
    
}
