/*
 * JaviSoftEnElWeb.java
 *
 * Created on 11 de abril de 2004, 18:57
 */
package App;
/**
 *
 * @author  Javito
 */
public class JaviSoftEnElWeb extends javax.swing.JDialog {
    
    /** Creates new form JaviSoftEnElWeb */
    public JaviSoftEnElWeb(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        initialize();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        pack();
    }//GEN-END:initComponents
    
    private javax.swing.JLabel getLabelTexto() {
	if (ivjLabelTexto == null) {
		try {
			ivjLabelTexto = new javax.swing.JLabel();
			ivjLabelTexto.setName("LabelTexto");
			//Quitar esto cuando se pueda
//			ivjLabelTexto.setText("Label con texto");
			// user code begin {1}
                        ivjLabelTexto.setText("<html><center>Para obtener nuevas versiones...<br>" + 
		    "Para realizar sugerencias ...<br>" +
		    "Para comunicar posibles problemas ...<br>" +
		    "Para comentar posibles necesidades ...<br>" +
		    "o simplemente para contar<br>" +
		    "como os ha ido ...<br><br>" + Configurar.get("pagina web") + 
			  "</center></html>");
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
//			handleException(ivjExc);
		}
	}
	return ivjLabelTexto;
}
    
 private javax.swing.JLabel getFondo() {
    javax.swing.JLabel fondo = new javax.swing.JLabel();
    try {
            fondo.setName("LabelFondo");
            fondo.setText("");
    } catch (java.lang.Throwable ivjExc) {
//	  handleException(ivjExc);
    }
    return fondo;
}
    
    private javax.swing.JButton getBotonCerrar() {
	if (ivjBotonCerrar == null) {
		try {
			ivjBotonCerrar = new javax.swing.JButton();
			ivjBotonCerrar.setName("BotonCerrar");
			ivjBotonCerrar.setMnemonic('c');
			ivjBotonCerrar.setText("Cerrar");
			ivjBotonCerrar.setActionCommand("");
            ivjBotonCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrar_ActionPerformed(evt);
            }
        });
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjBotonCerrar;
}
      
    
    public void botonCerrar_ActionPerformed(java.awt.event.ActionEvent actionEvent) {
	this.dispose();
	return;
}
    
    private void initialize () {
        JaviPanel aJaviPanel = new JaviPanel();
        setSize(300, 240);
        getContentPane().add(aJaviPanel); 
        setTitle(System.getProperty("app.companyName") + " en el Web ...");
        aJaviPanel.getPanel("Central").add(getLabelTexto());
        aJaviPanel.getPanel("Botones").add(getBotonCerrar());
        Utils.centrame(this);
    }
    
    
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog
    private void handleException(java.lang.Throwable exception) {

	/* Uncomment the following lines to print uncaught exceptions to stdout */
	// System.out.println("--------- UNCAUGHT EXCEPTION ---------");
	// exception.printStackTrace(System.out);
}
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new JaviSoftEnElWeb(new javax.swing.JFrame(), true).setVisible(true);
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    javax.swing.JLabel ivjLabelTexto = null;
    javax.swing.JButton ivjBotonCerrar = null;
   
}
