/*
 * FileSys.java
 *
 * Created on 27 de abril de 2004, 22:41
 */

/**
 *
 * @author  Javito Corleone
 **
 *NO SE USA ESTA CLASE
 *
 *
 */
package App;
import java.io.*;
import java.util.*;

public class FileSys2 extends ArrayList {

	// Tools to read and write files as single strings:
	public static String
	read(String fileName) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader in =
		new BufferedReader(new FileReader(fileName));
		String s;
		while((s = in.readLine()) != null) {
			sb.append(s);
			sb.append("\n");
		}
		in.close();
		return sb.toString();

	}

	public static void
	write(String fileName, String text) throws IOException {
		PrintWriter out = new PrintWriter(
		new BufferedWriter(new FileWriter(fileName)));
		out.print(text);
		out.close();
	}

	public FileSys2(String fileName) throws IOException {
		super(Arrays.asList(read(fileName).split("\n")));
	}

	public void write(String fileName) throws IOException {
		PrintWriter out = new PrintWriter(
		new BufferedWriter(new FileWriter(fileName)));
		for(int i = 0; i < size(); i++)
//                out.println(get(i));
//                out.close();
                 System.out.println(get(i));
	}

	// Simple test:
	public static void main(String[] args) throws Exception {
//		String file = read("TextFile.java");
//		write("test.txt", file);
                FileSys text = new FileSys("Tomas.sys");
                System.out.println(text);
//                text.write("Tomas.sys");
                text.write("Tomas.sys");
	}
} ///:~

