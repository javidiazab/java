/*
 * Mantenimiento.java
 *
 * Created on 9 de julio de 2004, 23:14
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
@SuppressWarnings("UnusedAssignment")
public class Mantenimiento extends javax.swing.JPanel{
    
    private JaviConfiguraPanel aJaviConf; // = new JaviConfiguraPanel(this);
    private final javax.swing.JButton [] tablaBotones = new javax.swing.JButton[6];
    private MainTabla mainTabla;
    private boolean modificando;
    
    private javax.swing.JComponent [] cliComponents = new javax.swing.JComponent[300];
    //    Comenzar aComenzar;
    //    javax.swing.JInternalFrame iFrame = null;
    //    javax.swing.JFrame frame = null;
    String panel = null;
    private String tabla = null;
    private String title = null;
    private BD cli = null;
    private DefCol col = new DefCol();
    
    
    //    public static int filaSeleccionada;
    public static int f;
    
    
    /** Creates a new instance of Mantenimiento */


    private Mantenimiento(String tabla) {
       
        // tenemos que incluir el Comenzar para tener una referencia al llamador para cerrar
        // la ventana cuando pulsamos Salir
        //        this.iFrame = null;
        //        frame.setTitle("Mantenimiento de la tabla de " + tabla);
        this.tabla = tabla;
        this.setName(tabla);
        aJaviConf = new JaviConfiguraPanel(this);
        //        aJaviConf = new JaviConfiguraPanel(aComenzar.getPanel(panel));
        initialize();
        
    }
    
    private void initialize() {
        llenarPanel();
    }
    
    private Mantenimiento getMantenimiento() {
        return this;
    }
    
    private void llenarPanel(){
        setUpFields();
        setUpButtons();
        setTitle((tabla.substring(0,1)).toUpperCase() + tabla.substring(1).toLowerCase());
    }
    
    public void llenarPanelConDatos(String tabla) {
        String []tablax = null;
        tablax = cli.select("select * from " + tabla); // <--------- mejorar
        int j = 0;
        for (int i = 0; cliComponents[i] != null; i++) {
            if (cliComponents[i] instanceof javax.swing.JTextField) {
                ((javax.swing.JTextField)cliComponents[i]).setText(tablax[j]);
                j++;
            }
            if (cliComponents[i] instanceof javax.swing.JTextArea) {
                ((javax.swing.JTextArea)cliComponents[i]).setText(tablax[j]);
                j++;
            }
        }
    }
    
    
    
    public void update() {
        String [] tabla1 = new String [100];
        if (cli == null) return;
        int j = 0;
        for (int i = 0; cliComponents[i] != null; i++) {
            if (cliComponents[i] instanceof javax.swing.JTextField) {
                //              no se sabe porque no se pude hacer esto. Por eso hacemos la linea de abajo
                //                   Utils.print(((javax.swing.JTextField)cliComponents[i]).getText());
                String a = ((javax.swing.JTextField)cliComponents[i]).getText();
                tabla1[j] = a;
                //                     tabla1[j] = ((javax.swing.JTextField)cliComponents[i]).getText();
                j++;
            }
            if (cliComponents[i] instanceof javax.swing.JTextArea) {
                String a = ((javax.swing.JTextArea)cliComponents[i]).getText();
                tabla1[j] = a;
                j++;
            }
        }
        cli.update(tabla1, "configuracion");
    }
    
    private void setUpButtons(){
        tablaBotones[0] = new javax.swing.JButton("Altas");   // crea la tabla de botones
        tablaBotones[1] = new javax.swing.JButton("Bajas");
        tablaBotones[2] = new javax.swing.JButton("Modificaciones");
        tablaBotones[3] = new javax.swing.JButton("Consultas");
        tablaBotones[4] = new javax.swing.JButton("Limpiar");
        tablaBotones[5] = new javax.swing.JButton("Salir");
        
        //     class ButtonListener implements java.awt.event.ActionListener {
        java.awt.event.ActionListener bl = e -> { //va a redestribuir las acciones
            String name1 = ((javax.swing.JButton)e.getSource()).getText();
            if (name1.equals(tablaBotones[0].getText())) altas();
            if (name1.equals(tablaBotones[1].getText())) bajas();
            if (name1.equals(tablaBotones[2].getText())) modificaciones();
            if (name1.equals(tablaBotones[3].getText())) consultas();
            if (name1.equals(tablaBotones[4].getText())) limpiar();
            if (name1.equals(tablaBotones[5].getText())) botonCerrar_ActionPerformed(e); //SALIR
        };
        
        
        for (int i = 0; i < tablaBotones.length && tablaBotones[i] != null; i++){
            // asocia el listener a todos los botones de abajo
            tablaBotones[i].addActionListener(bl);  // asocia el listener a todos los botones de abajo
            add("Botones", tablaBotones[i]);
         
        }
        
    }
    
    private void altas() {
        pantallaTabla();
            try {
                cli.execute(mainTabla.creaSelect(tabla));
                cli.insert(tablaBD());
//                Controlar mas adelante
//                if (cli.count() == 0) {
//                    cli.insert(tablaBD());
//                } else if (cli.count() == 1) {
//                    cli.insert(tablaBD());
//                } else if (cli.count() > 1) {
//                    cli.insert(tablaBD());
//                }
                  limpiar();
                
            } catch (Exception e) {e.printStackTrace();}

    }
    
    private void bajas() {
        pantallaTabla();
        cli.execute(mainTabla.creaSelect(tabla));
        if (cli.count() == 0) {
            Utils.dialog(getMantenimiento(), "Registro no encontrado.");
        } else if (cli.count() == 1) {
            BDTabla();
            tablaPantalla();
            if (javax.swing.JOptionPane.showConfirmDialog(
                null,
                "Confirmar baja",
                "Confirmaci�n",
                javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION) {
                cli.delete();
                limpiar();
            }
        } else if (cli.count() > 1) {
            if (cargarDialogo()) {
                BDTabla();
                tablaPantalla();
                if (javax.swing.JOptionPane.showConfirmDialog(
                    null,
                    "Confirmar baja",
                    "Confirmaci�n",
                    javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION) {
                    cli.delete();
                    limpiar();
                }
            }

        }
    }
    
    private void consultas() {
        pantallaTabla();
        cli.execute(mainTabla.creaSelect(tabla));
        if (cli.count() == 0) {
            Utils.dialog(getMantenimiento(), "Registro no encontrado.");
        } else if (cli.count() == 1) {
            BDTabla();
            tablaPantalla();
        } else if (cli.count() > 1) {
            if (cargarDialogo()) {
                BDTabla();
                tablaPantalla();
            }
        }
        modificando = true;
    }
    
    private void modificaciones() {
        pantallaTabla();
        if (modificando) {
              if (javax.swing.JOptionPane.showConfirmDialog(
                null,
                "Confirmar modificacion",
                "Confirmaci�n",
                javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION) {
                cli.update(tablaBD());
                limpiar();
            }
        }
        else {
            cli.execute(mainTabla.creaSelect(tabla));
            if (cli.count() == 0) {
                Utils.dialog(getMantenimiento(), "Registro no encontrado.");
            } else if (cli.count() == 1) {
                BDTabla();
                tablaPantalla();
    //            if (javax.swing.JOptionPane.showConfirmDialog(
    //                null,
    //                "Confirmar modificacion",
    //                "Confirmaci�n",
    //                javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION) {
    //                cli.update(tablaBD());
    //                limpiar();
    //            }
            } else if (cli.count() > 1) {
                if (cargarDialogo()) {
                    BDTabla();
                    tablaPantalla();
    //                if (javax.swing.JOptionPane.showConfirmDialog(
    //                    null,
    //                    "Confirmar baja",
    //                    "Confirmaci�n",
    //                    javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION) {
    //                    cli.update(tablaBD());
    //                    limpiar();
    //                }
                }

            }
            modificando = true;
        }
    }
    
    private void limpiar() {
        modificando = false;
        for (int i = 0; i < cliComponents.length && cliComponents[i] != null; i++){
            if (cliComponents[i] instanceof javax.swing.text.JTextComponent) 
            ((javax.swing.text.JTextComponent)cliComponents[i]).setText("");
            if (cliComponents[i] instanceof javax.swing.JComboBox) 
            ((javax.swing.JComboBox)cliComponents[i]).setSelectedIndex(0);
            if (cliComponents[i] instanceof javax.swing.AbstractButton) 
            ((javax.swing.AbstractButton)cliComponents[i]).setSelected(false);
//            if (cliComponents[i] instanceof
//            javax.swing.JTextField) ((javax.swing.JTextField)cliComponents[i]).setText("");
//            if (cliComponents[i] instanceof
//            javax.swing.JTextField) ((javax.swing.JTextField)cliComponents[i]).setText("");
        }
        setFocusFirstEdit();
    }
    
    private void BDTabla() {
        mainTabla.setIndex(0);
        for (; !mainTabla.EOF(); mainTabla.next()) {
            try {
                //Esta puta mierda es requerida porque no es igual mainTabla.get("ColumnEnPantalla") que
                // devuelve "no" que el literal "no"
                mainTabla.setValue(cli.get(mainTabla.getName()));
            } catch (Exception e) {e.printStackTrace();}
        }
    }
    
    private boolean cargarDialogo() {
        String[] columnNames = new String [500];
        Object[][] data = new Object [300][500];
        
        // vamos a recorrer las filas de la BD
        for (int filas = 0; !cli.EOF() && filas < data.length; cli.next(), filas++) {
            mainTabla.setIndex(0);
            // vamos a recorrer las filas de maintabla que son las columnas de la BD
            for (int j = 0; !mainTabla.EOF() && j < columnNames.length; mainTabla.next(), j++) {
                try {
                    columnNames[j] = mainTabla.getLabel();
                    data[filas][j] = cli.get(mainTabla.getName());
                    
                } catch (Exception e) {e.printStackTrace();}
            }
        }
        
        TableRows frame = new TableRows(columnNames, data);
        frame.setVisible(true);
        //       frame.setVisible(true);
        //       Utils.print("Seleccionada la fila " + filaSeleccionada);
        //       cli.setIndex(filaSeleccionada);
        // las filas del resulset empiezan en 1
        if (Utils.getFil() >= 0) {
            cli.setIndex(Utils.getFil() + 1);
            return true;
        } else return false;
        
        
    }
    private void add(String panel, javax.swing.JComponent component) {
        aJaviConf.getPanel(panel).add(component);  // a�ade los botones al panel
    }
    
    
    private void botonCerrar_ActionPerformed(java.awt.event.ActionEvent actionEvent) {
        ////        if (frame == null) iFrame.dispose();
        ////        else frame.dispose();
        
        boolean salir = false;
        java.awt.Container container = getParent();
        for (int i = 0; i < 10 && !salir; i++) {
            if (container instanceof java.awt.Window) {
                ((java.awt.Window)container).dispose();
                salir = true;
            }
            else if (container instanceof javax.swing.JFrame) {
                ((javax.swing.JFrame)container).dispose();
                salir = true;
            }
            else if (container instanceof javax.swing.JInternalFrame) {
                ((javax.swing.JInternalFrame)container).dispose();
                salir = true;
            }
            
            container = container.getParent();
        }
        if (!salir) Utils.dialog("Fue imposible cerrar esta ventana");
        
    }
    
    private void setUpFields() {
//        System.out.println(System.getProperty("app.app"));
        cli = new BD(System.getProperty("app.app"), tabla, true); // la vamos a actualizar con el true
        
        mainTabla = cli.getMainTabla();
        mainTabla.setIndex(0);

        for (int i = 0; !mainTabla.EOF(); mainTabla.next()) {
            try {
                //Esta puta mierda es requerida porque no es igual mainTabla.get("ColEnPantalla") que
                // devuelve "si" que el literal "si"
                if (mainTabla.get("ColEnPantalla").equals("si")) {
                    int value = new Integer(mainTabla.get("ColSize"));
                    String tipo = mainTabla.get(col.getIndexTypeSwing());
                    if (tipo.equals("JTextField")) {
                        cliComponents[i++] = new javax.swing.JLabel(mainTabla.get("ColLabel"));
                        //   establecemos el nombre del componente Label igual que el nombre de la col de la tabla + "Label"
                        ((javax.swing.JLabel)cliComponents[i-1]).setName("Label" + mainTabla.get("ColName"));
                        cliComponents[i++] = creaTextField(mainTabla);
                        if (value > 30) {
                            cliComponents[i++] = new javax.swing.JButton("...");
                            //establecemos el nombre del componente Buton igual que el nombre de la col de la tabla + "Button"
                            cliComponents[i-1].setName("Button" + mainTabla.get("ColName"));
                        }
                    } // if (mainTabla.get(col.getIndexTypeSwing()) == "JTextField") {
                    if (tipo.equals("JComboBox")) {
                        cliComponents[i++] = new javax.swing.JLabel(mainTabla.get("ColLabel"));
                        ((javax.swing.JLabel)cliComponents[i-1]).setName("Label" + mainTabla.get("ColName"));
                        cliComponents[i++] = creaComboBox(mainTabla);
                    } // if (mainTabla.get(col.getIndexTypeSwing()) != "JComboBox") {
                    if (tipo.equals("JTextArea")) {
                        cliComponents[i++] = new javax.swing.JLabel(mainTabla.get("ColLabel"));
                        ((javax.swing.JLabel)cliComponents[i-1]).setName("Label" + mainTabla.get("ColName"));
                        cliComponents[i++] = creaTextArea(mainTabla);
                        if (value > 30) {
                            cliComponents[i++] = new javax.swing.JButton("...");
                            //establecemos el nombre del componente Buton igual que el nombre de la col de la tabla + "Button"
                            cliComponents[i-1].setName("Button" + mainTabla.get("ColName"));
                        }
                    } // if (mainTabla.get(col.getIndexTypeSwing()) != "JTextArea") {
                    if (tipo.equals("JCheckBox")) {
                        cliComponents[i++] = new javax.swing.JLabel(mainTabla.get("ColLabel"));
                        ((javax.swing.JLabel)cliComponents[i-1]).setName("Label" + mainTabla.get("ColName"));
                        cliComponents[i++] = creaCheckBox(mainTabla);
                    } // if (mainTabla.get(col.getIndexTypeSwing()) != "JComboBox") {
                    
                } //if (!mainTabla.get("ColEnPantalla").equals("si")) {
            } catch (Exception e) {e.printStackTrace();}
        }
        
        
        java.awt.event.ActionListener cliBl = e -> {
            if (e.getSource() instanceof javax.swing.JButton) {
                for (int i = 0; cliComponents[i] != null; i++) {
                    if (((javax.swing.JButton)e.getSource()) == cliComponents[i])
                        // va a coger el anterior al texto que es el label con la descripcion
                        new DialogDesc(getMantenimiento(), ((javax.swing.JLabel)cliComponents[i-2]).getText(),
                        // va a coger el anterior que es el texto
                        ((javax.swing.text.JTextComponent)cliComponents[i-1]));
                }
            }
        };
        
        //lay the components in the Central panel and add Listeners
        for (int i = 0; cliComponents[i] != null; i++) {
            //           aJaviConf.getPanel("Central").add(cliComponents[i]);
            add("Central", cliComponents[i]);
            if (cliComponents[i] instanceof javax.swing.JTextField)
                ((javax.swing.JTextField)cliComponents[i]).addActionListener(cliBl);
            
            if (cliComponents[i] instanceof javax.swing.JButton)
                ((javax.swing.JButton)cliComponents[i]).addActionListener(cliBl);
        }
        //Va a asignar el focus al primer componente editable cuando el panel consiga el focus
        //       addFocusListener(new java.awt.event.FocusAdapter() {
        //                public void focusGained(java.awt.event.FocusEvent evt2) {
        //                    System.out.println("Focus en Mantenimiento dentro de mantenimiento");
        //                    setFocusFirstEdit();
        //                }
        //       });
    }
    
    private javax.swing.JComponent 
        creaTextField(MainTabla mainTabla) {
            
        javax.swing.JComponent componente;
        int value = new Integer(mainTabla.get("ColSize"));
            if (value > 30) {
                // si la longitud es mas de 30 presentamos el boton para desplegar
                componente = new javax.swing.JTextField(30);
            } else {
                componente = new javax.swing.JTextField(value);
           } // if (value > 30) {
        //establecemos el nombre del componente igual que el nombre de la col de la tabla
        componente.setName(mainTabla.get("ColName"));
        return componente;
    }
    
     private javax.swing.JComponent creaComboBox(MainTabla mainTabla) {
        javax.swing.JComponent componente = new javax.swing.JComboBox();
        int value = new Integer(mainTabla.get("ColSize"));
            String fichero = mainTabla.get(col.getIndexFichero());
            if (!fichero.equals("")) {
               try{
                  if ((new java.io.File(fichero)).exists()) {
                    FileSys fileTbl = new FileSys(fichero);
                    String [] tablaAux = fileTbl.read();
                    componente = new javax.swing.JComboBox(tablaAux);
                  }
              } catch (Exception e) {e.printStackTrace();} 
            } //if (!fichero.equals("")) {
           //establecemos el nombre del componente igual que el nombre de la col de la tabla
            componente.setName(mainTabla.get(col.getIndexName()));
            ((javax.swing.JComboBox)componente).setEditable(true);

        return componente;
    }
     
    private javax.swing.JComponent creaTextArea(MainTabla mainTabla) {
        javax.swing.JComponent componente;
        int value = new Integer(mainTabla.get("ColSize"));
            if (value > 30) {
                // si la longitud es mas de 30 presentamos el boton para desplegar
                componente = new javax.swing.JTextArea("", 1, 30);
            } else {
                componente = new javax.swing.JTextArea("", 1, value);
            } // if (value > 30) {
        componente.setName(mainTabla.get("ColName"));
        ((javax.swing.JTextArea)componente).setLineWrap(true);
        ((javax.swing.JTextArea)componente).setBorder(
                new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
        return componente;
    }

    private javax.swing.JComponent 
        creaCheckBox(MainTabla mainTabla) {
        javax.swing.JComponent componente;
            componente = new javax.swing.JCheckBox();
            componente.setName(mainTabla.get("ColName"));
        return componente;
    }

    
    private void setTitle(String title) {
        this.title = title;
    }
    
    public String getTitle() {
        return title;
    }
    
    
    public javax.swing.JPanel getPanel() {
        return this;
    }
    
    private void pantallaTabla() {
        for (int i = 0; i < cliComponents.length && cliComponents[i] != null; i++) {
            if (cliComponents[i] instanceof javax.swing.JLabel) {}
            
            if (cliComponents[i] instanceof javax.swing.JTextField) {
                mainTabla.setValue(((javax.swing.JTextField)cliComponents[i]).getText(),
                mainTabla.getIndex(((javax.swing.JTextField)cliComponents[i]).getName()));
            }
            //           .
            //           .
            //           .
            //           .
            //           incluir los demas componentes que contengan datos
            //           .
            //           .
            //           .
            //
            
            if (cliComponents[i] instanceof javax.swing.JButton) {}
        }
        //       mainTabla.printTabla();
    }
    
    private void tablaPantalla() {
        for (int i = 0; i < cliComponents.length && cliComponents[i] != null; i++) {
            if (cliComponents[i] instanceof javax.swing.JLabel) {}
            
            if (cliComponents[i] instanceof javax.swing.JTextField) {
                ((javax.swing.JTextField)cliComponents[i]).setText(
                mainTabla.getValue(mainTabla.getIndex(
                ((javax.swing.JTextField)cliComponents[i]).getName())));
            }
            //           .
            //           .
            //           .
            //           .
            //           incluir los demas componentes que contengan datos
            //           .
            //           .
            //           .
            //
            if (cliComponents[i] instanceof javax.swing.JButton) {}
        }
        //       mainTabla.printTabla();
    }
    
    private String[][] tablaBD() {
        // va a tener 
        // dni 111 num
        // nb  111 alfa
        // ape 11  alfa
        String [][] tablaToBD = new String[500][3];
        mainTabla.setIndex(0);
        for (int i = 0; !mainTabla.EOF(); i++, mainTabla.next()) {
            tablaToBD[i][0] = mainTabla.getName();
            tablaToBD[i][1] = mainTabla.getValue();
            tablaToBD[i][2] = mainTabla.getType();
        }
        return tablaToBD;
    }
    
    private void setFocusFirstEdit() {
        //Establece el focus en el primer componente editable
        for (int i = 0; i < cliComponents.length && cliComponents[i] != null; i++) {
            if (cliComponents[i] instanceof javax.swing.JTextField) {
                //               cliComponents[i].grabFocus();
                cliComponents[i].requestFocus();
                i = 500000; //hacemos esto para salir del for una vez establecido el focus
            }
            //           .
            //           .
            //           .
            //           .
            //           incluir los demas componentes que contengan datos
            //           .
            //           .
            //           .
            //
            
        }
        
    }
    
    public static void main(java.lang.String[] args) {
        //       javax.swing.JTabbedPane TabbedPaneComenzar = new javax.swing.JTabbedPane();
        Utils.setJaviProperties();
        //        Utils.print(System.getProperty("app.app"));
        javax.swing.JFrame frame = new javax.swing.JFrame();
        javax.swing.JInternalFrame iframe = new javax.swing.JInternalFrame();
        frame.setSize(662, 423);
        frame.getContentPane().add(iframe);
        
        Mantenimiento clientes = new Mantenimiento("clientes");
        iframe.getContentPane().add(clientes);
        iframe.show();
//        frame.show();
        frame.setVisible(true);
        
        javax.swing.JFrame frame2 = new javax.swing.JFrame();
        frame2.setSize(662, 423);
        Mantenimiento clientes2 = new Mantenimiento("proveedores");
        frame2.getContentPane().add(clientes2);
        frame2.setVisible(true);
        
        javax.swing.JFrame frame3 = new javax.swing.JFrame();
        frame3.setSize(662, 423);
        Mantenimiento clientes3 = new Mantenimiento("camellos");
        frame3.getContentPane().add(clientes3);
        frame3.setVisible(true);
    }
    
    
}
