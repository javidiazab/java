/*
 * FileSys.java
 *
 * Created on 23 de abril de 2004, 23:06
 */
package App;
/**
 *
 * @author  Javito Corleone
 * lee el fichero .sys para cargar la configuracion
 *
 * Clase usada en BD y en Configurar para leer el fichero
 *
 */
public class FileSys {
    String file;
    
    /** Creates a new instance of FileSys */
    public FileSys(String file) throws java.io.IOException {
            this.file = file;
//           String[] tablaConfiguracion = read(file);
    }
    
    public FileSys() throws java.io.IOException {
        this.file = System.getProperty("app.fileConf"); 
//            String[] tablaConfiguracion = read("Tomas.sys");
    }
    
    public String[] read(String fileName) throws java.io.IOException {
            try {
//                File.separatorChar
//                String curDir = System.getProperty("user.dir");
//                System.out.println(curDir);
		StringBuffer sb = new StringBuffer();
		java.io.BufferedReader in =
                    new java.io.BufferedReader(new java.io.FileReader(fileName));
//                System.out.println(fileName);
		String[] tabla = new String[5000]; // <-------- Optimizar
                int i = 0;
		while((tabla[i] = in.readLine()) != null) {
//                    System.out.println(tabla[i]);
			sb.append(tabla[i]);
                        i++;
		}
		in.close();
                String [] tablaAux = new String [i-1];
                for (int index = 0; index < tablaAux.length; index++) {
                    tablaAux[index] = tabla[index];
                }
		return tablaAux;
            } catch (java.io.IOException exception) {
                exception.printStackTrace();
                return null;
            }
            

	}
   public String[] read() throws java.io.IOException {
            try {
//                File.separatorChar
//                String curDir = System.getProperty("user.dir");
//                System.out.println(curDir);
		StringBuffer sb = new StringBuffer();
		java.io.BufferedReader in =
                    new java.io.BufferedReader(new java.io.FileReader(file));
//                System.out.println(fileName);
		String[] tabla = new String[5000]; // <-------- Optimizar
                int i = 0;
		while((tabla[i] = in.readLine()) != null) {
//                    System.out.println(tabla[i]);
			sb.append(tabla[i]);
                        i++;
		}
		in.close();
                String [] tablaAux = new String [i+1];
                for (int index = 0; index < tablaAux.length; index++) {
                    tablaAux[index] = tabla[index];
                }
		return tablaAux;
            } catch (java.io.IOException exception) {
                exception.printStackTrace();
                return null;
            }
    	}
   
    
    public static void main (String[] args) {
        Utils.setJaviProperties();
                String [] tabla = null;
                try{
                    FileSys fileSys = new FileSys(System.getProperty("app.fileConf")); 
                    tabla = fileSys.read(System.getProperty("app.fileConf")); // <----- Optimizar
//                    + file.separator + "Javi" + file.separator + "Java" + file.separator + "pgms" + file.separator + "Tomas" + file.separator + "Tomas.sys");
//                    FileSys a = new FileSys();
//                    tabla = a.read("Tomas.sys");
                      for (int i = 0; i < tabla.length; i ++) {
                        System.out.println(tabla[i]);
                      }
		} catch (java.io.IOException exception){};
                    
	}
        
}