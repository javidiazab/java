/*
 * TableRows.java

 *
 * Created on 17 de septiembre de 2004, 23:40
 */

/**
 *
 * @author  Javito Corleone
 */
package App;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;


public class TableRows extends javax.swing.JDialog {
    private boolean DEBUG = false;
    private boolean ALLOW_COLUMN_SELECTION = false;
    private boolean ALLOW_ROW_SELECTION = true;
    private String[] columnNames;
    private Object[][] data;
//    public static int filaSeleccionada;
    public static int f;

    
    public TableRows(String[] columnNames, Object[][] data) {
        this.columnNames = columnNames;
        this.data = data;
        initialize();
    }
    
    public TableRows(javax.swing.JFrame frame, String[] columnNames, Object[][] data) {
        super(frame);
        this.columnNames = columnNames;
        this.data = data;
        initialize();
    }
    
    public void initialize() {
//        super("Panel de selecci�n.");
        setModal(true);
        setTitle("Panel de selecci�n");
        int cols = 0;
        int fils = 0;
        //vamos a saber cuantas filas y cols tienen las tablas que nos han pasado
        // para crear unas tablas solo con ese numero de fils y cols
        for (int i = 0; i < columnNames.length && columnNames[i] != null && columnNames[i] != ""
        // vamos a restringir las columnas a 5 solo
                    && i < 5; i++) cols = i + 1;
        String[] columnNamesAux = new String [cols];
        for (int i = 0; i < data.length && (data[i][0] != null && data[i][0] != ""); i++) fils = i+1;
        Object[][] dataAux = new Object [fils][cols];
        
        //ahora vamos a cargar nuestras tablas con solo los datos pasados ya sabiendo cuantos son
        for (int fil = 0; fil < fils; fil++) {
            for (int col = 0; col < cols; col++) {
                columnNamesAux[col] = columnNames[col];
                dataAux[fil][col] = data[fil][col];
                
            }
        }
        
        this.columnNames = columnNamesAux;
        this.data = dataAux;
        
        MyTableModel myModel = new MyTableModel();
               
        JTable table = new JTable(myModel);
        
        // vamos a establecer el tama�o de las columnas (No funciona, ellas mismas se rearrange
        javax.swing.table.TableColumn column = null;
        for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) { 
            column = table.getColumnModel().getColumn(i);
            column.setPreferredWidth(1000);
        }
            
        table.setPreferredScrollableViewportSize(new Dimension(600, 100));
        

        //Create the scroll pane and add the table to it. Lo vamos a crear que se pueda 
        // scroll verticalmente y horizontalmente, pero solo funciona el vertical. Para eso
        // tambien se puede pasar solo la tabla sin mas
        JScrollPane scrollPane = new JScrollPane(table,
            javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
            javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //Add the scroll pane to this window.
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Utils.setFil(-1);
                dispose();
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (java.awt.event.KeyEvent.VK_ESCAPE == evt.getKeyChar())
                Utils.setFil(-1);
                dispose();
            }
        });
        
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        if (ALLOW_ROW_SELECTION) { // true by default
            ListSelectionModel rowSM = table.getSelectionModel();
            rowSM.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    //Ignore extra messages.
                    if (e.getValueIsAdjusting()) return;
                    
                    ListSelectionModel lsm = (ListSelectionModel)e.getSource();

                    if (lsm.isSelectionEmpty()) {
                        System.out.println("Ninguna fila seleccionada.");
                    } else {
                        int selectedRow = lsm.getMinSelectionIndex();
                        Utils.setFil(selectedRow);
//                        filaSeleccionada = selectedRow;
                        dispose();
//                        System.out.println("Row " + selectedRow
//                                           + " is now selected.");
                    }
                }
            });
        } else {
            table.setRowSelectionAllowed(false);

        }

        if (ALLOW_COLUMN_SELECTION) { // false by default
            if (ALLOW_ROW_SELECTION) {
                //We allow both row and column selection, which
                //implies that we *really* want to allow individual
                //cell selection.
                table.setCellSelectionEnabled(true);
            } 
            table.setColumnSelectionAllowed(true);
            ListSelectionModel colSM =
                table.getColumnModel().getSelectionModel();

            colSM.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    //Ignore extra messages.
                    if (e.getValueIsAdjusting()) return;

                    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
                    if (lsm.isSelectionEmpty()) {
                        System.out.println("Ninguna columna seleccionada.");
                    } else {

                        int selectedCol = lsm.getMinSelectionIndex();
                        System.out.println("Column " + selectedCol
                                           + " is now selected.");
                    }
                }
            });
        }
           
        pack();
        Utils.centrame(this);
    }

    class MyTableModel extends AbstractTableModel {
//        final String[] columnNames = {"First Name", 
//                                      "Last Name",
//                                      "Sport",
//                                      "# of Years",
//                                      "Vegetarian"};
//        final Object[][] data = {
//            {"Mary", "Campione", 
//             "Snowboarding", new Integer(5), new Boolean(false)},
//            {"Alison", "Huml", 
//
//             "Rowing", new Integer(3), new Boolean(true)},
//            {"Kathy", "Walrath",
//             "Chasing toddlers", new Integer(2), new Boolean(false)},
//            {"Mark", "Andrews",
//             "Speed reading", new Integer(20), new Boolean(true)},
//            {"Angela", "Lih",
//             "Teaching high school", new Integer(4), new Boolean(false)}
//        };
        
//        TableColumn column = null;
//        for (int i = 0; i < 5; i++) { 
//            column = table.getColumnModel().getColumn(i);
//            column.setPreferredWidth(100);
//        }

        public int getColumnCount() {
            return columnNames.length;
        }
        

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),

         * rather than a check box.
         */
        public Class getColumnClass(int c) {
//            Utils.print("getColumnClass " + c);
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 2) { 

                return false;
            } else {
                return true;
            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col
                                   + " to " + value
                                   + " (an instance of " 

                                   + value.getClass() + ")");
            }

            if (data[0][col] instanceof Integer                        
                    && !(value instanceof Integer)) {                  
                //With JFC/Swing 1.1 and JDK 1.2, we need to create    
                //an Integer from the value; otherwise, the column     
                //switches to contain Strings.  Starting with v 1.3,   
                //the table automatically converts value to an Integer,

                //so you only need the code in the 'else' part of this 
                //'if' block.                                          
                //XXX: See TableEditDemo.java for a better solution!!!
                try {
                    data[row][col] = new Integer(value.toString());
                    fireTableCellUpdated(row, col);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(TableRows.this,

                        "The \"" + getColumnName(col)
                        + "\" column accepts only integer values.");
                }
            } else {
                data[row][col] = value;
                fireTableCellUpdated(row, col);
            }

            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData() {
            int numRows = getRowCount();

            int numCols = getColumnCount();

            for (int i=0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j=0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }

    public static void main(String[] args) {
        final String[] columnNames = {"First Name", 
                                      "Last Name",
                                      "Sport",
                                      "# of Years",
                                      "Vegetarian"};
        final Object[][] data = {
            {"Mary", "Campione", 
             "Snowboarding", new Integer(5), new Boolean(false)},
            {"Alison", "Huml", 

             "Rowing", new Integer(3), new Boolean(true)},
            {"Kathy", "Walrath",
             "Chasing toddlers", new Integer(2), new Boolean(false)},
            {"Mark", "Andrews",
             "Speed reading", new Integer(20), new Boolean(true)},
            {"Angela", "Lih",
             "Teaching high school", new Integer(4), new Boolean(false)}
        };
        TableRows frame = new TableRows(columnNames, data);
        frame.setVisible(true);
    }
}