/*
 * Utils.java
 *
 * Created on 6 de marzo de 2004, 0:03
 */
package App;
/**
 *
 * @author  Javito
 */
public class Utils {
    static int filaSeleccionada;
               
/**
 * FileJavi constructor comment.
 */
public Utils() {
	super();
}
/**
 * Insert the method's description here.
 * Creation date: (22/02/2004 23:42:36)
 */
public static void setFil(int fila) {
    filaSeleccionada = fila;
}
public static int getFil() {
    return filaSeleccionada;
}
public static void centrame(java.awt.Component me) {
//    frame.setLocationRelativeTo(null); //center it
    
	//java.awt.Insets insets = me.getInsets();
	//	me.setSize(me.getWidth() + insets.left + insets.right, me.getHeight() + insets.top + insets.bottom);
	//	me.setLocation((screenSize.width - me.width) / 2, (screenSize.height - splashScreenSize.height) / 2);
	//	setLocationRelativeTo

	/* Calculate the screen size */
	java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		
	java.awt.Dimension meSize = me.getSize();
	if (meSize.height > screenSize.height)
			meSize.height = screenSize.height;
	if (meSize.width > screenSize.width)
			meSize.width = screenSize.width;
	me.setLocation((screenSize.width - meSize.width) / 2, (screenSize.height - meSize.height) / 2);		
		
		
		
}
/**
 * Insert the method's description here.
 * Creation date: (22/02/2004 23:45:16)
 * @param me java.awt.Window
 * @param parent java.awt.Component
 */
public static void centrame(java.awt.Window me, java.awt.Component parent) {
    
    //    frame.setLocationRelativeTo(null); //center it
    

	/* Calculate the screen size */
	java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	
	java.awt.Dimension meSize = me.getSize();
	if (meSize.height > screenSize.height)
			meSize.height = screenSize.height;
	if (meSize.width > screenSize.width)
			meSize.width = screenSize.width;
	me.setLocation((screenSize.width - meSize.width) / 2, (screenSize.height - meSize.height) / 2);
//	me.setLocationRelativeTo(parent);		
	
	
}
/**
 * Insert the method's description here.
 * Creation date: (23/02/2004 22:26:02)
 * @param me java.awt.Component
 */
public static void colocame(java.awt.Component me) {
	/* Calculate the screen size */
	java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		
	java.awt.Dimension meSize = me.getSize();
	if (meSize.height > screenSize.height)
			meSize.height = screenSize.height;
	if (meSize.width > screenSize.width)
			meSize.width = screenSize.width;

	//implement logic to get the point in screen and see if it exceeds the screen limits
	
	
	
}

public static void dialog(java.awt.Container ventana, String txt) {
    boolean salir = false;
    
    java.awt.Container container = ventana;
    for (int i = 0; i < 10 && salir == false; i++) {
        if (container instanceof java.awt.Frame) {
            javax.swing.JOptionPane.showMessageDialog(((java.awt.Frame)container), 
                txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            salir = true;
        }
        else if (container instanceof java.awt.Dialog) {
            javax.swing.JOptionPane.showMessageDialog(((java.awt.Dialog)container), 
                txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            salir = true;    
        }
        container = container.getParent();
    }
    if (salir == true) return;
    else javax.swing.JOptionPane.showMessageDialog(null, 
            txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
}

public static void dialog(String txt) {
    javax.swing.JOptionPane.showMessageDialog(null, 
            txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
}

public static void setIcono(javax.swing.JFrame frame) {
//    frame.setFrameIcon(new javax.swing.ImageIcon("error.gif"));
    frame.setIconImage(new javax.swing.ImageIcon(Configurar.get("icono")).getImage()); 
}

public static void setIcono(javax.swing.JFrame frame, String icono) {
    frame.setIconImage(new javax.swing.ImageIcon(icono).getImage()); 
}

public static void setIcono(javax.swing.JFrame frame, java.awt.Image icono) {
    frame.setIconImage(new javax.swing.ImageIcon(icono).getImage()); 
}

public static boolean isIn(String cadena1,String cadena2) {
//    frame.setFrameIcon(new javax.swing.ImageIcon("error.gif"));
    if (cadena2.indexOf(cadena1) > 0) return true;
    else return false;
}



/**
 * Insert the method's description here.
 * Creation date: (22/02/2004 19:28:57)
 * @return java.lang.String
 */
public static String getExtension(java.io.File file) {
	/*
     * Get the extension of a file.
     */
        String ext = null;
        String s = file.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
}
/**
 * Starts the application.
 * @param args an array of command-line arguments
 */


public static void print(java.lang.String cadena) {
       System.out.println(cadena);
}

public static void print(int i) {
       System.out.println(i);
}

public static void setJaviProperties() {
    try {
       java.io.FileInputStream propFile = new java.io.FileInputStream("app.cfg");
       java.util.Properties p = new java.util.Properties(System.getProperties());
       p.load(propFile);
       // set the system properties
       System.setProperties(p);
       // display new properties
//       System.getProperties().list(System.out);
    } catch (Exception e) {
        e.printStackTrace();
    }
}

public static void ejecucionComando(String comando) {
    try {
     Process process = Runtime.getRuntime().exec(comando);
     //esto si queremos ver el resultado de lo que devuelve el comando de ejecucion
     java.io.InputStream inputstream = process.getInputStream();
     java.io.BufferedInputStream bufferedinputstream = new java.io.BufferedInputStream(inputstream);
     } catch (Exception e){
         e.printStackTrace();
     }
}

public static void copyFile(String fileFrom, String fileTo) {
       try {
//        String output;
        String comando = "cmd /c copy \"" + fileFrom + "\" \"" + fileTo + "\"";
//        comando.replace('\', '\');
        Process process = Runtime.getRuntime().exec(comando);
//        java.io.BufferedReader cmd_input = 
//        new java.io.BufferedReader(new java.io.InputStreamReader(process.getInputStream()));
//        while ((output = cmd_input.readLine()) != null) {
//            System.out.println(output);
//        }
//        cmd_input.close();
        } catch (Exception e){
            e.printStackTrace();
        }
}

public static java.awt.Window getParentWindow(java.awt.Component component) {
       java.awt.Container container = component.getParent();
       while (!(container instanceof java.lang.Object)) {
            if (container instanceof java.awt.Window) {
                return ((java.awt.Window)container);
                
            }
            container = container.getParent();
       }
       return null;
}

public static java.awt.Container getParentContainer(java.awt.Component component) {
       java.awt.Container container = component.getParent();
       while (!(container instanceof java.lang.Object)) {
            if (container instanceof java.awt.Container) {
                return ((java.awt.Container)container);
                
            }
            container = container.getParent();
       }
       return null;
}
        
public static javax.swing.JFrame getParentJFrame(java.awt.Component component) {
       java.awt.Container container = component.getParent();
       while (!(container instanceof java.lang.Object)) {
            if (container instanceof javax.swing.JFrame) {
                return ((javax.swing.JFrame)container);
                
            }
            container = container.getParent();
       }
       return null;
}

public static javax.swing.JInternalFrame getParentJInternalFrame(java.awt.Component component) {
       java.awt.Container container = component.getParent();
       while (!(container instanceof java.lang.Object)) {
            if (container instanceof javax.swing.JInternalFrame) {
                return ((javax.swing.JInternalFrame)container);
                
            }
            container = container.getParent();
       }
       return null;
}



public static void main(java.lang.String[] args) {
//     Utils.copyFile("c:\\mierda.txt","c:\\mierda3.txt");
//     Utils.copyFile("c:\\1\\1 1\\1\\Tomas.mdb","C:\\1\\1 1\\1\\mierda 5.mdb");
//     Utils.copyFile("C:\\Archivos de programa\\NetBeans3.6\\Tomas.mdb", 
//     "C:\\Archivos de programa\\NetBeans3.6\\Copia de seguridad de BD Tomas 2004-5-6.mdb");
//    Utils.copyFile("c:" + System.getProperty("file.separator") + "mierda.txt",
//        "c:" + System.getProperty("file.separator") + "mierda4.txt");
	// Insert code to start the application here.
        Utils.setJaviProperties();
}


}

