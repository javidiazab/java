/*
 * JaviConfiguraPanel.java
 *
 * Created on 28 de marzo de 2004, 17:59
 */
package App;
/**
 *
 * @author  Javito
 */
public class JaviConfiguraPanel {
    private javax.swing.JPanel panel = null;
    private javax.swing.JPanel ivjPanelCentral = null;
    private javax.swing.JPanel ivjPanelEste = null;
    private javax.swing.JPanel ivjPanelNorte = null;
    private javax.swing.JPanel ivjPanelOeste = null;
    private javax.swing.JPanel ivjPanelSur = null;
    private javax.swing.JPanel ivjPanelMsg = null;
    private javax.swing.JPanel ivjPanelBotones = null;
    private java.awt.Component lastComponent = null;
    private javax.swing.JScrollPane scrollPane = null;

    
    /** Creates a new instance of JaviConfiguraPanel */
    public JaviConfiguraPanel(javax.swing.JPanel panel) {
        this.panel = panel;
        initialize();
    }
   
    
    
    public javax.swing.JPanel getPanel(String panel) {
	if (panel.equals("Norte")) return ivjPanelNorte;
	if (panel.equals("Sur")) return ivjPanelSur;
	if (panel.equals("Este")) return ivjPanelEste;
	if (panel.equals("Oeste")) return ivjPanelOeste;
	if (panel.equals("Central")) return ivjPanelCentral;
	if (panel.equals("Botones")) return ivjPanelBotones;
	if (panel.equals("Msg")) return ivjPanelMsg;
	return null;
    }

    
    private javax.swing.JPanel getPanelBotones() {
	if (ivjPanelBotones == null) {
		try {
			ivjPanelBotones = new javax.swing.JPanel();
			ivjPanelBotones.setName("PanelBotones");
			ivjPanelBotones.setLayout(new java.awt.FlowLayout());
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
	}
	return ivjPanelBotones;
    }

    private javax.swing.JPanel getPanelCentral() {
	if (ivjPanelCentral == null) {
		try {
			ivjPanelCentral = new javax.swing.JPanel();
			ivjPanelCentral.setName("PanelCentral");
			ivjPanelCentral.setLayout(new java.awt.FlowLayout());
//                         cargarListeners();
                        ivjPanelCentral.setPreferredSize(new java.awt.Dimension(10,10)); // estos no tienen utilidad
                        cargarListeners();
                 } catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}       
	}
	return ivjPanelCentral;
    }
    
    
    private javax.swing.JPanel getPanelEste() {
	if (ivjPanelEste == null) {
		try {
			ivjPanelEste = new javax.swing.JPanel();
			ivjPanelEste.setName("PanelEste");
			ivjPanelEste.setLayout(new VerticalBagLayout());
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
	}
	return ivjPanelEste;
    }
    
    private void cargarListeners() {
        try {
             ivjPanelCentral.addComponentListener(new java.awt.event.ComponentAdapter() {
                    public void componentResized(java.awt.event.ComponentEvent evt) {
                        if (lastComponent != null) {
                            ivjPanelCentral.setPreferredSize(new java.awt.Dimension(10,
                                lastComponent.getY() + lastComponent.getHeight() + 5));
                            Utils.print("x " + lastComponent.getY());
//                            scrollPane.validate();
//                            ivjPanelCentral.repaint();
                        }
                    }
            });
                        
            ivjPanelCentral.addContainerListener(new java.awt.event.ContainerAdapter() {
                public void componentAdded(java.awt.event.ContainerEvent evt) {
                    java.awt.Component [] components = evt.getContainer().getComponents();
                    lastComponent = components[components.length - 1];
                     Utils.print("y " + lastComponent.getY());
                    ivjPanelCentral.validate();
                    scrollPane.revalidate();
                }
            });
	} catch (java.lang.Throwable ivjExc) {
                handleException(ivjExc);
        }
    }
    
    public void mierda () {
        Utils.print("en resize 2");
        Utils.print(lastComponent.getY());
        ivjPanelCentral.setPreferredSize(new java.awt.Dimension(10,10));
        ivjPanelCentral.setPreferredSize(new java.awt.Dimension(10,
                                lastComponent.getY() + lastComponent.getHeight() + 5));
    }
    
    private javax.swing.JPanel getPanelMsg() {
	if (ivjPanelMsg== null) {
		try {
			ivjPanelMsg = new javax.swing.JPanel();
			ivjPanelMsg.setName("PanelMsg");
			ivjPanelMsg.setLayout(new java.awt.FlowLayout());
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
	}
	return ivjPanelMsg;
    }

    
    private javax.swing.JPanel getPanelNorte() {
	if (ivjPanelNorte == null) {
		try {
			ivjPanelNorte = new javax.swing.JPanel();
			ivjPanelNorte.setName("PanelNorte");
			ivjPanelNorte.setLayout(new java.awt.FlowLayout());
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
	}
	return ivjPanelNorte;
    }

    private javax.swing.JPanel getPanelOeste() {
	if (ivjPanelOeste == null) {
		try {
			ivjPanelOeste = new javax.swing.JPanel();
			ivjPanelOeste.setName("PanelOeste");
			ivjPanelOeste.setLayout(new VerticalBagLayout());
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
	}
	return ivjPanelOeste;
    }

    private javax.swing.JPanel getPanelSur() {
	if (ivjPanelSur == null) {
		try {
			ivjPanelSur = new javax.swing.JPanel();
			ivjPanelSur.setName("PanelSur");
			ivjPanelSur.setLayout(new VerticalBagLayout());
			ivjPanelSur.add(getPanelBotones());
			ivjPanelSur.add(getPanelMsg());
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
	}
	return ivjPanelSur;
    }


    private void handleException(java.lang.Throwable exception) {

	/* Uncomment the following lines to print uncaught exceptions to stdout */
	// System.out.println("--------- UNCAUGHT EXCEPTION ---------");
	// exception.printStackTrace(System.out);
    }

    private void initialize() {
	try {
		panel.setLayout(new java.awt.BorderLayout());
		panel.setSize(600, 400);
		panel.add(getPanelNorte(), "North");
		panel.add(getPanelEste(), "East");
		panel.add(getPanelOeste(), "West");
                //ADD SCROLLPANE ********************************************************************
                scrollPane = new javax.swing.JScrollPane(getPanelCentral(),
                        javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
                        javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                panel.add(scrollPane, "Center");
                //***************************************************************************************
		panel.add(getPanelSur(), "South");
	} catch (java.lang.Throwable ivjExc) {
		handleException(ivjExc);
	}
    }
    
    public static void main(java.lang.String[] args) {
    	javax.swing.JFrame frame = new javax.swing.JFrame();
       frame.setSize(662, 423);
       javax.swing.JPanel panel = new javax.swing.JPanel();
//       Comenzar aComenzar = new Comenzar("Nuevo Comenzar",true,true,true,true);
//       frame.getContentPane().add(aComenzar);
       JaviConfiguraPanel aJaviConf = new JaviConfiguraPanel(panel);
       frame.getContentPane().add(panel);
       
       aJaviConf.getPanel("Botones").add(new javax.swing.JButton("Botones Altas"));
       aJaviConf.getPanel("Botones").add(new javax.swing.JButton("Botones Bajas"));
       aJaviConf.getPanel("Norte").add(new javax.swing.JButton("Norte"));
       aJaviConf.getPanel("Este").add(new javax.swing.JButton("Este"));
       aJaviConf.getPanel("Oeste").add(new javax.swing.JButton("Oeste"));
       for (int i = 0; i < 60; i++) 
           aJaviConf.getPanel("Central").add(new javax.swing.JButton("Central" + i));
       aJaviConf.getPanel("Msg").add(new javax.swing.JLabel("Mensaje"));
//       frame.show();
//       aJaviConf.mierda();
       frame.setVisible(true);
    }
}
