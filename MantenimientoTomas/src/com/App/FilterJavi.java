/*
 * FilterJavi.java
 *
 * Created on 6 de marzo de 2004, 0:05
 */

/**
 *
 * @author  Javito
 */
package App;
class FilterJavi extends javax.swing.filechooser.FileFilter{
    private String [] extAFiltrar = new String [10];
    private String descripcion;
    private int extInd = 0;
    
    public FilterJavi() {
        super();
	this.extAFiltrar = extAFiltrar;
    }
    
    public boolean accept(java.io.File file) {
        if (file.isDirectory()) {
            return true;
        }
        String extension = Utils.getExtension(file);
        int i;
        for (i = 0; i < extInd; i++){
                if (extension != null) {
                    if (extension.equals(extAFiltrar[i])) return true;
                }
                return false;
        }
        return false;
    }
    
    public void add(String ext) {
        extAFiltrar[extInd] = ext;
        extInd++;
    }
    
    public String getDescription() {
        return descripcion;
    }
    
    public void setDescription(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    public static void main(java.lang.String[] args) {
	// Insert code to start the application here.
    }
}

