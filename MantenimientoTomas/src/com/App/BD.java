/*
 * BD.java
 *
 * Created on 19 de mayo de 2004, 23:42
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
public class BD {
    String bdNb = "";
    String tablaNb = "";
    
    DefCol col = new DefCol();
    
    int index = 0;
    java.sql.ResultSet result = null;
    boolean EOF = false;
    boolean update = false;
    
    java.sql.Statement stmt = null;
    java.sql.Connection con = null;
    
    String fileConfig;
    String [][] tablaMeta = new String[200][6];
    String [] tablaRow = new String[100];
    MainTabla mainTabla = new MainTabla();;
    
      /** Creates a new instance of BD */
    public BD() {
        this.update = true;
        setBdNb(System.getProperty("app.app"));
        conectar(getBdNb());
    }
    
    
    /** mierda   */
    public BD(String bdNb) {
        setBdNb(bdNb);
        this.update = true;
        conectar(getBdNb());
    }
     
    public BD(boolean update) {
        setBdNb(System.getProperty("app.app"));
        this.update = update; 
        conectar(getBdNb());
    }
    
    public BD(String bdNb, boolean update) {
        setBdNb(bdNb);
        this.update = update; 
        conectar(getBdNb());
    }
    
     public BD(String bdNb, String tablaNb) {
        setBdNb(bdNb);
        setTablaNb(tablaNb);
        this.update = true;
        conectar(getBdNb());
        cargarTabla(getTablaNb());
    }
     
     public BD(String bdNb, String tablaNb, boolean update) {
        setBdNb(bdNb);
        setTablaNb(tablaNb);
        this.update = update; 
        conectar(getBdNb());
        cargarTabla(tablaNb);
    }
   
      
    public void next(){
        if (EOF()) return;
        if (result == null ) {
            setEOF(true);
            return;
        }
        try {
            result.next();
            if (result.isAfterLast()) setEOF(true);
            else setEOF(false);
            
        } catch (Exception e) {
                e.printStackTrace();
        }
        
//        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
////             if (index > tablaLoad.length || index < 0) {
//            setEOF(true);
//        }
//        
//        index++;
    }
    
    public String get(String nbCol){
        try {
            result.getString(nbCol);
            if(result.wasNull()) return "";
            else return result.getString(nbCol);
        } catch (Exception e) {
                e.printStackTrace();
                return "";
        }
    }
    
    public String get(int nbCol){
        try {
            result.getString(nbCol);
            if(result.wasNull()) return "";
            else return result.getString(nbCol);
        } catch (Exception e) {
                e.printStackTrace();
                return "";
        }   
    }
    
    
    /** Cuenta las filas de la tabla que se le ha pasado */
//    public int count(){
//        if (isTablaNbBlank()) {
//            setEOF(true);
//            return 0;
//        }
//        int i = 0;
//        for (; i < tablaLoad.length && tablaLoad[i][0] != null; i++);
//        return i-1;
//    }
    
    
     /** Devuelve el estado del EOF */
     public boolean EOF(){
        return EOF;
    }
     
    private void setEOF(boolean eof) {
        this.EOF = eof;
    }
     
    public void setIndex(int index){
        if (isTablaNbBlank()) {return;}
            
//        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0); 
//        this.index = index;
        try {
            result.absolute(index);
        } catch (Exception e) {
                e.printStackTrace();
        }
    }
    
    public void execute(String sql) {
        if (sql != "") {
           try {
                result = stmt.executeQuery(sql);
//                result.first(); // also result.absolute(1); 
//                Utils.print(result.getString(1));
                if (count() == 0) setEOF(true);
                else setEOF(false);
//                for (int a = 0; a < tablaMeta.length; a++) {
//                    for (int j = 1; j < tablaMeta[a].length && tablaLoad[a][j] != null; j++) {
//                        System.out.println(tablaMeta[a][j] + " ");
//                     }
//                }
           } catch (Exception e) {
                e.printStackTrace();
           }
        }
    }
    
    public int count() {
        try {
            int rowPos = result.getRow();
                result.last();
                int count = result.getRow();
                if (rowPos < 1) result.first();
                else result.absolute(rowPos);
                return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    
   //* Select sobre la tabla establecida previamente */
    public void select() {
        if (isTablaNbBlank()) return;
         try {
            result = stmt.executeQuery(
                "SELECT * FROM " + tablaNb + ";");
            result.first(); // also result.absolute(1); 
            Utils.print(result.getString(1));
            Utils.print(result.getString(2));
//                for (int a = 0; a < tablaMeta.length; a++) {
//                    for (int j = 1; j < tablaMeta[a].length && tablaLoad[a][j] != null; j++) {
//                        System.out.println(tablaMeta[a][j] + " "); }}
        }
        catch (Exception e) {
          e.printStackTrace();
        }
  }
   
    
    /** Select que devuelve una tabla con los campos de la primera fila */
    public String[] select(String select) {
        // inicializamos la tabla que vamos a devolver antes de cargarla
        for (int i = 0; i < tablaRow.length; i++) tablaRow[i] = null;
        
        try {
            result = stmt.executeQuery(select);
//            result.first(); // also result.absolute(1); 
//            for (int i = 0; i < tablaRow.length && result.next() == true); i++) {
                result.next(); 
                setIndex(1);
//            for i
//            tabla[i] = result.getString(i+1);
                try {
                    for (int i = 0; ;i++) {
                        tablaRow[i] = result.getString(i+1);
                    }
                } catch (Exception e) {
                    //va a salir por aqui siempre porque recorremos las 
                    // columnas hasta que ya no hay mas, y tenemos una sql excepcion
                    if (tablaRow[0] != null) {
//                        for (int i = 0; i < tablaRow.length && tablaRow[i] != null; i++) {
//                           System.out.println(tablaRow[i]);
//                        }
                        return tablaRow;
                    }
                }
//                for (int i = 0; i < tablaRow.length && tablaRow[i] != null; i++) {
//                    System.out.println(tablaRow[i]);
//                }
                
            return tablaRow;
        }
        catch (java.sql.SQLException e) {
          e.printStackTrace();
          return null;
        }
  }
    
    
    public void update (String [] tabla, String tablaDB) {
//        if (result != null)
        try {
        java.sql.ResultSet result = stmt.executeQuery(
                "SELECT * FROM " + tablaDB + ";");
          result.next(); 
          for (int i = 0; tabla[i] !=  null; i++) {
            result.updateString(i + 1, tabla[i]);
            result.updateRow(); 
          }
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
    
     public void update (String [][] tablaToBD) {
        try {            
            for (int i = 0; tablaToBD[i][0] != "" && tablaToBD[i][0] != null; i++) {
                Utils.print(i);
                 Utils.print(tablaToBD[i][0]);
                 Utils.print(tablaToBD[i][1]);
//                
//              Hacer algo para que se controle el typo de dato  
                if (tablaToBD[i][2].equals("VARCHAR")) // || tablaToBD[i][2].equals("LONGCHAR")) 
                    result.updateString(tablaToBD[i][0], tablaToBD[i][1]); 
                else result.updateNull(tablaToBD[i][0]); 
                 
//                if (tablaToBD[i][2] == "DATETIME") 
//                    result.updateDate(tablaToBD[i][0], tablaToBD[i][1]); 
                 
            }
//           
//           result.updateString(1, "1"); // updates the 
//           result.updateString(2, "2"); // updates the 
//           result.updateString(3, "3"); // updates the 
//           result.updateString(4, "4"); // updates the 
//           result.updateString(5, "5"); // updates the 
//           result.updateString(6, "6"); // updates the 

           result.updateRow();
//           result.moveToCurrentRow();
        } catch(java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void insert (String [][] tablaToBD) {
        try {            
            result.moveToInsertRow(); // moves cursor to the insert row
            for (int i = 0; tablaToBD[i][0] != "" && tablaToBD[i][0] != null; i++) {
//                Utils.print(i);
//                 Utils.print(tablaToBD[i][0]);
//                 Utils.print(tablaToBD[i][1]);
//                
//              Hacer algo para que se controle el typo de dato  
                if (tablaToBD[i][2].equals("VARCHAR")) // || tablaToBD[i][2].equals("LONGCHAR")) 
                    result.updateString(tablaToBD[i][0], tablaToBD[i][1]); 
                else result.updateNull(tablaToBD[i][0]); 
//                if (tablaToBD[i][2] == "DATETIME") 
//                    result.updateDate(tablaToBD[i][0], tablaToBD[i][1]); 
            }
//           result.updateString(1, "1"); // updates the 
//           result.updateString(2, "2"); // updates the 
//           result.updateString(3, "3"); // updates the 
//           result.updateString(4, "4"); // updates the 
//           result.updateString(5, "5"); // updates the 
//           result.updateString(6, "6"); // updates the 
           result.insertRow();
//           result.moveToCurrentRow();
        } catch(java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
    
     public void delete () {
        try {
            result.deleteRow();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
     
             
    public void cargarTabla(String tabACargar){ 
//      Carga tablaLoad (gestionada en Maintabla) con ColumName, ColumLabel, Type, Size etc
//        en working: Los primeros datos desde la BD y los segundos datos
//        desde un archivo de texto si existe y si no desde la BD
//        tabACargar = tabACargar + System.getProperty("app.tableExtension");
//      Si esto fallara se intenta carga desde la misma BD
//       Utils.print(System.getProperty("user.dir"));
        cargarMainTabla(tabACargar);
        String extTbl = System.getProperty("app.tableExtension");
        if ((new java.io.File(tabACargar + extTbl)).exists())
            cargarTablaMasDesdeTbl(tabACargar);
        else
            cargarTblDesdeTabla(tabACargar);
    }

    
    private void cargarTablaMasDesdeTbl(String tabACargar) {
//  Se van a coger los segundos datos desde el fichero tbl    
        String currentFld = "";
        int index = 0;
        int begining = 0;
        int begining2 = 0;
      try{
        String extTbl = System.getProperty("app.tableExtension");
        FileSys fileTbl = new FileSys(tabACargar + extTbl);
        String [] tablaAux = fileTbl.read(); // <----- Optimizar

        tablaAux = ordenarRecords(tablaAux);

        mainTabla.setIndex(0);
//        mainTabla.printTabla();
        if (tablaAux[index].indexOf("ColName") > -1) {
            begining = tablaAux[index].indexOf("=");
            currentFld = tablaAux[index].substring(begining + 1).trim();
        }
        for (; !mainTabla.EOF(); mainTabla.next()) {
            
            while (mainTabla.get("ColName").equals(currentFld) && 
                   index < tablaAux.length && 
                   tablaAux[index] != null && tablaAux[index] != "") {
                begining = tablaAux[index].indexOf("=");      
                if (tablaAux[index].indexOf("ColEnPantalla") > -1) 
		    mainTabla.setEnPantalla(tablaAux[index].substring(begining + 1).trim());
                if (tablaAux[index].indexOf("ColTypeSwing") > -1) 
                    mainTabla.setTypeSwing(tablaAux[index].substring(begining + 1).trim());
                if (tablaAux[index].indexOf("ColLabelYN") > -1) 
                    mainTabla.setLabelYN(tablaAux[index].substring(begining + 1).trim());
                if (tablaAux[index].indexOf("ColFichero") > -1) 
                    mainTabla.setFichero(tablaAux[index].substring(begining + 1).trim());
                if (tablaAux[index].indexOf("ColTexto") > -1) 
                    mainTabla.setTexto(tablaAux[index].substring(begining + 1).trim());
                if (tablaAux[index].indexOf("ColMascara") > -1) 
                    mainTabla.setMascara(tablaAux[index].substring(begining + 1).trim());
                index++;
                if (tablaAux[index].indexOf("ColName") > -1) {
                    begining2 = tablaAux[index].indexOf("=");
                    currentFld = tablaAux[index].substring(begining2 + 1).trim();
                }
            } //while
        } // (int i = 0; !mainTabla.EOF(); mainTabla.next()) {
//        mainTabla.printTabla();
      } catch (Exception e) {e.printStackTrace();} 
        
//        for (int i = 0; i < tablaAux.length && tablaAux[i] != null; i ++) {
//            setEOF(false);
////            Utils.print(tablaAux[i]);
//           if (tablaAux[i].indexOf("ColEnPantalla") > -1) 
//                setMainTablaConDatosRecord(tablaAux[i], col.getIndexEnPantalla());
//            if (tablaAux[i].indexOf("ColTypeSwing") > -1) 
//                setMainTablaConDatosRecord(tablaAux[i], col.getIndexTypeSwing());
//            if (tablaAux[i].indexOf("ColLabelYN") > -1) 
//                setMainTablaConDatosRecord(tablaAux[i], col.getIndexLabelYN());
//            if (tablaAux[i].indexOf("ColFichero") > -1) 
//                setMainTablaConDatosRecord(tablaAux[i], col.getIndexFichero());
//            if (tablaAux[i].indexOf("ColTexto") > -1) 
//                setMainTablaConDatosRecord(tablaAux[i], col.getIndexTexto());
//            if (tablaAux[i].indexOf("ColMascara") > -1) 
//                setMainTablaConDatosRecord(tablaAux[i], col.getIndexMascara());
    
        // Visualiza los valores cargados
//                for (int a = 0; a < tablaLoad.length; a++) {
//                    for (int j = 0; j < tablaLoad[a].length && tablaLoad[a][0] != null; j++) {
//                        System.out.println(tablaLoad[a][j] + " ");
//                        
//                     }
//                }
//                if (index != -1) return tabla[i];  // al salir del for ya le a�ade 1
//                else return null;
    }
    
    private void cargarTblDesdeTabla(String tabACargar) {
        //Crea el fichero de la tabla para que quede la info para otra vez
        mainTabla.creaTbl(tabACargar);
    }
                    
    private void setMainTablaConDatosRecord(String record, int col) {
    // formato del record leido --> ColName[1]=dni
        int begining = record.indexOf("[") + 1;
        int end = record.indexOf("]");
       //Convierte de String a Integer
       //Se podria convertir de String a int usando parseInt(string,10); 
        Integer row = new Integer(record.substring(begining,end));
        //Convierte de Integer a int
        //tablaLoad[row.intValue() - 1][col.getIndexName()] = tablaAux[i].substring(endLits + 1).trim();
        mainTabla.set(record.substring(end + 2).trim(), row.intValue() - 1, col);
    }
    
    private String[] ordenarRecords (String [] tablaAux) {
        //Vamos a ordenar los Records del fichero Tbl para que se permita
        //incluir nuevas columnas en la tabla de access sin que se joda la app.
        String [][] RecordsOrdenados = new String[1000][30];
        int row; 
        for (int i = 0; i < tablaAux.length && tablaAux[i] != null; i++) {
            if (!tablaAux[i].equals("")) {
                int begining = tablaAux[i].indexOf("[") + 1;
                int endLits = tablaAux[i].indexOf("]");
               //Convierte de String a Integer. El siguiente a Int
    //          Integer row = new Integer(tablaAux[i].substring(begining,endLits));
                row = Integer.parseInt(tablaAux[i].substring(begining,endLits));
//                Vamos a poner en una tabla los registros ordenados.
//                En la misma fila de la tabla van a estar todos los records de un mismo campo
//                En la primera col de cada fila va a estar el ColName y en
//                las demas columnas los demas (como enpantalla, etc) sin ordenar
                if (tablaAux[i].indexOf("ColName") > -1) {
                    RecordsOrdenados[row - 1][0] = tablaAux[i];
                } else {
                    boolean enough = false;
                    for (int j = 1; j < RecordsOrdenados[row - 1].length && enough != true; j++) {
                        if (RecordsOrdenados[row - 1][j] == null || 
                            RecordsOrdenados[row - 1][j] == "") {
                            RecordsOrdenados[row - 1][j] = tablaAux[i];
                            enough = true;
                        }
                    }
                } //if (tablaAux[i].indexOf("ColName") > -1) {
            } //if (!tablaAux[i].equals("")) {
        } //for (int i = 0; i < tablaAux.length && tablaAux[i] != null; i++) {
        //Vamos a inic ializar la tabla para cargarla con datos ordenados
        for (int i = 0; i < tablaAux.length; i++) tablaAux[i] = "";
        int index = 0;
        for (int i = 0; i < RecordsOrdenados.length && RecordsOrdenados[i][0] != null; i++) {
            for (int j = 0; j < RecordsOrdenados[i].length && 
                                RecordsOrdenados[i][j] != null &&
                                RecordsOrdenados[i][j] != ""; j++) {
               tablaAux[index] = RecordsOrdenados[i][j];
               index++;
            }
        }
        return tablaAux;
    }
    
//    public void creaTabla(String tabla){
////      Crear tablas de access desde la tabla en working cargada previamente en cagarTabla desde un fichero
//        try {
////          Formato: String sql = "CREATE TABLE mierda2(mierda1 VARCHAR(254),mierda2 VARCHAR(254))";
//            StringBuffer buf = new StringBuffer("CREATE TABLE " + tabla + "(");
////            String sql = "CREATE TABLE " + tabla + "(";
//            for (int fila = 0; fila < tablaLoad.length; fila++) {
//                 if (tablaLoad[fila][col.getIndexName()] != null) {
//                        if (fila != 0 ) buf.append(",");
////                        if (fila != 0 ) sql = sql + ",";
//                        buf.append(tablaLoad[fila][col.getIndexName()].trim());
//                        buf.append(" ");
//                        buf.append(tablaLoad[fila][col.getIndexType()].trim());
////                        sql = sql + tablaLoad[fila][colName].trim() + " " + tablaLoad[fila][colType].trim();
////                      Ojo negacion. No queremos especificar longitud con estos campos
//                        if (!tablaLoad[fila][col.getIndexType()].trim().equalsIgnoreCase("LONGCHAR") &&
//                            !tablaLoad[fila][col.getIndexType()].trim().equalsIgnoreCase("DATETIME") &&
//                            !tablaLoad[fila][col.getIndexType()].trim().equalsIgnoreCase("BIT")) {
//                            buf.append("(");
//                            buf.append(tablaLoad[fila][col.getIndexSize()].trim());
//                            buf.append(")");
////                            sql = sql + "(" + tablaLoad[fila][colSize].trim() + ")";
//                        }      
//                 }       
//            }
//            buf.append(")");
////            sql = sql + ")";
//
//            
//            stmt.executeUpdate(buf.toString());
//////            stmt.executeUpdate(sql);
//            
//            } catch (java.sql.SQLException e) { e.printStackTrace();}
//    }
    
    public void conectar (String bDNb){
        String URL = "jdbc:odbc:" + bDNb;
        String username = "";
        String password = "";
        
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        } catch (Exception e) {
            System.out.println("Failed to load JDBC/ODBC driver.");
            return;
        }
   
        try {
            con = java.sql.DriverManager.getConnection (
                URL,
                username,
                password);
            if (update == true) 
                    stmt = con.createStatement
                    (java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE, 
                     java.sql.ResultSet.CONCUR_UPDATABLE); 
            else stmt = con.createStatement();
        
//            System.out.println("Todo bien");
        } catch (Exception e) {
            System.err.println("problemas connectando a " + URL);
        }
    }
    
    private void getDatosMetaTabla (String tabla){
//      Carga tablaMeta con ColumnName, Label, Type, Size etc en working desde una tabla de access
        try {
//            java.sql.ResultSet result = stmt.executeQuery(
            result = stmt.executeQuery(
                "SELECT * FROM " + tabla + ";");
            java.sql.ResultSetMetaData meta = result.getMetaData();

            int columns = meta.getColumnCount();
            for (int i = 0; i < columns; i++) {
                tablaMeta[i][col.getIndexName()] = meta.getColumnName(i+1);
                tablaMeta[i][col.getIndexLabel()] = meta.getColumnLabel(i+1);
                tablaMeta[i][col.getIndexType()] = meta.getColumnTypeName(i+1);
                // para convertir un numero a string
                tablaMeta[i][col.getIndexSize()] = String.valueOf(meta.getColumnDisplaySize(i+1));
                tablaMeta[i][col.getIndexPrecision()] = String.valueOf(meta.getPrecision(i+1));
            }
//                for (int a = 0; a < tablaMeta.length; a++) {
//                    for (int j = 1; j < tablaMeta[a].length && tablaLoad[a][j] != null; j++) {
//                        System.out.println(tablaMeta[a][j] + " "); }}
        }
        catch (Exception e) {
          e.printStackTrace();
        }
    }
    
    private void cargarMainTabla (String tabla){
//      Carga tablaLoad y fichero con CoulmName, Label, Type, Size etc 
//      en working desde una tabla de access
        try {
             result = stmt.executeQuery(
                "SELECT * FROM " + tabla + ";");
            java.sql.ResultSetMetaData meta = result.getMetaData();
            setEOF(false);
            int columns = meta.getColumnCount();
            for (int i = 0; i < columns; i++) {
                mainTabla.set(meta.getColumnName(i+1), i, col.getIndexName());
                mainTabla.set(meta.getColumnLabel(i+1), i, col.getIndexLabel());
                mainTabla.set(meta.getColumnTypeName(i+1), i, col.getIndexType());
                mainTabla.set(String.valueOf(meta.getColumnDisplaySize(i+1)), i, col.getIndexSize());
                mainTabla.set(String.valueOf(meta.getPrecision(i+1)), i, col.getIndexPrecision());
                mainTabla.set("si", i, col.getIndexEnPantalla());
                mainTabla.set("JTextField", i, col.getIndexTypeSwing());
                mainTabla.set("si", i, col.getIndexLabelYN());
                mainTabla.set("no", i, col.getIndexFichero());
                mainTabla.set("no", i, col.getIndexTexto());
                mainTabla.set("no", i, col.getIndexMascara());
            }
       } catch (Exception e) {
          e.printStackTrace();
        }
//        meta.getColumnLabel --> to get the title in prints
    }
    
            
    public String getTablaNb () {
        return tablaNb;
    }
    
    public String getBdNb () {
        return bdNb;
    }
    
    private void setTablaNb (String tablaNb) {
        this.tablaNb = tablaNb;
    }
    
    private void setBdNb (String bdNb) {
        this.bdNb = bdNb;
    }
    
    private boolean isTablaNbBlank () {
        if (tablaNb == "") return true;
        else return false;
    }
    
    private boolean isBdNbBlank () {
       if (bdNb == "") return true;
        else return false;
    }
    
    public MainTabla getMainTabla() {
        return mainTabla;
    }
    
    public void cargarConfiguracion(String file){
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Utils.setJaviProperties();
//        System.out.println(System.getProperty("user.dir"));
        BD bD = new BD(System.getProperty("app.app"));
//        bD.crearTablas();
//        bD.select("clientes");
//        bD.getMetaTabla("clientes");
//        bD.getMetaTabla("proveedores");
//         bD.select("clientes");
//        bD.getMetaTablaBD("clientes");
        bD.cargarTabla("clientes");
        String[] a = null;
        a=bD.select("select * from clientes");
//        bD.creaTabla("nueva1");
    }
}
