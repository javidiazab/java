/*
 * DialogDesc.java
 *
 * Created on 5 de septiembre de 2004, 15:21
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
public class DialogDesc {
    
    /** Creates a new instance of DialogDesc */
       
    public DialogDesc(java.awt.Container parent, String titulo, javax.swing.text.JTextComponent texto) {
    java.awt.Container container = parent;
    boolean salir = false;
    for (int i = 0; i < 10 && salir == false; i++) {
        if (container instanceof java.awt.Frame) {
            new Description(((java.awt.Frame)container), titulo, texto);
            salir = true;
        }
        else if (container instanceof java.awt.Dialog) {
            new Description(((java.awt.Dialog)container), titulo, texto);
            salir = true;    
        }
        container = container.getParent();
    }
    if (salir == true) return;
    else new Description(titulo, texto);

    }
    
    public DialogDesc(java.awt.Container parent, String titulo, javax.swing.text.JTextComponent texto, int x, int y) {
java.awt.Container container = parent;
    boolean salir = false;
    for (int i = 0; i < 10 && salir == false; i++) {
        if (container instanceof java.awt.Frame) {
            new Description(((java.awt.Frame)container), titulo, texto, x, y);
            salir = true;
        }
        else if (container instanceof java.awt.Dialog) {
            new Description(((java.awt.Dialog)container), titulo, texto, x, y);
            salir = true;    
        }
        container = container.getParent();
    }
    if (salir == true) return;
    else new Description(titulo, texto, x, y);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        javax.swing.JTextField texto = new javax.swing.JTextField("Hola esto es un TextField");
        javax.swing.JFrame jFrame = new javax.swing.JFrame();
        new DialogDesc(jFrame, "Nombre", texto);
        
        javax.swing.JTextArea area = new javax.swing.JTextArea("Hola esto es un TextArea");
        javax.swing.JFrame jFrame2 = new javax.swing.JFrame();
        new DialogDesc(jFrame2, "Nombre2", area);
    }
    
}
