/*
 * LookAndFeel.java
 *
 * Created on 17 de mayo de 2004, 20:57
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
public class LookAndFeel {
    
    //frame.setDefaultLookAndFeelDecorated(true);
    
    public static void setLookAndFeel() {
        try {
            javax.swing.UIManager.
            setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
           e.printStackTrace();
        }
    }
    public static void setLookAndFeel(int i) {
        try {
            if (i == 1) javax.swing.UIManager.
                    setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            if (i == 2) javax.swing.UIManager.
                    setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
            if (i == 3) javax.swing.UIManager.
                    setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
        } catch(Exception e) {
           e.printStackTrace();
        }
    }
    
    public static void setLookAndFeel (String feel) {
    	//System.out.println("mi feel " + feel);
        if(feel.equals("cross")) {
            try {
                javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.
                    getCrossPlatformLookAndFeelClassName());
            } catch(Exception e) {
                    e.printStackTrace();
            }
         } else if(feel.equals("system")) {
            try {	
                javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.
                    getSystemLookAndFeelClassName());
            } catch(Exception e) {
                    e.printStackTrace();
                    }
        } else if(feel.equals("motif")) {
            try {
                 javax.swing.UIManager.setLookAndFeel("com.sun.java."+
                    "swing.plaf.motif.MotifLookAndFeel");
            } catch(Exception e) {
                    e.printStackTrace();
            	}
        } //else  javax.swing.UIManager.getLookAndFeelDefaults();
    }
    
    
    public static String[] getLookAndFeels() {
        javax.swing.UIManager.LookAndFeelInfo[] info = javax.swing.UIManager.getInstalledLookAndFeels();
        String [] tabla = new String[info.length];
        for (int i=0; i<info.length; i++) {
            // Get the name of the look and feel that is suitable for display to the user
            tabla[i] = info[i].getName();
    
            String className = info[i].getClassName();
            // The className is used with UIManager.setLookAndFeel()
        }
        return tabla;
        
    }
     public static void main (String[] args) {
        LookAndFeel lookAndFeel  = new LookAndFeel();
        String [] tabla = lookAndFeel.getLookAndFeels();
        for (int i = 0; i < tabla.length; i ++) {
            System.out.println(tabla[i]);
        }
        
    }
    
    
}
