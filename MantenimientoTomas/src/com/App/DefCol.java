/*
 * DefColumnas.java
 *
 * Created on 23 de agosto de 2004, 22:36
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
public class DefCol {
    static int colName = 0;
    static int colValue = 1;
    static int colLabel = 2;
    static int colType = 3;
    static int colSize = 4;
    static int colPrecision = 5;
    static int colEnPantalla = 6;
    static int colTypeSwing = 7;
    static int colLabelYN = 8;
    static int colFichero = 9;
    static int colTexto = 10;
    static int colMascara = 11;
    
    static String[] colDescriptions = {"colName", "colValue",
        "colLabel", "colType", "colSize", "colPrecision", 
        "colEnPantalla", "colTypeSwing", "colLabelYN",
        "colFichero", "colTexto", "colMascara"};
        
    static int nCols = colDescriptions.length;
    
    
    /** Creates a new instance of DefColumnas */
    public DefCol() {
    }
    
    public boolean colValid(int col) {
        if (col < 0 || col > nCols) return false;
        else return true;
    }
    
    public int getIndexName() {
        return colName;
    }
    
    public int getIndexValue() {
        return colValue;
    }
    
    public int getIndexLabel() {
        return colLabel;
    }
    
    public int getIndexType() {
        return colType;
    }
    public int getIndexSize() {
        return colSize;
    }
    
    public int getIndexPrecision() {
        return colPrecision;
    }
    public int getIndexEnPantalla() {
        return colEnPantalla;
    }
    public int getIndexTypeSwing() {
        return colTypeSwing;
    }
    public int getIndexLabelYN() {
        return colLabelYN;
    }
    public int getIndexFichero() {
        return colFichero;
    }
    public int getIndexTexto() {
        return colTexto;
    }
    public int getIndexMascara() {
        return colMascara;
    }
    
    
}
