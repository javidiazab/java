package App;

class InitialFocusSetter {
    // Set component with initial focus
    // Must be done before the frame is made visible
    //InitialFocusSetter.setInitialFocus(frame, component2);
    
        public static void setInitialFocus(java.awt.Window w, java.awt.Component c) {
            w.addWindowListener(new FocusSetter(c));
        }
    
        public static class FocusSetter extends java.awt.event.WindowAdapter {
            java.awt.Component initComp;
            FocusSetter(java.awt.Component c) {
                initComp = c;
            }
            public void windowOpened(java.awt.event.WindowEvent e) {
                initComp.requestFocus();
    
                // Since this listener is no longer needed, remove it
                e.getWindow().removeWindowListener(this);
            }
        }
 }
    
    
    

