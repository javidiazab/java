/*
 * MainTabla.java
 *
 * Created on 23 de agosto de 2004, 22:12
 */
package App;
/**
 *
 * @author  Javito Corleone
 */
//  ESTRUCTURA DE LA TABLA
//  colNB   colValue    collabel    colType ... colPrecision etc
//  dni     1           DNI         char                    
//  nombre  1           Nombre      char
//  ape1    1           Apellido    char
//  etc

public class MainTabla {
    String [][] tablaLoad = new String [1000][50];
    int index = 0;
    DefCol col = new DefCol();
    boolean EOF = false;
    
    
    /** Creates a new instance of MainTabla */
    public MainTabla() {
        setEOF(true);
    }
       
    
    public MainTabla(String[][]tablaLoad) {
        this.tablaLoad = tablaLoad;
        if (tablaLoad[0][0] == null) setEOF(true);
        else setEOF(false);
    }
    
    
    public int getIndex(String fila) {
        // fila es el nombre del campo del que queremos saber el indice (fila)
        int i = 0;
        for (; tablaLoad[i][col.getIndexName()].trim() != fila && 
            i < tablaLoad.length && tablaLoad[i][0] != null; i++) {
        }
        if (tablaLoad[i][col.getIndexName()].trim() == fila) return i;
        Utils.print("No encontrada " + fila);
        return -1;
    }
    
    
    public void printTabla() {
        for (int f = 0; f < tablaLoad.length && tablaLoad[f][0] != null; f++) {
            Utils.print("");
            for (int c = 0; c < col.nCols; c++) {
                Utils.print(col.colDescriptions[c] + ": " + tablaLoad[f][c]);
            }
        }
    }
    
    public String[][] getTabla(){
        return tablaLoad;
    }
    
    
    public String getValue(int index){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
           return "";
        }
        return getFromTabla(col.colValue, index);
    }
    
    public String getValue(){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
            return "";
        }
        return getFromTabla(col.colValue, index);
    }
    
    public String getName(int index){
        //aqui el index NO es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
           return "";
        }
        return getFromTabla(col.colName, index);
    }
    
    public String getName(){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
            return "";
        }
        return getFromTabla(col.colName, index);
    }
    
    public String getLabel(int index){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
           return "";
        }
        return getFromTabla(col.colLabel, index);
    }
    
    public String getLabel(){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
            return "";
        }
        return getFromTabla(col.colLabel, index);
    }
    
    public String getType(int index){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
           return "";
        }
        return getFromTabla(col.colType, index);
    }
    
    public String getType(){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
            return "";
        }
        return getFromTabla(col.colType, index);
    }
    
     public String getSize(int index){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
           return "";
        }
        return getFromTabla(col.colType, index);
    }
    
    public String getSize(){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
            return "";
        }
        return getFromTabla(col.colSize, index);
    }
      
    public String get(int item){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
           return "";
        }
        return getFromTabla(item, index);
    }
    
     public String get(String item){
        //aqui el index es de la clase
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
           return "";
        }
        return getFromTabla(item, index);
    }
     
     public String get(int item, int index){
        //aqui el index es el pasado como parametro
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            return "";
        }
        return getFromTabla(item, index);
    }
    
     public String get(String item, int index){
                //aqui el index es el pasado como parametro
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
           return "";
        }
        return getFromTabla(item, index);
    }
     
     
     public String getFromTabla(int item, int index){
        if (col.colValid(item)) {
            if (tablaLoad[index][item] == null || tablaLoad[index][item].equals("")) return "";
            return tablaLoad[index][item].trim();
        } else return "";
     }
    
     public String getFromTabla(String item, int index){
         if (item.equals("ColName")) {
            if (tablaLoad[index][col.getIndexName()] == null) return "";
            return tablaLoad[index][col.getIndexName()].trim();
        }
         if (item.equals("ColValue")) {
            if (tablaLoad[index][col.getIndexValue()] == null) return "";
            return tablaLoad[index][col.getIndexValue()].trim();
        }
        if (item.equals("ColLabel")) {
            if (tablaLoad[index][col.getIndexLabel()] == null) return "";
            return tablaLoad[index][col.getIndexLabel()].trim();
        }
        if (item.equals("ColType")) {
            if (tablaLoad[index][col.getIndexType()] == null) return "";
            return tablaLoad[index][col.getIndexType()].trim();
        }
        if (item.equals("ColSize")) {
            if (tablaLoad[index][col.getIndexSize()] == null) return "";
            return tablaLoad[index][col.getIndexSize()].trim();
        }
        if (item.equals("ColPrecision")) {
            if (tablaLoad[index][col.getIndexPrecision()] == null) return "";
            return tablaLoad[index][col.getIndexPrecision()].trim();
        }
        if (item.equals("ColEnPantalla")) {
            if (tablaLoad[index][col.getIndexEnPantalla()] == null) return "";
            return tablaLoad[index][col.getIndexEnPantalla()].trim();
        }
        if (item.equals("ColTypeSwing")) {
            if (tablaLoad[index][col.getIndexTypeSwing()] == null) return "";
            return tablaLoad[index][col.getIndexTypeSwing()].trim();
        }
        if (item.equals("ColLabelYN")) {
            if (tablaLoad[index][col.getIndexLabelYN()] == null) return "";
            return tablaLoad[index][col.getIndexLabelYN()].trim();
        }
         if (item.equals("ColFichero")) {
            if (tablaLoad[index][col.getIndexFichero()] == null) return "";
            return tablaLoad[index][col.getIndexFichero()].trim();
        }
        if (item.equals("ColTexto")) {
            if (tablaLoad[index][col.getIndexTexto()] == null) return "";
            return tablaLoad[index][col.getIndexTexto()].trim();
        }
        if (item.equals("ColMascara")) {
            if (tablaLoad[index][col.getIndexMascara()] == null) return "";
            return tablaLoad[index][col.getIndexMascara()].trim();
        }
        return "";
    }
     
    public int find(String nbCol) {
        for (int i = 0; i < tablaLoad.length; i++) 
            if (tablaLoad[i][0].equals(nbCol)) return i;
        return -1;
    }
    
     public void setValue(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexValue()] = string;
    }
    
    public void setValue(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexValue()] = string;
    }
    
    public void setName(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexName()] = string;
    }
    
    public void setName(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexName()] = string;
    }
    
    public void setLabel(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexLabel()] = string;
    }
    
    public void setLabel(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexLabel()] = string;
    }
    
    public void setType(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexType()] = string;
    }
    
    public void setType(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexType()] = string;
    }
    
    
    public void setSize(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexSize()] = string;
    }
    
    public void setSize(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexSize()] = string;
    }
    
    public void setPrecision(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexPrecision()] = string;
    }
    
    public void setPrecision(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexPrecision()] = string;
    }
    
    public void setEnPantalla(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexEnPantalla()] = string;
    }
    
    public void setEnPantalla(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexEnPantalla()] = string;
    }
    
    public void setTypeSwing(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexTypeSwing()] = string;
    }
    
    public void setTypeSwing(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexTypeSwing()] = string;
    }
    
    public void setLabelYN(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexLabelYN()] = string;
    }
    
    public void setLabelYN(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexLabelYN()] = string;
    }
    
    public void setFichero(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexFichero()] = string;
    }
    
    public void setFichero(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexFichero()] = string;
    }
    
    public void setTexto(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexTexto()] = string;
    }
    
    public void setTexto(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexTexto()] = string;
    }
    
    public void setMascara(String string, int index) {
        checkIfValidIndex();
        tablaLoad[index][col.getIndexMascara()] = string;
    }
    
    public void setMascara(String string) {
        checkIfValidIndex();
        tablaLoad[this.index][col.getIndexMascara()] = string;
    }
    
    private void checkIfValidIndex() {
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
        }
    }
        
     public int getIndexOf(String item){
        for (int i = 0; i < tablaLoad.length && tablaLoad[i][0] != null; i++) {
            if (tablaLoad[i][0].trim() == item) return i;
        }
        return -1;
    }
     
     public void next() {
         if (EOF()) return;
          
         if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
//             if (index > tablaLoad.length || index < 0) {
             setEOF(true);
             return;
         }
        
        index++;
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
//             if (index > tablaLoad.length || index < 0) {
             setEOF(true);
             return;
         }
     }
     
     public int count(){
        
        int i = 0;
        for (; i < tablaLoad.length && tablaLoad[i][0] != null; i++);
        return i-1;
    }
     
      public boolean EOF(){
        return EOF;
    }
      
      public void setIndex(int index){
                 
        if (index > tablaLoad.length || tablaLoad[index][0] == null || index < 0) {
            setEOF(true);
            return;
        }
        this.index = index;
        setEOF(false);
        
    }
      
      private void setEOF(boolean eof) {
        this.EOF = eof;
    }
    
      public void set(String data, int fil, int col){
          tablaLoad[fil][col] = data;
          setEOF(false);
      }
      
      public void set(String data, int col){
          tablaLoad[index][col] = data;
          setEOF(false);
      }
      
      public MainTabla cloneTabla() {
          MainTabla clonedTb = new MainTabla();
          for (int i = 0; i < tablaLoad.length; i++) {
              for (int j = 0; j < tablaLoad[0].length; j++) {
                  clonedTb.set(tablaLoad[i][j], i, j);
              }
          }
          return clonedTb;
       }
      
      public void creaTbl(String tabACargar) {
        //Crea el fichero de la tabla para que quede la info para otra vez
          
        String extTbl = System.getProperty("app.tableExtension");
        java.io.File file = new java.io.File(tabACargar + extTbl);
        try {
            file.delete();
        } catch (Exception e) {
          e.printStackTrace();
        }    
        try {
            file.createNewFile();
            java.io.BufferedWriter out = 
                        new java.io.BufferedWriter(
                        new java.io.FileWriter(tabACargar + extTbl, true));
            setIndex(0);
            for (int i = 0; !EOF(); i++, next()) {
//          Mas tarde solo los primeros datos seran cogidos desde la BD no desde el fichero
                  out.newLine();
                  out.write("TblName[" + (i+1) + "]=" + tabACargar);
                  out.newLine();
                  out.write("ColName[" + (i+1) + "]=" + get("ColName"));
                  out.newLine();
                  out.write("ColLabel[" + (i+1) + "]=" + get("ColLabel"));
                  out.newLine();
                  out.write("ColType[" + (i+1) + "]=" + get("ColType"));
                  out.newLine();
                  out.write("ColSize[" + (i+1) + "]=" + get("ColSize"));
                  out.newLine();
                  out.write("ColPrecision[" + (i+1) + "]=" + get("ColPrecision"));
                  out.newLine();
                  out.write("ColEnPantalla[" + (i+1) + "]=" + get("ColEnPantalla"));
                  out.newLine();
                  out.write("ColTypeSwing[" + (i+1) + "]=" + get("ColTypeSwing"));
                  out.newLine();
                  out.write("ColLabelYN[" + (i+1) + "]=" + get("ColLabelYN"));
                  out.newLine();
                  out.write("ColFichero[" + (i+1) + "]=" + get("ColFichero"));
                  out.newLine();
                  out.write("ColTexto[" + (i+1) + "]=" + get("ColTexto"));
                  out.newLine();
                  out.write("ColMascara[" + (i+1) + "]=" + get("ColMascara"));
                  out.newLine();
            }
            out.close();
        } catch (Exception e) {
          e.printStackTrace();
        }

      }
      
      public String insertSelect(String tabla) {
            StringBuffer buf = new StringBuffer("INSERT INTO " + tabla + " (nombre) VALUES('Javi')");
            return buf.toString();
      }
      
      public String creaSelect(String tabla) {
          setIndex(0);
////          Formato: String sql = "SELECT * FROM <tabla> where name like(xxX), apel like(xxx) etc;
            StringBuffer buf = new StringBuffer("SELECT * FROM " + tabla + " WHERE ");
            for (; !EOF(); next()) {
                 if (!get(col.colValue).equals("") && !get(col.colValue).equals(null) &&
                     !get(col.colEnPantalla).equals("no")) {
                    if (!buf.toString().endsWith("WHERE ")) buf.append(" AND ");
                    buf.append(get(col.colName));
                    buf.append(" LIKE(");
                    if (get(col.colType).equalsIgnoreCase("LONGCHAR") ||
                        get(col.colType).equalsIgnoreCase("VARCHAR"))
                        buf.append("'");
                    buf.append(get(col.colValue));
                    if (get(col.colType).equalsIgnoreCase("LONGCHAR") ||
                        get(col.colType).equalsIgnoreCase("VARCHAR"))
                        buf.append("'");
                    buf.append(")");
//                if (!tablaLoad[index][col.getIndexValue()].trim().equals("") && 
//                    !tablaLoad[index][col.getIndexValue()].trim().equals(null)) {
//                    String mierda = tablaLoad[index][col.getIndexValue()].trim();
//                    String mierda2 = tablaLoad[index][col.getIndexValue()].trim();
//                    if (!buf.toString().endsWith("WHERE ")) buf.append(", ");
//                    buf.append(tablaLoad[index][col.getIndexName()].trim());
//                    buf.append(" like(");
//                    if (tablaLoad[index][col.getIndexType()].trim().equalsIgnoreCase("LONGCHAR") ||
//                        tablaLoad[index][col.getIndexType()].trim().equalsIgnoreCase("VARCHAR"))
//                        buf.append("\"");
//                    buf.append(tablaLoad[index][col.getIndexValue()].trim());
//                    if (tablaLoad[index][col.getIndexType()].trim().equalsIgnoreCase("LONGCHAR") ||
//                        tablaLoad[index][col.getIndexType()].trim().equalsIgnoreCase("VARCHAR"))
//                        buf.append("\"");
//                    buf.append(")");
                
                }
            }
            
            if (buf.toString().endsWith("WHERE ")) {
                 buf.delete(buf.indexOf("WHERE "), 100); // 100 para que se vaya al final del todo
            }
            //esta putamierda devuelve "Eangling meta character '*' near index 0
//            return buf.toString().replaceAll("*","%");
            return buf.toString();
      }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    }
    
}
