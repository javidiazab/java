/*
 * ConfiguraPanel.java
 *
 * Created on 7 de marzo de 2004, 17:12
 */
package App;
/**
 *
 * @author  Javito
 */
public class ConfiguraPanel2 {
    private javax.swing.JPanel panel = null;
    private javax.swing.JPanel newPanel = null;
    private javax.swing.JPanel ivjPanelCentral = null;
    private javax.swing.JPanel ivjPanelEste = null;
    private javax.swing.JPanel ivjPanelNorte = null;
    private javax.swing.JPanel ivjPanelOeste = null;
    private javax.swing.JPanel ivjPanelSur = null;
    private javax.swing.JPanel ivjPanelMsg = null;
    private javax.swing.JPanel ivjPanelBotones = null;
    
    
    
    /** Creates a new instance of ConfiguraPanel */
    public ConfiguraPanel2(javax.swing.JPanel panel) {
        this.panel = panel;
    }
    
      
    private void configuraPanel2() {
        try {
		// user code begin {1}
		// user code end
//		panel.setName("javiPanel");
		panel.setLayout(new java.awt.BorderLayout());
//		panel.setSize(600, 400);
		panel.add(getPanelNorte(), "North");
		panel.add(getPanelEste(), "East");
		panel.add(getPanelOeste(), "West");
		panel.add(getPanelCentral(), "Center");
		panel.add(getPanelSur(), "South");
		
	} catch (java.lang.Throwable ivjExc) {
		handleException(ivjExc);
	}
    }
    
    /*private  javax.swing.JPanel addPanel() {
        try {
		// user code begin {1}
		// user code end
		newPanel.setName("javiPanel");
		newPanel.setLayout(new java.awt.BorderLayout());
//		newPanel.setSize(600, 400);
		newPanel.add(getPanelNorte(), "North");
		newPanel.add(getPanelEste(), "East");
		newPanel.add(getPanelOeste(), "West");
		newPanel.add(getPanelCentral(), "Center");
		newPanel.add(getPanelSur(), "South");
                return newPanel;
		
	} catch (java.lang.Throwable ivjExc) {
		handleException(ivjExc);
	}
    }*/
    
  /**
 * Return the PanelEste property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelBotones() {
	if (ivjPanelBotones == null) {
		try {
			ivjPanelBotones = new javax.swing.JPanel();
			ivjPanelBotones.setName("PanelBotones");
			ivjPanelBotones.setLayout(new java.awt.FlowLayout());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelBotones;
    }
/**
 * Return the PanelCentral property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelCentral() {
	if (ivjPanelCentral == null) {
		try {
			ivjPanelCentral = new javax.swing.JPanel();
			ivjPanelCentral.setName("PanelCentral");
			ivjPanelCentral.setLayout(new java.awt.FlowLayout());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelCentral;
    }
/**
 * Return the PanelEste property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelEste() {
	if (ivjPanelEste == null) {
		try {
			ivjPanelEste = new javax.swing.JPanel();
			ivjPanelEste.setName("PanelEste");
			ivjPanelEste.setLayout(new VerticalBagLayout());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelEste;
    }
/**
 * Return the PanelEste property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelMsg() {
	if (ivjPanelMsg== null) {
		try {
			ivjPanelMsg = new javax.swing.JPanel();
			ivjPanelMsg.setName("PanelMsg");
			ivjPanelMsg.setLayout(new java.awt.FlowLayout());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelMsg;
    }
/**
 * Return the PanelNorte property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelNorte() {
	if (ivjPanelNorte == null) {
		try {
			ivjPanelNorte = new javax.swing.JPanel();
			ivjPanelNorte.setName("PanelNorte");
			ivjPanelNorte.setLayout(new java.awt.FlowLayout());
			// user code begin {1}
			//add(new JButton());
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelNorte;
    }
/**
 * Return the PanelOeste property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelOeste() {
	if (ivjPanelOeste == null) {
		try {
			ivjPanelOeste = new javax.swing.JPanel();
			ivjPanelOeste.setName("PanelOeste");
			ivjPanelOeste.setLayout(new VerticalBagLayout());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelOeste;
    }
/**
 * Return the PanelSur property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private javax.swing.JPanel getPanelSur() {
	if (ivjPanelSur == null) {
		try {
			ivjPanelSur = new javax.swing.JPanel();
			ivjPanelSur.setName("PanelSur");
			ivjPanelSur.setLayout(new VerticalBagLayout());
			// user code begin {1}
			ivjPanelSur.add(getPanelBotones());
			ivjPanelSur.add(getPanelMsg());
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjPanelSur;
    }

/**
 * Return the PanelSur property value.
 * @return javax.swing.JPanel
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
/**
 * Called whenever the part throws an exception.
 * @param exception java.lang.Throwable
 */
    private void handleException(java.lang.Throwable exception) {

	/* Uncomment the following lines to print uncaught exceptions to stdout */
	// System.out.println("--------- UNCAUGHT EXCEPTION ---------");
	// exception.printStackTrace(System.out);
    }
/**
 * Initialize the class.
 */
/* WARNING: THIS METHOD WILL BE REGENERATED. */
    private void initialize() {
	
	// user code begin {2}
	// user code end
    }
/**
 * Starts the application.
 * @param args an array of command-line arguments
 */
    public static void main(java.lang.String[] args) {
    	// Insert code to start the application here.
    }
    
}
