package cinema;

public class Spectator {
    private String name;
    private int age;
    private int money;
    private static int numSpectator = 1;

    public Spectator(int maxAge, int maxMoney) {
    	generateNewSpectator(maxAge, maxMoney);
    }
	
	public void generateNewSpectator(int maxAge, int maxMoney) {
		String number = ("0" + numSpectator++);
        setName("Person" + number.substring(number.length() - 2));     
        setAge((int) Math.floor(Math.random() * (maxAge - 10 + 1) + 10));
        setMoney((int) Math.floor(Math.random() * (maxMoney - 5 + 1) + 5));
        System.out.println(toString());
    }


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public int getMoney() {
		return money;
	}


	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public String toString() {
		return "Llega " + name + " que tiene " + age + " años y " + money + " euros";
	}
}
