package cinema;

import java.util.Optional;

public class MovieTheater {
	private Spectator[][] theaterGrid;
	private int movieTheaterCapacity = 72;
	private int sittedSpectators = 0;
	private final static String EMPTY_SEAT = "(        )  ";
	private final static String PRE_COL_LINE = "\n        ^            ^           ^           ^" + 
			"           ^           ^           ^           ^           ^"; 
	private final static String COL_LINE = "col     A            B           C           D           E" + 
			"           F           G           H           I";


	public MovieTheater () {
		initializeGrid();
	}

	public void initializeGrid() {
		this.theaterGrid = new Spectator[8][9];
	}

	public int getMovieTheaterCapacity() {
		return this.movieTheaterCapacity;
	}

	public boolean sitDown(Spectator spectator, int row, int col) {
		System.out.println(" en la " + (row + 1) + (char)(col + 65));
		if (sittedSpectators + 1 <= movieTheaterCapacity) {
			if (getSeat(row, col) == EMPTY_SEAT) {
				theaterGrid[row][col] = spectator;
				sittedSpectators++;
				return true;
			}
		} else {
			System.out.println("\nCapacidad maxima de la sala alcanzada. No se pueden vender mas entradas");
		}
		return false;
	}

	public void showMovieTheater() {
		for (int row = 7; row >= 0; row--){
			System.out.print("\n" + (row + 1) + " > ");
			for (int col = 0; col < 9; col++) {
				System.out.print(getSeat(row, col));
			}
		}
		System.out.println(PRE_COL_LINE);
		System.out.println(COL_LINE);
	}

	private String getSeat(int seatRow, int seatCol) {
		return Optional.ofNullable(theaterGrid[seatRow][seatCol])
				.map(Spectator::getName)
				.map(spectatorName -> "(" + spectatorName + ")  ")
				.orElse(EMPTY_SEAT);
	}
}
