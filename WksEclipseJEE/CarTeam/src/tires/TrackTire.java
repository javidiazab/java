package tires;

import java.time.LocalDate;

public class TrackTire extends Tire{
	public int adherence;
	public float temperature;
	
	public TrackTire () {
		super();
	}
	
	public TrackTire(String brand, LocalDate expDate, int hardness, String color, Drawing wDrawing, int adherence, float temperature) {
		super(brand, expDate, hardness, color, wDrawing);
		this.adherence = adherence;
		this.temperature = temperature;
	}
		
	@Override
	public String toString() {
		return "Pista [marca=" + brand + ", caducidad=" + expDate + ", dureza=" + hardness(hardness) + ", color=" + color + 
				", tipo dibujo=" + wDrawing.desc() + ", adherencia=" + adherence + ", temperatura=" + temperature + "]";
	}

	public int getAdherence() {
		return adherence;
	}


	public void setAdherence(int adherence) {
		this.adherence = adherence;
	}


	public float getTemperature() {
		return temperature;
	}


	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}





	
	

}
