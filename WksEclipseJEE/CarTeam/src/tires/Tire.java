package tires;

import java.time.LocalDate;

public class Tire implements TireInfo {
	protected String brand;
	protected LocalDate expDate;
	protected int hardness;
	protected String color;
	protected Drawing wDrawing;

	public Tire() {
		
	}
	
	public Tire(String brand, LocalDate expDate, int hardness, String color, Drawing wDrawing) {
		super();
		this.brand = brand;
		this.expDate = expDate;
		this.hardness = hardness;
		this.color = color;
		this.wDrawing = wDrawing;
	}


	@Override
	public String toString() {
		return "Neumatico [marca=" + brand + ", caducidad=" + expDate + ", dureza=" + hardness(hardness) + ", color=" + color
				+ ", tipo dibujo=" + wDrawing.name() + "]";
	}
	
	@Override
	public String basicInfo() {
		return "Marca=" + brand + ", caducidad=" + expDate; 
	}


	@Override
	public String ShowHardness() {
		return "Dureza: " + brand;
	}
	
	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public LocalDate getExpDate() {
		return expDate;
	}


	public void setExpDate(LocalDate expDate) {
		this.expDate = expDate;
	}


	public int getHardness() {
		return hardness;
	}


	public void setHardness(int hardness) {
		this.hardness = hardness;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public Drawing getwDrawing() {
		return wDrawing;
	}


	public void setwDrawing(Drawing wDrawing) {
		this.wDrawing = wDrawing;
	}
	
	public String hardness(int hardness) {
		if (hardness < 10 ) return "blando";
		else if (hardness >= 10 && hardness <= 50) return "medio";
		else return "duro";
	}
	
}
