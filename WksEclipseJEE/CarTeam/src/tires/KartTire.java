package tires;

import java.time.LocalDate;

public class KartTire extends Tire {
	public int rim;

	public KartTire() {
		super();
	}
	
	public KartTire(String brand, LocalDate expDate, int hardness, String color, Drawing wDrawing, int tire) {
		super(brand, expDate, hardness, color, wDrawing);
		this.rim = rim;
	}
		
	
	@Override
	public String toString() {
		return "Kart [marca=" + brand + ", caducidad=" + expDate + ", dureza=" + hardness(hardness) + ", color=" + color +
				", tipo dibujo=" + wDrawing.desc() + ", llanta=" + rim + "]";
	}
	

	public int getRim() {
		return rim;
	}

	public void setRim(int rim) {
		this.rim = rim;
	}

}
