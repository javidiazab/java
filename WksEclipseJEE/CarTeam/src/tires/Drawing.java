package tires;

public enum Drawing {
	WET("Humedo"),
	DRY("Seco"),
	INTERMEDIATE("Intermedio");
	
    private final String description;

    Drawing(String description) {
        this.description = description;
    }

    public String desc() {
        return description;
    }
}
