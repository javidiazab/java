package tires;

import java.time.LocalDate;

public class RallyTire extends Tire {
	public float pressure;
	public int drawingDeep;


	public RallyTire() {
		super();
	}

	public RallyTire(String brand, LocalDate expDate, int hardness, String color, Drawing wDrawing, float pressure, int drawinDeep) {
		super(brand, expDate, hardness, color, wDrawing);
		this.pressure = pressure;
		this.drawingDeep = drawinDeep;
	}


	@Override
	public String toString() {
		return "Rally [marca=" + brand + ", caducidad=" + expDate + ", dureza=" + hardness(hardness) + ", color=" + color + 
				", tipo dibujo=" + wDrawing.desc() + ", presion=" + pressure + ", profundidad=" + drawingDeep + "]";
	}


	public float getPressure() {
		return pressure;
	}


	public void setPressure(float pressure) {
		this.pressure = pressure;
	}


	public int getDrawingDeep() {
		return drawingDeep;
	}


	public void setDrawinDeep(int drawinDeep) {
		this.drawingDeep = drawinDeep;
	}
	
	

}
