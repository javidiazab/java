package mvc;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class ProgrammersTeam {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProgrammersTeam window = new ProgrammersTeam();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ProgrammersTeam() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new MainMenu();
		frame.setBounds(100, 100, 824, 320);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
