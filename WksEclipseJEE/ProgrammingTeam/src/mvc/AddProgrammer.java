package mvc;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;

public class AddProgrammer extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProgrammer frame = new AddProgrammer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddProgrammer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(47, 79, 61, 16);
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(120, 74, 350, 26);
		contentPane.add(textField);
		
		JLabel lblNewLabel_3 = new JLabel("Cargo:");
		lblNewLabel_3.setBounds(47, 154, 61, 16);
		contentPane.add(lblNewLabel_3);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(120, 112, 350, 26);
		contentPane.add(textField_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(120, 150, 350, 27);
		contentPane.add(comboBox);
		
		JButton btnNewButton_5 = new JButton("Anterior");
		btnNewButton_5.setBounds(358, 198, 112, 41);
		contentPane.add(btnNewButton_5);
		
		JLabel lblNewLabel = new JLabel("Programador");
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		lblNewLabel.setBounds(46, 25, 153, 26);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Edad:");
		lblNewLabel_2.setBounds(47, 118, 61, 16);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton_5_1 = new JButton("Guardar");
		btnNewButton_5_1.setBounds(234, 198, 112, 41);
		contentPane.add(btnNewButton_5_1);
	}
}
