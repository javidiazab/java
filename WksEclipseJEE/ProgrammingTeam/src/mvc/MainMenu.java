package mvc;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class MainMenu extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 840, 355);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Miembros Equipo Programacion");
		lblNewLabel.setBounds(20, 26, 227, 19);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		contentPane.add(lblNewLabel);
		
		JButton bNew = new JButton("Nuevo");
		bNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	    		AddProgrammer objVentanaAlta = new AddProgrammer();
	    		objVentanaAlta.setVisible(true);
			}
		});
		bNew.setBounds(647, 55, 140, 29);
		contentPane.add(bNew);
		
		JButton bModify = new JButton("Modificar");
		bModify.setBounds(647, 96, 140, 29);
		contentPane.add(bModify);
		
		JButton bDelete = new JButton("Eliminar");
		bDelete.setBounds(647, 139, 140, 29);
		contentPane.add(bDelete);
		

		
		JButton bSalir = new JButton("Salir");
		bSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		bSalir.setBounds(657, 248, 130, 29);
		contentPane.add(bSalir);
		
		JComboBox bCategoria = new JComboBox();
		bCategoria.setModel(new DefaultComboBoxModel(new String[] {"Todos", "Programador Senior", "Programador Junior", "Analista"}));
		bCategoria.setBounds(259, 24, 376, 27);
		contentPane.add(bCategoria);
		
		

	    DefaultTableModel modelo = new DefaultTableModel(new String[] {"id", "Nombre", "Edad", "Categoria"}, 0); 

		JTable tablaTrabajadores = new JTable(modelo);

	    JScrollPane miBarra = new JScrollPane(tablaTrabajadores);
	    
	    getContentPane().add(miBarra, null);
	    
	    miBarra.setBounds(20, 59, 625, 207);
	    
	    miBarra.setVisible(true);
	    tablaTrabajadores.setVisible(true);
	    
	    //ocultamos la columna del id
	    tablaTrabajadores.getColumnModel().getColumn(0).setMaxWidth(0);
	    tablaTrabajadores.getColumnModel().getColumn(0).setMinWidth(0);
	    tablaTrabajadores.getColumnModel().getColumn(0).setPreferredWidth(0);

	    
	}
}
