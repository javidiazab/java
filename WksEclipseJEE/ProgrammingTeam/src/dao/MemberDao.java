package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Member;
import Singleton.DBConnection;

public class MemberDao {
	private Connection con = null;


	public static MemberDao instance = null;
	
	
	public MemberDao() throws SQLException {
		con = DBConnection.getConnection();
	}
	
	public static MemberDao getInstance() throws SQLException {
		if (instance == null)
			instance = new MemberDao();
		return instance;
	}
	
		
	public void insertardao(Member n) throws SQLException {
		
		PreparedStatement ps = con
				.prepareStatement("INSERT INTO Memberes (nombre, edad, idrango) VALUES (?,?,?)");
		ps.setString(1, n.getName());
		ps.setInt(2, n.getAge());
		ps.setString(3, n.getCathegory());
		ps.executeUpdate();
		ps.close();
	}
	
	public int modificardao(Member n) throws SQLException {
		
		PreparedStatement ps = con
				.prepareStatement("UPDATE Memberes set nombre = ?, edad = ?, idrango = ? where id = ?");
		ps.setString(1, n.getName());
		ps.setInt(2, n.getAge());
		ps.setString(3, n.getCathegory());
		ps.setLong(4, n.getId());
		//int rowsUpdated = ps.executeUpdate();
		int rowsUpdated = ps.executeUpdate();
		//ps.executeUpdate();
		ps.close();
		return rowsUpdated;
	}

	public void borrardao(Member n) throws SQLException {
		
		PreparedStatement ps = con
				.prepareStatement("DELETE FROM Member where id = ?");
		ps.setLong(1, n.getId());
		ps.executeUpdate();
		ps.close();
	}


	
	public String consultaAVG() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT avg(age) as media from Member");
		ResultSet rs = ps.executeQuery();
		String result = null;
		if (rs.next()) {
			result = rs.getString(1);
		}
		rs.close();
		ps.close();
		return result;
	}

	public String consultaTotal() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT count(name) from Member");
		ResultSet rs = ps.executeQuery();
		String result = null;
		if (rs.next()) {
			result = rs.getString(1);
		}
		rs.close();
		ps.close();
		return result;
	}
	
	public ArrayList<Member> listaMember() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT * from Member "
				+ "where cathegory = b.idrango order by 1 desc ");  //TODO
		ResultSet rs = ps.executeQuery();

		ArrayList<Member> result = null;

		while (rs.next()) {
			if (result == null)
				result = new ArrayList<>();
			result.add(new Member(rs.getLong("id"), rs.getString("name"), rs.getInt("age"),
					rs.getString("cathegory")));
		}
		rs.close();
		ps.close();
		return result;
	}

	public ArrayList<Member> buscarRango(String idRango) throws SQLException {  //TODO
		PreparedStatement ps = con.prepareStatement("SELECT id, name, age, cathegory, desc_rango from Memberes as a, rango as b where a.idrango = b.idrango and b.idrango = ? order by 1 desc");
		ps.setString(1, idRango);
		ResultSet rs = ps.executeQuery();

		ArrayList<Member> result = null;

		while (rs.next()) {
			if (result == null)
				result = new ArrayList<>();
			result.add(new Member(rs.getLong("id"), rs.getString("name"), rs.getInt("age"),
					rs.getString("cathegory")));
		}
		rs.close();
		ps.close();
		return result;
	}

}
