package arrays;

import java.util.Scanner;


public class ArraysProyect {

	private static final int TOTAL_COMBINATION = 9;
	private static final int MAX_RETRIES = 15;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		String[] combination = new String[TOTAL_COMBINATION];

		addNumbersToCombination(combination);

		System.out.println("La combinacion si quieres ganar es la siguiente:");

		for(String item : combination){
			System.out.print(item + "  ");
		}

		int number;
		int numHits = 0;
		int numRetries = 0;
		do {
			System.out.println("\nIntento: " + ++numRetries + " - Introduce apuesta: ");
			try {
				number = sc.nextInt();
				
			} catch (Exception e){
				System.out.println("\nQue bien!, has escogido el 99");
				number = 99;
			}

			for (int i = 0; i < 9; i++) {
				if (combination[i].equals(Integer.toString(number))) {
					combination[i] = "XX";
					for(String num : combination){
						System.out.print(num + " ");
					}

					if (++numHits == 9){ 
						System.out.println("Enhorabuena!! Acabas de ganar el bote del mes!!!");
						number = 0;
					}
				}
			}
		}
		while (number > 0 && numRetries < MAX_RETRIES); 
		System.out.println("Fin pgm primitiva");
	}
	
	private static boolean addNumbersToCombination(String[] combination) {		
		for (int i = 0; i < TOTAL_COMBINATION; i++) {
			boolean included = false;
			do {
				String value = Integer.toString((int)Math.floor(Math.random() * (99 - 10 + 1) + 10));
				if (!numberExists(combination, value)) {
					combination[i] = value;
					included = true;
				}
			} while (!included);
		}
		return true;
	}
	
	private static boolean numberExists(String[] combination, String newValue) {
		for (int i = 0; i < 9; i++) {
			if (null == combination[i]) return false;
			if (combination[i].equals(newValue)) return true;
		}
		return false;
	}

}

