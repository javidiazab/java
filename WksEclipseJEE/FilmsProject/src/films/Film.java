package films;


public class Film {
	private String title;
	private int duration;
	private String starring; 
	private String idiom;
    
	public Film() {}
	
	public Film(String title, int duration, String starring, String idiom) {
		this.title = title;
		this.duration = duration;
		this.starring = starring;
		this.idiom = idiom;
	}

	public void query() {
		System.out.println(this.toString());
	}
	
	@Override
	public String toString() {
		return "Title: " + title + " Duration: " + duration + " Starring: " + starring + " Idiom: " + idiom;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getStarring() {
		return starring;
	}

	public void setStarring(String starring) {
		this.starring = starring;
	}

	public String getIdiom() {
		return idiom;
	}

	public void setIdiom(String idiom) {
		this.idiom = idiom;
	}
}
