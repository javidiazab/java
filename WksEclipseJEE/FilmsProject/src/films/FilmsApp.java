package films;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FilmsApp {
	private Scanner sc;
	private List<Film> films;


	public FilmsApp() {
		films = new ArrayList<>();
		sc = new Scanner(System.in);
	}

	public void start() {
		menu();
	}

	private int menu () {		
		int option = 0;

		do {
			System.out.println("\n\n     Menu Peliculas      ");
			System.out.println("*************************\n");
			System.out.println("1.- Insertar pelicula");
			System.out.println("2.- Ver datos de pelicula");
			System.out.println("3.- Numero total de peliculas en la lista");
			System.out.println("4.- Imprimir todas la peliculas");
			System.out.println("0.- Salir ");
			System.out.println("\nSeleccione una opcion: ");
			try {		
				option = sc.nextInt();
			}catch(Exception e) {
				option = 9;
			}

			switch (option) {
			case 1:
				addFilm (); 
				break;
			case 2:
				filmQuery();
				break;
			case 3:
				totalFilms();
				break;
			case 4:
				filmList();
				break;
			case 0:
				break;
			default:
				System.out.println("Opcion incorrecta");
				pause();
				break;
			}
		}while (option!=0);
		System.out.println("Fin pgm Peliculas");

		return option;
	}

	private void addFilm() {
		films.add(createNewFilm());
	}

	private void filmList() {
		System.out.println("\n\nLista de Peliculas");
		System.out.println("==================\n");
		films.stream().forEach(film -> printDetails(film));
		pause();
	}

	private Film createNewFilm() {		
		Film film = new Film();

		System.out.println("\n\nAlta de Pelicula");
		System.out.println("================");

		sc.nextLine();

		System.out.println("Pelicula: ");
		film.setTitle(sc.nextLine());

		System.out.println("Duracion: ");
		film.setDuration(sc.nextInt());

		sc.nextLine();
		System.out.println("Protagonista: ");
		film.setStarring(sc.nextLine());

		System.out.println("Idioma: ");
		film.setIdiom(sc.nextLine());

		return film;
	}

	private void filmQuery() {
		System.out.println("Introduce id de pelicula: ");
		printDetails(films.get(sc.nextInt() - 1));
		pause();
	}

	private void totalFilms() {
		System.out.println("Total Peliculas: " + films.size());
		pause();
	}


	private void printDetails(Film film) {
		System.out.println("Datos pelicula " + film.toString());
	}

	private void pause() {
		System.out.println("\nPulse una tecla para continuar ...");
		sc.nextLine();
		sc.nextLine();
	}
}

