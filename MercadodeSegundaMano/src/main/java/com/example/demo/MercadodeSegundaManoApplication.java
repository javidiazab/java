package com.example.demo;

import com.example.demo.modelo.Producto;
import com.example.demo.modelo.Usuario;
import com.example.demo.servicios.ProductoServicio;
import com.example.demo.servicios.UsuarioServicio;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class MercadodeSegundaManoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MercadodeSegundaManoApplication.class, args);
	}


	@Bean
	public CommandLineRunner initData(UsuarioServicio usuarioServicio, ProductoServicio productoServicio) {
		return args -> {

			Usuario usuario = new Usuario("Antonio", "Cardador Cabello", "acardador@masteratrium.com", "antonio");
			usuario = usuarioServicio.registrar(usuario);

			Usuario usuario2 = new Usuario("Antonio", "Otero Veiga", "aotero@mastersatrium", "antonio");
			usuario2 = usuarioServicio.registrar(usuario2);
			
			Usuario usuario3 = new Usuario("Laura","Garcia Prieto","lgarcia@gmail.com","laura");
			usuario3 = usuarioServicio.registrar(usuario3);

			
			List<Producto> listado = Arrays.asList(new Producto("Bicicleta de montaña", 100.0f,
					"https://cdn.grupoelcorteingles.es/SGFM/dctm/MEDIA03/201909/24/00108450807105____1__640x640.jpg", usuario),
					new Producto("Golf GTI Serie 2", 2500.0f,
							"https://www.minicar.es/large/Volkswagen-Golf-GTi-G60-Serie-II-%281990%29-Norev-1%3A18-i22889.jpg",
							usuario),
					new Producto("Raqueta de tenis", 10.5f, "https://cdn.grupoelcorteingles.es/SGFM/dctm/MEDIA03/201904/26/00108432116302____2__1200x1200.jpg", usuario),
					new Producto("Xbox One X", 425.0f, "https://cdn.grupoelcorteingles.es/SGFM/dctm/MEDIA03/202009/21/00194482000018____5__1200x1200.jpg", usuario2),
					new Producto("Trípode flexible", 10.0f, "https://cdn.grupoelcorteingles.es/SGFM/dctm/MEDIA03/202009/22/00189041203541____2__1200x1200.jpg", usuario2),
					new Producto("Iphone 7 128 GB", 350.0f, "https://store.storeimages.cdn-apple.com/4667/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone7/rosegold/iphone7-rosegold-select-2016?wid=470&hei=556&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1472430205982", usuario2),
					new Producto("Pelotas de tenis", 22.0f, "https://i.ebayimg.com/images/g/GTIAAOSw75lfGEoN/s-l300.jpg", usuario2),
					new Producto("Palo de Golf básico", 99.0f, "https://previews.123rf.com/images/yupiramos/yupiramos1708/yupiramos170825281/84665624-palo-de-golf-icono-sobre-fondo-blanco-ilustraci%C3%B3n-vectorial.jpg", usuario2),
					new Producto("LLavero de Madrid-Metro", 99.0f, "https://www.zings.es/10124-amazon/llaveros-de-madrid.jpg", usuario));
			
			listado.forEach(productoServicio::insertar);
			

		};
	}
	
}
