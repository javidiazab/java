package com.example.demo.controladores;

import com.example.demo.modelo.Producto;
import com.example.demo.servicios.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/public")
public class zonaPublicaController {

	@Autowired
	ProductoServicio productoServicio;
	
	@ModelAttribute("productos")
	public List<Producto> productosNoVendidos() {
		return productoServicio.productosSinVender();
	}
	
	@GetMapping({"/","index"})
	public String index(Model model, @RequestParam(name="q",required=false)String query) {
		if (query!=null) {
			model.addAttribute("productos", productoServicio.buscar(query));
		}
		return "index";
	}
	
	@GetMapping("/producto/{id}")
	public String showProduct(Model model, @PathVariable Long id) {
		Producto result = productoServicio.findById(id); 
		if (result != null) {
			result.setImagen(result.getImagen());
			model.addAttribute("producto", result);
			return "producto";
		}
		return "redirect:/public";
	}
}
