package com.example.demo.repositorios;

import com.example.demo.modelo.Compra;
import com.example.demo.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompraRepositorio extends JpaRepository<Compra, Long>{

	List<Compra> findByPropietario(Usuario propietario);
	
}
