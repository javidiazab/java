package com.examplecoding.java1.interview;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SetTest {
    Set empty = new Set();
    Set one = new Set();
    Set many = new Set();

    @BeforeEach
    void setUp() {
        one.add("1");
        many.add("1");
        many.add("2");
    }

    @Test
    void empty() {
        assertEquals(true, empty.empty());
        assertEquals(false, one.empty());
        assertEquals(false, many.empty());
    }

    @Test
    void size() {
        assertEquals(0, empty.size());
        assertEquals(1, one.size());
        assertEquals(true, many.size() > 1);
    }

    @Test
    void contains() {
        assertEquals(false, empty.contains("1"));
        assertEquals(false, empty.contains("2"));
        assertEquals(true, one.contains("1"));
        assertEquals(false, one.contains("2"));
        assertEquals(true, many.contains("1"));
        assertEquals(true, many.contains("2"));
    }

    @Test
    void ingnoreDuplicates() {
        one.add("1");
        assertEquals(1, one.size());
        assertEquals(true, one.contains("1"));

        int size = many.size();
        many.add("1");
        assertEquals(size, many.size());
        assertEquals(true, many.contains("1"));
        assertEquals(true, many.contains("2"));
    }

    @Test
    void remove() {
        Set set = new Set();

        set.add("1");
        set.add("2");
        set.add("3");
        set.add("4");

        set.remove("2");
//        set.remove("does not exist");

        assertEquals(3, set.size());
        assertEquals(false, set.contains("2"));
        assertEquals(true, set.contains("1"));
        assertEquals(true, set.contains("3"));
        assertEquals(true, set.contains("4"));
    }

    @Test
    void growsWhenFull() {
        Set set = new Set(1);

        set.add("1");
        set.add("2");

        assertEquals(2, set.size());
        assertEquals(true, set.contains("1"));
        assertEquals(true, set.contains("2"));
    }
}
