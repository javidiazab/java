package com.examplecoding.java1.examples.varargs;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class VarArgsTest {

    @InjectMocks
    private VarArgs varArgs;

    @Test
    void shoudPrintArgs() {
        varArgs.initialize("Uno", "dos", "tres", "cuatro");
        varArgs.print();
    }

}