package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import uk.co.santander.gtscommons.model.api.attribute.PaymentAmount;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static uk.co.santander.gtscommons.fixtures.TestConstants.EUR_CURRENCY;

public class ValidPaymentAmountTest {

    private AmountValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new AmountValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidAmount initializeConstraint(boolean nullable) {
        final ValidAmount constraint = mock(ValidAmount.class);

        when(constraint.nullable()).thenReturn(nullable);

        return constraint;
    }

    @Test
    public void shouldReturnTrue_whenValidElement() {
        //Given
        final PaymentAmount givenElementToValidate = PaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenPaymentAmountWithoutCurrency() {
        //Given
        final PaymentAmount givenElementToValidate = PaymentAmount.builder()
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenPaymentAmountWithEmptyCurrency() {
        //Given
        final PaymentAmount givenElementToValidate = PaymentAmount.builder()
                .currency("")
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenPaymentAmountWithoutAmount() {
        //Given
        final PaymentAmount givenElementToValidate = PaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenPaymentAmountWithNegativeAmount() {
        //Given
        final PaymentAmount givenElementToValidate = PaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .amount(BigDecimal.valueOf(-11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenPaymentAmountWithAmountWithMoreThanTwoDecimals() {
        //Given
        final PaymentAmount givenElementToValidate = PaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .amount(BigDecimal.valueOf(11.068))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }
}
