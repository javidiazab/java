package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static uk.co.santander.gtscommons.fixtures.TestConstants.ES_COUNTRY_CODE;

public class ValidISOCountryCodeTest {

    private ISOCountryCodeValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new ISOCountryCodeValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidISOCountryCode initializeConstraint(boolean nullable) {
        final ValidISOCountryCode constraint = mock(ValidISOCountryCode.class);

        when(constraint.nullable()).thenReturn(nullable);

        return constraint;
    }

    @Test
    public void shouldReturnTrue_whenValidElement() {
        //When
        boolean actualValidationResult = validator.isValid(ES_COUNTRY_CODE, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenInvalidIsoCountryCode() {
        //When
        boolean actualValidationResult = validator.isValid("INVALID", null);

        //Then
        assertFalse(actualValidationResult);
    }
}
