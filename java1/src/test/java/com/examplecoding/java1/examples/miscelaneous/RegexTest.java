package com.examplecoding.java1.examples.miscelaneous;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class RegexTest {
    @InjectMocks
    private Regex regex;

    @Test
    void shouldPrintResult_whenRequested() {
        String actualResult = regex.execute();
        assertTrue(actualResult.contains("modified"));
    }
}