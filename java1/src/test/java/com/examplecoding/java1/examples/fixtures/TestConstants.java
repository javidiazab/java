package com.examplecoding.java1.examples.fixtures;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class TestConstants {

    public static final String EUR_CURRENCY = "EUR";
    public static final String GBP_CURRENCY = "GBP";
    public static final String ES_COUNTRY_CODE = "ES";

    public static final String MESSAGE_ID = "message-id";
    public static final String INSTRUCTION_ID = "instruction-id";
    public static final String TRANSACTION_ID = "transaction-id";
    public static final String E2E_ID = "123456789012345678901234567890123";

    public static final String DEBTOR_BIC = "CAHMESMMXXX";
    public static final String DEBTOR_IBAN = "ES13LOYD77950925513335";

    public static final String CREDITOR_BIC = "BSCHGBM0XXX";
    public static final String CREDITOR_IBAN = "GB91BARC20032634945865";

    public static final String CORRESPONDENT_BIC = "BSABESBBXXX";
    public static final String CORRESPONDENT_IBAN = "GB30ABBY09000125513335";

    public static final String CORRESPONDENT_BANKS_PAYMENT_SCHEME = "CORRESPONDENTBANKS";

    public static LocalDate LOCAL_DATE = LocalDate.of(2020, 6, 6);
    public static LocalTime LOCAL_TIME = LocalTime.of(0, 0);
    public static LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(LOCAL_DATE, LOCAL_TIME);
    public static ZoneOffset ZONE_OFFSET = ZoneOffset.ofHoursMinutes(0, 0);
    public static OffsetDateTime OFFSET_DATE_TIME = OffsetDateTime.of(LOCAL_DATE_TIME, ZONE_OFFSET);
}
