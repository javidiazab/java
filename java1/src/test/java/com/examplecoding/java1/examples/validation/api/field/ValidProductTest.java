package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ValidProductTest {

    @Mock
    private ConstraintValidatorContext context;

    @Mock
    private ConstraintValidatorContext.ConstraintViolationBuilder givenConstraintBuilder;

    private ProductValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new ProductValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidProduct initializeConstraint(boolean nullable) {
        final ValidProduct constraint = mock(ValidProduct.class);

        when(constraint.nullable()).thenReturn(nullable);
        when(constraint.allowedProductIds()).thenReturn(new String[]{"Book_to_Book",
                "International_Instant_Payment",
                "Do_and_Follow",
                "International_Transfers"});

        return constraint;
    }

    @ParameterizedTest
    @ValueSource(strings = {"Book_to_Book",
            "International_Instant_Payment",
            "Do_and_Follow",
            "International_Transfers",
    })
    public void shouldReturnTrue_whenValidProductId(String givenProductId) {
        //When
        boolean actualValidationResult = validator.isValid(givenProductId, context);

        //Then
        verifyNoInteractions(context);
        assertTrue(actualValidationResult);
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void shouldReturnFalse_whenBlankProductId_andNullableValuesAllowed(String givenProductId) {
        //When
        boolean actualValidationResult = validator.isValid(givenProductId, context);

        //Then
        assertTrue(actualValidationResult);
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void shouldReturnFalse_whenBlankProductId_andNullableValuesNotAllowed(String givenProductId) {
        //Given
        initializeValidator(false);
        when(context.buildConstraintViolationWithTemplate(any())).thenReturn(givenConstraintBuilder);

        //When
        boolean actualValidationResult = validator.isValid(givenProductId, context);

        //Then
        assertFalse(actualValidationResult);
    }

    @ParameterizedTest
    @ValueSource(strings = {"book_to_book",
            "international_instant_payment",
            "do_and_follow",
            "international_transfers",
            "BOOK_TO_BOOK",
            "INTERNATIONAL_INSTANT_PAYMENT",
            "DO_AND_FOLLOW",
            "INTERNATIONAL_TRANSFERS"})
    public void shouldReturnFalse_whenInvalidProductId(String invalidProductId) {
        //given
        final String expectedErrorMessage
                = "Invalid product id specified. Valid values: " +
                "[Book_to_Book, Do_and_Follow, International_Instant_Payment, International_Transfers]";

        when(context.buildConstraintViolationWithTemplate(any())).thenReturn(givenConstraintBuilder);

        //When
        boolean actualValidationResult = validator.isValid(invalidProductId, context);

        //Then
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate(expectedErrorMessage);
        assertFalse(actualValidationResult);
    }
}
