package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import uk.co.santander.gtscommons.model.api.attribute.Account;
import uk.co.santander.gtscommons.model.api.attribute.Intermediate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static uk.co.santander.gtscommons.fixtures.TestConstants.CREDITOR_BIC;
import static uk.co.santander.gtscommons.fixtures.TestConstants.CREDITOR_IBAN;

public class ValidIntermediateTest {

    private IntermediateValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new IntermediateValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidIntermediate initializeConstraint(boolean nullable) {
        final ValidIntermediate constraint = mock(ValidIntermediate.class);

        when(constraint.nullable()).thenReturn(nullable);
        when(constraint.accountTypes()).thenReturn(new String[]{"IBA", "BBA", "IBAN", "BBAN", "TXT"});
        when(constraint.agentNameMaxLength()).thenReturn(70);
        when(constraint.agentAddressMaxLength()).thenReturn(242);

        return constraint;
    }

    @Test
    public void shouldReturnTrue_whenValidElement() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAccountWithoutAccountId() {
        //Given
        final Account givenAccount = Account.builder()
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAccountWithoutAccountType() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAccountWithInvalidAccountType() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("INVALID")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithoutBic() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithInvalidBic() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent("INVALID")
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenAgentWithoutName() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithNameToLong() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Agent name too looooooooooooooooooooooooooooooooooooooooooooooooooooong")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenAgentWithoutAddress() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithAddressToLong() {
        //Given
        final Account givenAccount = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        final Intermediate givenElementToValidate = Intermediate.builder()
                .intermediateAccount(givenAccount)
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address to looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                        "ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                        "ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }
}
