package com.examplecoding.java1.examples.validation.api.type;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class MandatoryFieldsGroupTest {

    private MandatoryFieldsGroupValidator mandatoryFieldsGroupValidator;

    @BeforeEach
    void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeExclusionValidator();
    }

    private void initializeExclusionValidator() {
        final MandatoryFieldsGroup givenConstraintAnnotation = mock(MandatoryFieldsGroup.class);

        final String[] mandatoryFieldsNames = { "birthDate", "birthCity", "birthCountry" };
        final String errorMessageBase
                = "The following fields form a mandatory group. You should fill all or none. Mandatory fields: ";

        when(givenConstraintAnnotation.mandatoryFieldsNames()).thenReturn(mandatoryFieldsNames);
        when(givenConstraintAnnotation.message()).thenReturn(errorMessageBase);

        mandatoryFieldsGroupValidator = new MandatoryFieldsGroupValidator();
        mandatoryFieldsGroupValidator.initialize(givenConstraintAnnotation);
    }

    @Test
    void shouldReturnTrue_whenAllMandatoryFieldsAreInformed() {
        //Given
        final ConstraintValidatorContext givenContext = mock(ConstraintValidatorContext.class);
        final TestClass givenTestClass
                = new TestClass("source", "date", "city", "country");

        //When
        boolean actualValidationResult = mandatoryFieldsGroupValidator.isValid(givenTestClass, givenContext);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    void shouldReturnTrue_whenNoMandatoryFieldsAreInformed() {
        //Given
        final TestClass givenTestClass = new TestClass("source", null, null, "");
        final ConstraintValidatorContext givenContext = mock(ConstraintValidatorContext.class);

        //When
        boolean actualValidationResult = mandatoryFieldsGroupValidator.isValid(givenTestClass, givenContext);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenSomeMandatoryFieldsAreInformedAndOthersAreNotInformed() {
        //Given
        final TestClass givenTestClass = new TestClass("source", null, null, "city");
        final ConstraintValidatorContext givenContext = mock(ConstraintValidatorContext.class);
        final ConstraintViolationBuilder givenConstraintViolationBuilder = mock(ConstraintViolationBuilder.class);
        final String expectedErrorMessage = "The following fields form a mandatory group. " +
                "You should fill all or none. Mandatory fields: birthDate, birthCity, birthCountry";

        when(givenContext.buildConstraintViolationWithTemplate(any())).thenReturn(givenConstraintViolationBuilder);

        //When
        boolean actualValidationResult = mandatoryFieldsGroupValidator.isValid(givenTestClass, givenContext);

        //Then
        assertFalse(actualValidationResult);
        verify(givenContext).buildConstraintViolationWithTemplate(expectedErrorMessage);
        verify(givenConstraintViolationBuilder).addConstraintViolation();
    }

    @MandatoryFieldsGroup(mandatoryFieldsNames = { "birthDate", "birthCity", "birthCountry" })
    @Value
    @RequiredArgsConstructor
    private static class TestClass {
        private String source;
        private String birthDate;
        private String birthCity;
        private String birthCountry;
    }
}