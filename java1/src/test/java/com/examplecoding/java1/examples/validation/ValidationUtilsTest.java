package com.examplecoding.java1.examples.validation;

import com.examplecoding.java1.examples.validator.ValidationUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ValidationUtilsTest {

    @Test
    public void shouldReturnTrue_whenCheckAllowedNullValue() {
        //Given
        final Integer nullInteger = null;
        final String nullString = null;

        //When - Then
        assertTrue(ValidationUtils.checkAllowedNullValue(true, nullInteger));
        assertTrue(ValidationUtils.checkAllowedNullValue(true, nullString));
        assertTrue(ValidationUtils.checkAllowedNullValue(true, ""));
        assertTrue(ValidationUtils.checkAllowedNullValue(true, " "));
    }

    @Test
    public void shouldReturnFalse_whenCheckAllowedNullValue() {
        //Given
        final Integer integer = 1;
        final String string = "1";
        final Integer nullInteger = null;
        final String nullString = null;

        //When - Then
        assertFalse(ValidationUtils.checkAllowedNullValue(true, integer));
        assertFalse(ValidationUtils.checkAllowedNullValue(false, integer));
        assertFalse(ValidationUtils.checkAllowedNullValue(false, nullInteger));

        assertFalse(ValidationUtils.checkAllowedNullValue(true, string));
        assertFalse(ValidationUtils.checkAllowedNullValue(false, string));
        assertFalse(ValidationUtils.checkAllowedNullValue(false, nullString));
        assertFalse(ValidationUtils.checkAllowedNullValue(false, ""));
        assertFalse(ValidationUtils.checkAllowedNullValue(false, " "));
    }

    @Test
    public void shouldReturnTrue_whenCheckAllowedLength() {
        //Given
        final int maxAllowedLength = 5;

        //When - Then
        assertTrue(ValidationUtils.checkAllowedLength(maxAllowedLength, null));
        assertTrue(ValidationUtils.checkAllowedLength(maxAllowedLength, "short"));
        assertTrue(ValidationUtils.checkAllowedLength(0, ""));
        assertTrue(ValidationUtils.checkAllowedLength(0, null));
    }

    @Test
    public void shouldReturnFalse_whenCheckAllowedLength() {
        //Given
        final int maxAllowedLength = 5;

        //When - Then
        assertFalse(ValidationUtils.checkAllowedLength(maxAllowedLength, "too long"));
    }

    @Test
    public void shouldReturnTrue_whenCheckAllowedAmount() {
        //When - Then
        assertTrue(ValidationUtils.checkAllowedAmount(ONE));
        assertTrue(ValidationUtils.checkAllowedAmount(new BigDecimal("1.3")));
        assertTrue(ValidationUtils.checkAllowedAmount(new BigDecimal("1.33")));
    }

    @Test
    public void shouldReturnFalse_whenCheckAllowedAmount() {
        //When - Then
        assertFalse(ValidationUtils.checkAllowedAmount(null));
        assertFalse(ValidationUtils.checkAllowedAmount(ZERO));
        assertFalse(ValidationUtils.checkAllowedAmount(new BigDecimal("-1.3")));
        assertFalse(ValidationUtils.checkAllowedAmount(new BigDecimal("1.333")));
    }

    @Test
    public void shouldReturnTrue_whenCheckAllowedAmountOrZero() {
        //When - Then
        assertTrue(ValidationUtils.checkAllowedAmountOrZero(ZERO));
        assertTrue(ValidationUtils.checkAllowedAmountOrZero(ONE));
        assertTrue(ValidationUtils.checkAllowedAmountOrZero(new BigDecimal("1.3")));
        assertTrue(ValidationUtils.checkAllowedAmountOrZero(new BigDecimal("1.33")));
    }

    @Test
    public void shouldReturnFalse_whenCheckAllowedAmountOrZero() {
        //When - Then
        assertFalse(ValidationUtils.checkAllowedAmountOrZero(null));
        assertFalse(ValidationUtils.checkAllowedAmountOrZero(new BigDecimal("-1.3")));
        assertFalse(ValidationUtils.checkAllowedAmountOrZero(new BigDecimal("1.333")));
    }

    @Test
    public void shouldAddErrorMessageToContext() {
        //Given
        final String givenErrorMessage = "Some error message to set on context";
        final ConstraintValidatorContext givenContext = mock(ConstraintValidatorContext.class);
        final ConstraintViolationBuilder givenConstraintBuilder = mock(ConstraintViolationBuilder.class);

        when(givenContext.buildConstraintViolationWithTemplate(any())).thenReturn(givenConstraintBuilder);

        //When
        ValidationUtils.addErrorMessage(givenContext, givenErrorMessage);

        //Then
        verify(givenContext).disableDefaultConstraintViolation();
        verify(givenContext).buildConstraintViolationWithTemplate(givenErrorMessage);
    }
}