package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.examplecoding.java1.examples.validator.validator2.ValidCounterValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ValidCounterValueTest {

    private CounterValueValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new CounterValueValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidCounterValue initializeConstraint(boolean nullable) {
        final ValidCounterValue constraint = mock(ValidCounterValue.class);

        when(constraint.nullable()).thenReturn(nullable);

        return constraint;
    }

    @Test
    public void shouldReturnTrue_whenValidElement() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .tradeCurrency(GBP_CURRENCY)
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithoutCurrency() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .tradeCurrency(GBP_CURRENCY)
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithEmptyCurrency() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency("")
                .tradeCurrency(GBP_CURRENCY)
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithoutTradeCurrency() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency(GBP_CURRENCY)
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithEmptyTradeCurrency() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .tradeCurrency("")
                .amount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithoutAmount() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .tradeCurrency(GBP_CURRENCY)
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithNegativeAmount() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .tradeCurrency(GBP_CURRENCY)
                .amount(BigDecimal.valueOf(-11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenCounterValuePaymentAmountWithAmountWithMoreThanTwoDecimals() {
        //Given
        final CounterValuePaymentAmount givenElementToValidate = CounterValuePaymentAmount.builder()
                .currency(EUR_CURRENCY)
                .tradeCurrency(GBP_CURRENCY)
                .amount(BigDecimal.valueOf(11.068))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }
}
