package com.examplecoding.java1.examples.interfaces;

import com.examplecoding.java1.examples.interfaces.Interfaces1.PrintService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PrintServiceTest {
    @Autowired
    private PrintService printService;

    @Test
    void shouldPrint() {
        assertTrue(printService.print());
    }
}