package com.examplecoding.java1.examples.interfacesList;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.stream.Stream;

@SpringBootTest
class ValidatorServiceTest {
    @Autowired
    private ValidatorService validatorService;

    @ParameterizedTest(name = "[{0}]")
    @MethodSource("vehiclesProvider")
    void shouldBValidateVehicle(Vehicle vehicle) {
        validatorService.process(vehicle);
    }

    static Stream<Arguments> vehiclesProvider() {
        return Stream.of(
                Arguments.of(new Car(LocalDate.now(), "BFB 3333", 5, "2000", Color.BLUE, "Javi", LocalDate.of(2022, 02, 01))),
                Arguments.of(new Car(LocalDate.now(), "CCC 5555", 5, "1500", Color.WHITE, "Jesus", LocalDate.of(2020, 03,01)))
        );
    }
}