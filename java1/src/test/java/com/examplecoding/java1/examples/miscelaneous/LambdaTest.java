package com.examplecoding.java1.examples.miscelaneous;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class LambdaTest {
    @InjectMocks
    private Lambda lambda;

    @Test
    void shoudReturnTrue() {
        boolean actualResult = lambda.execute();
        assertTrue(actualResult);
    }

}