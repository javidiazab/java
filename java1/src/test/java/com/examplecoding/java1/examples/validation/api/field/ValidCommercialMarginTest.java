package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.examplecoding.java1.examples.validator.validator2.CommercialMarginValidator;
import com.examplecoding.java1.examples.validator.validator2.ValidCommercialMargin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import uk.co.santander.gtscommons.model.api.attribute.FxComercialMargin;

import java.math.BigDecimal;

import static com.examplecoding.java1.examples.fixtures.TestConstants.EUR_CURRENCY;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static uk.co.santander.gtscommons.fixtures.TestConstants.EUR_CURRENCY;

class ValidCommercialMarginTest {

    private CommercialMarginValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new CommercialMarginValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidCommercialMargin initializeConstraint(boolean nullable) {
        final ValidCommercialMargin constraint = mock(ValidCommercialMargin.class);

        when(constraint.nullable()).thenReturn(nullable);

        return constraint;
    }

    @Test
    void shouldReturnTrue_whenValidElement() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginCurrency(EUR_CURRENCY)
                .fxComercialMarginAmount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenCommercialMarginWithoutCurrency() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginAmount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenCommercialMarginWithEmptyCurrency() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginCurrency("")
                .fxComercialMarginAmount(BigDecimal.valueOf(11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenCommercialMarginWithoutAmount() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginCurrency(EUR_CURRENCY)
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenCommercialMarginWithNegativeAmount() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginCurrency(EUR_CURRENCY)
                .fxComercialMarginAmount(BigDecimal.valueOf(-11.06))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    void shouldReturnFalse_whenCommercialMarginWithAmountWithMoreThanTwoDecimals() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginCurrency(EUR_CURRENCY)
                .fxComercialMarginAmount(BigDecimal.valueOf(11.068))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    void shouldReturnTrue_whenCommercialMarginWithZeroAmount() {
        //Given
        final FxComercialMargin givenElementToValidate = FxComercialMargin.builder()
                .fxComercialMarginCurrency(EUR_CURRENCY)
                .fxComercialMarginAmount(BigDecimal.valueOf(0.00))
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }
}
