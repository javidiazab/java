package com.examplecoding.java1.examples.configurationProperties;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LoadVariablesTest {

    @Autowired
    private LoadVariables loadVariables;

    @Test
    void shouldContainConfigurationValues_whenConfigurationPropertiesSet() {
        List<String> actualServers = loadVariables.getServers();
        assertEquals(2, actualServers.size());
        assertEquals("dev.bar.com", actualServers.get(0));
        assertEquals("foo.bar.com", actualServers.get(1));
    }
}