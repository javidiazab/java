package com.examplecoding.java1.examples.validation.api.type;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.LoggerFactory;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FieldsExclusionTest {

    private ExclusionValidator exclusionValidator;

    private static Stream<Arguments> provideCorrectRequests() {
        return Stream.of(
                Arguments.of(new TestClass("origin", null)),
                Arguments.of(new TestClass(null, "source"))
        );
    }

    private static Stream<Arguments> provideErroneousBookToBookRequests() {
        return Stream.of(
                Arguments.of(new TestClass("origin", "source")),
                Arguments.of(new TestClass(null, null))
        );
    }

    @BeforeEach
    void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeExclusionValidator();
    }

    private void initializeExclusionValidator() {
        final FieldsExclusion givenConstraintAnnotation = mock(FieldsExclusion.class);
        when(givenConstraintAnnotation.field()).thenReturn("origin");
        when(givenConstraintAnnotation.fieldAgainst()).thenReturn("source");

        exclusionValidator = new ExclusionValidator();
        exclusionValidator.initialize(givenConstraintAnnotation);
    }

    @ParameterizedTest
    @MethodSource("provideCorrectRequests")
    void shouldReturnTrue_whenOnlyOneOfTheTwoFieldsIsInformed(TestClass testClass) {
        //When
        boolean actualValidationResult = exclusionValidator.isValid(testClass, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @ParameterizedTest
    @MethodSource("provideErroneousBookToBookRequests")
    void shouldReturnFalse_whenBothFieldsAreNullOrBothAreNotNull(TestClass testClass) {
        //When
        boolean actualValidationResult = exclusionValidator.isValid(testClass, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @FieldsExclusion(field = "origin", fieldAgainst = "source", message = "Only one can be set: origin or source")
    @Value
    @RequiredArgsConstructor
    private static class TestClass {
        private final String origin;
        private final String source;
    }
}