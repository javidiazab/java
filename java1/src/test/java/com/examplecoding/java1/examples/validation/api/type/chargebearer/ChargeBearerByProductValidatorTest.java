package com.examplecoding.java1.examples.validation.api.type.chargebearer;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ChargeBearerByProductValidatorTest {

    private static final String ERROR_MESSAGE = "Error on charge bearer, mate";

    private ChargeBearerByProductValidator validator;

    @BeforeEach
    void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator();
    }

    private void initializeValidator() {
        validator = new ChargeBearerByProductValidator();
        validator.initialize(generateValidChargeBearerByProduct());
    }

    private ValidChargeBearerByProduct generateValidChargeBearerByProduct() {
        final ValidChargeBearerByProduct constraint = mock(ValidChargeBearerByProduct.class);

        final String[] values = { "SHA","OUR","BEN" };
        final ChargeBearerByProduct[] productConfiguration = { generateInstantPaymentConfig(), generateB2BConfig() };

        when(constraint.values()).thenReturn(values);
        when(constraint.message()).thenReturn(ERROR_MESSAGE);
        when(constraint.product2ValidChargeBearer()).thenReturn(productConfiguration);
        when(constraint.nullable()).thenReturn(true);

        return constraint;
    }

    private ChargeBearerByProduct generateB2BConfig() {
        final ChargeBearerByProduct b2bConfig = mock(ChargeBearerByProduct.class);

        when(b2bConfig.productId()).thenReturn("Book_to_Book");
        when(b2bConfig.nullable()).thenReturn(true);

        final String[] b2bValues = {"OUR"};
        when(b2bConfig.validChargeBearers()).thenReturn(b2bValues);

        return b2bConfig;
    }

    private ChargeBearerByProduct generateInstantPaymentConfig() {
        final ChargeBearerByProduct instantPaymentConfig = mock(ChargeBearerByProduct.class);

        when(instantPaymentConfig.productId()).thenReturn("International_Instant_Payment");
        when(instantPaymentConfig.nullable()).thenReturn(true);

        final String[] ipValues = {"OUR","BEN"};
        when(instantPaymentConfig.validChargeBearers()).thenReturn(ipValues);

        return instantPaymentConfig;
    }

    @ParameterizedTest
    @MethodSource("validProductId")
    void shouldReturnPassOnValidation_givenValidTestedObject(Object givenTestObject) {
        //Given
        final ConstraintValidatorContext givenContext = mock(ConstraintValidatorContext.class);

        //When
        final boolean actualResult = validator.isValid(givenTestObject, givenContext);

        //Then
        assertTrue(actualResult);
    }

    private static Stream<Object> validProductId() {
        return Stream.of(
                createTestObject("productID",null),
                createTestObject("productID","SHA"),
                createTestObject("productID","OUR"),
                createTestObject("productID","BEN"),
                createTestObject("Book_to_Book",null),
                createTestObject("Book_to_Book","OUR"),
                createTestObject("International_Instant_Payment",null),
                createTestObject("International_Instant_Payment","OUR"),
                createTestObject("International_Instant_Payment","BEN")
        );
    }

    private static TestClass createTestObject(String productId, String chargeBearer) {
        return new TestClass(productId, chargeBearer);
    }

    @ParameterizedTest
    @MethodSource("invalidProductId")
    void shouldReturnFalse_whenInvalidProductId(Object givenTestObject, String validValues) {
        //Given
        final ConstraintValidatorContext givenContext = mock(ConstraintValidatorContext.class);
        final ConstraintViolationBuilder givenConstraintViolationBuilder = mock(ConstraintViolationBuilder.class);

        final String expectedErrorMessage
                = String.format(ERROR_MESSAGE + " [Invalid optional field 'chargeBearer' value, allowed values: %s]",
                                validValues);

        when(givenContext.buildConstraintViolationWithTemplate(any())).thenReturn(givenConstraintViolationBuilder);

        //When
        final boolean actualResult = validator.isValid(givenTestObject, givenContext);

        //Then
        assertFalse(actualResult);
        verify(givenContext).buildConstraintViolationWithTemplate(expectedErrorMessage);
        verify(givenConstraintViolationBuilder).addConstraintViolation();
    }

    private static Stream<Object> invalidProductId() {
        return Stream.of(
                Arguments.of(createTestObject("productID","chargeBearer"), "BEN, SHA, OUR"),
                Arguments.of(createTestObject("Book_to_Book","BEN"), "OUR"),
                Arguments.of(createTestObject("Book_to_Book","SHA"), "OUR"),
                Arguments.of(createTestObject("Book_to_Book","chargeBearer"), "OUR"),
                Arguments.of(createTestObject("International_Instant_Payment","SHA"), "BEN, OUR"),
                Arguments.of(createTestObject("International_Instant_Payment","chargeBearer"), "BEN, OUR")
        );
    }

    @Value
    @RequiredArgsConstructor
    private static class TestClass {
        String productId;
        String chargeBearer;
    }
}