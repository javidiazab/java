package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.examplecoding.java1.examples.validator.validator2.AgentValidator;
import com.examplecoding.java1.examples.validator.validator2.ValidAgent;
import com.examplecoding.java1.examples.validator.validator2.attribute.Agent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import static com.examplecoding.java1.examples.fixtures.TestConstants.CREDITOR_BIC;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ValidAgentTest {

    private AgentValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true, false);
    }

    private void initializeValidator(boolean nullable, boolean blankAgentAllowed) {
        validator = new AgentValidator();
        validator.initialize(initializeConstraint(nullable, blankAgentAllowed));
    }

    private ValidAgent initializeConstraint(boolean nullable, boolean blankAgentAllowed) {
        final ValidAgent constraint = mock(ValidAgent.class);

        when(constraint.nullable()).thenReturn(nullable);
        when(constraint.nullableBic()).thenReturn(blankAgentAllowed);
        when(constraint.nameMaxLength()).thenReturn(70);
        when(constraint.addressMaxLength()).thenReturn(242);
        when(constraint.localInfoMaxLength()).thenReturn(70);

        return constraint;
    }

    @Test
    public void shouldReturnTrue_whenValidElement() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address")
                .agentLocalInformation("LocalInfo")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualAccountValidationResult = validator.isValid(null, null );

        //Then
        assertTrue(actualAccountValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false, false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithoutBicAndNullableBicIsFalse() {
        //Given
        initializeValidator(true, false);
        final Agent givenElementToValidate = Agent.builder()
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenAgentWithoutBicAndNullableBicIsTrue() {
        //Given
        initializeValidator(true, true);
        final Agent givenElementToValidate = Agent.builder()
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithInvalidBic() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent("INVALID")
                .agentName("Name")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenAgentWithoutName() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }


    @Test
    public void shouldReturnFalse_whenAgentWithNameToLong() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .agentName("Agent name too looooooooooooooooooooooooooooooooooooooooooooooooooooong")
                .addressAgent("Address")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenAgentWithoutAddress() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithAddressToLong() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address to looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                        "ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                        "ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenAgentWithoutLocalInfo() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .addressAgent("Address")
                .agentName("name")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAgentWithLocalInfoToLong() {
        //Given
        final Agent givenElementToValidate = Agent.builder()
                .agent(CREDITOR_BIC)
                .agentName("Name")
                .addressAgent("Address")
                .agentLocalInformation("LocalInfo to looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                        "oooooooong")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }
}
