package com.examplecoding.java1.examples.validator;

import com.examplecoding.java1.examples.validator.validator1.Adress;
import com.examplecoding.java1.examples.validator.validator1.InputService;
import com.examplecoding.java1.examples.validator.validator1.ModelDto;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
class InputServiceTest {
    @InjectMocks
    private InputService inputService;

    @ParameterizedTest(name = "[{0}]")
    @MethodSource("modelDtos")
    void shouldValiate(ModelDto modelDto) {
        inputService.process(modelDto);
    }

    static Stream<Arguments> modelDtos() {
        return Stream.of(
                Arguments.of(ModelDto.builder()
                        .name("Javi")
                        .surname("Diaz")
                        .num(15)    //-->
                        .email("xxx@xxx.es")
                        .web("www.xxx.es")
                        .birthDate(LocalDate.of(1980, 01, 01))
                        .build()),
                Arguments.of(ModelDto.builder()
                        .surname("Perez")
                        .num(5)
                        .email("lula")   //-->
                        .web("www.xxx.es")
                        .birthDate(LocalDate.of(1980, 01, 01))
                        .build()),
                Arguments.of(ModelDto.builder()
                        .name("Luis")
                        .surname("Maroto")
                        .num(5)
                        .email("xxx@xxx.es")
                        .web("www")  //-->
                        .birthDate(LocalDate.of(1980, 01, 01))
                        .build()),
                Arguments.of(ModelDto.builder()
                        .name("Luis")
                        .surname("Maroto")
                        .num(5)
                        .email("xxx@xxx.es")
                        .web("www")
                        .birthDate(LocalDate.of(1900, 01, 01)) //-->
                        .build()),
                Arguments.of(ModelDto.builder()
                        .name("Pepe")
                        .surname("Contreras")
                        .num(5)
                        .adress(Adress.builder()
                                .country("ES")   //--> should fail
                                .build())
                        .email("xxx@xxx.es")
                        .web("www.ddd.es")
                        .birthDate(LocalDate.of(1900, 01, 01)) //-->
                        .build())
        );
    }
}