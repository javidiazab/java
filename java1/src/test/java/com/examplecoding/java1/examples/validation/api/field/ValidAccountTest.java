package com.examplecoding.java1.examples.validation.api.field;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.examplecoding.java1.examples.validator.validator2.AccountValidator;
import com.examplecoding.java1.examples.validator.validator2.ValidAccount;
import com.examplecoding.java1.examples.validator.validator2.attribute.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import static com.examplecoding.java1.examples.fixtures.TestConstants.CREDITOR_IBAN;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ValidAccountTest {

    private AccountValidator validator;

    @BeforeEach
    public void setUp() {
        ((Logger) LoggerFactory.getLogger("ma.glasnost.orika")).setLevel(Level.INFO);
        initializeValidator(true);
    }

    private void initializeValidator(boolean nullable) {
        validator = new AccountValidator();
        validator.initialize(initializeConstraint(nullable));
    }

    private ValidAccount initializeConstraint(boolean nullable) {
        final ValidAccount constraint = mock(ValidAccount.class);

        when(constraint.nullable()).thenReturn(nullable);
        when(constraint.accountTypes()).thenReturn(new String[]{"IBA", "BBA", "IBAN", "BBAN", "TXT"});

        return constraint;
    }

    @Test
    public void shouldReturnTrue_whenValidElement() {
        //Given
        final Account givenElementToValidate = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("IBAN")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnTrue_whenNullElement_andNullableValuesAllowed() {
        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertTrue(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenNullElement_andNullableValuesNotAllowed() {
        //Given
        initializeValidator(false);

        //When
        boolean actualValidationResult = validator.isValid(null, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAccountWithoutAccountId() {
        //Given
        final Account givenElementToValidate = Account.builder()
                .accountIdType("IBAN")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAccountWithoutAccountType() {
        //Given
        final Account givenElementToValidate = Account.builder()
                .accountId(CREDITOR_IBAN)
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }

    @Test
    public void shouldReturnFalse_whenAccountWithInvalidAccountType() {
        //Given
        final Account givenElementToValidate = Account.builder()
                .accountId(CREDITOR_IBAN)
                .accountIdType("INVALID")
                .build();

        //When
        boolean actualValidationResult = validator.isValid(givenElementToValidate, null);

        //Then
        assertFalse(actualValidationResult);
    }
}
