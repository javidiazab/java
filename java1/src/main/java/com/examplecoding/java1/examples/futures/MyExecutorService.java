package com.examplecoding.java1.examples.futures;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MyExecutorService {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //Creates a thread pool that creates new threads as needed, but will reuse previously constructed threads when they are available.
//        ExecutorService executor = Executors.newCachedThreadPool();
        ExecutorService executor = Executors.newFixedThreadPool(3);

        Callable<String> callableTask1 = () -> {
            TimeUnit.MILLISECONDS.sleep(9000);
            return "Task1's execution";
        };

        Callable<String> callableTask2 = () -> {
            TimeUnit.MILLISECONDS.sleep(6000);
            return "Task2's execution";
        };

        Callable<String> callableTask3 = () -> {
            TimeUnit.MILLISECONDS.sleep(1000);
            return "Task3's execution";
        };

        List<Callable<String>> callableTasks = new ArrayList<>();
        callableTasks.add(callableTask1);
        callableTasks.add(callableTask2);
        callableTasks.add(callableTask3);

        System.out.println("Before invoking all");
        List<Future<String>> futures = executor.invokeAll(callableTasks);

        System.out.println("  futures.get(0).isDone() " + futures.get(0).isDone());
        System.out.println("  futures.get(1).isDone() " + futures.get(1).isDone());
        System.out.println("  futures.get(2).isDone() " + futures.get(2).isDone());

        while(!futures.get(0).isDone() && !futures.get(1).isDone() && !futures.get(2).isDone()) {
            System.out.println("Calculating..." + futures.get(0).isDone() + " " + futures.get(1).isDone() +" " + futures.get(2).isDone());
            Thread.sleep(500);
        }

        System.out.println("Before shutdown");
        System.out.println("terminated: " + executor.isTerminated());
        try {
            if (!executor.awaitTermination(300, TimeUnit.MILLISECONDS)) {
                System.out.println("shuting down now");
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }

        Future<String> stringFuture0 = futures.get(0);
        Future<String> stringFuture1 = futures.get(1);
        Future<String> stringFuture2 = futures.get(2);


        System.out.println("Before getting");
        String result0 = stringFuture0.get();
        System.out.println("  Resultado 0: " + result0);
        String result1 = stringFuture1.get();
        System.out.println("  Resultado 1: " + result1);
        String result2 = stringFuture2.get();
        System.out.println("  Resultado 2: " + result2);

    }

}
