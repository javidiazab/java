package com.examplecoding.java1.examples.objectMapper.Config;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeParseException;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Configuration
public class JacksonConfig {

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules()
                .registerModule(new JavaTimeModule())
                .registerModule(offsetSerializationModule())
                .setSerializationInclusion(NON_EMPTY)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }


    private SimpleModule offsetSerializationModule() {
        return new SimpleModule() {
            @Override
            public void setupModule(SetupContext context) {
                super.setupModule(context);
                context.addDeserializationProblemHandler(new DeserializationProblemHandler() {
                    @Override
                    public Object handleWeirdStringValue(DeserializationContext ctxt, Class<?> targetType, String valueToConvert, String failureMsg) throws IOException {
                        if (OffsetDateTime.class.isAssignableFrom(targetType)) {
                            try {
                                return OffsetDateTime.of(LocalDateTime.parse(valueToConvert), ZoneOffset.UTC);

                            } catch (DateTimeParseException ex) {
                                return super.handleWeirdStringValue(ctxt, targetType, valueToConvert, failureMsg);
                            }
                        }
                        return super.handleWeirdStringValue(ctxt, targetType, valueToConvert, failureMsg);
                    }
                });
            }
        };
    }
}