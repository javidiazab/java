package com.examplecoding.java1.examples.countDownLatch;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

@Slf4j
public class CountDownService implements Runnable{
    private final String name;
    private final int timeToStart;
    private final CountDownLatch latch;

    public CountDownService(String name, int timeToStart, CountDownLatch latch) {
        this.name = name;
        this.timeToStart = timeToStart;
        this.latch = latch;
    }


    @Override
    public void run() {
        try {
            Thread.sleep(timeToStart);
        } catch (InterruptedException ie) {
            System.out.println(CountDownService.class.getName() + ie.getMessage());
        }
        System.out.println(name + " is UP");
        latch.countDown();
    }
}
