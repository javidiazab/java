package com.examplecoding.java1.examples.desingPatterns.duck;

public interface FlyBehavior {

    void fly();
}
