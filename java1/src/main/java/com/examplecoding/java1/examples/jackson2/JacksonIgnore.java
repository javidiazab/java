package com.examplecoding.java1.examples.jackson2;

import java.io.IOException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonIgnore {
    public static void main(String args[]){
        ObjectMapper mapper = new ObjectMapper();
        try{
            Student2 student = new Student2(1,11,"1ab","Mark");
            String jsonString = mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(student);
            System.out.println(jsonString);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
class Student2 {
    public int id;
    @JsonIgnore
    public String systemId;
    public int rollNo;
    public String name;

    Student2(int id, int rollNo, String systemId, String name){
        this.id = id;
        this.systemId = systemId;
        this.rollNo = rollNo;
        this.name = name;
    }
}
