package com.examplecoding.java1.examples.countDownLatch;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {

    public static void main(String[] args) {
        final CountDownLatch latch = new CountDownLatch(3);
        Thread cacheService = new Thread(new CountDownService("CacheService", 1000, latch));
        Thread alertService = new Thread(new CountDownService("AlertService", 1000, latch));
        Thread validationService = new Thread(new CountDownService("ValidationService", 1000, latch));
        cacheService.start();
        alertService.start();
        validationService.start();

        try{
            latch.await();
            System.out.println("All services are up, Application is stating now");
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
