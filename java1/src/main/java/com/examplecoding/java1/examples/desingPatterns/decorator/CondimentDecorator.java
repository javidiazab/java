package com.examplecoding.java1.examples.desingPatterns.decorator;

public abstract class CondimentDecorator extends Beberage{
    public abstract String getDescription();
}
