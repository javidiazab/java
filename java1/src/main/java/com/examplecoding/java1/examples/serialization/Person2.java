package com.examplecoding.java1.examples.serialization;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Person2 implements Serializable {
    private int age;
    private String name;
    private Address country; // must be serializable too
}
