package com.examplecoding.java1.examples.validator.validator2;

import com.examplecoding.java1.examples.validator.validator2.attribute.Account;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


import java.util.Set;

import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedNullValue;
import static java.util.Objects.nonNull;
import static org.junit.platform.commons.util.StringUtils.isNotBlank;


public class AccountValidator implements ConstraintValidator<ValidAccount, Account> {

    private boolean nullable;
    private Set<String> accountTypes;

    @Override
    public void initialize(ValidAccount validAccount) {
        nullable = validAccount.nullable();
        accountTypes = Set.of(validAccount.accountTypes());
    }

    public void initialize(String... accountTypes) {
        this.accountTypes = Set.of(accountTypes);
    }

    @Override
    public boolean isValid(Account account, ConstraintValidatorContext context) {
        return isValid(account);
    }

    public boolean isValid(Account account) {
        return checkAllowedNullValue(nullable, account) || checkValidAccount(account);
    }

    public boolean checkValidAccount(Account account) {
        return nonNull(account)
                && isNotBlank(account.getAccountId())
                && checkValidAccountTypeId(account.getAccountIdType());
    }

    private boolean checkValidAccountTypeId(String accountTypeId) {
        return isNotBlank(accountTypeId) && accountTypes.contains(accountTypeId);
    }
}
