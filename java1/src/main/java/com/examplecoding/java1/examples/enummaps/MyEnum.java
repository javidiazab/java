package com.examplecoding.java1.examples.enummaps;

import com.examplecoding.java1.examples.enums.Operation;

import java.util.Arrays;

public enum MyEnum {
    A("Letra A"),
    B("Letra B"),
    C("Letra C");

    private String desc;

    MyEnum(String desc) {
        this.desc = desc;
    }

    public static void main(String[] args) {
        MyEnum a = MyEnum.A;
        System.out.printf("%s es %s%n", a.name(), a.desc);

        for (MyEnum me : MyEnum.values())
            System.out.printf("MyEnum %s es la %s%n", me.name(), me.desc);
    }
}
