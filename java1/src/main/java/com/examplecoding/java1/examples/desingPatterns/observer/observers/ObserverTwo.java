package com.examplecoding.java1.examples.desingPatterns.observer.observers;

public class ObserverTwo implements Observer {
    @Override
    public void update(String newState) {
        System.out.println("ObserveTwo changed to state: " + newState);
    }
}
