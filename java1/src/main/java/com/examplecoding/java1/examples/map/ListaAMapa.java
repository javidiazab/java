package com.examplecoding.java1.examples.map;

import java.util.*;
import java.util.stream.Collectors;

public class ListaAMapa {
    public static void main(String[] args) {
        // Crear una lista de personas
        List<Persona> listaPersonas = Arrays.asList(
                new Persona(1, "Juan"),
                new Persona(2, "Ana"),
                new Persona(3, "Pedro")
        );

        // Convertir la lista en un mapa usando Collectors.toMap()
        Map<Integer, Persona> mapaPersonas = listaPersonas.stream()
                .collect(Collectors.toMap(Persona::getId, persona -> persona));

        // Imprimir el mapa
        mapaPersonas.forEach((id, persona) -> System.out.println("ID: " + id + ", Persona: " + persona.getNombre()));
    }
}

class Persona {
    private int id;
    private String nombre;

    public Persona(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}