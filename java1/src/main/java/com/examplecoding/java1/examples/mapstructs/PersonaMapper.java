package com.examplecoding.java1.examples.mapstructs;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PersonaMapper {

    //Haria falta esta linea?
    PersonaMapper INSTANCE = Mappers.getMapper(PersonaMapper.class);

    //Se pueden quitar porque se llaman igual
    @Mapping(source = "id", target = "id")
    @Mapping(source = "nombre", target = "nombre")
    @Mapping(source = "edad", target = "edad")
    PersonaDTO personaToPersonaDTO(Persona persona);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "nombre", target = "nombre")
    @Mapping(source = "edad", target = "edad")
    Persona personaDTOToPersona(PersonaDTO personaDTO);
}
