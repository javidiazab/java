package com.examplecoding.java1.examples.validator.validator2;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ProductValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidProduct {

    String message() default "Invalid product id specified";

    boolean nullable() default true;

    String[] allowedProductIds();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
