package com.examplecoding.java1.examples.configurationProperties;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "app.vbles-with-map2")
public class LoadVariablesWithMap2 {
    private Map<String, String> mappings;  //------->

    public static void main(String[] args) {
        LoadVariablesWithMap2 loadVariablesWithMap2 = new LoadVariablesWithMap2();
        System.out.println(loadVariablesWithMap2.getMappings());
    }
}
