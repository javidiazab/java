package com.examplecoding.java1.examples.sort;

import java.util.Arrays;
import java.util.Comparator;

public class OrdenarArrayPorColumnas {
    public static void main(String[] args) {
        String[] datos = {
                "Juan,25,Programador",
                "Ana,30,Ingeniera",
                "Pedro,28,Analista",
                "Maria,25,Ingeniera"
        };

        Arrays.sort(datos, new MultiColumnComparator());

        for (String fila : datos) {
            System.out.println(fila);
        }
    }
}

class MultiColumnComparator implements Comparator<String> {
    @Override
    public int compare(String fila1, String fila2) {
        // Dividir las filas en columnas (asumiendo una coma como separador)
        String[] columna1 = fila1.split(",");
        String[] columna2 = fila2.split(",");

        // Comparar por la primera columna (nombre) en orden ascendente
        int comparacionPorColumna1 = columna1[0].compareTo(columna2[0]);

        // Si las primeras columnas son iguales, compara por la segunda columna (edad) en orden descendente
        if (comparacionPorColumna1 == 0) {
            int edad1 = Integer.parseInt(columna1[1]);
            int edad2 = Integer.parseInt(columna2[1]);
            return Integer.compare(edad2, edad1); // Orden descendente
        }

        return comparacionPorColumna1;
    }
}