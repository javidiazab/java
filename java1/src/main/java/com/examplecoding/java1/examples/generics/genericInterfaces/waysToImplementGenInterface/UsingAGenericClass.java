package com.examplecoding.java1.examples.generics.genericInterfaces.waysToImplementGenInterface;

public class UsingAGenericClass<T> implements JGI<T> {

//    By Creating a Generic Class
//    One of the methods is to create a generic class that implements the Java generic
//    interface. The class definition will use the same formal type parameters two times, First, after the
//    class name and then after the interface name that is implemented.

    private String address;
    private T value;

    @Override
    public void setValue(T t, String AddressCode) {
        value = t;
        address = AddressCode;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public String getAddress() {
        return address;
    }
}
