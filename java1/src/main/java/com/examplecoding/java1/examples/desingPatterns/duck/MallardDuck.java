package com.examplecoding.java1.examples.desingPatterns.duck;

public class MallardDuck extends Duck{
    public MallardDuck() {
        this.flyBehavior = new FlyWithWings();
        this.quackBehavior = new Quak();
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard Duck");
    }
}
