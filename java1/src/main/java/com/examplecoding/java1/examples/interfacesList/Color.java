package com.examplecoding.java1.examples.interfacesList;

public enum Color {
    RED,
    BLUE,
    WHITE,
    BLACK,
    GREEN,
    YELLOW,
    BROWN
}
