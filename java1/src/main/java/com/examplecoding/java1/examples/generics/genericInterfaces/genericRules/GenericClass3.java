package com.examplecoding.java1.examples.generics.genericInterfaces.genericRules;

public class GenericClass3 implements JavaGenericInterface<String> {
    //A non-generic class can be used if a specific parameterized type is provided with the generic interface

    String name;

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        GenericClass3 user = new GenericClass3();
        user.setName("Javi");
        System.out.println("My name is " + user.getName());
    }
}
