package com.examplecoding.java1.examples.desingPatterns.observer.contracts;

import com.examplecoding.java1.examples.desingPatterns.observer.observers.Observer;

public interface Observable {
    void subscribe(Observer observer);
    void changeState(String newState);
}
