package com.examplecoding.java1.examples.sha256;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/*
SHA is the Secure Hash Algorithm. It uses a cryptographic function that takes up the 32-bit plain-text
 password and converts it into a fixed size 256-bit hash value. This hashing technique is implemented
 using the MessageDiagest class of java.security package.
It is a one-way encryption technique. Once the passphrase is encrypted it cannot be decrypted back.
* */
public class PassEnc {
    public static byte[] getSHA(String input) throws NoSuchAlgorithmException
    {
        /* MessageDigest instance for hashing using SHA256 */
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        /* digest() method called to calculate message digest of an input and return array of byte */
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public static String toHexString(byte[] hash)
    {
        /* Convert byte array of hash into digest */
        BigInteger number = new BigInteger(1, hash);

        /* Convert the digest into hex value */
        StringBuilder hexString = new StringBuilder(number.toString(16));

        /* Pad with leading zeros */
        while (hexString.length() < 32)
        {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }

    /* Driver code */
    public static void main(String args[])
    {
        try
        {
            String string1 = "zmfrracjdwafvced";
            System.out.println("\n" + string1 + " : " + toHexString(getSHA(string1)));

            String string2 = "hashtrial";
            System.out.println("\n" + string2 + " : " + toHexString(getSHA(string2)));
        }
        catch (NoSuchAlgorithmException e)
        {
            System.out.println("Exception thrown for incorrect algorithm: " + e);
        }
    }
}
