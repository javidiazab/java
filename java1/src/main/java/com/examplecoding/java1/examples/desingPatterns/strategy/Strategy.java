package com.examplecoding.java1.examples.desingPatterns.strategy;

public interface Strategy {
    void behaviour ();
}
