package com.examplecoding.java1.examples.validator.validator2;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = AgentValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidAgent {

    String message();

    boolean nullable() default true;

    boolean nullableBic() default false;

    int nameMaxLength() default 70;

    int addressMaxLength() default 242;

    int localInfoMaxLength() default 70;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
