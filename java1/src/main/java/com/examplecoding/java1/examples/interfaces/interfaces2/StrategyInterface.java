package com.examplecoding.java1.examples.interfaces.interfaces2;

public interface StrategyInterface {

    String execute(String paymentHubId, String hubMessage);

    String getEventName();

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
