package com.examplecoding.java1.examples.lambdas.Lamdas1;

@FunctionalInterface
public interface Printable {

    void print();

}
