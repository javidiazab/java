package com.examplecoding.java1.examples.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;


//en un Dto si se define un campo como boolean ya aparece por defecto un metodo para checkear is....

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class ClassDto {

    private static final String REGISTRY_CODE = "00";
    private static final String SERVICE_ID = "STI";
    private static final String ACCOUNTABLE_OPERATION = "C";
    private static final String NO_ACCOUNTABLE_OPERATION = "N";

    private String registerCode;
    private String serviceId;

    private boolean accountableOperation;
    private boolean justChecking;

    @NotBlank(message = "Entity BIC is required")
    @Size(min = 1, max = 11, message = "Invalid entity BIC size")
    private String entityBic;

    @NotNull(message = "Liquidation date is required")
    @PastOrPresent(message = "Liquidation date is not valid")
    private LocalDate liquidationDate;

    @Min(value = 0, message = "Invalid cycle")
    @Max(value = 8, message = "Invalid cycle")
    private int cycle;

    @NotNull(message = "File creation is required")
    @PastOrPresent(message = "File creation is not valid")
    private LocalDateTime fileCreation;

    public static void main(String[] args) {
        new ClassDto().isAccountableOperation();
        new ClassDto().isJustChecking();
    }

}
