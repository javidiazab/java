package com.examplecoding.java1.examples.validator.validator2.attribute;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DebtorAmlInformation {
    private Address debtorAddress;
    private LocalDate debtorBirthDate;
    private String debtorBirthPlace;
    private String debtorBirthCountry;
    private DocumentAmlInformation debtorDocument;
    private String debtorCustomerId;
}
