package com.examplecoding.java1.examples.interfacesList;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class Car extends Vehicle {
    private static final int numWheels = 4;

    public Car(LocalDate registrationDate, String numPlate, int numPassengers, String engine, Color color, String owner, LocalDate lastDateItv) {
        super(registrationDate, numPlate, numPassengers, engine, color, numWheels, owner, lastDateItv);
    }

    @Override
    void run() {
        super.run();
    }

    @Override
    void stop() {
        super.stop();
    }
}
