package com.examplecoding.java1.examples.jackson;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties({ "id" })
public class BeanWithIgnoreProperties {
    public int id;
    public String name;

    public BeanWithIgnoreProperties(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static void main(String[] args) throws JsonProcessingException {
        BeanWithIgnoreProperties bean = new BeanWithIgnoreProperties(1, "My bean");

        String result = new ObjectMapper()
                .writeValueAsString(bean);
        System.out.println("result: " + result);
    }
}
