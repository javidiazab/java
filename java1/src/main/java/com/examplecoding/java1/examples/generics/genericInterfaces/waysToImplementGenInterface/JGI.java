package com.examplecoding.java1.examples.generics.genericInterfaces.waysToImplementGenInterface;

public interface JGI<T> {

    void setValue(T t, String AddressCode);

    T getValue();

    String getAddress();
}
