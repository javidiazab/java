package com.examplecoding.java1.examples.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BeanWithJsonInclude {
    public int id;
    public String name;

    public BeanWithJsonInclude(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static void main(String[] args) throws JsonProcessingException {
        BeanWithJsonInclude bean = new BeanWithJsonInclude(1, null);

        String result = new ObjectMapper()
                .writeValueAsString(bean);
        System.out.println("result: " + result);
    }
}
