package com.examplecoding.java1.examples.mapstructs;

import lombok.Data;

@Data
public class PersonaDTO {
    private Long id;
    private String nombre;
    private int edad;

    // Getters y setters
}
