package com.examplecoding.java1.examples.desingPatterns.decorator;

public abstract class Beberage {
    String description = "Unknown Beberage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
