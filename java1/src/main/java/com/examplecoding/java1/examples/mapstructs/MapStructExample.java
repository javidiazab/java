package com.examplecoding.java1.examples.mapstructs;

import org.mapstruct.factory.Mappers;

public class MapStructExample {

    public static void main(String[] args) {
        //Manera 1 de utilizarlo

        // Crear una instancia del mapeador
        PersonaMapper mapper = Mappers.getMapper(PersonaMapper.class);

// Mapear de Persona a PersonaDTO
        Persona persona = new Persona();
        persona.setId(1L);
        persona.setNombre("Juan");
        persona.setEdad(30);

        PersonaDTO personaDTO = mapper.personaToPersonaDTO(persona);

// Mapear de PersonaDTO a Persona
        PersonaDTO personaDTO2 = new PersonaDTO();
        personaDTO2.setId(2L);
        personaDTO2.setNombre("Ana");
        personaDTO2.setEdad(25);

        Persona persona2 = mapper.personaDTOToPersona(personaDTO2);

        //Manera 2 de utilizarlo
        Persona persona1 = PersonaMapper.INSTANCE.personaDTOToPersona(personaDTO);

        //Manera 3 de utilizarlo
        PersonaMapper instance = PersonaMapper.INSTANCE;
        instance.personaToPersonaDTO(persona1);
    }
}
