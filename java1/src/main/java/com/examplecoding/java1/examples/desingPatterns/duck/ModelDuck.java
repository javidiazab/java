package com.examplecoding.java1.examples.desingPatterns.duck;

public class ModelDuck extends Duck{
    public ModelDuck() {
        this.flyBehavior = new FlyNoWay();
        this.quackBehavior = new Quak();
    }

    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}
