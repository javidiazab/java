package com.examplecoding.java1.examples.desingPatterns.duck;

public class FlyWithWings implements  FlyBehavior{

    public void fly(){
        System.out.println("I'm flying");
    }
}
