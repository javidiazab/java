package com.examplecoding.java1.examples.desingPatterns.decorator;

public class StarbuzzCoffee {
    public static void main(String[] args) {
        Beberage beberage = new Espresso();
        System.out.println(beberage.getDescription() + " $" + beberage.cost());

        Beberage beberage2 = new DarkRoast();
        beberage2 = new Mocha(beberage2);
        beberage2 = new Mocha(beberage2);
        beberage2 = new Whip(beberage2);
        beberage2 = new Soy(beberage2);
        System.out.println(beberage2.getDescription() + " $" + beberage2.cost());

//        Beberage beberage3 = new Whip(new Soy(new Mocha(new HouseBlend())));
//        System.out.println(beberage3.getDescription() + " $" + beberage3.cost());

    }
}
