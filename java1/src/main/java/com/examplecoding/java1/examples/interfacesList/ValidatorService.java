package com.examplecoding.java1.examples.interfacesList;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ValidatorService {
    private final List<Validator> validatorList;

    public void process(Vehicle vehicle) {
        validateVehicle(vehicle);
    }

    private void validateVehicle(Vehicle vehicle) {
        List<Validator> allowedVehicles = validatorList.stream()
                .filter(x -> x.allowVehicle(vehicle))
                .collect(Collectors.toList());

        for (var validator : allowedVehicles) {
            validator.validate(vehicle);
        }
    }
}
