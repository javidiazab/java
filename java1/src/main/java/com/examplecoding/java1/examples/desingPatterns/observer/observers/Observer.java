package com.examplecoding.java1.examples.desingPatterns.observer.observers;

public interface Observer {
    void update(String newState);
}
