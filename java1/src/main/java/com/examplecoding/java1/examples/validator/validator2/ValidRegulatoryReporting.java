package com.examplecoding.java1.examples.validator.validator2;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = RegulatoryReportingValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidRegulatoryReporting {

    String message();

    boolean nullable() default true;

    String[] codeTypes() default {"BEN", "OUR", "SHA"};

    int narrativeMaxLength() default 93;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
