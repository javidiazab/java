package com.examplecoding.java1.examples.validator.validator2;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class AmountValidator implements ConstraintValidator<ValidAmount, PaymentAmount> {

    private boolean nullable;

    @Override
    public void initialize(ValidAmount validAmount) {
        nullable = validAmount.nullable();
    }

    @Override
    public boolean isValid(PaymentAmount paymentAmount, ConstraintValidatorContext context) {
        return isValid(paymentAmount);
    }

    public boolean isValid(PaymentAmount paymentAmount) {
        return checkAllowedNullValue(nullable, paymentAmount) || checkValidAmount(paymentAmount);
    }

    public boolean checkValidAmount(PaymentAmount paymentAmount) {
        return nonNull(paymentAmount)
                && isNotBlank(paymentAmount.getCurrency())
                && checkAllowedAmount(paymentAmount.getAmount());
    }
}
