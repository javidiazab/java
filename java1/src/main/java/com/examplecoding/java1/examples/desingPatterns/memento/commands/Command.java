package com.examplecoding.java1.examples.desingPatterns.memento.commands;

public interface Command {
    String getName();
    void execute();
}
