package com.examplecoding.java1.examples.miscelaneous;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    private String textToModify = "1aaaok";
    private static final String REGEX_STRING = "^[1|2|3]a{3,}";

    public String execute() {
        Pattern pattern = Pattern.compile(REGEX_STRING, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(textToModify);
        boolean matchFound = matcher.find();
        if(matchFound) {
            System.out.println(matcher.replaceAll("modified-"));
            return matcher.replaceAll("modified-");
        } else {
            return "Match not found";
        }
    }
}
