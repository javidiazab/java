package com.examplecoding.java1.examples.interfacesList;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
public abstract class Vehicle {
    private LocalDate registrationDate;
    private String numPlate;
    private int numPassengers;
    private String engine;
    private Color color;
    private int numWheels;
    private String owner;
    private LocalDate itvLastItv;

    void run() {System.out.println("running with a vehicle");}
    void stop() {System.out.println("stopping with a vehicle");}
}
