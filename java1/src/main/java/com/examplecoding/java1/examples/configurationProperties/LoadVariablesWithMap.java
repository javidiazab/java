package com.examplecoding.java1.examples.configurationProperties;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "app.vbles-with-map")
public class LoadVariablesWithMap {
    private SecurityProperties security;
    private Map<FilterName, FilterProperties> mappings;  //------->

    @Data
    public static class FilterProperties {
        private String pattern;
        private String ibpUrl;
        private String sanesUrl;
        private boolean secured;
    }

    @Data
    public static class SecurityProperties {
        private String[] ignored;
    }

    public enum FilterName {
        DISP_DIFU("dispDifuRoute"),
        EXAMPLE_GET("exampleRoute");

        @Getter
        private final String routeName;

        FilterName(String routeName) {
            this.routeName = routeName;
        }
    }

    public static void main(String[] args) {
        LoadVariablesWithMap loadVariablesWithMap = new LoadVariablesWithMap();
        System.out.println(loadVariablesWithMap.getMappings());
    }
}
