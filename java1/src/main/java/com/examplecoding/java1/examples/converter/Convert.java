package com.examplecoding.java1.examples.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

@Component
public class Convert {
    //Este ejemplo usa el conversionService de spring
    @Autowired
    ConversionService conversionService;

    public B convert() {
        B b = conversionService.convert(new A("1", "2"), B.class);
        System.out.println(b);
        return b;
    }

}
