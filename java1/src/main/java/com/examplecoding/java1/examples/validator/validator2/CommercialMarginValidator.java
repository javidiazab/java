package com.examplecoding.java1.examples.validator.validator2;


import com.examplecoding.java1.examples.validator.validator2.attribute.FxComercialMargin;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedAmountOrZero;
import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedNullValue;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class CommercialMarginValidator implements ConstraintValidator<ValidCommercialMargin, FxComercialMargin> {

    private boolean nullable;

    @Override
    public void initialize(ValidCommercialMargin validCommercialMargin) {
        nullable = validCommercialMargin.nullable();
    }

    @Override
    public boolean isValid(FxComercialMargin amount, ConstraintValidatorContext context) {
        return isValid(amount);
    }

    public boolean isValid(FxComercialMargin amount) {
        return checkAllowedNullValue(nullable, amount) || checkValidAmount(amount);
    }

    public boolean checkValidAmount(FxComercialMargin amount) {
        return nonNull(amount)
                && isNotBlank(amount.getFxComercialMarginCurrency())
                && checkAllowedAmountOrZero(amount.getFxComercialMarginAmount());
    }
}
