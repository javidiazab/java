package com.examplecoding.java1.examples.miscelaneous;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Lambda {

    public boolean execute() {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);
        Consumer<Integer> method = (n) -> System.out.println(n);
        //se puede sustituir por esto: Consumer<Integer> method = System.out::println;
        numbers.forEach(method);
        System.out.println("Ahora printNumbers");
        numbers.stream().forEach(printNumbers);


        //----------------------------------------

        StringFunction exclaim = (s) -> s + "!";
        StringFunction ask = (s) -> s + "?";
        printFormatted("Hello", exclaim);
        printFormatted("Hello", ask);

        //--------------------------------------

        List<String> names = Arrays.asList("bob", "josh", "megan");
        names.replaceAll(name -> name.toUpperCase());

        //-----------------------------------------

        List<Integer> values = Arrays.asList(3, 5, 8, 9, 12);
        int sum = values.stream().reduce(0, (i1, i2) -> i1 + i2);

        //----------------------------------------

        Map<String, Integer> nameMap = new HashMap<>();
        Integer value = nameMap.computeIfAbsent("John", s -> s.length());

        //-----------------------------------------
        //Predicate
        List<String> names1 = Arrays.asList("Angela", "Aaron", "Bob", "Claire", "David");

        List<String> namesWithA = names1.stream()
                .filter(name -> name.startsWith("A"))
                .collect(Collectors.toList());

        //--------------------------

        //------------------------------------------
        ArrayList<Integer> numbers1 = new ArrayList<Integer>();
        numbers1.add(-1);
        numbers1.add(-2);
        numbers1.add(0);
        numbers1.add(1);
        numbers1.add(2);
        Predicate<Integer> method1 = p -> p > 0;
        System.out.println("Ahora method1");
        numbers1.stream().filter(method1)
                .collect(Collectors.toList())
                .forEach(System.out::println);
        System.out.println("Ahora isgreaterThan");
        numbers1.stream().filter(isGreaterThan0())
                .forEach(System.out::println);

        //------------------------------------------
        ArrayList<Integer> numbers2 = new ArrayList<Integer>();
        numbers2.add(0);
        numbers2.add(1);
        numbers2.add(2);
        numbers2.add(3);
        System.out.println("Ahora method2");
        Function<Integer, Integer> method2 = (n) -> n += 100;
        numbers2.stream().map(method2)
                .collect(Collectors.toList())
                .forEach(System.out::println);
        System.out.println("Ahora plus100");
        numbers2.stream().map(plus100())
                .forEach(System.out::println);


        //--------------------------------------------
        return true;
    }

    public static Predicate<Integer> isGreaterThan0() {return p -> p > 0 ? true: false;}

    public static Function<Integer, Integer> plus100() {return (n) -> n += 100;}

    public static Consumer<Integer> printNumbers = (n) -> System.out.println(n);

    public static Supplier<LocalDateTime> SupplierDateTime = () -> LocalDateTime.now();

    public double squareLazy(Supplier<Double> lazyValue) {
        return Math.pow(lazyValue.get(), 2);
    }

    public static void printFormatted(String str, StringFunction format) {
        String result = format.run(str);
        System.out.println(result);
    }

    public interface StringFunction {
        String run(String str);
    }

//    public Supplier<List<T>> supplier() {
//        return ArrayList::new;
//    }
}
