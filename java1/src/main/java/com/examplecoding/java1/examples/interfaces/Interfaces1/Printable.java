package com.examplecoding.java1.examples.interfaces.Interfaces1;

public interface Printable {
    boolean print();

    default void processMessage() {
        System.out.println("Se rompe la impresora");
    }
}
