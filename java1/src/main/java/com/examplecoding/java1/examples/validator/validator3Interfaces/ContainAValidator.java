package com.examplecoding.java1.examples.validator.validator3Interfaces;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ContainAValidator implements Validator{


    @Override
    public void validate(String value) throws Exception {
        List<String> collect = Arrays.stream(value.split(""))
                .toList();
        if (collect.size() < 1) throw new Exception("El numero de 'a's es menor de 2");

    }

    @Override
    public boolean allowValidation(String value) {
        return value.contains("a");
    }
}
