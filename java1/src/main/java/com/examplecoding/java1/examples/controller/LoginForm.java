package com.examplecoding.java1.examples.controller;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LoginForm {
    private String username;
    private String password;
}
