package com.examplecoding.java1.examples.interfacesList;

import org.springframework.stereotype.Component;

@Component
public class BusValidator implements Validator {
    @Override
    public boolean validate(Vehicle vehicle) {
        System.out.println("Validating a Bus");
        return vehicle.getNumPassengers() > 7;
    }
}
