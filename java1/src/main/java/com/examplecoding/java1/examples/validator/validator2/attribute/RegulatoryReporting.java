package com.examplecoding.java1.examples.validator.validator2.attribute;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegulatoryReporting {

    private String regulatoryReportingTypeCode;
    private String country;
    private String narrative;
}
