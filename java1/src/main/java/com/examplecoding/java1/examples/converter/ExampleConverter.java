package com.examplecoding.java1.examples.converter;

import lombok.Builder;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ExampleConverter implements Converter<A, B> {

    @Override
    public B convert(A source) {
        return B.builder()
                .a(source.getA())
                .b(source.getB())
                .build();
    }

}

@Data
@Builder
class A {
    String a;
    String b;
}

@Data
@Builder
class B {
    String a;
    String b;
}
