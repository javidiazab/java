package com.examplecoding.java1.examples.validator.validator2;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static uk.co.santander.gtscommons.validation.ValidationUtils.checkAllowedNullValue;

public class ISOCountryCodeValidator implements ConstraintValidator<ValidISOCountryCode, String> {

    private static final Set<String> ISO_COUNTRIES = new HashSet<>(Arrays.asList(Locale.getISOCountries()));

    private boolean nullable;

    @Override
    public void initialize(ValidISOCountryCode validISOCountryCode) {
        nullable = validISOCountryCode.nullable();
    }

    @Override
    public boolean isValid(String countryCode, ConstraintValidatorContext constraintValidatorContext) {
        return isValid(countryCode);
    }

    public boolean isValid(String countryCode) {
        return checkAllowedNullValue(nullable, countryCode) || checkValidCountryCode(countryCode);
    }

    private boolean checkValidCountryCode(String countryCode) {
        return isNotBlank(countryCode) && ISO_COUNTRIES.contains(countryCode);
    }
}
