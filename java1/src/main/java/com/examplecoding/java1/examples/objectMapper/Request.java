package com.examplecoding.java1.examples.objectMapper;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class Request {
    private Car car;
    private Date datePurchased;

    // standard getters setters
}
