package com.examplecoding.java1.examples.validator.validator2.attribute;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

import static javax.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    @Pattern(regexp = "home|work|tax|correspondence",
            flags = CASE_INSENSITIVE,
            message = "Invalid addressType. Allowed values: home, work, tax and correspondence")
    private String addressType;
    private String streetName;
    private String streetBuildingIdentification;
    private String postCodeIdentification;
    private String townName;
    private String state;
    private String buildingName;
    private String floor;
    private String districtName;
    private String regionIdentification;
    private String countyIdentification;
    private String postOfficeBox;
    private String province;
    private String department;
    private String subDepartment;
    private String location;
    private String country;
}
