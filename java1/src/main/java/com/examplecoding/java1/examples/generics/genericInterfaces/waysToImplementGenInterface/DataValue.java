package com.examplecoding.java1.examples.generics.genericInterfaces.waysToImplementGenInterface;

public class DataValue implements JGI<String> {

//    Create A Class to use Non-generic Types
//    By using a class, the actual type can be used instead of using formal type parameters.
//    For instance, can use <Data> instead of <T>. Despite using a Java Generic Interface,
//    it allows using the formal type parameter that is a non-generic type.

//    If a class implements a specific type of generic interface the implementing class does not need to be generic


    private String address;
    private String value;

    @Override
    public void setValue(String t, String addressCode) {
        value = t;
        address = addressCode;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getAddress() {
        return address;
    }
}
