package com.examplecoding.java1.examples.validator.validator2;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = AccountValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidAccount {

    String message();

    boolean nullable() default true;

    String[] accountTypes() default {"IBA", "BBA", "IBAN", "BBAN", "TXT"};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
