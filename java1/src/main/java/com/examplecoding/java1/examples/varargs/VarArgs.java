package com.examplecoding.java1.examples.varargs;

import java.util.Set;

public class VarArgs {
    private Set<String> accountTypes;

    public void initialize(String... accountTypes) {
        this.accountTypes = Set.of(accountTypes);
    }

    public void print() {
        this.accountTypes.forEach(System.out::println);
    }
}
