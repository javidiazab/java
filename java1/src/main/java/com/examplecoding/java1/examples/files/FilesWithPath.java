package com.examplecoding.java1.examples.files;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static java.nio.file.StandardOpenOption.APPEND;

public class FilesWithPath {

    public static void main(String[] args) throws IOException {
        //
        InputStream inputStream = FileUtils.class.getClassLoader().getResourceAsStream("static/FilePath.txt");
        //TODO
        //IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());

        //Retrives from file system
        Path path0 = Paths.get("/Users/n90578/Documents/WksJavi/java1/src/main/resources/static/FilePath.txt");
        Path path0b = Paths.get("/Users/n90578/Documents/WksJavi/java1/src/main/resources/static/new.txt");
        Path path1 = Paths.get("/Users/n90578/Documents/WksJavi/java1/src/main/resources/static", "FilePath.txt");
        Path path2 = Paths.get("/Users/n90578/Documents/WksJavi/java1/src/main/resources/static").resolve("FilePath.txt");
        Path path3 = Paths.get(".");
        Path path4 = Paths.get("..");
        System.out.println(".: " + path3.toFile().getAbsolutePath());
        path3.forEach(System.out::println);
        System.out.println("---------------");
        System.out.println("..: " + path4.toFile().getAbsolutePath());
        path4.forEach(System.out::println);
        System.out.println("---------------");

        System.out.println(path0.toFile().getAbsolutePath());
        //Files.createFile(path0b);
        System.out.println("Last ModifiedDate: " + Files.getLastModifiedTime(path0b));
        Files.writeString(path0b, "Hola\n", APPEND);

        FileTime lastModifiedTime = Files.getLastModifiedTime(path0b);
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime convertedFileTime = LocalDateTime.ofInstant(lastModifiedTime.toInstant(), ZoneId.systemDefault());
        System.out.println("lastModifiedTime: " + lastModifiedTime);
        System.out.println("convertedFileTime: " + convertedFileTime);


        //Retrives from jar/war con resourceUtils
        File file = ResourceUtils.getFile("classpath:static/FilePath.txt");
        Path path5 = file.toPath();
        path5.forEach(System.out::println);
        System.out.println("=======================");
        String s = Files.readString(path5);
        System.out.println(s);

        System.out.println("2***********************");
        System.out.println("path0 " + path0.toAbsolutePath() + " " + Files.isRegularFile(path0));
        System.out.println("path1 " + path1.toAbsolutePath() + " " + Files.isRegularFile(path1));
        System.out.println("path2 " + path2.toAbsolutePath() + " " + Files.isRegularFile(path2));
        System.out.println("path3 " + path3.toAbsolutePath() + " " + Files.isDirectory(path3));
        System.out.println("3***********************");

//        System.out.println("is file " + path.toFile().isFile());
//        System.out.println("is file " + Paths.get("static").toFile().isFile());

//        Stream<String> lines = Files.lines(path);
//        lines.forEach(System.out::println);
//        System.out.println(Files.exists(path));
//        Files.isDirectory(Paths.get("files"));

        System.out.println("---------------------");

        //List the directory content
        DirectoryStream<Path> paths = Files.newDirectoryStream(path3);
        paths.forEach(System.out::println);
//        DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get("files/"));
//        paths.forEach(System.out::println);
    }
}
