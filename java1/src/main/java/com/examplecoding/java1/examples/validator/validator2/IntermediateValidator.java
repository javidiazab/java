package com.examplecoding.java1.examples.validator.validator2;

import uk.co.santander.gtscommons.model.api.attribute.Account;
import uk.co.santander.gtscommons.model.api.attribute.Agent;
import uk.co.santander.gtscommons.model.api.attribute.Intermediate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static uk.co.santander.gtscommons.validation.ValidationUtils.checkAllowedNullValue;

public class IntermediateValidator implements ConstraintValidator<ValidIntermediate, Intermediate> {

    private boolean nullable;
    private AccountValidator accountValidator;
    private AgentValidator agentValidator;

    @Override
    public void initialize(ValidIntermediate validIntermediate) {
        nullable = validIntermediate.nullable();

        accountValidator = new AccountValidator();
        accountValidator.initialize(validIntermediate.accountTypes());

        agentValidator = new AgentValidator();
        agentValidator.initialize(validIntermediate.agentNameMaxLength(), validIntermediate.agentAddressMaxLength());
    }

    @Override
    public boolean isValid(Intermediate intermediate, ConstraintValidatorContext context) {
        return checkAllowedNullValue(nullable, intermediate) || checkValidIntermediate(intermediate);
    }

    private boolean checkValidIntermediate(Intermediate intermediate) {
        return nonNull(intermediate)
                && validateAccount(intermediate)
                && validateAgent(intermediate);
    }

    public boolean validateAccount(Intermediate intermediate) {
        final Account intermediateAccount = intermediate.getIntermediateAccount();
        return isNull(intermediateAccount) || accountValidator.isValid(intermediateAccount);
    }

    public boolean validateAgent(Intermediate intermediate) {
        final Agent agent = generateAgent(intermediate);
        return agentValidator.isValid(agent);
    }

    private Agent generateAgent(Intermediate intermediate) {
        return Agent.builder()
                .agent(intermediate.getAgent())
                .agentName(intermediate.getAgentName())
                .addressAgent(intermediate.getAddressAgent())
                .build();
    }
}
