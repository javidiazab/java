package com.examplecoding.java1.examples.configurationProperties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "app.my-variables")
public class LoadVariables {

    private List<String> servers;

    public static void main(String[] args) {
        LoadVariables loadVariables = new LoadVariables();
        System.out.println(loadVariables.getServers());
    }

}
