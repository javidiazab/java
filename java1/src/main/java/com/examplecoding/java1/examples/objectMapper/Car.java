package com.examplecoding.java1.examples.objectMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class Car {

    private final String color;
    private final String type;

    public String setColor(String color) {
        return this.color;
    }
}
