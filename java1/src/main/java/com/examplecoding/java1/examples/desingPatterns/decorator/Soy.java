package com.examplecoding.java1.examples.desingPatterns.decorator;

public class Soy extends CondimentDecorator{
    Beberage beberage;

    public Soy(Beberage beberage) {
        this.beberage = beberage;
    }

    @Override
    public double cost() {
        return .15 + beberage.cost();
    }

    @Override
    public String getDescription() {
        System.out.println("antes de get description de soy");
        String s = beberage.getDescription() + ", Soy";
        System.out.println("devolviendo descripcion de soy");
        return s;
    }
}
