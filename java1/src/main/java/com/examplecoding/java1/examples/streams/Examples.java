package com.examplecoding.java1.examples.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

public class Examples {
    public static void main(String[] args) {

        String[] arrayOfString = new String[]{"one", "two", "three", "four"};
        //Joining Strings
        Arrays.stream(arrayOfString)
                //.map(...)
                .collect(Collectors.joining(","));

        Arrays.asList(arrayOfString)
                .stream()
                //.map(...)
                .collect(Collectors.joining(",", "[", "]"));

// Splitting Strings
        String str = "Hola";

        Stream.of(str.split(","))
                .map(elem -> new String(elem))
                .collect(Collectors.toList());


        str.chars()
                .mapToObj(item -> (char) item)
                .collect(Collectors.toList());

        //String Array to Map With Stream API
        Arrays.asList(arrayOfString)
                .stream()
                .map(str1 -> str1.split(":"))
                .collect(toMap(str1 -> str1[0], str1 -> str1[1]));

        //Adding elements
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);
        Integer[] items = {1, 2, 3, 4, 5};
        Integer sum1 = integers.stream()
                .reduce(0, (a, b) -> a + b);

        Integer sum2 = integers.stream()
                .reduce(0, Integer::sum);


        Integer sum4 = integers.stream()
                .collect(Collectors.summingInt(Integer::intValue));

        Integer sum5 = integers.stream()
                .mapToInt(Integer::intValue)
                .sum();

        Integer sum8 = integers.stream()
                .reduce(0, Integer::sum);

        Integer sum9 = integers.stream()
                .reduce(0, (a, b) -> a + b);

        Integer sum10 = integers.stream()
                .collect(Collectors.summingInt(Integer::intValue));

        //Reducing
        //1 * 2 * 3 * 4 = 24
        int product = IntStream.range(1, 5)
                .reduce((num1, num2) -> num1 * num2)
                .orElse(-1);
        //1 + 2 + 3 + 4 = 10
        int sum =  IntStream.range(1, 5).sum();
        System.out.println(product);
        System.out.println(sum);

    }


    //Tests ----------------------

    @Test
    public void givenArray_transformedToStream_convertToString() {
        String[] programmingLanguages = {"java", "python", "nodejs", "ruby"};
        String expectation = "java,python,nodejs,ruby";

        String result = String.join("", programmingLanguages);
        assertEquals(result, expectation);
    }
//        Copy
//        Next, let's create another one to test our simple splitting functionality:

    @Test
    public void givenString_transformedToStream_convertToList() {
        String programmingLanguages = "java,python,nodejs,ruby";

        List<String> expectation = new ArrayList<>();
        expectation.add("java");
        expectation.add("python");
        expectation.add("nodejs");
        expectation.add("ruby");

        List<String> result = Arrays.stream(programmingLanguages.split(",")).collect(Collectors.toList());

        assertEquals(result, expectation);
    }

//        Copy
//        Finally, let's test our String array to map functionality:

    @Test
    public void givenStringArray_transformedToStream_convertToMap() {

        String[] programming_languages = new String[]{"language:java", "os:linux", "editor:emacs"};

        Map<String, String> expectation = new HashMap<>();
        expectation.put("language", "java");
        expectation.put("os", "linux");
        expectation.put("editor", "emacs");

//        Map<String, String> result = JoinerSplitter.arrayToMap(programming_languages);
        Map<String, String> result = Map.of("language", "java", "os", "linux", "editor", "emacs");
                assertEquals(result, expectation);

    }

}


