package com.examplecoding.java1.examples.interfaces.Interfaces1;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Epson implements Printable{
    @Override
    public boolean print() {
        System.out.println("Printing from an Epson");
        return true;
    }
}
