package com.examplecoding.java1.examples.desingPatterns.duck;

public class FlyRocketPowered implements  FlyBehavior{

    public void fly(){
        System.out.println("I'm flying with a rocket!");
    }
}
