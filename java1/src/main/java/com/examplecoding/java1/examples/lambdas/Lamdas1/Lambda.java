package com.examplecoding.java1.examples.lambdas.Lamdas1;

public class Lambda {
    public static void main(String[] args) {
        printThing(() -> System.out.println("Prueba1"));

        Printable lambdaPrintable = () -> System.out.println("Prueba2");
        printThing(lambdaPrintable);
    }


    static void printThing(Printable thing) {
        thing.print();
    }
}
