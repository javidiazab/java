package com.examplecoding.java1.examples.generics.genericInterfaces.sample1;

class Class1<T extends Comparable<T>> implements SmallestLargest<T> {
    T[] values;

    //Constructor
    public Class1(T[] values) {
        this.values = values;
    }

// Definition of findSmallest() and findLargest() methods
    public T findSmallest() {
        T value = values[0];
        for (int i = 1; i < values.length; i++)
            if (values[i].compareTo(value) < 0)
                value = values[i];
        return value;
    }

    public T findLargest() {
        T value = values[0];
        for (int i = 1; i < values.length; i++)
            if (values[i].compareTo(value) > 0)
                value = values[i];
        return value;
    }

    public static void main(String[] args) {
        Integer myArr[] = {88, 21, 63, 51, 95, 23, 97};
        SmallestLargest<Integer> obj1 = new Class1<>(myArr);
        System.out.println("Smallest value is " + obj1.findSmallest());
        System.out.println("Largest value is " + obj1.findLargest());
    }
}
