package com.examplecoding.java1.examples.interfacesList;

import org.springframework.stereotype.Component;

@Component
public class BikeValidator implements Validator {
    @Override
    public boolean validate(Vehicle vehicle) {
        System.out.println("Validating a Bike");
        return vehicle.getNumPassengers() > 7;
    }
}
