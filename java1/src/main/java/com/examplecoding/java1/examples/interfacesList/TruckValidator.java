package com.examplecoding.java1.examples.interfacesList;

import org.springframework.stereotype.Component;

@Component
public class TruckValidator implements Validator{
    @Override
    public boolean validate(Vehicle vehicle) {
        System.out.println("Validating a truck");
        return vehicle.getNumWheels() > 4;
    }
}
