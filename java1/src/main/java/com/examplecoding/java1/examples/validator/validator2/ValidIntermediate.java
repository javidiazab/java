package com.examplecoding.java1.examples.validator.validator2;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = IntermediateValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidIntermediate {

    String message();

    boolean nullable() default true;

    String[] accountTypes() default {"IBA", "BBA", "IBAN", "BBAN", "TXT"};

    int agentNameMaxLength() default 70;

    int agentAddressMaxLength() default 242;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
