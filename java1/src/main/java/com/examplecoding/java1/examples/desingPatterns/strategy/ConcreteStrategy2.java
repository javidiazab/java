package com.examplecoding.java1.examples.desingPatterns.strategy;

public class ConcreteStrategy2 implements Strategy {
    @Override
    public void behaviour() {
        System.out.println("Estrategia B");
    }
}
