package com.examplecoding.java1.examples.interfaces.Interfaces1;

import org.springframework.stereotype.Component;

@Component
public class Hp implements Printable{
    @Override
    public boolean print() {
        System.out.println("Printing from an Hp");
        return true;
    }
}
