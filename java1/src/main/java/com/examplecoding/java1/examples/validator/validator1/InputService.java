package com.examplecoding.java1.examples.validator.validator1;


import jakarta.validation.Valid;

public class InputService {

    public boolean process (@Valid ModelDto modelDto) {
        System.out.println(modelDto.toString());
        return true;
    }
}
