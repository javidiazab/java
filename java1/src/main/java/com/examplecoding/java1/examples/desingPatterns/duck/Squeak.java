package com.examplecoding.java1.examples.desingPatterns.duck;

public class Squeak implements com.examplecoding.java1.examples.duck.QuackBehavior {

    public void quack(){
        System.out.println("Squeak");
    }
}
