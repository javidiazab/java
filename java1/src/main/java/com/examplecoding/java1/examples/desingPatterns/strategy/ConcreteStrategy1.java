package com.examplecoding.java1.examples.desingPatterns.strategy;

public class ConcreteStrategy1 implements Strategy {
    @Override
    public void behaviour() {
        System.out.println("Estrategia A");
    }
}
