package com.examplecoding.java1.examples.map;

import lombok.val;

import java.util.HashMap;
import java.util.Map;

public class EjemploMap {

    public static void main(String[] args) {

        Map<String, String> clients = new HashMap<>();
        clients.put("unoKey", "unoValue");
        clients.put("dosKey", "dosValue");
        clients.put("tresKey", "tresValue");

        for (var entry : clients.entrySet()) {   //esto es necesario porque map no es una collection ni extiende de iterable
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
    }

}
