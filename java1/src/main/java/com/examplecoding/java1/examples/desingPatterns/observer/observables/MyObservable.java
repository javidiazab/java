package com.examplecoding.java1.examples.desingPatterns.observer.observables;


import com.examplecoding.java1.examples.desingPatterns.observer.contracts.Observable;
import com.examplecoding.java1.examples.desingPatterns.observer.observers.Observer;

import java.util.ArrayList;
import java.util.List;

public class MyObservable implements Observable {
    private final List<Observer> observers = new ArrayList<>();

    @Override
    public void subscribe(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void changeState(String newState) {
        observers.forEach(o -> o.update(newState));
    }
}
