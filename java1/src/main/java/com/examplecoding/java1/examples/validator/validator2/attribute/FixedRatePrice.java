package com.examplecoding.java1.examples.validator.validator2.attribute;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FixedRatePrice {

    private BigDecimal rate;
    private BigDecimal minimumAmount;
    private BigDecimal maximumAmount;
    private String currency;
}
