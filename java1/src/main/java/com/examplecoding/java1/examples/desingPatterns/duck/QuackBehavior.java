package com.examplecoding.java1.examples.desingPatterns.duck;

public interface QuackBehavior {
    void quack();
}
