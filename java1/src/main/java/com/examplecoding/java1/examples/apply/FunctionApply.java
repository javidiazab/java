package com.examplecoding.java1.examples.apply;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionApply {

    public static void main(String[] args) {
        List<StudentA> list = Arrays.asList(
                new StudentA("Ram", 20),
                new StudentA("Shyam", 22),
                new StudentA("Kabir", 18));

        // Simple use of function
        for (StudentA st : list) {
            System.out.println("style 0: " + st.customShow(s -> s.name + ": " + s.age));
        }

        //Style one to declare function
        Function<StudentA, String> styleOne = s -> {
            String result = "Name style 1:" + s.name + " and Age:" + s.age;
            return result;
        };

        //Style two to declare function
        Function<StudentA, String> styleTwo = s ->
                "Name style 2:" + s.name + " and Age:" + s.age;

        System.out.println("--print value by style one--");
        //print the values of list using style one function
        for (StudentA st : list) {
            System.out.println("style 1: " + st.customShow(styleOne));
        }

        System.out.println("--print value by style two--");
        //print the values of list using style two function
        for (StudentA st : list) {
            System.out.println("style 2: " + st.customShow(styleTwo));
        }

    }
}

//Class declaration ------------------------------------------------------
class StudentA {
    public String name;
    public int age;

    public StudentA(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String customShow(Function<StudentA, String> fun) {
        return fun.apply(this);
    }
}
