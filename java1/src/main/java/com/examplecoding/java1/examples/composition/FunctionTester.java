package com.examplecoding.java1.examples.composition;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionTester {
    public static void main(String[] args) {
        Predicate<String> hasName = text -> text.contains("name");
        Predicate<String> hasPassword = text -> text.contains("password");
        Predicate<String> hasBothNameAndPassword = hasName.and(hasPassword);
        String queryString = "name=test;password=test";
        System.out.println(hasBothNameAndPassword.test(queryString));

        Function<Integer, Integer> multiply = t -> t *3;
        Function<Integer, Integer> add = t -> t  + 3;
        Function<Integer, Integer> FirstMultiplyThenAdd = multiply.compose(add);
        Function<Integer, Integer> FirstAddThenMultiply = multiply.andThen(add);
        System.out.println(FirstMultiplyThenAdd.apply(3));
        System.out.println(FirstAddThenMultiply.apply(3));

        //Eager vs Lazy Evaluation
        //Here checkInEagerWay() function first evaluates the parameters then executes its statement.
        // Whereas checkInLazyWay() executes its statement and evaluates the parameter on need basis.
        // As && is a short-circuit operator, checkInLazyWay only evaluates first parameter which
        // comes as false and does not evaluate the second parameter at all.
        String queryString2 = "password=test";
        System.out.println(checkInEagerWay(hasName(queryString2)
                , hasPassword(queryString2)));
        System.out.println(checkInLazyWay(() -> hasName(queryString2)
                , () -> hasPassword(queryString2)));
    }

    private static boolean hasName(String queryString){
        System.out.println("Checking name: ");
        return queryString.contains("name");
    }

    private static boolean hasPassword(String queryString){
        System.out.println("Checking password: ");
        return queryString.contains("password");
    }

    private static String checkInEagerWay(boolean result1, boolean result2){
        return (result1 && result2) ? "all conditions passed": "failed.";
    }

    private static String checkInLazyWay(Supplier<Boolean> result1, Supplier<Boolean> result2){
        return (result1.get() && result2.get()) ? "all conditions passed": "failed.";
    }
}
