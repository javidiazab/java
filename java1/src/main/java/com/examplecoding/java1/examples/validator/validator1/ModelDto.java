package com.examplecoding.java1.examples.validator.validator1;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDate;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModelDto {
    private static final String WEB_PATTERN = "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)";

    @NotBlank(message = "Missing id")
    private Long id;
    @Builder.Default
    private String name = "Name";
    private String surname;
    @Min(1)
    @Max(10)
    private int num;
    @Valid
    private Adress adress;
    @Email
    @NonNull
    private String email;
    @Pattern(regexp = WEB_PATTERN, flags = CASE_INSENSITIVE, message = "Invalid web site")
    private String web;
    @ValidateBirthDate(message = "Date of birth invalid", isThisCentury = false)
    private LocalDate birthDate;
}
