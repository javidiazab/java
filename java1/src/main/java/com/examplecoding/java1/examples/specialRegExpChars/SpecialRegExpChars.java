package com.examplecoding.java1.examples.specialRegExpChars;

import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpecialRegExpChars {

    @Test
    public void givenRegexWithDot_whenMatchingStr_thenMatches() {
        String strInput = "foof";
        String strRegex = "foo.";
        assertEquals(true, strInput.matches(strRegex));

        strInput = "foof";
        strRegex = "foo\\.";
        assertEquals(false, strInput.matches(strRegex));

        //Escaping Using \Q & \E
        strInput = "foo|bar|hello|world";
        strRegex = "\\Q|\\E";
        assertEquals(4, strInput.split(strRegex).length);

        //The Pattern.quote(String S) Method
        strInput = "foo|bar|hello|world";
        strRegex = "|";
        assertEquals(4, strInput.split(Pattern.quote(strRegex)).length);

        //Aditional
        strInput = "I gave $50 to my brother. He bought candy for $35. Now he has $15 left.";
        strRegex = "\\$";
        String strReplacement = "£";
        String output = "I gave £50 to my brother. He bought candy for £35. Now he has £15 left.";
        Pattern p = Pattern.compile(strRegex);
        Matcher m = p.matcher(strInput);
        assertEquals(output, m.replaceAll(strReplacement));
    }
}
