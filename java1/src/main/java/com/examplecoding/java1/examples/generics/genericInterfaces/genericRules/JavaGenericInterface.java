package com.examplecoding.java1.examples.generics.genericInterfaces.genericRules;

public interface JavaGenericInterface <T> {

   void setName(T name);
   public T getName();

}
