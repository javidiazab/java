package com.examplecoding.java1.examples.miscelaneous;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamClass {


    public boolean execute() {
        java.util.stream.Stream.generate(SupplierStrings);

        java.util.stream.Stream orquestas = java.util.stream.Stream.of("Grupo Niche",  "Guayacán", "Son de Cali");

        java.util.stream.Stream<Object> empty = java.util.stream.Stream.empty();

        int[]  enteros = new int[]{1,2,3,4,5};
        IntStream streamEnteros = Arrays.stream(enteros);

        List<String>  canciones = List.of("cancion1", "cancion2");
        java.util.stream.Stream<String> streamCanciones = canciones.stream();

        //Primeros 10 números impares positivos iniciando en  el número 1
        java.util.stream.Stream impares = java.util.stream.Stream.iterate(1, x -> x + 2).limit(10);

        List<String> lista = Arrays.asList("Taller", "Taller Lambdas y API  Stream");
        java.util.stream.Stream<String> distinct = lista.stream()
                .map(s -> s.split(" ")) // Stream<String[]>
                .map(Arrays::stream) //  Stream<Stream<String>>
                .flatMap(Function.identity()) // Stream<String>
                .distinct();//  Stream<String> de 5 elementos

        IntStream.rangeClosed(1, 10)
                .peek(StreamClass::peepTest)
                .peek(i -> System.out.println("processing count : " + i))
                .mapToObj(i -> new Customer(i, "customer" + i))
                .collect(Collectors.toList());

        return true;
    }

    private static String peepTest(int i) {
        return i + "valor";
    }

    public static Supplier<List<String>> SupplierStrings = () -> List.of("1", "2", "3");

    @Data
    @AllArgsConstructor
    public class Customer {
        int id;
        String name;
    }
}
