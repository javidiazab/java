package com.examplecoding.java1.examples.generics.genericInterfaces.sample1;

interface SmallestLargest<T extends Comparable<T>> {

    T findSmallest();
    T findLargest();

}
