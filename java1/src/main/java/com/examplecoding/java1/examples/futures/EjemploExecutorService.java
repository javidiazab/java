package com.examplecoding.java1.examples.futures;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EjemploExecutorService {
    public static void main(String[] args) {
        // Crear un ExecutorService con un grupo de hilos fijos
        ExecutorService executor = Executors.newFixedThreadPool(3);

        // Enviar tareas para su ejecución
        executor.execute(new MiTarea("Tarea 1"));
        executor.execute(new MiTarea("Tarea 2"));
        executor.execute(new MiTarea("Tarea 3"));

        // Cerrar el ExecutorService cuando ya no se necesite
        executor.shutdown();
    }
}

class MiTarea implements Runnable {
    private final String nombre;

    public MiTarea(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void run() {
        System.out.println("Ejecutando " + nombre + " en el hilo " + Thread.currentThread().getName());
    }
}