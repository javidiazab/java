package com.examplecoding.java1.examples.desingPatterns.strategy;

public class StrategyPattern {
    public void client(String args[]) {
        //Usamos la estrategia 1
        Strategy strategy1 = new ConcreteStrategy1();
        Context context = new Context(strategy1);
        context.some_method();

        //Decidimos usar la estrategia 2
        Strategy strategy2 = new ConcreteStrategy2();
        context.setStrategy(strategy2);
        context.some_method();

        //Finalmente, usamos de nuevo la estrategia 1
        context.setStrategy(strategy1);
        context.some_method();

        /** Salida:
         * Estrategia 1
         * Estrategia 2
         * Estrategia 1
         **/
    }

    public class Context {
        Strategy c;

        public Context(Strategy c) {
            this.c = c;
        }

        public void setStrategy(Strategy c) {
            this.c = c;
        }

        //Método de estrategia 'c'
        public void some_method() {
            c.behaviour();
        }
    }

}
