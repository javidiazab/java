package com.examplecoding.java1.examples.desingPatterns.decorator;

public class DarkRoast extends Beberage{
    public DarkRoast() {
        description = "Dark Road Coffee";
        System.out.println("creando " + description);
    }

    @Override
    public double cost() {
        return 0.99;
    }
}
