package com.examplecoding.java1.examples.validator.validator2.attribute;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@Builder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ConceptListResponse extends ArrayList<ConceptListDetails> {

    private static final long serialVersionUID = 1L;
}
