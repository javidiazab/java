package com.examplecoding.java1.examples.desingPatterns.decorator;

public class Mocha extends CondimentDecorator {
    Beberage beberage;

    public Mocha(Beberage beberage) {
        this.beberage = beberage;
    }

    @Override
    public double cost() {
        return .20 + beberage.cost();
    }

    @Override
    public String getDescription() {
        System.out.println("antes de get description de mocha");
        String s = beberage.getDescription() + ", Mocha";
        System.out.println("devolviendo descripcion de mocha");

        return s;
    }

}
