package com.examplecoding.java1.examples.interfaces.interfaces2;

public class ConcreteInterface implements StrategyInterface{

    @Override
    public String execute(String paymentHubId, String hubMessage) {
        return null;
    }

    @Override
    public String getEventName() {
        return null;
    }

}
