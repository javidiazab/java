package com.examplecoding.java1.examples.desingPatterns.duck;

public class MuteQuak implements com.examplecoding.java1.examples.duck.QuackBehavior {

    public void quack(){
        System.out.println("<<Silence>>");
    }
}
