package com.examplecoding.java1.examples.validator.validator1;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Adress {
    @NotNull
    private String Street;
    @NotNull
    private String cp;
    private String province;
    private String country;
}
