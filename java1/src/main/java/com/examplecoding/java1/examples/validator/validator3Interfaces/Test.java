package com.examplecoding.java1.examples.validator.validator3Interfaces;

import lombok.RequiredArgsConstructor;

import java.util.List;

@Component
@RequiredArgsConstructor
public class Test {

    private final List<Validator> validators;

    void validateSttring(String value) throws Exception {
        List<Validator> listOfValidatorThatApply = validators.stream()
                .filter(x -> x.allowValidation(value))
                .toList();

        for (var validator : listOfValidatorThatApply) {
            validator.validate(value);
        }

    }
}
