package com.examplecoding.java1.examples.miscelaneous;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class myConsumer {
    public static void main(String[] args) {
        Consumer<String> p = data -> System.out.println(data);
        p.accept("hola Javi");
        p.andThen(p).accept("Hola otra vez");

        //Otro ejemplo -----------------------------------------
        // Consumer to display a number
        Consumer<Integer> display = a -> System.out.println(a);

        // Implement display using accept()
        display.accept(10);

        // Consumer to multiply 2 to every integer of a list
        Consumer<List<Integer> > modify = list ->
        {
            for (int i = 0; i < list.size(); i++)
                list.set(i, 2 * list.get(i));
        };

        // Consumer to display a list of numbers
        Consumer<List<Integer> >
                dispList = list -> list.stream().forEach(a -> System.out.print(a + " "));

        List<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(1);
        list.add(3);

        // Implement modify using accept()
        modify.accept(list);

        // Implement dispList using accept()
        dispList.accept(list);



        //Otro ejemplo con andThen() ----------------------------

        // Consumer to multiply 2 to every integer of a list
        Consumer<List<Integer>> modify2 = list2 ->
        {
            for (int i = 0; i < list2.size(); i++)
                list2.set(i, 2 * list2.get(i));
        };

        // Consumer to display a list of integers
        Consumer<List<Integer> >
                dispList2 = list2 -> list2.stream().forEach(a -> System.out.print(a + " "));

        List<Integer> list2 = new ArrayList<Integer>();
        list2.add(2);
        list2.add(1);
        list2.add(3);

        // using addThen()
        modify2.andThen(dispList2).accept(list2);
        ;
    }
}
