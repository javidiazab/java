package com.examplecoding.java1.examples.desingPatterns.duck;

public class FlyNoWay implements  FlyBehavior{

    public void fly(){
        System.out.println("I Cant fly");
    }
}
