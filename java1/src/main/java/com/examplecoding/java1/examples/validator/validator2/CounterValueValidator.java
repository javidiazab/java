package com.examplecoding.java1.examples.validator.validator2;


import com.examplecoding.java1.examples.validator.validator2.attribute.CounterValuePaymentAmount;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedAmount;
import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedNullValue;
import static io.micrometer.common.util.StringUtils.isNotBlank;
import static java.util.Objects.nonNull;


public class CounterValueValidator implements ConstraintValidator<ValidCounterValue, CounterValuePaymentAmount> {

    private boolean nullable;

    @Override
    public void initialize(ValidCounterValue counterValue) {
        nullable = counterValue.nullable();
    }

    @Override
    public boolean isValid(CounterValuePaymentAmount counterValue, ConstraintValidatorContext context) {
        return isValid(counterValue);
    }

    public boolean isValid(CounterValuePaymentAmount counterValue) {
        return checkAllowedNullValue(nullable, counterValue) || checkValidCounterValue(counterValue);
    }

    public boolean checkValidCounterValue(CounterValuePaymentAmount counterValue) {
        return nonNull(counterValue)
                && isNotBlank(counterValue.getCurrency())
                && isNotBlank(counterValue.getTradeCurrency())
                && checkAllowedAmount(counterValue.getAmount());
    }
}
