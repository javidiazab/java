package com.examplecoding.java1.examples.generics.genericInterfaces.genericRules;

public class GenericClass<T> implements JavaGenericInterface<T> {
    //The implementing class MUST also be generic

    T name;

    @Override
    public void setName(T name) {
        this.name = name;
    }

    @Override
    public T getName() {
        return name;
    }

    public static void main(String[] args) {
        JavaGenericInterface<String> user = new GenericClass<>();
        user.setName("Javi");
        System.out.println("My name is " + user.getName());
    }
}
