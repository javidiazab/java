package com.examplecoding.java1.examples.desingPatterns.decorator;

public class HouseBlend extends Beberage{
    public HouseBlend() {
        description = "House Blend Coffe";
        System.out.println("creando " + description);
    }

    @Override
    public double cost() {
        return 0.89;
    }
}
