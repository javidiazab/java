package com.examplecoding.java1.examples.validator.validator2;

import uk.co.santander.gtscommons.model.api.attribute.RegulatoryReporting;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static uk.co.santander.gtscommons.validation.ValidationUtils.addErrorMessage;
import static uk.co.santander.gtscommons.validation.ValidationUtils.checkAllowedLength;
import static uk.co.santander.gtscommons.validation.ValidationUtils.checkAllowedNullValue;

public class RegulatoryReportingValidator
    implements ConstraintValidator<ValidRegulatoryReporting, RegulatoryReporting> {

    private static final ISOCountryCodeValidator ISO_COUNTRY_CODE_VALIDATOR = new ISOCountryCodeValidator();

    private boolean nullable;
    private Set<String> codeTypes;
    private int narrativeMaxLength;

    @Override
    public void initialize(ValidRegulatoryReporting validRegulatoryReporting) {
        nullable = validRegulatoryReporting.nullable();
        codeTypes = Set.of(validRegulatoryReporting.codeTypes());
        narrativeMaxLength = validRegulatoryReporting.narrativeMaxLength();
    }

    @Override
    public boolean isValid(RegulatoryReporting rep, ConstraintValidatorContext context) {
        return checkAllowedNullValue(nullable, rep) || checkIsValidRegulatoryReporting(rep, context);
    }

    private boolean checkIsValidRegulatoryReporting(RegulatoryReporting rep, ConstraintValidatorContext context) {
        return validateNarrative(rep, context)
                && validateCountry(rep, context)
                && validateCode(rep, context);
    }

    private boolean validateNarrative(RegulatoryReporting rep, ConstraintValidatorContext context) {
        final String narrative = rep.getNarrative();

        if (isNull(narrative)) {
            addErrorMessage(context, "Field narrative is required");
            return false;
        }

        if (!checkAllowedLength(narrativeMaxLength, narrative)) {
            final String error = String.format("Field narrative must not exceed %s characters", narrativeMaxLength);
            addErrorMessage(context, error);
            return false;
        }

        return true;
    }

    private boolean validateCountry(RegulatoryReporting rep, ConstraintValidatorContext context) {
        final String country = rep.getCountry();
        final boolean countryPresent = isNotBlank(country);
        final boolean codePresent = isNotBlank(rep.getRegulatoryReportingTypeCode());

        if (codePresent && !countryPresent) {
            addErrorMessage(context, "Field country is required when code is provided");
            return false;
        }

        if (!codePresent && countryPresent) {
            addErrorMessage(context, "Field code is required when country is provided");
            return false;
        }

        if (!checkValidCountryCode(country)) {
            addErrorMessage(context, "Field country contains invalid ISO 3166 Alpha-2 country code");
            return false;
        }

        return true;
    }

    private boolean checkValidCountryCode(String country) {
        return isNull(country) || ISO_COUNTRY_CODE_VALIDATOR.isValid(country);
    }

    private boolean validateCode(RegulatoryReporting rep, ConstraintValidatorContext context) {
        final String code = rep.getRegulatoryReportingTypeCode();
        final boolean codeIsValid = isNull(code) || codeTypes.contains(code);

        if (!codeIsValid) {
            final String codeTypes = String.join(", ", this.codeTypes);
            final String error = String.format("Invalid value for field code. Allowed values %s", codeTypes);
            addErrorMessage(context, error);
        }

        return codeIsValid;
    }
}
