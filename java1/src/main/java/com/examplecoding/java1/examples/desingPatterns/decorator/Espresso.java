package com.examplecoding.java1.examples.desingPatterns.decorator;

public class Espresso extends Beberage{
    public Espresso() {
        description = "Espresso";
        System.out.println("creando " + description);
    }

    @Override
    public double cost() {
        return 1.99;
    }
}
