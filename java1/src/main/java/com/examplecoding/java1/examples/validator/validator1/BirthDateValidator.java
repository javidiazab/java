package com.examplecoding.java1.examples.validator.validator1;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;

public class BirthDateValidator implements ConstraintValidator<ValidateBirthDate, LocalDate> {
    private boolean nullable;
    private boolean isThisCentury;

    @Override
    public void initialize(ValidateBirthDate birthDate) {
        ConstraintValidator.super.initialize(birthDate);
        nullable = birthDate.nullable();
        isThisCentury = birthDate.isThisCentury();
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        return isValid(value);
    }

    public boolean isValid(LocalDate value) {
        return value.isAfter(LocalDate.of(1971, 05, 22));
    }
}
