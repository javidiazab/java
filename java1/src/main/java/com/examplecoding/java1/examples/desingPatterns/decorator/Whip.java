package com.examplecoding.java1.examples.desingPatterns.decorator;

public class Whip extends CondimentDecorator {
    Beberage beberage;

    public Whip(Beberage beberage) {
        this.beberage = beberage;
    }

    @Override
    public double cost() {
        return .10 + beberage.cost();
    }

    @Override
    public String getDescription() {
        System.out.println("antes de get description de whip");
        String s = beberage.getDescription() + ", Whip";
        System.out.println("devolviendo descripcion de whip");
        return s;
    }
}
