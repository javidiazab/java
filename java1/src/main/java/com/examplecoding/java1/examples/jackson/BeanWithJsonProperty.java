package com.examplecoding.java1.examples.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class BeanWithJsonProperty {

        public int id;
        private String name;

    public BeanWithJsonProperty(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @JsonProperty("name")
        public void setTheName(String name) {
            this.name = name;
        }

        @JsonProperty("name")
        public String getTheName() {
            return name;
        }

        public static void main(String[] args) throws JsonProcessingException {
                BeanWithJsonProperty bean = new BeanWithJsonProperty(1, "My bean");

                String result = new ObjectMapper().writeValueAsString(bean);

                BeanWithJsonProperty resultBean = new ObjectMapper()
                        .readerFor(BeanWithJsonProperty.class)
                        .readValue(result);
        }
}
