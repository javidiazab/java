package com.examplecoding.java1.examples.desingPatterns.memento.commands;

import com.examplecoding.java1.examples.desingPatterns.memento.Editor;
import com.examplecoding.java1.examples.desingPatterns.memento.commands.Command;
import com.examplecoding.java1.examples.desingPatterns.memento.shapes.Shape;

import java.awt.*;

public class ColorCommand implements Command {
    private Editor editor;
    private Color color;

    public ColorCommand(Editor editor, Color color) {
        this.editor = editor;
        this.color = color;
    }

    @Override
    public String getName() {
        return "Colorize: " + color.toString();
    }

    @Override
    public void execute() {
        for (Shape child : editor.getShapes().getSelected()) {
            child.setColor(color);
        }
    }
}
