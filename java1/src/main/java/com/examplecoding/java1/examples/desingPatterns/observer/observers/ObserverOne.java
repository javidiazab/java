package com.examplecoding.java1.examples.desingPatterns.observer.observers;

public class ObserverOne implements Observer {
    @Override
    public void update(String newState) {
        System.out.println("ObserverOne changed to state: " + newState);
    }
}
