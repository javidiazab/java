package com.examplecoding.java1.examples.enummaps;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class EnumsMaps {

    public static void main(String[] args) {
        try {
            EnumMap<DayOfWeek, String> activityMap = new EnumMap<>(DayOfWeek.class);
            activityMap.put(DayOfWeek.MONDAY, "Soccer");

            //Constructor copy
            EnumMap<DayOfWeek, String> activityMap2 = new EnumMap<>(DayOfWeek.class);
            activityMap.put(DayOfWeek.MONDAY, "Soccer");
            activityMap.put(DayOfWeek.TUESDAY, "Basketball");
            EnumMap<DayOfWeek, String> activityMapCopy = new EnumMap<>(activityMap2);  //---->

            //Otro constructor copy
            Map<DayOfWeek, String> ordinaryMap = new HashMap<>();
            ordinaryMap.put(DayOfWeek.MONDAY, "Soccer");
            EnumMap enumMap = new EnumMap(ordinaryMap);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
