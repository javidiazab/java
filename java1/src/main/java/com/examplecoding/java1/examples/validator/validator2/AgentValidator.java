package com.examplecoding.java1.examples.validator.validator2;


import com.examplecoding.java1.examples.validator.validator2.attribute.Agent;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Pattern;

import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedLength;
import static com.examplecoding.java1.examples.validator.ValidationUtils.checkAllowedNullValue;
import static java.util.Objects.nonNull;


public class AgentValidator implements ConstraintValidator<ValidAgent, Agent> {

    @SuppressWarnings("java:NoSonar")
    private static final Pattern BIC_MATCHER = Pattern.compile("^[a-zA-Z]{6}[0-9a-zA-Z]{2}([0-9a-zA-Z]{3})?\\z"); //NOSONAR

    private boolean nullable;
    private boolean nullableBic;
    private int nameMaxLength;
    private int addressMaxLength;
    private int localInfoMaxLength;

    @Override
    public void initialize(ValidAgent validAgent) {
        nullable = validAgent.nullable();
        nameMaxLength = validAgent.nameMaxLength();
        addressMaxLength = validAgent.addressMaxLength();
        nullableBic = validAgent.nullableBic();
        localInfoMaxLength = validAgent.localInfoMaxLength();
    }

    public void initialize(int nameMaxLength, int addressMaxLength) {
        this.nameMaxLength = nameMaxLength;
        this.addressMaxLength = addressMaxLength;
    }

    @Override
    public boolean isValid(Agent agent, ConstraintValidatorContext context) {
        return isValid(agent);
    }

    public boolean isValid(Agent agent) {
        return checkAllowedNullValue(nullable, agent) || checkValidAgent(agent);
    }

    public boolean checkValidAgent(Agent agent) {
        return nonNull(agent)
                && checkValidBic(agent.getAgent())
                && checkAllowedLength(nameMaxLength, agent.getAgentName())
                && checkAllowedLength(addressMaxLength, agent.getAddressAgent())
                && checkAllowedLength(localInfoMaxLength, agent.getAgentLocalInformation());
    }

    private boolean checkValidBic(String bic) {
        return checkAllowedNullValue(nullableBic, bic)
                || (nonNull(bic) && BIC_MATCHER.matcher(bic).matches());
    }
}
