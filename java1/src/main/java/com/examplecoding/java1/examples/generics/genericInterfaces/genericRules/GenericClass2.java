package com.examplecoding.java1.examples.generics.genericInterfaces.genericRules;

public class GenericClass2<X, T> implements JavaGenericInterface<X> {
    // The generic class can have other parameterized type parameters.

    X name;
    T age;

    @Override
    public void setName(X name) {
        this.name = name;
    }

    @Override
    public X getName() {
        return name;
    }

    public void setAge(T age) {
        this.age = age;
    }

    public T getAge() {
        return age;
    }

    public static void main(String[] args) {
        GenericClass2<String, Integer> user = new GenericClass2<>();
        user.setName("Shaharyar Malik Lalani");
        user.setAge(22);
        System.out.println("My name is " + user.getName() + ", " + "I am " +
                user.getAge() + " years old.");
    }
}
