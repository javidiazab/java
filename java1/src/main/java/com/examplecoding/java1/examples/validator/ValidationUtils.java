package com.examplecoding.java1.examples.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

import static java.lang.Math.max;
import static java.math.BigDecimal.ZERO;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.junit.platform.commons.util.StringUtils.isBlank;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationUtils {

    public static boolean checkAllowedNullValue(boolean nullValuesAllowed, Object value) {
        return nullValuesAllowed && isNull(value);
    }

    public static boolean checkAllowedNullValue(boolean nullValuesAllowed, String value) {
        return nullValuesAllowed && isBlank(value);
    }

    public static boolean checkAllowedLength(int maxAllowedLength, String value) {
        return isNull(value) || value.length() <= maxAllowedLength;
    }

    public static boolean checkAllowedAmount(BigDecimal amount) {
        return checkAllowedAmount(amount, false);
    }

    private static boolean checkAllowedAmount(BigDecimal amount, boolean allowZero) {
        return nonNull(amount)
                && checkAmountIsPositive(amount, allowZero)
                && checkAmountWithAtMostTwoDecimals(amount);
    }

    private static boolean checkAmountIsPositive(BigDecimal amount, boolean allowZero) {
        return allowZero ? amount.compareTo(ZERO) >= 0 : amount.compareTo(ZERO) > 0;
    }

    private static boolean checkAmountWithAtMostTwoDecimals(BigDecimal quantity) {
        return max(0, quantity.stripTrailingZeros().scale()) <= 2;
    }

    public static boolean checkAllowedAmountOrZero(BigDecimal amount) {
        return checkAllowedAmount(amount, true);
    }

    public static void addErrorMessage(ConstraintValidatorContext context, String errorMessage) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errorMessage).addConstraintViolation();
    }
}
