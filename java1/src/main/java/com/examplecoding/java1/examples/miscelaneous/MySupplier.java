package com.examplecoding.java1.examples.miscelaneous;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Supplier;

public class MySupplier {
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {

        Supplier<LocalDateTime> s = () -> LocalDateTime.now();
        LocalDateTime time = s.get();

        System.out.println(time);

        Supplier<String> s1 = () -> dtf.format(LocalDateTime.now());
        String time2 = s1.get();

        System.out.println(time2);

        //Otro ejemplo con el metodo
        // This function returns a random value.
        Supplier<Double> randomValue = () -> Math.random();
        // Print the random value using get()
        System.out.println(randomValue.get());

    }
}
