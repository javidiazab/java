package com.examplecoding.java1.examples.validator.validator2.attribute;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uk.co.santander.gtscommons.validation.api.field.ValidAmount;
import uk.co.santander.gtscommons.validation.api.field.ValidCommercialMargin;
import uk.co.santander.gtscommons.validation.api.type.FieldsExclusion;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("java:S1845")
public class FxValueDetails {

    private String fxContravalueId;
    private FxComercialMargin fxComercialMargin;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate fxExecutionDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate fxValueDate;

    private BigDecimal fxExchangeRate;
    private PivotNominal pivotNominal;
    private BigDecimal pivotExchangeRate;

    /**
     * "Repeated" field as workaround to keep retro-compatibility among versions.
     */
    @ValidAmount(message = "Invalid fxCounterpaymentAmount")
    private PaymentAmount fxCounterpaymentAmount;

    @ValidAmount(message = "Invalid fxCounterPaymentAmount")
    private PaymentAmount fxCounterPaymentAmount;

    public PaymentAmount getFxCounterpaymentAmount() {
        return fxCounterpaymentAmount;
    }

    public PaymentAmount getFxCounterPaymentAmount() {
        return fxCounterPaymentAmount;
    }
}
