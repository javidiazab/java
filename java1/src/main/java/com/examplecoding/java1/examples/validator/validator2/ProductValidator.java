package com.examplecoding.java1.examples.validator.validator2;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;

import static uk.co.santander.gtscommons.validation.ValidationUtils.addErrorMessage;
import static uk.co.santander.gtscommons.validation.ValidationUtils.checkAllowedNullValue;

public class ProductValidator implements ConstraintValidator<ValidProduct, String> {

    private boolean nullable;
    private List<String> allowedProductIds;

    @Override
    public void initialize(ValidProduct validProduct) {
        nullable = validProduct.nullable();
        initialize(validProduct.allowedProductIds());
    }

    public void initialize(String... products) {
        this.allowedProductIds = List.of(products).stream().sorted().collect(Collectors.toList());
    }

    @Override
    public boolean isValid(String product, ConstraintValidatorContext context) {
        final boolean isValid = isValid(product);

        if (!isValid) {
            addErrorMessage(context, "Invalid product id specified. Valid values: " + allowedProductIds);
        }

        return isValid;
    }

    public boolean isValid(String product) {
        return checkAllowedNullValue(nullable, product) || checkValidProductId(product);
    }

    public boolean checkValidProductId(String product) {
        return allowedProductIds.stream()
                .anyMatch(allowedProduct -> StringUtils.equals(allowedProduct, product));
    }
}
