package com.examplecoding.java1.examples.dates;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import static java.nio.file.StandardOpenOption.APPEND;

public class DatesWW3School {

    public static void main(String[] args) throws IOException {
        LocalDate myObj = LocalDate.now(); // Create a date object
        System.out.println(myObj); // Display the current date

        LocalTime myObj1 = LocalTime.now();
        System.out.println(myObj1);

        LocalDateTime myObj2 = LocalDateTime.now();
        System.out.println(myObj);

        //Formatting
        LocalDateTime myDateObj = LocalDateTime.now();
        System.out.println("Before formatting: " + myDateObj);
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        String formattedDate = myDateObj.format(myFormatObj);
        System.out.println("After formatting: " + formattedDate);

        //-----------------
        LocalDate localDate = LocalDate.now();
        LocalDate.of(2015, 02, 20);
        LocalDate.parse("2015-02-20");
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        LocalDate previousMonthSameDay = LocalDate.now().minus(1, ChronoUnit.MONTHS);
        DayOfWeek sunday = LocalDate.parse("2016-06-12").getDayOfWeek();

        int twelve = LocalDate.parse("2016-06-12").getDayOfMonth();
        boolean leapYear = LocalDate.now().isLeapYear();
        boolean notBefore = LocalDate.parse("2016-06-12")
                .isBefore(LocalDate.parse("2016-06-11"));

        boolean isAfter = LocalDate.parse("2016-06-12")
                .isAfter(LocalDate.parse("2016-06-11"));
        LocalDateTime beginningOfDay = LocalDate.parse("2016-06-12").atStartOfDay();
        LocalDate firstDayOfMonth = LocalDate.parse("2016-06-12")
                .with(TemporalAdjusters.firstDayOfMonth());

        //------------------------
        LocalTime now = LocalTime.now();
        LocalTime sixThirty = LocalTime.parse("06:30");
        LocalTime sixThirty2 = LocalTime.of(6, 30);
        LocalTime sevenThirty = LocalTime.parse("06:30").plus(1, ChronoUnit.HOURS);
        int six = LocalTime.parse("06:30").getHour();
        boolean isbefore = LocalTime.parse("06:30").isBefore(LocalTime.parse("07:30"));
        LocalTime maxTime = LocalTime.MAX;

        //--------------------------
        LocalDateTime.now();
        LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
        LocalDateTime localDateTime = LocalDateTime.parse("2015-02-20T06:30:00");
        localDateTime.getMonth();
        localDateTime.plusDays(1);
        localDateTime.minusHours(2);

        //ZoneId y ZoneOffset
        //----------------------------
        ZoneId zoneId = ZoneId.of("Europe/Paris");
        Set<String> allZoneIds = ZoneId.getAvailableZoneIds();
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);
        ZonedDateTime.parse("2015-05-03T10:15:30+01:00[Europe/Paris]");

        LocalDateTime localDateTime2 = LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
        ZoneOffset offset = ZoneOffset.of("+02:00");
        OffsetDateTime offSetByTwo = OffsetDateTime.of(localDateTime2, offset);

        System.out.println("offSetByTwo.format(DateTimeFormatter.ofPattern(yyyy/MM/dd)); " + offSetByTwo.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));

        LocalDateTime now2 = LocalDateTime.now();
        ZoneId zone = ZoneId.of("Europe/Berlin");
        ZoneOffset zoneOffSet = zone.getRules().getOffset(now2);
        System.out.println("now2: " + now2);
        System.out.println("zone: " + zone);
        System.out.println("zoneOffSet: " + zoneOffSet);

        ZoneId zone1 = ZoneId.of("Europe/Berlin");
        ZonedDateTime date = ZonedDateTime.now(zone1);
        System.out.println("date: " + date);

        ZoneOffset zoneOffSet1 = ZoneOffset.of("+02:00");
        OffsetDateTime date1 = OffsetDateTime.now(zoneOffSet1);
        System.out.println("OffsetDateTime: " + date1);

        System.out.println("OffsetDateTime parsed: " + OffsetDateTime.parse("2018-12-12T13:30:30+05:00"));

        //---------------------------
        LocalDate initialDate = LocalDate.parse("2007-05-10");
        LocalDate finalDate = initialDate.plus(Period.ofDays(5));
        int five = Period.between(initialDate, finalDate).getDays();
        long five2 = ChronoUnit.DAYS.between(initialDate, finalDate);

        LocalTime initialTime = LocalTime.of(6, 30, 0);
        LocalTime finalTime = initialTime.plus(Duration.ofSeconds(30));
        long thirty = Duration.between(initialTime, finalTime).getSeconds();
        long thirty2 = ChronoUnit.SECONDS.between(initialTime, finalTime);

        //---------------------------
        LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault());
        //LocalDateTime.ofInstant(calendar().toInstant(), ZoneId.systemDefault());

        //Date and Time Formatting
        LocalDateTime localDateTime1 = LocalDateTime.of(2015, Month.JANUARY, 25, 6, 30);
        localDateTime1.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        System.out.println("localDateTime1.format(DateTimeFormatter.ofPattern(\"yyyy/MM/dd\")); " + localDateTime1);
        localDateTime1.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK));
        System.out.println("localDateTime1.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK)); " + localDateTime1);


        //Definite Date and time formatting
        Date now1 = new Date();
        System.out.println(now1);
        SimpleDateFormat isoDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String sDate = isoDate.format(now1);
        System.out.println(sDate);


        //----------------
        Path path0b = Paths.get("/Users/n90578/Documents/WksJavi/java1/src/main/resources/static/new.txt");

        System.out.println(path0b.toFile().getAbsolutePath());
        //Files.createFile(path0b);
        System.out.println("Last ModifiedDate: " + Files.getLastModifiedTime(path0b));
        Files.writeString(path0b, "Hola\n", APPEND);

        FileTime lastModifiedTime = Files.getLastModifiedTime(path0b);
        LocalDateTime convertedFileTime = LocalDateTime.ofInstant(lastModifiedTime.toInstant(), ZoneId.systemDefault());
        System.out.println("lastModifiedTime: " + lastModifiedTime);
        System.out.println("convertedFileTime: " + convertedFileTime);


        Instant now4 = Instant.now();
        lastModifiedTime.toInstant().isAfter(now4);
        lastModifiedTime.toInstant().isBefore(now4);
        lastModifiedTime.toInstant().minus(20, ChronoUnit.DAYS);
        System.out.println();

        //Delete Files
//        Files.walk(Paths.get(""))
//                .filter(Files::isRegularFile)
//                .forEach(file -> {
//                    try {
//                        if (Files.getLastModifiedTime(file).toInstant().plus(7, ChronoUnit.DAYS).isAfter(Instant.now())) {
//                            Files.delete(file);
//                        };
//                    } catch (IOException e) {
//                        System.out.println("Error deleting DB BackUpFiles");
//                    }
//                });


    }
}
