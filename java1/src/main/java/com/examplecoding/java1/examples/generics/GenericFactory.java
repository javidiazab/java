package com.examplecoding.java1.examples.generics;

public class GenericFactory<T> {

    Class theClass = null;

    public GenericFactory(Class theClass) {
        this.theClass = theClass;
    }

    public T createInstance() throws IllegalAccessException, InstantiationException {
        return (T) this.theClass.newInstance();
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        GenericFactory<String> factory = new GenericFactory<>(String.class); //It is just a shorthand for "same as in declaration". Used In IntellyJ

        String myClassInstance = factory.createInstance();

        GenericFactory<Integer> factory2 = new GenericFactory<Integer>(Integer.class);

        Integer someObjectInstance = factory2.createInstance();
    }

}
