package com.examplecoding.java1.examples.validator.validator2.attribute;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Concepts {

    private String conceptId;
    private String conceptName;
    private SpecialPrice specialPrice;
    private StandardPrice standardPrice;
    private ConceptAmount conceptAmount;
}
