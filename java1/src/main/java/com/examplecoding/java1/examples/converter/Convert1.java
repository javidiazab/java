package com.examplecoding.java1.examples.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Convert1 {
    //Este ejemplo usa el ejemplo nuestro
    @Autowired
    ExampleConverter exampleConverter;

    public B convert() {
        B b = exampleConverter.convert(new A("1", "2"));
        System.out.println(b);
        return b;
    }

}
