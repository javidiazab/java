package com.examplecoding.java1.examples.printf;

import java.util.Date;
import java.util.Locale;

public class PrintFExamples {

    public static void main(String[] args) {

        //Format
        //%<flags><width><.precision>conversion-character

        System.out.printf("%b%n", null);
        System.out.printf("%x%n", 100);
        System.out.printf("%s%n", "hello world!");
        System.out.printf("'%S' %n", "hello world!");
        System.out.printf("'%10s' %n", "Hello");
        System.out.printf ("'%-10s' %n", "Hello");
        /*
        * you can also limit the number of characters in the argument string by specifying a precision,
        * If we consider the syntax, %x.ys, The number x defines the padding on the left and y is the
        * number of characters in the string.
        * */
        System.out.printf("'%3.5s'%n", "Hello World!");
        System.out.printf("it is an integer: %d%n", 10000);
        System.out.printf(Locale.US, "%,d %n", 12300);
        System.out.printf(Locale.ITALY, "%,d %n", 10000);
        System.out.printf("%f%n", 3.1423);
        System.out.printf("'%3.2f'%n", 3.1423);
        System.out.printf("'%3.2e'%n", 3.1423);

        /*
         * The conversion characters consist of two characters: the t/T character accompanied with the conversion suffix
         * For formatting time using Java Printf, H, M, and S characters are used
         * for extracting the hours, minutes, and seconds from the input Date value.
         * L and N characters represent the time in milliseconds and nanoseconds
         * accordingly. p character is used to add a.m./p.m. formatting and lastly, z prints out the time-zone offset.
         */
        Date date = new Date();
        System.out.printf("%tT%n", date);

        System.out.printf("hours: %tH%n minutes: %tM%n seconds: %tS%n", date, date, date);

        System.out.printf("%1$tA, %1$tB %1$tY %n", date);
        System.out.printf("%1$td-%1$tm-%1$ty %n", date);

        /*

        Specifier	Explanation
            %c	Format characters
            %d	Format decimal (integer) numbers (base 10)
            %e	Format exponential floating-point numbers
            %f	Format floating-point numbers
            %i	Format integers (base 10)
            %o	Format octal numbers (base 8)
            %s	Format string of characters
            %S	Format string of characters capitalised
            %u	Format unsigned decimal (integer) numbers
            %x	Format numbers in hexadecimal (base 16)
            %n	add a new line character


        * Characters	Usage
            A	prints out the full day of the week.
            d	formats a two-digit day of the month.
            B	Prints the full month name.
            m	formats a two-digit month.
            Y	outputs the year in four digits.
            y	outputs the last two digits of the year.
        * */
    }
}
