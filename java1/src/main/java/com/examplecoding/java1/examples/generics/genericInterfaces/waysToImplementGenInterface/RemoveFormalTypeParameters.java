package com.examplecoding.java1.examples.generics.genericInterfaces.waysToImplementGenInterface;

public class RemoveFormalTypeParameters implements JGI {

//    Removing or ignoring the formal type parameters is an ex.
//    This is a way around to get to use the features of Java generic Interfaces.
//    This method has only made the list because if you are still working with
//    JDK version 1.4 or lower than this is the only option you have as Java Generics
//    were not available before JDK version 1.5.

    private String address;
    private Object value;

    @Override
    public void setValue(Object v, String addressCode) {
        value = v;
        address = addressCode;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String getAddress() {
        return address;
    }
}
