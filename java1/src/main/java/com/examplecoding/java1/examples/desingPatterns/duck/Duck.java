package com.examplecoding.java1.examples.desingPatterns.duck;

public abstract class Duck {

    FlyBehavior flyBehavior;
    com.examplecoding.java1.examples.duck.QuackBehavior quackBehavior;

    public abstract void display();

    public void performFly(){
        flyBehavior.fly();
    }
    public void performQuack(){
        quackBehavior.quack();
    }

    public void swing(){
        System.out.println("Swimming");
    }

    public void setFlyBehavior(FlyBehavior fb) {
        this.flyBehavior = fb;

    }

    public void setQuackBehavior(com.examplecoding.java1.examples.duck.QuackBehavior qb) {
        this.quackBehavior = qb;

    }

}
