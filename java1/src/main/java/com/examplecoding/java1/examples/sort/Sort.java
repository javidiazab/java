package com.examplecoding.java1.examples.sort;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public class Sort {
    Employee employee1 = new Employee("0", "0", "0", "0");
    Employee employee2 = new Employee("0", "0", "0", "1");
    Employee employee3 = new Employee("0", "0", "1", "0");
    Employee employee4 = new Employee("0", "0", "1", "1");
    Employee employee5 = new Employee("3", "1", "1", "1");
    Employee employee6 = new Employee("3", "2", "2", "1");
    Employee employee7 = new Employee("7", "1", "1", "1");
    List<Employee> employees = Arrays.asList(employee2, employee1, employee7, employee6,
            employee5, employee4, employee3);

    void process() {
//        Arrays.sort(s, Comparator.nullsFirst(Comparator.reverseOrder()));

//        Arrays.sort(s, (a, b) -> b.compareTo(a));

//        Guava: Ordering.natural().reverse()

//        class CustomComparator<T extends Comparable<T>> implements Comparator<T> {
//            @Override
//            public int compare(T a, T b) {
//                return b.compareTo(a);
//            }
//        }
//        class Main {
//            public static void main(String[] args) {
//                String[] s = { "B", "C", "A" };
//                Arrays.sort(s, new CustomComparator());
//                System.out.println(Arrays.toString(s));
//            }
//        }

        employees.sort(Comparator.nullsFirst(Comparator
                .comparing(Employee::getName)
                .thenComparing(Employee::getDateOfBirth)
                .thenComparing(Employee::getId)
                .thenComparing((Employee::getTask)).reversed())
        );
        employees.forEach(System.out::println);

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public class Employee {
        String name;
        String dateOfBirth;
        String id;
        String task;

        @Override
        public String toString() {
            return "Employee{" +
                    "name='" + name + '\'' +
                    ", dateOfBirth='" + dateOfBirth + '\'' +
                    ", id='" + id + '\'' +
                    ", task='" + task + '\'' +
                    '}';
        }
    }

    public static void main (String[] args) {
        Sort sort = new Sort();
        sort.process();
    }
}
