package com.examplecoding.java1.examples.interfacesList;

import org.springframework.stereotype.Component;

@Component
public class CarValidator implements Validator{

    @Override
    public boolean validate(Vehicle vehicle) {
        System.out.println("Validating a car");
        return vehicle.getNumWheels() == 4;
    }
}
