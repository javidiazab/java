package com.examplecoding.java1.examples.generics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GenericMethod {

    public static <T> T addAndReturn(T element, Collection<T> collection){
        collection.add(element);
        return element;
    }

    public static void main (String[] args) {
        String stringElement = "stringElement";
        List<String> stringList = new ArrayList<>();

        String theElement = addAndReturn(stringElement, stringList);


        Integer integerElement = new Integer(123);
        List<Integer> integerList = new ArrayList<>();

        Integer theElement2 = addAndReturn(integerElement, integerList);
    }


}
