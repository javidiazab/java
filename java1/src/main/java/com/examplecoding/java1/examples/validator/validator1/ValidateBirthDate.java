package com.examplecoding.java1.examples.validator.validator1;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import javax.validation.valueextraction.Unwrapping;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BirthDateValidator.class) //TODO descomentar
public @interface ValidateBirthDate {

    boolean nullable() default false;

    String message() default "{BirthDate.invalid}";

    boolean isThisCentury() default true;

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}
