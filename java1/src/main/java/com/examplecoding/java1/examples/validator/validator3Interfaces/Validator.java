package com.examplecoding.java1.examples.validator.validator3Interfaces;

public interface Validator {

    void validate (String value) throws Exception;
    default boolean allowValidation(String value) {
        return true;
    }
}
