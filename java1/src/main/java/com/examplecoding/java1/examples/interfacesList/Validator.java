package com.examplecoding.java1.examples.interfacesList;

import java.util.Optional;

public interface Validator {

    boolean validate(Vehicle vehicle);

    default boolean allowVehicle(Vehicle vehicle) {
        return Optional.ofNullable(vehicle.getNumPlate()).isPresent();
    }
}
