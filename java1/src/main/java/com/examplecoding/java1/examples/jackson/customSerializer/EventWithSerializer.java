package com.examplecoding.java1.examples.jackson.customSerializer;

import com.examplecoding.java1.examples.jackson.customSerializer.CustomDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;

import java.util.Date;

@AllArgsConstructor
public class EventWithSerializer {
    public String name;

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date eventDate;
}