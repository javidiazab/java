package com.examplecoding.java1.examples.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/request")
public class ControllerExample {

    @GetMapping("/test")
    public ResponseTransfer gettResponseController() {
        return new ResponseTransfer("Thanks For Getting!!!");
    }

    @PostMapping("/response")
    public ResponseTransfer postResponseController(@RequestBody LoginForm loginForm) {
        return new ResponseTransfer("Thanks For Posting!!!");
    }

    @PostMapping(value = "/contentJSON", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseTransfer postResponseJsonContent(@RequestBody LoginForm loginForm) {
        return new ResponseTransfer("JSON Content!");
    }

    @PostMapping(value = "/contentXML", produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public ResponseTransfer postResponseXmlContent(@RequestBody LoginForm loginForm) {
        return new ResponseTransfer("XML Content!");
    }

    @Data
    @AllArgsConstructor
    public class ResponseTransfer {
        private String text;
    }


/*
curl -i \ -H "Accept: application/json" \                                                                 16:39:26
-H "Content-Type:application/json" \
 "http://localhost:9192/request/test"


curl -i \                                                                                               16:41:42
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-X POST --data '{"username": "johnny", "password": "password"}' "http://localhost:9192/request/response"
The CURL command returns a JSON response:
{"text":"Thanks For Posting!!!"}


curl -i \                                                                                               16:41:42
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-X POST --data '{"username": "johnny", "password": "password"}' "http://localhost:9192/request/contentJSON"
The CURL command returns a JSON response:
{"text":"JSON Content!"}

Now, let's change the Accept parameter:

curl -i \
-H "Accept: application/xml" \
-H "Content-Type:application/json" \
-X POST --data '{"username": "johnny", "password": "password"}' "https://localhost:9192/request/contentXML"

 As anticipated, we get an XML content this time:
  <ResponseTransfer><text>XML Content!</text></ResponseTransfer>
 */
}
