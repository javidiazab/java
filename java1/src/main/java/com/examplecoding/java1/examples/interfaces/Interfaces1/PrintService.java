package com.examplecoding.java1.examples.interfaces.Interfaces1;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PrintService {
    private final Printable printable;

    public boolean print() {
        return printable.print();
    }
}
