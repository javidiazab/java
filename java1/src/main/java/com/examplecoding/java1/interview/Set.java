package com.examplecoding.java1.interview;

public class Set {
    private int size = 0;
    //    private Object[] elements = new Object[10];
    private Object[] elements;

    public Set(int initialCapacity) {
        this.elements = new Object[initialCapacity];
    }

    public Set() {
        this(10);
    }

    public boolean empty() {
        return size == 0;
    }

    public void add(String value) {
        if (contains(value)) return;

        if (full()) {
            grow();
        }

        elements[size] = value;
        size++;
    }

    private void grow() {
        Object[] bigger = new Object[elements.length * 2 + 1];
        System.arraycopy(elements, 0, bigger, 0, elements.length);
        elements = bigger;
    }

    private boolean full() {
        return size == elements.length;
    }

    public int size() {
        return size;
    }

    public boolean contains(Object value) {
//        for (int i = 0; i < size; i++) {
//            if (values[i].equals(value)) {
//                return true;
//            }
//        }
//        return false;
        return indexOf(value) >= 0;
    }

    public void remove(Object value) {
//        int index = indexOf(value);
//        if (index >= 0) {
        if (contains(value)) {
            elements[indexOf(value)] = elements[size - 1];
            elements[size - 1] = null;
            size--;
        }
    }

    private int indexOf(Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }
}
