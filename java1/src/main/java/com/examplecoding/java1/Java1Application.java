package com.examplecoding.java1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class Java1Application {

    public static void main(String[] args) {
        SpringApplication.run(Java1Application.class, args);
    }

}
