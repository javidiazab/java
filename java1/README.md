## Pending ##
1. crear implementacion por defecto de un interface con @Component
2. EventBus
1. Anotation para validar


# MarkDown Language #

**Notes:**
```               ---- estas 3 lineas van dentro de una caja
xxx
```
tabla:
|titulo1|titulo2|
|xxxxx|xxxxx|

linea separatoria: ----------------------------------------

## Headers

# This is a Heading h1
# This is a Heading h1 #
#This is a Heading h1
## This is a Heading h2
###### This is a Heading h6

## Emphasis

*This text will be italic*  
_This will also be italic_

**This text will be bold**  
__This will also be bold__

_You **can** combine them_

## Lists

### Unordered

* Item 1
* Item 2
    * Item 2a
    * Item 2b

### Ordered

1. Item 1
1. Item 2
1. Item 3
    1. Item 3a
    1. Item 3b

## Images

![This is a alt text.](/image/sample.png "This is a sample image.")

## Links

You may be using [Markdown Live Preview](https://markdownlivepreview.com/).

## Blockquotes

> Markdown is a lightweight markup language with plain-text-formatting syntax, created in 2004 by John Gruber with Aaron Swartz.
>
>> Markdown is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor.

## Inline code

This web site is using `markedjs/marked`.

Para hacer un link:
* [Official Gradle documentation](https://docs.gradle.org)