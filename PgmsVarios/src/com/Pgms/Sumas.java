package Pgms;

/*
 * GridBagLayoutDemo.java is a 1.4 application that requires no other files.
 * Saca numeros y hayq ue sumarlos
   //suma.repaint();  ///No funciona
 */

import java.awt.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import paqueteNuevo.Utils;

public class Sumas {
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    final static JButton button = new JButton("OK");
    final static JLabel suma = new JLabel();
    final static int resultado = 0;
    public static int a = Utils.Aleatorio(0, 99);
    public static int b = Utils.Aleatorio(0, 99);
    public static int c = Utils.Aleatorio(0, 99);
    public static int d = Utils.Aleatorio(0, 99);
    public static int e = Utils.Aleatorio(0, 99);

    public static void addComponentsToPane(Container pane) {
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }

        pane.setLayout(new GridBagLayout());
        GridBagConstraints grid = new GridBagConstraints();
        if (shouldFill) {
            //natural height, maximum width
            grid.fill = GridBagConstraints.HORIZONTAL;
        }
        

        JLabel suma = new JLabel();
        JLabel label = new JLabel();
        
        label = new JLabel();
        grid.gridx = 0;
        grid.gridy = 0;
        label.setText(" ");
        pane.add(suma, grid);
        
        grid.gridx = 0;
        grid.gridy = 1;
        suma.setText("    " + a + " + " + b + " + " + c + " + " + d + " + " + e + "____" + "    ");
        pane.add(suma, grid);
        
        label = new JLabel();
        grid.gridx = 0;
        grid.gridy = 2;
        label.setText(" ");
        pane.add(label, grid);

    
        java.awt.event.ActionListener cliBl = new java.awt.event.ActionListener() {
            //va a redestribuir las acciones sobre los textos y los botones de las tablas
            public void actionPerformed(java.awt.event.ActionEvent e) {
                if (e.getSource() instanceof javax.swing.JButton) {
                	modificaTexto();
                }
            }
        };
        
        grid.gridx = 0;
        grid.gridy = 3;
        pane.add(button, grid);
        button.addActionListener(cliBl);
        
    }
    
    public static void modificaTexto() {
    	
    	button.setText(a + b + c + d + e + "");
        a = Utils.Aleatorio(0, 99);
        b = Utils.Aleatorio(0, 99);
        c = Utils.Aleatorio(0, 99);
        d = Utils.Aleatorio(0, 99);
        e = Utils.Aleatorio(0, 99);
        suma.setText("    " + a + " + " + b + " + " + c + " + " + d + " + " + e + "____" + "    ");
        suma.repaint();  ///No funciona
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        JFrame frame = new JFrame("GridBagLayoutDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

