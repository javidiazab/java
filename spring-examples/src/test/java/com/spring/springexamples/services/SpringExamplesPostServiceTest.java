package com.spring.springexamples.services;

import com.spring.springexamples.component.repository.TbPruebaRepository;
import com.spring.springexamples.component.repository.TbRedisRepository;
import com.spring.springexamples.model.RequestJavi;
import com.spring.springexamples.model.ResponseJavi;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class SpringExamplesPostServiceTest {

    @Mock
    private TbPruebaRepository tbPruebaRepository;

    @Mock
    private TbRedisRepository tbRedisRepository;

    @InjectMocks
    private SpringExamplesPostService springExamplesPostService;

    @Test
    void shouldReturnResponse_whenRequestIsReceived() {
        //Given
        final RequestJavi givenRequestJavi = RequestJavi.builder()
                .dato1("dato1")
                .dato2("dato2")
                .dato3("dato3")
                .build();
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1("dato1")
                .details2("dato2")
                .details3("dato3")
                .build();

        //When
        ResponseJavi actualResponse = springExamplesPostService.processPost(givenRequestJavi);

        //Then
        assertEquals(expectedResponse, actualResponse);
        verify(tbPruebaRepository).save(any());
        verify(tbRedisRepository).save(any());
    }
}