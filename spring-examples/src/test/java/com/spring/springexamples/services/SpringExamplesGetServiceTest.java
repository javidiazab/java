package com.spring.springexamples.services;

import com.spring.springexamples.component.repository.TbPruebaRepository;
import com.spring.springexamples.component.repository.TbRedisRepository;
import com.spring.springexamples.model.ResponseJavi;
import com.spring.springexamples.model.entity.TbPrueba;
import com.spring.springexamples.model.entity.TbRedis;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SpringExamplesGetServiceTest {

    @Mock
    private TbPruebaRepository tbPruebaRepository;

    @Mock
    private TbRedisRepository tbRedisRepository;

    @InjectMocks
    private SpringExamplesGetService springExamplesGetService;

    @Test
    void shouldReturnResponse_whenIdIsReceived() {
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("000")
                .details1("col1")
                .details2("col2")
                .details3("col3")
                .build();
        final TbPrueba givenTbPrueba = TbPrueba.builder().col1("col1").col2("col2").col3("col3").build();
        when(tbPruebaRepository.findById(any())).thenReturn(Optional.of(givenTbPrueba));

        //When
        ResponseJavi actualResponse = springExamplesGetService.processGetTbPrueba(givenId);

        //Then
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void shouldNotReturnResponse_whenIdIsNotFoundInDBPrueba() {
        //TODO
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("100TbPrueba")
                .build();
        when(tbPruebaRepository.findById(any())).thenReturn(Optional.empty());

        //When
        ResponseJavi actualResponse = springExamplesGetService.processGetTbPrueba(givenId);

        //Then
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void shouldNotReturnResponse_whenIdIsNotFoundInDBRRedis() {
        //TODO
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("100TbRedis")
                .build();
        when(tbRedisRepository.findById(any())).thenReturn(Optional.empty());

        //When
        ResponseJavi actualResponse = springExamplesGetService.processGetTbRedis(givenId);

        //Then
        assertEquals(expectedResponse, actualResponse);
    }
}