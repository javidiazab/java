package com.spring.springexamples.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoxTest {

    @Test
    void shouldAceptAnyType() {
        final Box<Integer> box = new Box<>();
        box.put(46);
        final Integer x = box.getItem();
        assertEquals(46, x);
    }

    @Test
    void shouldTestAGenericMethod() {
        final Box<Integer> box1 = new Box<>();
        box1.putInBox(52, box1);

        final Box<String> box2 = new Box<>();
        Box.staticPutInBox("any_string", box2);
    }
}