package com.spring.springexamples.generics;

import org.junit.jupiter.api.Test;

class BoxNumbersTest {

    @Test
    void shouldAceptAnyTypeThatExtendsNumbers_Integer() {
        BoxNumbers<Integer> boxNumbers = new BoxNumbers<>();
        boxNumbers.put(46);
        Integer x = boxNumbers.getItem();
    }

    @Test
    void shouldAceptAnyTypeThatExtendsNumbers_Double() {
        BoxNumbers<Double> boxNumbers = new BoxNumbers<>();
        boxNumbers.put(10.0);
        Double x = boxNumbers.getItem();
    }



}