package com.spring.springexamples.generics;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class BoxAppleTest {

    @Test
    void shouldCreateNewBoxApple() {
        final BoxApple box = new BoxApple();
        final Apple apple = new Apple(1);
        box.put(apple);
        final int flavor = box.getFlavor();
    }
}