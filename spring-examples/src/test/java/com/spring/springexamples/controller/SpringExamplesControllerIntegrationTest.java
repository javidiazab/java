package com.spring.springexamples.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.springexamples.IntegrationTestsConfiguration;
import com.spring.springexamples.component.repository.TbPruebaRepository;
import com.spring.springexamples.component.repository.TbRedisRepository;
import com.spring.springexamples.model.RequestJavi;
import com.spring.springexamples.model.ResponseJavi;
import com.spring.springexamples.model.entity.TbPrueba;
import com.spring.springexamples.model.entity.TbRedis;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import redis.embedded.RedisServer;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = IntegrationTestsConfiguration.class)
@AutoConfigureMockMvc
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SpringExamplesControllerIntegrationTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TbPruebaRepository tbPruebaRepository;

    @Autowired
    private TbRedisRepository tbRedisRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        final TbPrueba givenTbPrueba = TbPrueba.builder().col1("col1").col2("col2").col3("col3").build();
        final TbRedis givenTbRedis = TbRedis.builder().col1("col1").col2("col2").col3("col3").build();
        tbPruebaRepository.save(givenTbPrueba);
        tbRedisRepository.save(givenTbRedis);
    }

    @Test
    void ShouldReturnResponseFromPostConsumes_whenRequestIsReceived() throws Exception {
        //Given
        final String givenRequestJavi = "input";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1(givenRequestJavi)
                .details2("dato2")
                .details3("dato3")
                .createdDate(null)
                .build();
        //ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        MvcResult mvcResult = mockMvc.perform(post("/spring-examples")
//                .header(HttpHeaders.AUTHORIZATION, SERENITY_TOKEN)
                .contentType(TEXT_PLAIN)
                .content(givenRequestJavi))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
//                .getResponse()
//                .getContentAsString();

        //Then
        String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        ResponseJavi actualResponseEntity = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);
        assertEquals(expectedResponse, actualResponseEntity);
    }

    @Test
    void ShouldReturnResponseFromPost_whenRequestIsReceived() throws Exception {
        //Given
        final RequestJavi givenRequestJavi = RequestJavi.builder()
                .dato1("dato1")
                .dato2("dato2")
                .dato3("dato3")
                .build();
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1("dato1")
                .details2("dato2")
                .details3("dato3")
                .createdDate(null)
                .build();
        //ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        MvcResult mvcResult = mockMvc.perform(post("/spring-examples/post")
//                .header(HttpHeaders.AUTHORIZATION, SERENITY_TOKEN)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(givenRequestJavi)))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
//                .getResponse()
//                .getContentAsString();

        //Then
        String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        ResponseJavi actualResponseEntity = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);
        assertEquals(expectedResponse, actualResponseEntity);
    }

    @Test
    void ShouldReturnResponseFromPostValue_whenRequestIsReceived() throws Exception {
        //Given
        final RequestJavi givenRequestJavi = RequestJavi.builder()
                .dato1("dato1")
                .dato2("dato2")
                .dato3("dato3")
                .build();
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1("dato1")
                .details2("dato2")
                .details3("dato3")
                .createdDate(null)
                .build();
        //ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        MvcResult mvcResult = mockMvc.perform(post("/spring-examples/post/value")
//                .header(HttpHeaders.AUTHORIZATION, SERENITY_TOKEN)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(givenRequestJavi)))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
//                .getResponse()
//                .getContentAsString();

        //Then
        String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        ResponseJavi actualResponseEntity = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);
        assertEquals(expectedResponse, actualResponseEntity);
    }

    @Test
    void ShouldReturnResponseFromGetTbPrueba_whenIdIsReceivedAndFoundInDb() throws Exception {
        //Given
        final String givenId = "col1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("000")
                .details1("col1")
                .details2("col2")
                .details3("col3")
                .createdBy(null)
                .createdDate(null)
                .lastModifiedBy(null)
                .lastModifiedDate(null)
                .build();
        //ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        final MvcResult mvcResult = mockMvc.perform(get("/spring-examples/tb-prueba/" + givenId)
                .header(AUTHORIZATION, ""))
                .andExpect(status().isOk())
                .andReturn();
        final String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        final ResponseJavi actualResponse = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);

        //Then
        assertEquals(expectedResponse.getStatus(), actualResponse.getStatus());
        assertEquals(expectedResponse.getDetails1(), actualResponse.getDetails1());
        assertEquals(expectedResponse.getDetails2(), actualResponse.getDetails2());
        assertEquals(expectedResponse.getDetails3(), actualResponse.getDetails3());
        assertEquals(expectedResponse.getCreatedBy(), actualResponse.getCreatedBy());
        assertTrue(actualResponse.getCreatedDate().isBefore(LocalDateTime.now()));
        assertTrue(actualResponse.getCreatedDate().isAfter(LocalDateTime.now().minusMinutes(1)));
        assertEquals(expectedResponse.getLastModifiedBy(), actualResponse.getLastModifiedBy());
        assertTrue(actualResponse.getLastModifiedDate().isBefore(LocalDateTime.now()));
        assertTrue(actualResponse.getLastModifiedDate().isAfter(LocalDateTime.now().minusMinutes(1)));
    }

    @Test
    void ShouldReturnResponse100FromGetTbPrueba_whenIdIsNotFoundInDb() throws Exception {
        //Given
        final String givenId = "col2";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("100TbPrueba")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        final String mvcResult = mockMvc.perform(get("/spring-examples/tb-prueba/" + givenId)
                .header(AUTHORIZATION, ""))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        //final String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        //final ResponseJavi actualResponse = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);
        final ResponseJavi actualResponse = objectMapper.readValue(mvcResult, ResponseJavi.class);

        //Then
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void ShouldReturnResponseFromGetTbRedis_whenIdIsReceivedAndFoundInDb() throws Exception {
        //Given
        final String givenId = "col1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("000")
                .details1("col1")
                .details2("col2")
                .details3("col3")
                .createdDate(null)
                .build();
        //ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        final MvcResult mvcResult = mockMvc.perform(get("/spring-examples/tb-redis/" + givenId)
                .header(AUTHORIZATION, ""))
                .andExpect(status().isOk())
                .andReturn();
        final String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        final ResponseJavi actualResponse = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);

        //Then
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void ShouldReturnResponse100FromGetTbRedis_whenIdIsNotFoundInDb() throws Exception {
        //Given
        final String givenId = "col2";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("100TbRedis")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);

        //When
        final String mvcResult = mockMvc.perform(get("/spring-examples/tb-redis/" + givenId)
                .header(AUTHORIZATION, ""))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        //final String actualResponseAsJsonString = mvcResult.getResponse().getContentAsString();
        //final ResponseJavi actualResponse = objectMapper.readValue(actualResponseAsJsonString, ResponseJavi.class);
        final ResponseJavi actualResponse = objectMapper.readValue(mvcResult, ResponseJavi.class);

        //Then
        assertEquals(expectedResponse, actualResponse);
    }
}