package com.spring.springexamples.controller;

import com.spring.springexamples.model.RequestJavi;
import com.spring.springexamples.model.ResponseJavi;
import com.spring.springexamples.services.SpringExamplesGetService;
import com.spring.springexamples.services.SpringExamplesPostService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SpringExamplesControllerTest {

    @Mock
    private SpringExamplesPostService springExamplesPostService;

    @Mock
    private SpringExamplesGetService springExamplesGetService;

    @InjectMocks
    private SpringExamplesController springExamplesController;

    @Test
    void ShouldReturnResponseFromPost_whenRequestIsReceived() {
        //Given
        final RequestJavi givenRequestJavi = RequestJavi.builder()
                .dato1("dato1")
                .dato2("dato2")
                .dato3("dato3")
                .build();
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1("dato1")
                .details2("dato2")
                .details3("dato3")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesPostService.processPost(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesPost(givenRequestJavi);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }

    void ShouldReturnResponseFromPostValue_whenRequestIsReceived() {   //TODO
        //Given
        final RequestJavi givenRequestJavi = RequestJavi.builder()
                .dato1("dato1")
                .dato2("dato2")
                .dato3("dato3")
                .build();
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1("dato1")
                .details2("dato2")
                .details3("dato3")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesPostService.processPost(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesPost(givenRequestJavi);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }

    void ShouldReturnResponseFromPostConsume_whenRequestIsReceived() {   //TODO
        //Given
        final RequestJavi givenRequestJavi = RequestJavi.builder()
                .dato1("dato1")
                .dato2("dato2")
                .dato3("dato3")
                .build();
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("statusPost")
                .details1("dato1")
                .details2("dato2")
                .details3("dato3")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesPostService.processPost(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesPost(givenRequestJavi);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }

    @Test
    void ShouldReturnResponseFromGetTbPrueba_whenIdIsReceived() {
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("000TbPrueba")
                .details1("details1")
                .details2("details2")
                .details3("details3")
                .createdDate(null)
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesGetService.processGetTbPrueba(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesGetTbPrueba(givenId);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }

    @Test
    void ShouldReturnErrorResponseFromGetTbPrueba_whenIdIsNotValid() {
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("100TbPrueba")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesGetService.processGetTbPrueba(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesGetTbPrueba(givenId);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }

    @Test
    void ShouldReturnResponseFromGetTbRedis_whenIdIsReceived() {
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("000TbRedis")
                .details1("details1")
                .details2("details2")
                .details3("details3")
                .createdDate(null)
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesGetService.processGetTbRedis(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesGetTbRedis(givenId);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }

    @Test
    void ShouldReturnErrorResponseFromGetTbRedis_whenIdIsNotValid() {
        //Given
        final String givenId = "id1";
        final ResponseJavi expectedResponse = ResponseJavi.builder()
                .status("100TbRedis")
                .build();
        ResponseEntity<ResponseJavi> expectedResponseEntity = ResponseEntity.ok(expectedResponse);
        when(springExamplesGetService.processGetTbRedis(any())).thenReturn(expectedResponse);

        //When
        ResponseEntity<ResponseJavi> actualResponse = springExamplesController.springExamplesGetTbRedis(givenId);

        //Then
        assertEquals(expectedResponseEntity, actualResponse);
    }
}