package com.spring.springexamples;

import org.springframework.boot.test.context.TestConfiguration;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@TestConfiguration
public abstract class IntegrationTestsConfiguration {

    private RedisServer redisServer;

    @PostConstruct
    public void postConstruct() {
        redisServer = RedisServer.builder().port(6379).setting("maxheap 51200000").build();
        redisServer.start();
    }

    @PreDestroy
    public void preDestroy() {
        redisServer.stop();
    }
}
