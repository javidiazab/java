CREATE TABLE tb_prueba (
	col1_id VARCHAR(100) PRIMARY KEY,
	col2_id VARCHAR(100),
	col3_id TEXT,
	created_by VARCHAR(10),
	created_date DATE,
	last_modified_by VARCHAR(10),
	last_modified_date DATE
);