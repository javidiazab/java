package com.spring.springexamples.utils;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public final class ImplUtil {
    private ImplUtil() {
    }

    public static <T> Function<List<T>, T> firstInList() {
        return (list) -> {
            return list.isEmpty() ? null : list.get(0);
        };
    }

    public static <T> Function<List<T>, T> lastInList() {
        return (list) -> {
            return list.isEmpty() ? null : list.get(list.size() - 1);
        };
    }

    public static <T> Function<List<T>, T> getOrCreatePositionInList(int position, Supplier<T> constructor) {
        return (list) -> {
            IntStream.rangeClosed(list.size(), position).forEach((i) -> {
                list.add(constructor.get());
            });
            return list.get(position);
        };
    }
}