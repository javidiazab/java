package com.spring.springexamples.controller;

import com.spring.springexamples.model.exceptions.SpringExampleException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
public class SpringExamplesControllerAdvice {

    private int stackTraceLimit = 3;

//    @ExceptionHandler(SpringExampleException.class)
//    private ResponseEntity<ErrorResponse> handle(SpringExampleException e) {
//        final HttpStatus httpStatus = getHttpStatus(e);
//        final ErrorResponse errorResponse = ErrorResponse.builder()
//                .httpStatus(httpStatus)
//                .message(e.getMessage())
//                .errors(getExceptionErrorList(e))
//                .build();
//
//        return ResponseEntity.status(httpStatus).body(errorResponse);
//    }
//
//    private HttpStatus getHttpStatus(SpringExampleException e) {
//        return Optional.of(e)
//                .map(SpringExampleException::getHttpStatus)
//                .orElse(INTERNAL_SERVER_ERROR);
//    }

    private List<String> getExceptionErrorList(SpringExampleException exception) {
        return stream(exception.getStackTrace())
                .limit(stackTraceLimit)
                .map(this::formatStackTrace)
                .collect(toList());
    }

    private String formatStackTrace(StackTraceElement stackTraceElement) {
        return String.format("%s:%s", stackTraceElement.getClassName(), stackTraceElement.getMethodName());
    }
}
