package com.spring.springexamples.controller;

import com.spring.springexamples.model.RequestJavi;
import com.spring.springexamples.model.ResponseJavi;
import com.spring.springexamples.services.SpringExamplesGetService;
import com.spring.springexamples.services.SpringExamplesPostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/spring-examples")
@RequiredArgsConstructor
public class SpringExamplesController {

    private final SpringExamplesPostService springExamplesPostService;
    private final SpringExamplesGetService springExamplesGetService;

    @PostMapping("/post")
    public ResponseEntity<ResponseJavi> springExamplesPost (@RequestBody RequestJavi requestJavi) {
        return ResponseEntity.ok(springExamplesPostService.processPost(requestJavi));
    }

    //esto es lo mismo que no poner 'value ='
    @PostMapping(value = "/post/value")
    public ResponseEntity<ResponseJavi> springExamplesPostValue (@RequestBody RequestJavi requestJavi) {
        return ResponseEntity.ok(springExamplesPostService.processPost(requestJavi));
    }

    //eso no añade a la url. solo dice que el texto de entrada es texto (no json)
    @PostMapping(consumes = "test/plain")
    public ResponseEntity<ResponseJavi> springExamplesPostConsumes (@RequestBody String requestJavi) {
        return ResponseEntity.ok(ResponseJavi.builder()
                .status("statusPost")
                .details1(requestJavi)
                .details2("dato2")
                .details3("dato3")
                .createdDate(null)
                .build());
    }

    @GetMapping("/tb-prueba/{id}")
    public ResponseEntity<ResponseJavi> springExamplesGetTbPrueba (@PathVariable String id) {
        return ResponseEntity.ok(springExamplesGetService.processGetTbPrueba(id));
    }

    @GetMapping("/tb-redis/{id}")
    public ResponseEntity<ResponseJavi> springExamplesGetTbRedis (@PathVariable String id) {
        return ResponseEntity.ok(springExamplesGetService.processGetTbRedis(id));
    }
}
