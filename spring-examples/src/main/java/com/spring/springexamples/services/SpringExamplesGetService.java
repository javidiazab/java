package com.spring.springexamples.services;

import com.spring.springexamples.component.repository.TbPruebaRepository;
import com.spring.springexamples.component.repository.TbRedisRepository;
import com.spring.springexamples.model.ResponseJavi;
import com.spring.springexamples.model.entity.TbPrueba;
import com.spring.springexamples.model.entity.TbRedis;
import com.spring.springexamples.model.exceptions.SpringExampleException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class SpringExamplesGetService {
    private final TbPruebaRepository tbPruebaRepository;
    private final TbRedisRepository tbRedisRepository;

    public ResponseJavi processGetTbPrueba(String id) {
        Optional<TbPrueba> tbPrueba = tbPruebaRepository.findById(id);
        if (!tbPrueba.isPresent()) {
            return ResponseJavi.builder()
                    .status("100TbPrueba").build();
        }

        return ResponseJavi.builder()
                .status("000")
                .details1(tbPrueba.map(TbPrueba::getCol1).orElse(null))
                .details2(tbPrueba.map(TbPrueba::getCol2).orElse(null))
                .details3(tbPrueba.map(TbPrueba::getCol3).orElse(null))
                .createdBy(tbPrueba.map(TbPrueba::getCreatedBy).orElse(null))
                .createdDate(tbPrueba.map(TbPrueba::getCreateDate).orElse(null))
                .lastModifiedBy(tbPrueba.map(TbPrueba::getLastModifiedBy).orElse(null))
                .lastModifiedDate(tbPrueba.map(TbPrueba::getLastModifiedDate).orElse(null))
                .build();
    }

    public ResponseJavi processGetTbRedis(String id) {
        Optional<TbRedis> tbRedis = tbRedisRepository.findById(id);
        if (tbRedis.isPresent()) {
            throw new SpringExampleException("Error en 'processGetTbRedis'");
        }

        return ResponseJavi.builder()
                .status("000")
                .details1(tbRedis.map(TbRedis::getCol1).orElse(null))
                .details2(tbRedis.map(TbRedis::getCol2).orElse(null))
                .details3(tbRedis.map(TbRedis::getCol3).orElse(null))
                .build();
    }
}
