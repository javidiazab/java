package com.spring.springexamples.services;

import com.spring.springexamples.component.repository.TbPruebaRepository;
import com.spring.springexamples.component.repository.TbRedisRepository;
import com.spring.springexamples.model.RequestJavi;
import com.spring.springexamples.model.ResponseJavi;
import com.spring.springexamples.model.entity.TbPrueba;
import com.spring.springexamples.model.entity.TbRedis;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SpringExamplesPostService {

    private final TbPruebaRepository tbPruebaRepository;
    private final TbRedisRepository tbRedisRepository;

    public ResponseJavi processPost(RequestJavi requestJavi) {

        TbPrueba tbPrueba = TbPrueba.builder()
                .col1(requestJavi.getDato1())
                .col2(requestJavi.getDato2())
                .col3(requestJavi.getDato3())
                .build();

        TbRedis tbRedis = TbRedis.builder()
                .col1(requestJavi.getDato1())
                .col2(requestJavi.getDato2())
                .col3(requestJavi.getDato3())
                .build();

        tbPruebaRepository.save(tbPrueba);
        tbRedisRepository.save(tbRedis);

        return ResponseJavi.builder()
                .status("statusPost")
                .details1(requestJavi.getDato1())
                .details2(requestJavi.getDato2())
                .details3(requestJavi.getDato3())
                .createdDate(null)
                .build();
    }
}
