package com.spring.springexamples.model.exceptions;

public class SpringExampleException extends RuntimeException {
    public SpringExampleException(String message) {
        super(message);
    }
}
