package com.spring.springexamples.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Column;

@RedisHash(value = "RedisRegistry")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TbRedis {

    @Id
    @Column(name = "col1_id")
    private String col1;
    @Column(name = "col2_id")
    private String col2;
    @Column(name = "col3_id")
    private String col3;
}
