package com.spring.springexamples.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequestJavi {
    String dato1;
    String dato2;
    String dato3;
}
