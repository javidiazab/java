package com.spring.springexamples.generics;

public class Box<T>{
    T item;

    public T getItem() {
        return item;
    }

    public void put(T item) {
        this.item = item;
    }

    public <U> void putInBox(U u, Box<U> box){
        box.put(u);
    }

    public static <U> void staticPutInBox(U u, Box<U> box){
        box.put(u);
    }

}
