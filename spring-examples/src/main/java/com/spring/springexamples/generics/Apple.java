package com.spring.springexamples.generics;

public class Apple {
    private int flavor;

    public Apple(int flavor) {
        this.flavor = flavor;
    }

    public int getFlavor() {
        return flavor;
    }
}
