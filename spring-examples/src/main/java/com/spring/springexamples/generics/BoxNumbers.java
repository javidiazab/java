package com.spring.springexamples.generics;

public class BoxNumbers <T extends Number>{
    T item;

    public T getItem() {
        return item;
    }

    public void put(T item) {
        this.item = item;
    }

    public <U extends Double> void inspect(U u) {
        System.out.println("U: \" + u.getClass().getName()");
    }
}
