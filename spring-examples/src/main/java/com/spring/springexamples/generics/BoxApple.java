package com.spring.springexamples.generics;

public class BoxApple extends Box<Apple> {
    public int getFlavor() {
        return item.getFlavor();
    }
}
