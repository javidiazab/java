package com.spring.springexamples.component.repository;

import com.spring.springexamples.model.entity.TbPrueba;
import com.spring.springexamples.model.entity.TbRedis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface TbRedisRepository extends CrudRepository<TbRedis, String> {
}
