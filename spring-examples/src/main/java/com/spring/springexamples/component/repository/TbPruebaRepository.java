package com.spring.springexamples.component.repository;

import com.spring.springexamples.model.entity.TbPrueba;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbPruebaRepository extends JpaRepository<TbPrueba, String> {
}
