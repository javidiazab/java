package com.spring.springexamples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SpringExamplesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringExamplesApplication.class, args);
	}

}
