package com.appexample.app;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
public class Delete {
    private final Delete1 a;
    private final String b;
    private String c;

    public void delete () {
        System.out.println(this.toString());
    }
}
