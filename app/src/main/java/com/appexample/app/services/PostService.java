package com.appexample.app.services;

import com.appexample.app.Enum1;
import com.appexample.app.model.InputDto;
import com.appexample.app.model.OutputDto;

public class PostService {

    public OutputDto procces(InputDto inputDto) {
        Enum1 enum1 = Enum1.CUATRO;

        System.out.println("name: " + enum1.name());
        System.out.println("num1: " + enum1.getNum1());
        System.out.println("value1: " + enum1.getValue1());
        return OutputDto.builder().returnCode("01").message("message").build();
    }
}
