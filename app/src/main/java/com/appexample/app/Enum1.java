package com.appexample.app;

public enum Enum1 {
    UNO("1x", 1),
    DOS("2x", 2),
    TRES("3x", 3),
    CUATRO("4x", 4),
    CINCO("5x", 5),
    SEIS("6x", 6),
    SIETE("7x", 7),
    OCHO("8x", 8),
    NUEVE("9x", 9),
    DIEZ("10x", 10);

    private String num1;
    private Integer value1;

    Enum1(String num1, Integer value1) {
        this.num1 = num1;
        this.value1 = value1;
    }

    public String getNum1() {
        return num1;
    }

    public Integer getValue1() {
        return value1;
    }
}
