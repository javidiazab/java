package com.appexample.app.controller;

import com.appexample.app.model.InputDto;
import com.appexample.app.model.OutputDto;
import com.appexample.app.services.GetService;
import com.appexample.app.services.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/app")
@RequiredArgsConstructor
public class AppController {
    private final PostService postService;
    private final GetService getService;

    @PostMapping("/post")
    public ResponseEntity<OutputDto> procces(@Valid @RequestBody InputDto inputDto) {
        OutputDto outputDto = postService.procces(inputDto);
        return new ResponseEntity<>(outputDto, HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<OutputDto> getProcess(@PathVariable String id) {
        OutputDto outputDto = getService.procces(id);
        //return new ResponseEntity.ok(outputDto);   //TODO da error
        return new ResponseEntity<>(outputDto,HttpStatus.ACCEPTED);
    }
}
