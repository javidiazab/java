package com.appexample.app;

import com.appexample.app.model.InputDto;
import com.appexample.app.services.PostService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class appApplication {
    public static void main(String[] args) {
        SpringApplication.run(appApplication.class, args);

        System.out.println("-------------------------------------------");

        List<String> list = Arrays.asList("uno", "dos", "tres", "cuatro", "cinco");
        for (String s : list) {
            System.out.printf("lista: ", s, "again: ", s);
        }

        System.out.println("-------------------------------------------");

    }
}
