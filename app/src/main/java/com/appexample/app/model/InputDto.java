package com.appexample.app.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.lang.NonNull;

import java.util.Date;

@Data
@Builder
public class InputDto {
    @NonNull
    private Integer id;
    @NonNull
    private String name;
    private Date date;

}
