package com.appexample.app.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutputDto {
    private String returnCode;
    private String message;
}
