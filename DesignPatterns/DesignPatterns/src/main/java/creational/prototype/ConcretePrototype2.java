package creational.prototype;
/**
 * Implements an operation for cloning itself
 */
public class ConcretePrototype2 implements Prototype {

    public ConcretePrototype2() {
        System.out.println("constructing ConcretePrototype2");
    }

    public Prototype getClone() {
        // perform 'deep copy' if required
        return new ConcretePrototype1();
    }

    public void operation() {
        System.out.println("ConcretePrototype2.operation() executing");
    }

}