package creational.prototype;
/**
 * Implements an operation for cloning itself
 */
public class ConcretePrototype1 implements Prototype {

    public ConcretePrototype1() {
        System.out.println("constructing ConcretePrototype1");
    }

    public Prototype getClone() {
        // perform 'deep copy' if required
        return new ConcretePrototype1();
    }

    public void operation() {
        System.out.println("ConcretePrototype1.operation() executing");
    }

}