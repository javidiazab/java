package creational.prototype;

/**
 * Defines an interface for cloning itself
 */
public interface Prototype {

    /** getClone() is used to seperate from Object's clone() method */
    public Prototype getClone();
    public void operation();

}