package creational.prototype;
/**
 * Represents the client of this pattern.
 * Creates a new object by asking a prototype to clone itself
 */
public class Client {

    public static void main(String[] args) {
        // create prototypical objects
        Prototype p1 = new ConcretePrototype1();
        Prototype p2 = new ConcretePrototype2();

        // generate objects from prototypical objects
        Prototype gp1 = p1.getClone();
        Prototype gp2 = p2.getClone();

        // call 'cloned' object's methods
        gp1.operation();
        gp2.operation();
    }

}