package creational.factorymethod;
/**
 * Implementation of Product interface
 */
public class ConcreteProduct implements Product {

    public void operation() {
        System.out.println("Product.operation() executed");
    }

}