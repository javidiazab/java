package creational.factorymethod;
/**
 * Declares the factory method, which returns a Product object
 */
public abstract class Creator {

    public abstract Product factoryMethod();

}