package creational.factorymethod;

/**
 * Defines the interface of products the factory creates
 */
public interface Product {

    public void operation();

}