package creational.factorymethod;
/**
 * Implements the factory method to return a ConcreteProduct instance
 */
public class ConcreteCreator extends Creator {

    public Product factoryMethod() {
        return new ConcreteProduct();
    }

}