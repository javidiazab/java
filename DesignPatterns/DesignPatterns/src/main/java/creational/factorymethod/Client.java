package creational.factorymethod;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // create creator (strange ;) )
        Creator c = new ConcreteCreator();

        // use factory method to create product
        Product p = c.factoryMethod();

        // use product
        p.operation();

    }

}