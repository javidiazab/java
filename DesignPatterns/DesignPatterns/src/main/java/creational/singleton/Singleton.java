package creational.singleton;

public class Singleton {
    private static Singleton instance; // Own private instance

    private double instanceID;

    /* Protected to enable controlled subclassing */
    private Singleton() {
        instanceID = Math.random() * 20;
    }

    public static Singleton getInstance() {
        // Lazy evaluation of instance
        if (instance == null) {
            instance = new Singleton();
        }

        return instance;
    }

    public void operation() {
        System.out.println("Singleton.operation() executing at: " + getInstanceID());
    }

    public double getInstanceID() {
        // Return the instance ID, a random value
        return instanceID;
    }

}