package creational.builder;

/**
 * Interface for the complex object under construction
 */
public interface Product {

    public void operation();

}