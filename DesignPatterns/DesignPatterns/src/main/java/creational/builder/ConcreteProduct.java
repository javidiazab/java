package creational.builder;

/**
 * Implementation of a  for the complex object under construction
 */
public class ConcreteProduct implements Product {

    public ConcreteProduct() {
        System.out.println("constructing ConcreteProduct object");
    }

    public void operation() {
        System.out.println("ConcreteProduct.operation() executed");
    }

}