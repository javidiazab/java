package creational.builder;
/**
 * Specifies an interface for creating Product objects
 */
public interface Builder {

    public void buildPart();
    public Product getPart();

}