package creational.builder;
/**
 * Constructs and assembles parts of the product
 * Defines and tracks representation it creates
 * Provides an interface for retrieving the product
 */
public class ConcreteBuilder implements Builder {
    Product p;

    public void buildPart() {
        p = new ConcreteProduct();
        // some more complex work with product
    }

    public Product getPart() {
        return p;
    }

}