package creational.builder;

/**
 * Constructs an object using the Builder interface
 */
public class Director {
    Builder build;

    public Director(Builder builder) {
        this.build = builder;
    }

    public void construct() {
        build.buildPart();
    }

}