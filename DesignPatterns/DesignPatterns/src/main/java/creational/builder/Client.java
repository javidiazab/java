package creational.builder;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // create builder
        Builder b = new ConcreteBuilder();

        // create director
        Director d = new Director(b);

        // construct, obtain and use
        d.construct();
        Product p = b.getPart();
        p.operation();
    }


}