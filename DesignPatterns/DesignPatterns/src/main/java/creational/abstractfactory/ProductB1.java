package creational.abstractfactory;
/**
 * Declares a product object to be created by the corresponding factory
 * Implements the AbstractProductB interface
 */
public class ProductB1 implements AbstractProductB {

    public void operationB() {
        System.out.println("I am a ProductB1");
    }

}