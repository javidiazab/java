package creational.abstractfactory;
/**
 * Declares a product object to be created by the corresponding factory
 * Implements the AbstractProductA interface
 */
public class ProductA2 implements AbstractProductA {

    public void operationA() {
        System.out.println("I am a ProductA2");
    }

}