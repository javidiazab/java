package creational.abstractfactory;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // create factories
        AbstractFactory factoryOne = new ConcreteFactory1();
        AbstractFactory factoryTwo = new ConcreteFactory2();

        // use factories to create products
        AbstractProductA productA1 = factoryOne.createProductA();
        AbstractProductB productB1 = factoryOne.createProductB();

        AbstractProductA productA2 = factoryTwo.createProductA();
        AbstractProductB productB2 = factoryTwo.createProductB();

        // call methods on the products
        productA1.operationA();
        productB1.operationB();

        productA2.operationA();
        productB2.operationB();
    }

}