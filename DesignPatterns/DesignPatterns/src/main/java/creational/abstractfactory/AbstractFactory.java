package creational.abstractfactory;

/**
 * Declares an interface for operations that create
 * abstract product objects.
 */
public interface AbstractFactory {

    public AbstractProductA createProductA();
    public AbstractProductB createProductB();

}