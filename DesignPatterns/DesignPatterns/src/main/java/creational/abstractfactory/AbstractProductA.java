package creational.abstractfactory;

/**
 * Declares an interface for a type of product object.
 */
public interface AbstractProductA {

    public void operationA();

}