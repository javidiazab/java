package structural.flyweight;

import java.util.*;

/**
 * Creates and manages Flyweight objects.
 * Ensures flyweights are shared properly.
 * When a client requests a flyweight, the FlyweightFactory object supplies an existing instance
 * or creates one if none exist.
 */
public class FlyweightFactory {
    private static Map flyweights = new HashMap();

    /* Return a Flyweight if in the collection, otherwise create, add to collection and then return */
    public static Flyweight getFlyweight(String key) {
        Flyweight fw = null;

        try {
            if (flyweights.containsKey(key)) {
                fw = (Flyweight) flyweights.get(key);

            } else {
                // create a flyweight with an arbitrary 'true' state
                fw = new ConcreteFlyweight(new IntrinsicState(true));
                flyweights.put(key, fw);

            }

        } catch (ClassCastException e) {
            System.out.println(e.getMessage());
        }

        return fw;

    }

}