package structural.flyweight;
/**
 * Defines an flyweight's extrinsic state.
 */
public class ExtrinsicState {
    private boolean state;

    public ExtrinsicState(boolean extrinsicState) {
        this.state = extrinsicState;
    }

    public boolean getState() {
        return state;
    }

}