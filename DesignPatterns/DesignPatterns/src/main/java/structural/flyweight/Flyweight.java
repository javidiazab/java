package structural.flyweight;
/**
 * Declares an interface through which flyweights can receive
 * and act on extrinsic state.
 */
public interface Flyweight {

    public void operation(ExtrinsicState state);

}