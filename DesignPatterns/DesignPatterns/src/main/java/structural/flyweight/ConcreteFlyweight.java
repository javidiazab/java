package structural.flyweight;
/**
 * Implements the Flyweight interface and adds storage for intrinsic state.<br>
 * Must be shareable.<br>
 * Any state it stores must be intrinsic (independant from the ConcreteFlyweight
 * object's context.
 */
public class ConcreteFlyweight implements Flyweight {
    private boolean intrinsicState;

    public ConcreteFlyweight(IntrinsicState state) {
        this.intrinsicState = state.getState();
    }

    public void operation(ExtrinsicState state) {
        // uses extrinsic state passed by client, and intrinsic state from within
        System.out.println("ConcreteFlyweight.operation(ExtrinsicState state) executing\n" +
            "Intrinsic State = " + intrinsicState + "\n" +
            "Extrinsic State = " + state.getState());
    }

}