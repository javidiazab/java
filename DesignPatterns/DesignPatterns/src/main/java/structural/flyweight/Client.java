package structural.flyweight;
/**
 * Represents the client of this pattern.
 * Maintains a reference to flyweights.
 * Computes or stores the extrinsic state of flyweights.
 */
public class Client {

    public static void main(String[] args) {
        // create extrinsic states
        ExtrinsicState state1 = new ExtrinsicState(false);
        ExtrinsicState state2 = new ExtrinsicState(true);

        // obtain same 'key' flyweight twice
        Flyweight fw1 = FlyweightFactory.getFlyweight("Flyweight1");
        Flyweight fw2 = FlyweightFactory.getFlyweight("Flyweight1");

        // use operation on flyweight
        fw1.operation(state1);
        fw2.operation(state2);
    }

}