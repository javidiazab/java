package structural.flyweight;

/**
 * Defines an flyweight's intrinsic state.
 */
public class IntrinsicState {
    private boolean state;

    public IntrinsicState(boolean extrinsicState) {
        this.state = extrinsicState;
    }

    public boolean getState() {
        return state;
    }

}