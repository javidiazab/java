package structural.decorator;
/**
 * Adds responsibilities to the component.
 */
public class ConcreteDecoratorA extends Decorator {
    boolean addedState;

    public void operation() {
        super.operation();
        System.out.println("ConcreteDecoratorA.operation() executing");
        addedState = true;
        System.out.println("addedState is now " + addedState);
    }

}