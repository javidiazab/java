package structural.decorator;
/**
 * Defines an object to which additional responsibilities can be attached.
 */
public class ConcreteComponent implements Component {

    public void operation() {
        System.out.println("ConcreteComponent.operation() executing");
    }
}