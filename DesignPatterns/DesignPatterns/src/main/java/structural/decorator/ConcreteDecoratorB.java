package structural.decorator;
/**
 * Adds responsibilities to the component.
 */
public class ConcreteDecoratorB extends Decorator {

    public void operation() {
        super.operation();
        System.out.println("ConcreteDecoratorB.operation() executing");
        anotherOperation();
    }

    private void anotherOperation() {
        System.out.println("ConcreteDecoratorB.anotherOperation() executing");
    }

}