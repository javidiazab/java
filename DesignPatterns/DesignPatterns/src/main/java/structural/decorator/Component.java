package structural.decorator;
/**
 * Defines an interface for objects that can have responsibilities added to them dynamically.
 */
public interface Component {

    public void operation();

}