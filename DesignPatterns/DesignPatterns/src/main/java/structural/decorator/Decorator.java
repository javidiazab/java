package structural.decorator;

/**
 * Maintains a reference to a Component object and defines an interface that conforms to
 * Component's interface.
 */
public class Decorator implements Component {
    Component c = new ConcreteComponent();

    public void operation() {
        c.operation();
    }
}