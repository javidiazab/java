package structural.proxy;
/**
 * Maintains a reference that lets the proxy access the real subject.<br>
 * Provides identical interface to RealSubject's so that a proxy can be
 * substituted for the real object.<br>
 * Controls access to the real subject and may be responsible for creating
 * and deleting it.
 */
public class Proxy implements Subject {
    private RealSubject rs = new RealSubject();

    public void request() {
        rs.request();
    }

}