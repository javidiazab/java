package structural.proxy;
/**
 * Defines the real subject the proxy represents.
 */
public class RealSubject implements Subject {

    public void request() {
        System.out.println("RealSubject.request() executing");
    }

}