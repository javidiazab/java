package structural.proxy;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // create subject proxy
        Subject s = new Proxy();

        // call request through proxy
        s.request();
    }

}