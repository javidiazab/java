package structural.proxy;

/**
 * Defines the common interface for RealSubject and Proxy so that a Proxy can be used
 * anywhere a RealSubject is expected.
 */
public interface Subject {

    public void request();

}