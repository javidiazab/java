package structural.composite;

/**
 * Represents a leaf in the composition, no children
 */
public class Leaf extends Component {
    private String identifier;

    public Leaf(String info) {
        this.identifier = info;
    }

    public void operation() {
        System.out.println("Leaf.operation() [info: " + identifier + "]");
    }
}