package structural.composite;
/**
 * Declares interface for objects in the composition
 * Implements default behavior for interface common to classes
 */
public abstract class Component {

    public abstract void operation();
    public void add(Component c) { /* empty */; }
    public void remove(Component c) { /* empty */; }
    public Component getChild(int index) { return null; }

}