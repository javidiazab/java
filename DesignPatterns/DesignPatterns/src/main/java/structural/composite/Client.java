package structural.composite;
/**
 * Represents the client of this pattern.
 */
public class Client {

/*
                         componentsCollection
                                      |
                            -----------------
                          |                      |
                   components     component3
                          |
                -----------------
               |                     |
       component1     component2
*/

    public static void main(String[] args) {
        // create leaf nodes
        Component component1 = new Leaf("leaf one");
        Component component2 = new Leaf("leaf two");
        Component component3 = new Leaf("leaf three");

        // create compositions of components
        Component components = new Composite();
        Component componentsCollection = new Composite();

        // add leaves to composition
        components.add(component1);
        components.add(component2);

        // add composition to composition
        componentsCollection.add(components);

        // add a leaf to collection of compositions
        componentsCollection.add(component3);

        // test this by calling operation() on composition (should recurse to leaves)
        componentsCollection.operation();
    }
}








