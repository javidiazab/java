package structural.bridge;
/**
 * Implements the Implementor interface and defines it's concrete implementation.
 */
public class ConcreteImplementorA implements Implementor {

    public ConcreteImplementorA() {
        System.out.println("ConcreteImplementorA created");
    }

    public void operationImpl() {
        System.out.println("ConcreteImplementorA.operationImpl() called");
    }

}