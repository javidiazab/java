package structural.bridge;

/**
 * Implements the interface defined by Abstraction.
 */
public class RefinedAbstractionB implements Abstraction {
    Implementor imp = new ConcreteImplementorB();

    public RefinedAbstractionB() {
        System.out.println("RefinedAbstractionB created");
    }

    public void operation() {
        imp.operationImpl();
    }

}