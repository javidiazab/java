package structural.bridge;

/**
 * Implements the interface defined by Abstraction.
 */
public class RefinedAbstractionA implements Abstraction {
    Implementor imp = new ConcreteImplementorA();

    public RefinedAbstractionA() {
        System.out.println("RefinedAbstractionA created");
    }

    public void operation() {
        imp.operationImpl();
    }

}