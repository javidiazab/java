package structural.bridge;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {

        Abstraction a = new RefinedAbstractionA();
        a.operation();

        Abstraction b = new RefinedAbstractionB();
        b.operation();
    }

}