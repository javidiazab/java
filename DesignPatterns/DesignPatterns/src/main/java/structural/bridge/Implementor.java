package structural.bridge;
/**
 * Defines the interface for implementation classes&46; This interface doesn't have to
 * correspond exactly to Abstraction's interface; the two interfaces can be different&46;
 * Typically the Impementor interface only provides primitive operations, and Abstraction
 * defines higher-level operations based on these primitives.
 */
public interface Implementor {

    public void operationImpl();

}