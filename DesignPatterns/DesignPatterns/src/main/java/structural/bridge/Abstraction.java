package structural.bridge;
/**
 * Defines the abstraction's interface.<br>
 * Maintains a reference to an object of type Implementor.
 */
public interface Abstraction {

    public void operation();

}