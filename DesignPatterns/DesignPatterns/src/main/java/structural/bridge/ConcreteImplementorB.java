package structural.bridge;
/**
 * Implements the Implementor interface and defines it's concrete implementation.
 */
public class ConcreteImplementorB implements Implementor {

    public ConcreteImplementorB() {
        System.out.println("ConcreteImplementorB created");
    }

    public void operationImpl() {
            System.out.println("ConcreteImplementorB.operationImpl() called");
    }

}