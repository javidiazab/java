package structural.facade;
/**
 * Implements sub-system functionality
 * Handles work assigned by the Facade
 * Has no knowledge of the Facade, (keep no references).
 */
public class SubSystemA {

    public void operationA() {
        System.out.println("SubSystemA.operationA() executing");
    }

}