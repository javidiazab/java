package structural.facade;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {

        // construct and use a fa�ade
        new Facade().operation();

    }

}