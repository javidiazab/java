package structural.facade;

/**
 * Implements sub-system functionality.<br>
 * Handles work assigned by the Fa�ade.<br>
 * Has no knowledge of the Fa�ade, (keep no references).
 */
public class SubSystemB {

    public void operationB() {
        System.out.println("SubSystemB.operationB() executing");
    }

}