package structural.facade;
/**
 * Knows which subsystem classes are responsible for a request.
 * Delegates client requests to appropriate subsystem classes.
 */
public class Facade {

    public void operation() {
        // call specific sub-system object's methods
        new SubSystemA().operationA();
        new SubSystemB().operationB();
    }

}