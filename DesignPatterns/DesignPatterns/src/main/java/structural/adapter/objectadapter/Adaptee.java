package structural.adapter.objectadapter;
/**
 * Provides Specific method implemention.
 */
public class Adaptee {

    public String specificRequest() {
        return "Specific request on Adaptee";
    }

}