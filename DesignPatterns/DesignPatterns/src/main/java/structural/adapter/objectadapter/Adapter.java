package structural.adapter.objectadapter;
/**
 * Using composition, expose a domian specific interface for client.
 */
public class Adapter implements Target {
    private Adaptee a;

    public String request() {
            // lazy create an Adaptee
            if (a == null) { a = new Adaptee(); }

            return a.specificRequest();
    }

}