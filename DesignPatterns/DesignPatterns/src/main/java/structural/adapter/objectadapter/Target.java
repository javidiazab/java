package structural.adapter.objectadapter;

/**
 * Domain specific interface
 */
public interface Target {

    public String request();

}