package structural.adapter.objectadapter;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // create and call target method.
        Target a = new Adapter();
        System.out.println(a.request());
    }

}