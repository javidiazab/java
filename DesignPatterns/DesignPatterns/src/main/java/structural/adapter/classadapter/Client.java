package structural.adapter.classadapter;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // create and call target method.
        Target t = new Adapter();
        System.out.println(t.request());

    }
}