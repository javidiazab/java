package structural.adapter.classadapter;
/**
 * Implements concrete specific request
 */
public class Adaptee {

    public String specificRequest() {
        return "Specific request on Adaptee";
    }

}