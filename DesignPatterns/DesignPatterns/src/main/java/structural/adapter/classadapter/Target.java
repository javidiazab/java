package structural.adapter.classadapter;

/**
 * Target interface implementing domain specific interface
 */
public interface Target {

    public String request();

}