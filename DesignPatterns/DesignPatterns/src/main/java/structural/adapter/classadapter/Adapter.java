package structural.adapter.classadapter;
/**
 * Provides adaptation for Client
 */
public class Adapter extends Adaptee implements Target {

    public String request() {
        return specificRequest();
    }

}