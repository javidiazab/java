package javaPatterns;

/*
   * Some Design patterns in Java itself
 */

import java.net.Inet4Address;
import java.util.*;

public class JavaPatternsMain {

    public static void main (String args []){

        // Iterator pattern
        iteratorPattern();

        // Factory pattern
        factoryPattern();

        // Visitor pattern
        visitorPattern();

        // Flyweight pattern
        flyweightPattern();

        // Abstract Factory pattern
        abstractFactory();

        // Builder pattern
        builderFactory();

        // Façade pattern
        facadePattern();

    } // End main

    private static void iteratorPattern(){
        ArrayList<String> patterns = new ArrayList<String>();
        patterns.add("Factory");
        patterns.add("Template");
        patterns.add("Observer");
        patterns.add("Singleton");
        // Get the iterator
        Iterator<String> it = patterns.iterator();

        System.out.println("Iterator pattern");
        // Print all items
        while (it.hasNext()) {
            System.out.println(it.next());
        } // End while
    } // End iteratorPattern

    private static void factoryPattern(){
        Set synchronizedSet = Collections.synchronizedSet(new HashSet());
        List<Integer> unmodifiableList = Collections.unmodifiableList(new ArrayList());
        System.out.println("Factory pattern");
        System.out.println(synchronizedSet);
        System.out.println(unmodifiableList);
    } // End factoryPattern

    private static void visitorPattern(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("First", "Element");
        map.put("Second", "Item");
        map.put("Third", "Object");
        System.out.println("Visitor pattern");
        for(String anObject : map.values()){
            System.out.println (anObject.length());
        } // End for
    } // End visitorPattern

    private static void flyweightPattern(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("First", "Element");
        map.put("Second", "Item");
        map.put("Third", "Object");
        System.out.println("Flyweight Pattern");
        System.out.println(map.keySet());
    } // End flyweightPattern

    private static void abstractFactory(){
        // Set class is abstract to it cannot be instantiated but it can be used
        // ERROR Set theSet = new Set();
        Set theSet = new HashSet();
        System.out.println("Abstract Factory Pattern");
        System.out.println(theSet.size());
    } // End abstractFactory

    private static void builderFactory(){

        double item1 = Double.valueOf("1");
        double item2 = Double.valueOf(0x1);
        double item3 = Double.valueOf("1.0");
        System.out.println("Builder Pattern");
        System.out.println(item1);
        System.out.println(item2);
        System.out.println(item3);
    } // End builderFactory

    private static void facadePattern(){
        TreeSet<String> ts = new TreeSet<>();
        ts.add("A");
        ts.add("C");
        ts.add("N");
        ts.add("W");
        ts.add("Z");
        ts.add("O");
        ts.add("F");
        ts.add("B");

        Set<String> subSet = ts.subSet("A", "H");
        System.out.println("Façade Pattern");
        System.out.println(subSet);

    } // End facadePattern

} // End class JavaPatternsMain
