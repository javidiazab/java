package behavioural.observer;
/**
 * Defines an updating interface for objects that should be
 * notified of changes in a particular subject
 */
public interface Observer {

    public void update();

}