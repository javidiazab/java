package behavioural.observer.tail;

import java.io.File;

/**
 * ObserverTail.java
 * Class implementing the actual algorithm, extends Thread to allow
 * creating a separate process for execution
 */
public class ObserverTail extends java.lang.Thread{

    // Object to be notified if the file is changed
    private IObserverTail objectToBeNotified = null;
    public ObserverTail(IObserverTail tobeNotified) {
        this.objectToBeNotified = tobeNotified;
        // Start the watching thread
        new Thread(this).start();
    } // End Constructor

    public void run() {
        long fileLength = getFileLength("FileObserved.txt");
        long oldFileLenght = getFileLength("FileObserved.txt");
        try{
            while(true) {
                if (oldFileLenght != fileLength){
                    // Notify the object that the file has changed
                    objectToBeNotified.changed();
                    // Store the old file length
                    oldFileLenght = fileLength;
                } // End if
                // Get actual file length
                fileLength = getFileLength("FileObserved.txt");
                TestObserverTail.wait (1000, "");
                // System.out.println("fileLength: " + fileLength + ", oldFileLenght: " + oldFileLenght);
            } // End while
        } catch (Exception ex){
            // Control errors here
        } // End try-catch
    } // End run

    private long getFileLength(String fileName){
        try {
            File f =  new File(fileName);
            if (!f.exists())
                f.createNewFile();
            // Get the length of the file
            return f.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

} // End class
