package behavioural.observer.tail;

/**
 * IObserverTail.java
 * Interface to be implemented by all classes that watch for a
 * change in an item: Observer pattern
 */
public interface IObserverTail {
    // This method will be called each time the Observed item changes
    public void changed();

} // End Interface
