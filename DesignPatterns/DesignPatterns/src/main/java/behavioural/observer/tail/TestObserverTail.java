package behavioural.observer.tail;

/**
 * Class that implements interface IObserverTail and therefore, contains
 * the method changed()
 */
public class TestObserverTail implements IObserverTail {
    @SuppressWarnings("unused")
    public static void main(String[] args){
        // Instantiate this object
        TestObserverTail tot = new TestObserverTail ();
        // Instantiate an object to watch for the file and
        // register this object to be notified.
        ObserverTail ot = new ObserverTail(tot);
        while (true) {
            wait(1000, "Waiting for a change...");
        } // End while
    } // End main

    public void changed() {
        System.out.println("File has changed!");
    } // End changed

    public static void wait(int ms, String message){
        try {
            Thread.sleep(ms);
            if (message!="") System.out.println(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    } // End wait
} // End class
