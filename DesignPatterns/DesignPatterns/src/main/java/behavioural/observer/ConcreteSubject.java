package behavioural.observer;
/**
 * Stores the state of interest to ConcreteObserver objects.
 * Sends a notification to it's observers when it's state changes.
 */
public class ConcreteSubject extends Subject {
    private static boolean subjectState; // arbitrary state

    public static boolean getState() {
        return subjectState;
    }

    public static void setState(boolean state) {
        subjectState = state;
    }

}