package behavioural.observer;

/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // construct observer
        Observer observer1 = new ConcreteObserver();
        Observer observer2 = new ConcreteObserver();

        // construct subject
        ConcreteSubject subject = new ConcreteSubject();

        // attach observer to the subject
        subject.attach(observer1);
        subject.attach(observer2);

        // set state and notifyStateChange
        subject.setState(true);
        subject.notifyStateChange();

        subject.detach(observer1);

        // set state again and notifyStateChange
        subject.setState(false);
        subject.notifyStateChange();

    }

}