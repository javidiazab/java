package behavioural.observer;
import java.util.*;

/**
 * Knows it's observers. Any number of Observer objects may observe
 * a subject&46;Provides an interface for attaching and detaching Observer objects.
 */
public abstract class Subject {
    List<Observer> observers = new ArrayList<>();

    /** Attach an observer */
    public void attach(Observer o) {
        observers.add(o);
    }

    /** Detach a particular observer */
    public void detach(Observer o) {
        observers.remove(observers.indexOf(o));
    }

    /** Notify all registered observers */
    public void notifyStateChange() {

        for (Object observer : observers) {
            ((Observer) observer).update();
        }

    }

}