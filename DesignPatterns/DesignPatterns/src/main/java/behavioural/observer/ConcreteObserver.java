package behavioural.observer;
/**
 * Maintains a reference to the ConcreteSubject object.
 * Stores state that should stay consistent with the observer.
 * Implements the Observer updating interface to keep it's state
 * consistent with the subject's.
 */
public class ConcreteObserver implements Observer {
    private boolean observerState;
    private ConcreteSubject subject;

    public void update() {

        if (subject == null) { subject = new ConcreteSubject(); }

        observerState = subject.getState();
        System.out.println("ConcreteObserver.update() reports observerState now " + observerState);
    }

}