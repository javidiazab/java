package behavioural.state;

/**
 * Defines an interface for excapsulating the behavior
 * associated with a particular state of the Context.
 */
public interface State {

    public void handle();

}