package behavioural.state;
/**
 * Each subclass implements a behavior associated with a state
 * of the Context.
 */
public class ConcreteState1 implements State {

    public void handle() {
        System.out.println("ConcreteState1.handle() executing");
    }

}