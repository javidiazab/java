package behavioural.state;
/**
 * Defines the interface of interest to clients.
 * Maintains an instance of a concrete state subclass that defines the
 * current state.
 */
public class Context {
    public static final int STATE_ONE = 0;
    public static final int STATE_TWO = 1;

    private State currentState = new ConcreteState1();

    // storage for lazy loaded state classes
//  private ConcreteState1 state1;
//  private ConcreteState2 state2;


    public void request() {
        currentState.handle();
    }

    public void changeState(int state) {

        switch (state) {

            case STATE_ONE:
                currentState    = new ConcreteState1();
                break;

            case STATE_TWO:
                currentState    = new ConcreteState2();
                break;

        }

    }

}