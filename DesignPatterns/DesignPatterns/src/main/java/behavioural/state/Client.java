package behavioural.state;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // construct context
        Context ctx = new Context();

        // call request
        ctx.request();

        // change the state
        ctx.changeState(Context.STATE_TWO);

        // call request
        ctx.request();

    }

}