package behavioural.memento;


/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {

        // run the Caretaker
        Caretaker.perform();

    }

}