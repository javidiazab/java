package behavioural.memento;

/**
 * Responsible for the memento's safekeeping&46; Never operates on or examines the
 * contents of a memento.
 */
public class Caretaker {
    private static Memento state;

    public static void perform() {
        // create originator
        Originator orig = new Originator();

        // use originator to create a memento
        Memento memento = orig.createMemento();
        orig.printStatus();

        // change memento state
        orig.changeState();
        orig.printStatus();

        // undo by restoring memento state
        orig.setMemento(memento);
        orig.printStatus();

    }

}