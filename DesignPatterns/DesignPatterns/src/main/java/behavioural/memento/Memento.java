package behavioural.memento;
/**
 * Stores internal state of the Originator's object. The memento may store
 * as much or as little of the originator's internal state as necessary at it's
 * originators discretion.
 * Protects against access by objects other than the originator. Mementos have
 * effectively two interfaces. Caretaker sees a <i>narrow</i> interface to the
 * Memento; it can only pass the memento to other objects. Originator, in contrast,
 * sees a wide interface, one that lets it access all the data necessary to restore itself
 * to it's previous state. Ideally, only the originator that produced the memento
 * would be permitted to access the memento's internal state.
 */
public class Memento {
    /* Memento represents a 'narrow' interface by limiting it's constructor and methods to package-private */
    private boolean mementoState;

    /** Default package access given to simulate C++ friend class */
    Memento(boolean state) {
        this.mementoState = state;
    }

    /** Default package access given to simulate C++ friend class */
    boolean getState() {
        return this.mementoState;
    }

}