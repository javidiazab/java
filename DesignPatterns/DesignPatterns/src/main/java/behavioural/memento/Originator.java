package behavioural.memento;
/**
 * Creates a memento containing a snapshot of it's current internal state&46; Uses the memento
 * to restore it's internal state.
 */
public class Originator {
    private boolean state;

    /** Restore memento state */
    public void setMemento(Memento m) {
        /* use direct package-private access to method */
        this.state = m.getState();
    }

    public Memento createMemento() {
        return new Memento(state);
    }

    /** Arbitrary state machine. Toggles between true/false */
    public void changeState() {
        state = state ? false : true;
    }

    public void printStatus() {
        System.out.println("Originator.printStatus() reports; state is currently [" + state + "]");
    }

}