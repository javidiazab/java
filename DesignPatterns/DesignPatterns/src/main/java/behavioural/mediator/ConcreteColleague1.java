package behavioural.mediator;
/**
 * Each Colleague knows it's Mediator object.<br>
 * Each Colleague communicates with it's Mediator whenever it would have otherwise
 * communicated with another colleague.
 */
public class ConcreteColleague1 implements Colleague {
    private Mediator med;
    private boolean state;

    public ConcreteColleague1(Mediator mediator) {
        this.med = mediator;
        med.register(this);
    }

    /* Arbitrary state machine */
    public void changeState() {
        state = state ? false : true;
        med.stateChanged();
    }

    public String toString() {
        return new Boolean(state).toString();
    }

}