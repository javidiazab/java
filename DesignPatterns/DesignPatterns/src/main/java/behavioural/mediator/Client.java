package behavioural.mediator;

/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // construct mediator
        Mediator med = new ConcreteMediator();

        // construct colleagues, passing in the mediator
        Colleague colleague1 = new ConcreteColleague1(med);
        Colleague colleague2 = new ConcreteColleague2(med);

        // get initial state and value of colleagues
        System.out.println("colleague1.toString() [" + colleague1 + "]");
        System.out.println("colleague2.toString() [" + colleague2 + "]");

        // change state on colleague one, mediator should get other to change
        ((ConcreteColleague1) colleague1).changeState();

        // get final state and value of colleagues
        System.out.println("colleague1.toString() [" + colleague1 + "]");
        System.out.println("colleague2.toString() [" + colleague2 + "]");

    }

}