package behavioural.mediator;
/**
 * Implements cooperative behaviour by coordinating Colleague objects.
 * Knows and maintains it's colleagues.
 */
public class ConcreteMediator implements Mediator {
    Colleague colleague1;
    Colleague colleague2;

    public void register(Colleague colleague) {

        if (colleague instanceof ConcreteColleague1) {
            this.colleague1 = (ConcreteColleague1) colleague;

        } else if (colleague instanceof ConcreteColleague2) {
            this.colleague2 = (ConcreteColleague2) colleague;
        }

    }

    public void stateChanged() {
        String s = (colleague2.toString().equals("false")) ? "true" : "false";
        ((ConcreteColleague2) colleague2).setValue(s);
    }

}