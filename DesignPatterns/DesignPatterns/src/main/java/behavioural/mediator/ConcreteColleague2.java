package behavioural.mediator;
/**
 * Each Colleague knows it's Mediator object.<br>
 * Each Colleague communicates with it's Mediator whenever it would have otherwise
 * communicated with another colleague.
 */
public class ConcreteColleague2 implements Colleague {
    private Mediator med;
    private String val = "false";

    public ConcreteColleague2(Mediator mediator) {
        this.med = mediator;
        med.register(this);
    }

    public void setValue(String value) {
        this.val = value;
    }

    public String toString() {
        return val;
    }

}