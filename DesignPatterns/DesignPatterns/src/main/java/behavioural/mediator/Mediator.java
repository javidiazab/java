package behavioural.mediator;
/**
 * Defines an interface for communicating with colleague objects.
 */
public interface Mediator {

    public void register(Colleague colleague);
    public void stateChanged();

}