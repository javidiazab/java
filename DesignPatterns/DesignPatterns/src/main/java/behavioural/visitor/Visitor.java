package behavioural.visitor;

/**
 * Declares a visit operation for each class of ConcreteElement in the
 * object structure&#46; The operations name and signature identifies
 * the class that sends the 'visit' request to the visitor&#46; That lets the
 * visitor determine the concrete class of the element being visited&#46;
 * Then the visitor can access the element directly through it's particular
 * interface&#46;
 */
public interface Visitor {

    public void visitConcreteElement1(ConcreteElement1 element1);
    public void visitConcreteElement2(ConcreteElement2 element2);

}