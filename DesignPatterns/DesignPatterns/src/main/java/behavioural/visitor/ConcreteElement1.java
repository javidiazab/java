package behavioural.visitor;
/**
 * Implements an accept operation that takes a visitor as an argument.
 */
public class ConcreteElement1 implements Element {

    public void accept(Visitor v) {
        v.visitConcreteElement1(this);
    }

    public void operationA() {
        System.out.println("ConcreteElement1.operationA() executing");
    }

}