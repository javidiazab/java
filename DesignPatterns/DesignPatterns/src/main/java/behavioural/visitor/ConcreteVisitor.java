package behavioural.visitor;
/**
 * Implements each operation declared by the Visitor interface&#46;
 * Each operation implements a fragment of the algorithmdefined for the
 * corresponding class of the object in the stucture&#46; ConcreteVisitor
 * provides the context for the algorithm, and stores it's local state&#46;
 * The state often accumulates results during the traversal of the structure&#46;
 */
public class ConcreteVisitor implements Visitor {

    public void visitConcreteElement1(ConcreteElement1 element1) {
        element1.operationA();
    }

    public void visitConcreteElement2(ConcreteElement2 element2) {
        element2.operationB();
    }

}