package behavioural.visitor;
/**
 * Implements an accept operation that takes a visitor as an argument.
 */
public class ConcreteElement2 implements Element {

    public void accept(Visitor v) {
        v.visitConcreteElement2(this);
    }

    public void operationB() {
        System.out.println("ConcreteElement2.operationB() executing");
    }

}