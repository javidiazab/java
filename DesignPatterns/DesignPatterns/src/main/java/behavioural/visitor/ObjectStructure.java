package behavioural.visitor;

import java.util.*;

/**
 * Can enumerate it's elements&#46;
 * May provide a high-level interface to allow the visitor to visit it's elements&#46;
 * May either be a composite, or a collection such as a list or set&#46;
 */
public class ObjectStructure {
    private List objectStruct;
    private Visitor visitor;

    public ObjectStructure(Element[] elements) {
        objectStruct = Arrays.asList(elements);
    }

    public void visitElements() {

        // lazy construction
        if (visitor == null) { visitor = new ConcreteVisitor(); }

        for (Iterator iter = objectStruct.iterator(); iter.hasNext();) {
            ((Element) iter.next()).accept(visitor);
        }

    }

}