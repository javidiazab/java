package behavioural.visitor;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // construct array of Elements
        Element[] elements = new Element[2];
        elements[0] = new ConcreteElement1();
        elements[1] = new ConcreteElement2();

        // construct and run ObjectStructure
        new ObjectStructure(elements).visitElements();

    }

}