package behavioural.visitor;
/**
 * Defines an accept operation that takes a visitor as an argument.
 */
public interface Element {

    public void accept(Visitor v);

}