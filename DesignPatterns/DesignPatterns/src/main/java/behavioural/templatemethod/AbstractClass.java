package behavioural.templatemethod;
/**
 * Defines abstract <b>primitive operations</b> that concrete
 * subclasses define to implement steps of an algorithm.
 * Implements a template method defining operations as well as
 * operations defined in AbstractClass or those of other objects.
 */
public abstract class AbstractClass {

    public void templateMethod() {
        /* call methods in extending classes would probably
            contain some other processing*/
        System.out.println("AbstractClass.templateMethod() calling:");
        primitiveOperation1();
        primitiveOperation2();
    }

    public abstract void primitiveOperation1();
    public abstract void primitiveOperation2();

}