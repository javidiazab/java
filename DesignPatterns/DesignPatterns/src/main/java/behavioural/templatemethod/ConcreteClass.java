package behavioural.templatemethod;

/**
 * Implements the primitive operations to carry out subclass-specific steps of the algorithm.
 */
public class ConcreteClass extends AbstractClass {

    public void primitiveOperation1() {
        System.out.println("ConcreteClass.primitiveOperation1()");
    }

    public void primitiveOperation2() {
        System.out.println("ConcreteClass.primitiveOperation2()");
    }

}