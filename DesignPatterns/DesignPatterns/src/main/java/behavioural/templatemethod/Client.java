package behavioural.templatemethod;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // construct
        AbstractClass ac = new ConcreteClass();

        // call the template method - calls ConcreteClass' specific methods
        ac.templateMethod();
    }

}