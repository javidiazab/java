package behavioural.chainofresponsibility;

/**
 * Handles requests it is responsible for&46; Can access it's sucessor.
 * If the ConcreteHandler can handle the request, it does so; otherwise it
 * forwards the request on to it's sucessor.
 */
public class ConcreteHandler2 implements Handler {

    public void handleRequest(Request request) {
        // implement further chaining if required
        doRequest();
    }

    private void doRequest() {
        System.out.println("ConcreteHandler2.handleRequest(Request request) executed");
    }

}