package behavioural.chainofresponsibility;

/**
 * Handles requests it is responsible for&46; Can access it's sucessor&46;
 * If the ConcreteHandler can handle the request, it does so; otherwise it
 * forwards the request on to it's sucessor.
 */
public class ConcreteHandler1 implements Handler {

    public void handleRequest(Request request) {

        switch (request.getType()) {

            case Request.TYPE_ONE:
                doRequest();
                break;

            case Request.TYPE_TWO:
                new ConcreteHandler2().handleRequest(request);
                break;

        }

    }

    private void doRequest() {
        System.out.println("ConcreteHandler1.handleRequest(Request request) executed");
    }

}