package behavioural.chainofresponsibility;

/**
 * Type of request that can be passed for handling&46; We decide on the type of request
 * by setting it's (int) type during construction from public static feilds:
 * This will determine which handler 'grabs' it&46; We could pass a reference to the
 * next 'link' in the chain as a Request.
 */
public class Request {
    public final static int TYPE_ONE = 0;
    public final static int TYPE_TWO = 1;

    private int type;

    public Request(int type) throws Exception {

        if ((type == TYPE_ONE) || (type == TYPE_TWO)) {
            this.type = type;

        } else {
            throw new Exception("Illegal Request creation with unknown type");
        }

    }

    public int getType() {
        return type;
    }

}