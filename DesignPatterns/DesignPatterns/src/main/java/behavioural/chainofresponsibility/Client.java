package behavioural.chainofresponsibility;

/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {

        try {
            // create type one and type two requests
            Request r1 = new Request(Request.TYPE_ONE);
            Request r2 = new Request(Request.TYPE_TWO);

            // create handler
            Handler h = new ConcreteHandler1();

            // handle requests
            h.handleRequest(r1);
            h.handleRequest(r2);

        } catch (Exception e) { // to catch Request construction exceptions
            System.out.println(e.getMessage());
        }

    }

}