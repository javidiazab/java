package behavioural.chainofresponsibility;

/**
 * Defines an interface for handling requests&46; Optionally implements the sucessor link.
 */
public interface Handler {

    public void handleRequest(Request request);

}