package behavioural.iterator;

import java.util.*;

/**
 * Implements the Iterator Interface&46; Keeps track of the current position in the traversal
 * of the aggregate.
 */
public class ConcreteIterator implements Iterator {
    private List elements;
    private int index;

    public ConcreteIterator(Aggregate a) {
        elements = (List) a.elements();
        index = 0;
    }

    public Object next() throws RuntimeException {

        try {
            return elements.get(index++);

        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("No Such Element");

        }
    }

    public boolean hasNext() {

        return (index < elements.size()) ? true : false;
    }

}