package behavioural.iterator;

/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {

        String[] list = {"alpha", "bravo", "charlie", "delta", "echo", "foxtrot" };
        Aggregate ag = new ConcreteAggregate(list);

        // use fully qualified name to prevent collision with java.util.Iterator
        Iterator iter = ag.createIterator();

        // iterate through the Iterator, printing items to stdout
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

    }

}