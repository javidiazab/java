package behavioural.iterator;

/**
 * Defines an interface for accessing and traversing elements.
 */
public interface Iterator {

    public Object next();
    public boolean hasNext();

}