package behavioural.iterator;

import java.util.*;

/**
 * Aggregate:
 * Defines an interface for creating an Iterator object
 */
public interface Aggregate {

    public Iterator createIterator();
    public Collection elements();

}