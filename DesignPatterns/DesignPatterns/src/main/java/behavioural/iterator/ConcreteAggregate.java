package behavioural.iterator;

import java.util.*;

/**
 * Implements the Iterator creation interface to return an instance of the proper ConcreteIterator.
 */
public class ConcreteAggregate implements Aggregate {
    private List elements;

    public ConcreteAggregate(Object[] oa) {
        elements = new ArrayList();

        for (int i=0; i < oa.length; i++) {
            elements.add(oa[i]);
        }
    }

    public Iterator createIterator() {

        return new ConcreteIterator(this);
    }

    public Collection elements() {

        return Collections.unmodifiableList(elements);
    }

}