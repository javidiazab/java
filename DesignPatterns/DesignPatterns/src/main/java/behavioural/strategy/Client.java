package behavioural.strategy;
/**
 * Represents the client of this pattern.
 */
public class Client {

    public static void main(String[] args) {
        // construct strategies
        Strategy strategy1 = new ConcreteStrategy1();
        Strategy strategy2 = new ConcreteStrategy2();
        Strategy strategy3 = new ConcreteStrategy3();

        // execute contexts one by one
        new Context(strategy1).contextInterface();
        new Context(strategy2).contextInterface();
        new Context(strategy3).contextInterface();
    }

}