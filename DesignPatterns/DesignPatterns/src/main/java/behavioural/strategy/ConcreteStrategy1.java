package behavioural.strategy;
/**
 * Implements the algorithm using the Strategy interface.
 */
public class ConcreteStrategy1 implements Strategy {

    public void algorithmInterface() {
        System.out.println("ConcreteStrategy1.algorithmInterface() executed");
    }

}