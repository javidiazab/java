package behavioural.strategy;
/**
 * Implements the algorithm using the Strategy interface.
 */
public class ConcreteStrategy3 implements Strategy {

    public void algorithmInterface() {
        System.out.println("ConcreteStrategy3.algorithmInterface() executed");
    }

}