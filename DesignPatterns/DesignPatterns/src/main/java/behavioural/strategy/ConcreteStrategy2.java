package behavioural.strategy;
/**
 * Implements the algorithm using the Strategy interface.
 */
public class ConcreteStrategy2 implements Strategy {

    public void algorithmInterface() {
        System.out.println("ConcreteStrategy2.algorithmInterface() executed");
    }

}