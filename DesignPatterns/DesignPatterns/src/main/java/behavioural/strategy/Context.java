package behavioural.strategy;
/**
 * Is configured with a ConcreteStrategy object.
 * Maintains a reference to a Strategy object.
 * May define an interface that lets Strategy access its data.
 */
public class Context {
    Strategy strategy;

    public Context(Strategy s) {
        this.strategy = s;
    }

    public void contextInterface() {
        strategy.algorithmInterface();
    }

}