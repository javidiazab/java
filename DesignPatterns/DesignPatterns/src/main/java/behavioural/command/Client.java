package behavioural.command;

/**
 * Represents the client of this pattern&46; Creates a ConcreteCommand object
 * and sets it's reciever.
 */
public class Client {

    public static void main(String[] args) {
        // create two different receivers to implement effect of execute()
        Receiver rcv1 = new ReceiverA();
        Receiver rcv2 = new ReceiverB();

        // create command objects and attach receivers
        Command cmd1 = new ConcreteCommand(rcv1);
        Command cmd2 = new ConcreteCommand(rcv2);

        //create invokers (actual items that need the processing)
        Invoker inv1 = new Invoker();
        Invoker inv2 = new Invoker();

        // store commands in invokers
        inv1.storeCommand(cmd1);
        inv2.storeCommand(cmd2);

        // 'invoke' invokers to test command
        inv1.invoke();
        inv2.invoke();
    }

}