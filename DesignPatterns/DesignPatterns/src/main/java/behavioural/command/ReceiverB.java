package behavioural.command;
/**
 * Knows how to perform the operations associated with carrying out a request.
 */
public class ReceiverB implements Receiver {

    public void action() {
        System.out.println("ReceiverB.action() executing");
    }

}