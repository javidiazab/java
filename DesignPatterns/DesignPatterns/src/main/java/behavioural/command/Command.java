package behavioural.command;

/**
 * Defines an interface for executing an operation.
 */
public interface Command {

    public void execute();

}