package behavioural.command;

/**
 * Asks the command to carry out the request.
 */
public class Invoker {
    private Command command;

    public void storeCommand(Command cmd) {
        this.command = cmd;
    }

    public void invoke() {
        command.execute();
    }

}