package behavioural.command;

/**
 * Defines a binding between a Receiver object and an action&46; Implements execute by invoking
 * the corresponding operations on receiver.
 */
public class ConcreteCommand implements Command {
    private Receiver receiver;

    public ConcreteCommand(Receiver receive) {
        this.receiver = receive;
    }

    public void execute() {
            receiver.action();
    }

}