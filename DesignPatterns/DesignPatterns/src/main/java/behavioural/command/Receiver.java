package behavioural.command;
/**
 * Knows how to perform the operations associated with carrying out a request&46; Any class
 * may server as a Receiver.
 */
public interface Receiver {

    public void action();

}