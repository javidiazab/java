package behavioural.command;
/**
 * Knows how to perform the operations associated with carrying out a request.
 */
public class ReceiverA implements Receiver {

    public void action() {
        System.out.println("ReceiverA.action() executing");
    }

}