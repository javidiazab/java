package behavioural.interpreter;
/**
 * Declares an abstract Interpret operation that is common to all nodes in the abstract syntax tree.
 */
public interface AbstractExpression {

    public boolean interpret(Context ctx);

}