package behavioural.interpreter;
import java.util.*;

/**
 * Contains information that is global to the interpreter.
 */
public class Context {
    private Map nonterminalExpressions = new HashMap();

    public boolean lookup(String name) {

        return ((Boolean) nonterminalExpressions.get(name)).booleanValue();
    }

    public void assign(NonterminalExpression exp, boolean val) {
        nonterminalExpressions.put(exp.getName(), val);
    }

}