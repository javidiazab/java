package behavioural.interpreter;
/**
 * Implements an Interpret operation associated with terminal symbols in the grammar.
 * An instance is required for every terminal symbol in a sentance.
 */
public class TerminalExpression implements AbstractExpression { // AndExp
    AbstractExpression operand1;
    AbstractExpression operand2;

    public TerminalExpression(AbstractExpression exp1, AbstractExpression exp2) {
        this.operand1 = exp1;
        this.operand2 = exp2;
    }

    public boolean interpret(Context ctx) {

        return operand1.interpret(ctx) && operand2.interpret(ctx);
    }
}