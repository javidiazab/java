package behavioural.interpreter;
/**
 * NonterminalExpression: [AlternationExpression, RepititionExpression, SequenceExpressions]
 * One class is required for every rule in the grammar.
 * Maintains instance variables of type AbstractExpression for each of the symbols.
 * Implements an Interpret operation for non-terminal symbols in the grammar. Interpret
 * usually calls itself recursively.
 */
public class NonterminalExpression implements AbstractExpression { // VariableExp
    private String name;

    public NonterminalExpression(String name) {
        this.name = name;
    }

    public boolean interpret(Context ctx) {
        return ctx.lookup(name);
    }

    public String getName() {
        return name;
    }

}