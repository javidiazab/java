package behavioural.interpreter;
/**
 * Builds an abstract syntax tree representing a particular sentence in the language that the
 * grammar defines. The abstract syntax tree is assembled from instances of the
 * NonterminalExpression and TerminalExpression classes.
 * Invokes the interpret() operation.
 */
public class Client {

        /**
         * Standard implementation of the Interpreter pattern. Usual use would be to
         * Implement a parser that would build a Composite Pattern abstract tree from
         * a sentence and then pass the tree to a Chain of Responsibility to process.
         * Insted the tree is being built by steam, by creating two operands, and then
         * one expression containing these two operands.
         */
        public static void main(String[] args) {
            // create operands
            AbstractExpression x = new NonterminalExpression("X");
            AbstractExpression y = new NonterminalExpression("Y");

            AbstractExpression expression = new TerminalExpression(x, y);

            Context context = new Context();
            context.assign((NonterminalExpression) x, true);
            context.assign((NonterminalExpression) y, true);

            boolean result = expression.interpret(context);
            System.out.println("AbstractExpression.interpret() value [" + result + "]");
        }
}