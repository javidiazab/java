package creational.singleton;

import java.lang.reflect.Constructor;

import static org.junit.jupiter.api.Assertions.*;

class SingletonTest {

    @org.junit.jupiter.api.Test
    void getInstance() {
        // Check that, effectively, there is only a single instance of the class, same object
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        System.out.println("Singleton object in memoru, s1 " + s1 + ", s2 " + s2);
        assert(s1 == s2);
    }

    @org.junit.jupiter.api.Test
    void getInstanceID() {
        // Check that, effectively, there is only a single instance of the class by checking the random instanceID
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        System.out.println("Must be the same., instance 1 random ID: " + s1.getInstanceID() +  ", instance 2 random ID: " + s2.getInstanceID());
        assert(s1.getInstanceID() == s2.getInstanceID());
    }

    @org.junit.jupiter.api.Test
    void noConstructorsAvailable() throws NoSuchMethodException {
        // Direct access to constructor not allowed as it is private
        // Singleton s3 = new Singleton();
        Class aClass = Singleton.class;
        Constructor [] constructors = aClass.getConstructors();
        System.out.println("Number of constructors accesible (zero if private): "+ constructors.length);
        assert(constructors.length == 0);
    }
}