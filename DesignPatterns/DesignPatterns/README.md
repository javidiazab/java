# Design Patterns meetup, Wednesday 9th September 2020

## Index
1.       Introduction
2.       Design Patterns
3.       Enterprise Architecture Patterns
4.       Anti-patterns
5.       Java patterns (I mean, patterns in Java itself)
6.       Q&A

The source code was done with IntelliJ community edition

The presentation uses Power Point

The recording of the session will be stored at: 
https://confluence.almuk.santanderuk.corp/display/PAYMENTSHU/12+Recordings+Catalogue
