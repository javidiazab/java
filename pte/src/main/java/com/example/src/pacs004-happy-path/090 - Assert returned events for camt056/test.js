E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 200', function () {
  pm.response.to.have.status(200);
});

const expectedEvents = E2E.environment.getVariable('camt056-returned-events').split(',');
const paymentHubId = E2E.pm.environment.get('paymentHubIdCamt056DataDomain');

E2E.assertions.retryAssertion(
  (response) => E2E.assertHelper.eventWasNotRaised(response, expectedEvents[expectedEvents.length - 1], `(hub id = ${paymentHubId})`),
  {
    numRetries: E2E.pm.environment.get('retries'),
    asserts: (response) => E2E.assertHelper.assertAllEvents(response, expectedEvents)
  });

E2E.tearDown.clearIfFailure();
