E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 201', function () {
  pm.response.to.have.status(201);
});

pm.test('Data is correct', function () {
  E2E.logger.debug(`Response Json : ${JSON.stringify(pm.response.json(), null, 2)}`);

  const paymentsHubIdCamt056 = pm.response.json()?.data?.paymentsHubId;
  const status = pm.response.json()?.data?.status;

  pm.expect(paymentsHubIdCamt056).to.not.be.null;
  pm.expect(paymentsHubIdCamt056).to.eql(E2E.pm.environment.get('paymentHubIdCamt056'));

  pm.expect(status).to.not.be.null;
  pm.expect(status).to.eql("PENDING");
});
