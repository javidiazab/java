E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 200', function () {
  pm.response.to.have.status(200);
});

pm.test('Audit contains one element', function () {
  const auditResponse = pm.response.json();
  E2E.logger.debug('elementos: ', auditResponse?.totalElements);
  pm.expect(auditResponse?.totalElements).to.eql(1);

  const audit = auditResponse?.content && auditResponse?.content.length
    ? auditResponse.content[0]
    : {};

  E2E.pm.environment.set('pacs004-paymentHubId', audit?.paymentHubId);
  E2E.logger.debug('pacs004-paymentHubId: ' + E2E.pm.environment.get('pacs004-paymentHubId'));
});
