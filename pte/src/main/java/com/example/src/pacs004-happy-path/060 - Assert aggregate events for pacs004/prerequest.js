E2E.environment.setPostmanEnvironment(pm, postman);
E2E.pm.environment.unset('sso_access_token');
E2E.auth.sso();

E2E.messageHelpers.delay(2000).then(() => {
});

E2E.logger.debug('searching for events of pacs004-paymentHubId: ' + E2E.pm.environment.get('pacs004-paymentHubId'));
E2E.pm.environment.set('paymentHubIdPacs004DataDomain', E2E.pm.environment.get('pacs004-paymentHubId'));


