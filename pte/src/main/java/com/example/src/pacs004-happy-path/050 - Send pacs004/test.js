E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 200', function () {
  pm.response.to.have.status(200);
});

pm.test('Response code is IBP200', function () {
  E2E.logger.debug(`Response Json : ${JSON.stringify(pm.response.json(), null, 2)}`);
  const status = pm.response.json()?.Status;
  pm.expect(status).to.not.be.null;
  pm.expect(status).to.eql("IBP200");
});
