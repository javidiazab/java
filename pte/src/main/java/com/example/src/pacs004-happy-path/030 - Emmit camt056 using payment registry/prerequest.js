E2E.environment.setPostmanEnvironment(pm, postman);
//E2E.pm.environment.unset('sso_access_token');
E2E.auth.sso('reactor.payments-registry');

const ids = E2E.messageHelpers.generateHubIds();
const clientId = E2E.pm.environment.get('iberpay-coexistences-clientid');
const paymentHubIdCamt056 = E2E.coreUtil.createPaymentHubId(ids.msgId, clientId);
E2E.pm.environment.set('paymentHubIdCamt056', paymentHubIdCamt056);

const debtorAgt = E2E.pm.environment.get('uk-bic');
const creditorAgt = E2E.pm.environment.get('es-bic');
const debtorIban = E2E.pm.environment.get('uk-iban');
const creditorIban = E2E.pm.environment.get('es-iban');
const settlementDate = E2E.messageHelpers.getFormattedIsoDate();

const pacs008Ids = E2E.pm.environment.get('pacs008Ids');
const originalSettlementDate = E2E.pm.environment.get('originalSettlementDate');

const payload = E2E.environment.getVariable('camt056-payload')
  .replaceAll('{{CLIENT_ID}}', clientId)
  .replaceAll('{{MESSAGE_ID}}', ids.msgId)
  .replaceAll('{{CREATION_DATE}}', E2E.messageHelpers.getFullDate())
  .replaceAll('{{DEBTOR_BIC}}', debtorAgt)
  .replaceAll('{{DEBTOR_ACCOUNT}}', debtorIban)
  .replaceAll('{{CREDITOR_BIC}}', creditorAgt)
  .replaceAll('{{CREDITOR_ACCOUNT}}', creditorIban)
  .replaceAll('{{CANCELLATION_ID}}', ids.cxlId)
  .replaceAll('{{ORIGINAL_MESSAGE_ID}}', pacs008Ids.msgId)
  .replaceAll('{{ORIGINAL_SETTLEMENT_DATE}}', originalSettlementDate)
  .replaceAll('{{ORIGINAL_TRX_ID}}', pacs008Ids.txtId)
  .replaceAll('{{ORIGINAL_INSTRUCTION_ID}}', pacs008Ids.instrId)
;

E2E.logger.info(`[${paymentHubIdCamt056}] MsgId ${ids.msgId} - ClientId ${clientId}`);
E2E.pm.environment.set('camt056Ids', ids);

E2E.pm.environment.set('camt056Payload', payload);
