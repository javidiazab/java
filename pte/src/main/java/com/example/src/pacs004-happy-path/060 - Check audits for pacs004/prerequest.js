E2E.environment.setPostmanEnvironment(pm, postman);
E2E.auth.sso();

E2E.messageHelpers.delay(2000).then(() => {
});


E2E.logger.debug('searching pacs004 in DB with payhubId: ' + E2E.pm.environment.get('paymentHubIdPacs004'));
E2E.pm.environment.set('paymentHubId' , E2E.pm.environment.get('paymentHubIdPacs004'));
