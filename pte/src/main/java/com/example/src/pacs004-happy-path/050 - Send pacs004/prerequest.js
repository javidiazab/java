E2E.environment.setPostmanEnvironment(pm, postman);

const ids = E2E.messageHelpers.generateIberpayIds('STI00400102');
const clientId = E2E.pm.environment.get('iberpay-coexistences-clientid');

const debtorAgt = E2E.pm.environment.get('uk-bic');
const creditorAgt = E2E.pm.environment.get('es-bic');
const debtorIban = E2E.pm.environment.get('uk-iban');
const creditorIban = E2E.pm.environment.get('es-iban');
const settlementDate = E2E.pm.environment.get('originalSettlementDate');
const phMessageId = E2E.coreUtil.hash(ids.txtId + '-' + debtorAgt + '-' + settlementDate).replaceAll('-', '');
const hubId = E2E.coreUtil.createPaymentHubId(phMessageId, clientId);
E2E.pm.environment.set('paymentHubIdPacs004', hubId);

E2E.logger.info(`[${hubId}] ClientId ${clientId} - Expected phMessage id with (${ids.txtId}+${debtorAgt}+${settlementDate}) : ${phMessageId}`);

const pacs008Ids = E2E.pm.environment.get('pacs008Ids');

const xmlPayload = E2E.environment.getVariable('pacs004-payload')
  .replaceAll('{{MESSAGE_ID}}', ids.msgId)
  .replaceAll('{{CREATION_DATE}}', E2E.messageHelpers.getFullDate())
  .replaceAll('{{SETTLEMENT_DATE}}', E2E.pm.environment.get('originalSettlementDate'))
  .replaceAll('{{TRANSACTION_ID}}', ids.rtrId)
  .replaceAll('{{E2E_ID}}', pacs008Ids.e2eId)
  .replaceAll('{{ORIGINAL_TRX_ID}}', pacs008Ids.txtId)
  .replaceAll('{{INSTRUCTION_ID}}', pacs008Ids.instrId)
  .replaceAll('{{ORIGINAL_MESSAGE_ID}}', pacs008Ids.msgId)
  .replaceAll('{{DEBTOR_BIC}}', debtorAgt)
  .replaceAll('{{DEBTOR_ACCOUNT}}', debtorIban)
  .replaceAll('{{CREDITOR_BIC}}', creditorAgt)
  .replaceAll('{{CREDITOR_ACCOUNT}}', creditorIban);

E2E.pm.environment.set('pacs004Iberpay', E2E.messageHelpers.encodeIberpayRequest(xmlPayload));

E2E.logger.info('Pacs004: ', xmlPayload);
