E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 200', function () {
  pm.response.to.have.status(200);
});

const expectedEvents = E2E.environment.getVariable('pacs008-expected-events').split(',');
const paymentHubId = E2E.pm.environment.get('paymentHubId');

E2E.assertions.retryAssertion(
  (response) => E2E.assertHelper.eventWasNotRaised(response, expectedEvents[expectedEvents.length - 1], `(hub id = ${paymentHubId})`),
  {
    numRetries: E2E.pm.environment.get('retries'),
    asserts: (response) => E2E.assertHelper.assertAllEvents(response, expectedEvents)
  });

E2E.tearDown.clearIfFailure();
