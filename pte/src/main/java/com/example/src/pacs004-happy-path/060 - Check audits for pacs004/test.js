E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 200', function () {
  pm.response.to.have.status(200);
});

pm.test('Audit contains one element and has valid status', function () {
  const auditResponse = pm.response.json();
  pm.expect(auditResponse?.totalElements).to.eql(1);

  const audit = auditResponse?.content && auditResponse?.content.length
    ? auditResponse.content[0]
    : {};

  pm.expect(audit?.status).to.eql('RECEIVED');
});
