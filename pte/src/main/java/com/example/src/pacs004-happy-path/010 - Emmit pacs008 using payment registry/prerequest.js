E2E.environment.setPostmanEnvironment(pm, postman);
//E2E.pm.environment.unset('sso_access_token');
E2E.auth.sso('reactor.payments-registry');

const ids = E2E.messageHelpers.generateHubIds();
const clientId = E2E.pm.environment.get('iberpay-coexistences-clientid');
const paymentHubId = E2E.coreUtil.createPaymentHubId(ids.msgId, clientId);
E2E.pm.environment.set('paymentHubId', paymentHubId);

const debtorAgt = E2E.pm.environment.get('uk-bic');
const creditorAgt = E2E.pm.environment.get('es-bic');
const debtorIban = E2E.pm.environment.get('uk-iban');
const creditorIban = E2E.pm.environment.get('es-iban');
const settlementDate = E2E.messageHelpers.getFormattedIsoDate();

const payload = E2E.environment.getVariable('pacs008-payload')
  .replaceAll('{{CLIENT_ID}}', clientId)
  .replaceAll('{{MESSAGE_ID}}', ids.msgId)
  .replaceAll('{{CREATION_DATE}}', E2E.messageHelpers.getFullDate())
  .replaceAll('{{SETTLEMENT_DATE}}', settlementDate)
  .replaceAll('{{TRANSACTION_ID}}', ids.txtId)
  .replaceAll('{{E2E_ID}}', ids.e2eId)
  .replaceAll('{{INSTRUCTION_ID}}', ids.instrId)
  .replaceAll('{{DEBTOR_BIC}}', debtorAgt)
  .replaceAll('{{DEBTOR_ACCOUNT}}', debtorIban)
  .replaceAll('{{CREDITOR_BIC}}', creditorAgt)
  .replaceAll('{{CREDITOR_ACCOUNT}}', creditorIban)
;

E2E.logger.info(`[${paymentHubId}] MsgId ${ids.msgId} - ClientId ${clientId}`);

E2E.pm.environment.set('pacs008Payload', payload);

E2E.pm.environment.set('pacs008Ids', ids);
E2E.pm.environment.set('originalSettlementDate', settlementDate);
