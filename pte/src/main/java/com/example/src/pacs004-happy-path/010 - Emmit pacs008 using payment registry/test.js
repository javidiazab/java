E2E.environment.setPostmanEnvironment(pm, postman);

pm.test('Status code is 201', function () {
  pm.response.to.have.status(201);
});

pm.test('Data is correct', function () {
  E2E.logger.debug(`Response Json : ${JSON.stringify(pm.response.json(), null, 2)}`);

  const paymentsHubId = pm.response.json()?.data?.paymentsHubId;
  const status = pm.response.json()?.data?.status;

  pm.expect(paymentsHubId).to.not.be.null;
  pm.expect(paymentsHubId).to.eql(E2E.pm.environment.get('paymentHubId'));

  pm.expect(status).to.not.be.null;
  pm.expect(status).to.eql("PENDING");
});
