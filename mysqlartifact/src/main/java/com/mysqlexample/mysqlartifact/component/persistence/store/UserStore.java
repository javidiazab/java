package com.mysqlexample.mysqlartifact.component.persistence.store;

import com.mysqlexample.mysqlartifact.component.persistence.repository.*;
import com.mysqlexample.mysqlartifact.model.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserStore {
    @Autowired
    private final UserRepository userRepository;

    public User insertDB (String name, String email) {
        return userRepository.save(
                User.builder()
                .name(name)
                .email(email)
                .build()
        );
    }

    public Iterable<User> findAll () {
        return userRepository.findAll();
    }
}
