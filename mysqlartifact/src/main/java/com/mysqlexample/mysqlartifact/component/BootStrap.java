package com.mysqlexample.mysqlartifact.component;

import com.mysqlexample.mysqlartifact.component.persistence.store.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrap implements CommandLineRunner {

    @Autowired
    private UserStore userStore;

    @Override
    public void run(String... args) throws Exception {

        userStore.insertDB("name1", "email1");
        userStore.insertDB("name2", "email2");
        userStore.findAll().forEach(System.out::println);
    }
}
