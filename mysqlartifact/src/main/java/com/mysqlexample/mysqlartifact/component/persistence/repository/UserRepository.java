package com.mysqlexample.mysqlartifact.component.persistence.repository;

import com.mysqlexample.mysqlartifact.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

}


//@Repository
//public interface Camt087MappingRepository extends JpaRepository<Camt087Mapping, String> {
//    List<Camt087Mapping> findByCamt87MsgId(String camt87MsgId);
//    List<Camt087Mapping> findByGeneratedMT199Id(String camt27MsgId);
//}
