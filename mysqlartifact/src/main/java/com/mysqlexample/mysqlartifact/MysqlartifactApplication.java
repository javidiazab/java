package com.mysqlexample.mysqlartifact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlartifactApplication {

	public static void main(String[] args) {
		SpringApplication.run(MysqlartifactApplication.class, args);
	}
}
