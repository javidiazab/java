package com.example.varios.canvas;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class SaveCanvasAsImage {
    public static void main(String[] args) {
        // Crear un lienzo y dibujar algo en él
        Canvas canvas = new Canvas();
        canvas.setSize(400, 300);
        Graphics g = canvas.getGraphics();
        g.setColor(Color.RED);
        g.fillRect(50, 50, 200, 100);
        g.setColor(Color.BLACK);
        g.drawString("Ejemplo de texto en el lienzo", 60, 120);

        // Convertir el lienzo en una imagen
        BufferedImage image = new BufferedImage(canvas.getWidth(), canvas.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        canvas.paint(graphics);

        // Guardar la imagen en un archivo
        File file = new File("mi_canvas.png");
        try {
            ImageIO.write(image, "png", file);
            System.out.println("Lienzo guardado como imagen: " + file.getAbsolutePath());
        } catch (Exception e) {
            System.out.println("Error al guardar la imagen: " + e.getMessage());
        }
    }
}
