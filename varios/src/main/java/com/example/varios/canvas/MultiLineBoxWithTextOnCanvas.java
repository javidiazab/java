package com.example.varios.canvas;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

public class MultiLineBoxWithTextOnCanvas extends Canvas {

    private String texto = "Texto en la caja\ncon varias líneas\njjj\nkk";

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Establecer la fuente y el color del texto
        g.setFont(new Font("Arial", Font.PLAIN, 14));
        g.setColor(Color.BLACK);

        // Dividir el texto en líneas individuales
        String[] lineas = texto.split("\n");
        List<String> lineasList = new ArrayList<>();
        for (String linea : lineas) {
            lineasList.add(linea);
        }

        // Obtener las métricas de la fuente para el texto
        FontMetrics metrics = g.getFontMetrics();
        int stringHeight = metrics.getHeight();

        // Calcular el espacio requerido para cada línea y la altura total
        int padding = 10; // Espacio adicional alrededor del texto
        int maxWidth = 0;
        for (String linea : lineasList) {
            int stringWidth = metrics.stringWidth(linea);
            maxWidth = Math.max(maxWidth, stringWidth);
        }
        int width = maxWidth + 2 * padding;
        int height = (lineasList.size() * stringHeight) + 2 * padding;

        // Calcular la posición para centrar el texto en la caja
        int x = 50;
        int y = 50;

        // Dibujar la caja
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, width, height);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, width, height);

        // Dibujar cada línea de texto en la caja
        int yTexto = y + padding + metrics.getAscent();
        for (String linea : lineasList) {
            int xTexto = x + padding;
            g.drawString(linea, xTexto, yTexto);
            yTexto += stringHeight;
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Caja con Texto de Varias Líneas en Canvas");
        Canvas canvas = new MultiLineBoxWithTextOnCanvas();
        frame.add(canvas);
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

