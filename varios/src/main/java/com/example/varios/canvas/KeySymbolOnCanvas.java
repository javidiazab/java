package com.example.varios.canvas;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JFrame;

public class KeySymbolOnCanvas extends Canvas {

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Definir el texto con el símbolo de la llave musical
        String llave = "\u2669"; // Código Unicode para el símbolo de la llave musical

        // Establecer la posición del texto
        int x = 100;
        int y = 150;

        // Establecer el tamaño de la fuente (más grande)
        int fontSizeLarge = 72;
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.PLAIN, fontSizeLarge));
        // Dibujar el texto con el símbolo de la llave musical (más grande)
        g.drawString(llave, x, y);

        // Establecer el tamaño de la fuente (más pequeño)
        int fontSizeSmall = 24;
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.PLAIN, fontSizeSmall));
        // Dibujar el texto con el símbolo de la llave musical (más pequeño)
        g.drawString(llave, x, y + fontSizeLarge); // Mover hacia abajo para evitar superposición
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Símbolo de Llave en Canvas");
        Canvas canvas = new KeySymbolOnCanvas();
        frame.add(canvas);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
