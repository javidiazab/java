package com.example.varios.canvas;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JFrame;

public class TextOnCanvas extends Canvas {

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Definir las coordenadas donde se colocará el texto
        int x = 100;
        int y = 100;

        // Establecer el color del texto
        g.setColor(Color.BLACK);

        // Establecer la fuente del texto
        g.setFont(new Font("Arial", Font.PLAIN, 20));

        // Dibujar el texto en las coordenadas especificadas
        g.drawString("Texto en Canvas", x, y);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Texto en Canvas");
        Canvas canvas = new TextOnCanvas();
        frame.add(canvas);
        frame.setSize(4000, 3000);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}