package com.example.varios.canvas;

import javax.swing.*;
import java.awt.*;

public class ScrollableCanvas {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Canvas con Scroll");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Crear un JPanel para contener el Canvas
        JPanel canvasPanel = new JPanel();
        canvasPanel.setLayout(new BorderLayout());

        // Crear un Canvas grande
        Canvas canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(1000, 1000)); // Tamaño grande

        // Agregar el Canvas al JPanel
        canvasPanel.add(canvas);

        // Crear un JScrollPane y agregar el JPanel con el Canvas
        JScrollPane scrollPane = new JScrollPane(canvasPanel);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        // Agregar el JScrollPane al JFrame
        frame.add(scrollPane);

        frame.setSize(400, 300);
        frame.setVisible(true);
    }
}