package com.example.varios.canvas;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

public class MultiLineBoxesWithTextAndAdjustedCoordinatesOnCanvas extends Canvas {

    private String textoPrimeraCaja = "Texto en la primera caja\ncon varias líneas";

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Establecer la fuente y el color del texto
        g.setFont(new Font("Arial", Font.PLAIN, 14));
        g.setColor(Color.BLACK);

        // Dividir el texto en líneas individuales para la primera caja
        String[] lineasPrimeraCaja = textoPrimeraCaja.split("\n");
        List<String> lineasListPrimeraCaja = new ArrayList<>();
        for (String linea : lineasPrimeraCaja) {
            lineasListPrimeraCaja.add(linea);
        }

        // Obtener las métricas de la fuente para el texto de la primera caja
        FontMetrics metricsPrimeraCaja = g.getFontMetrics();
        int stringHeightPrimeraCaja = metricsPrimeraCaja.getHeight();

        // Calcular el espacio requerido para cada línea y la altura total de la primera caja
        int paddingPrimeraCaja = 10; // Espacio adicional alrededor del texto
        int maxWidthPrimeraCaja = 0;
        for (String linea : lineasListPrimeraCaja) {
            int stringWidth = metricsPrimeraCaja.stringWidth(linea);
            maxWidthPrimeraCaja = Math.max(maxWidthPrimeraCaja, stringWidth);
        }
        int widthPrimeraCaja = maxWidthPrimeraCaja + 2 * paddingPrimeraCaja;
        int heightPrimeraCaja = (lineasListPrimeraCaja.size() * stringHeightPrimeraCaja) + 2 * paddingPrimeraCaja;

        // Calcular la posición para la primera caja
        int xPrimeraCaja = 50;
        int yPrimeraCaja = 50;

        // Dibujar la primera caja
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(xPrimeraCaja, yPrimeraCaja, widthPrimeraCaja, heightPrimeraCaja);
        g.setColor(Color.BLACK);
        g.drawRect(xPrimeraCaja, yPrimeraCaja, widthPrimeraCaja, heightPrimeraCaja);

        // Dibujar cada línea de texto en la primera caja
        int yTextoPrimeraCaja = yPrimeraCaja + paddingPrimeraCaja + metricsPrimeraCaja.getAscent();
        for (String linea : lineasListPrimeraCaja) {
            int xTexto = xPrimeraCaja + paddingPrimeraCaja;
            g.drawString(linea, xTexto, yTextoPrimeraCaja);
            yTextoPrimeraCaja += stringHeightPrimeraCaja;
        }

        // Calcular la posición y las dimensiones para la segunda caja
        int coordenadasY = yPrimeraCaja + heightPrimeraCaja + 50;
        int widthSegundaCaja = 250;
        int heightSegundaCaja = 150;

        // Dibujar la segunda caja
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(xPrimeraCaja, coordenadasY, widthSegundaCaja, heightSegundaCaja);
        g.setColor(Color.BLACK);
        g.drawRect(xPrimeraCaja, coordenadasY, widthSegundaCaja, heightSegundaCaja);

        // Dibujar el texto en la segunda caja con las posiciones de la primera caja
        String coordenadasTexto = "Posiciones de la primera caja:";
        g.drawString(coordenadasTexto, xPrimeraCaja + paddingPrimeraCaja, coordenadasY + paddingPrimeraCaja + metricsPrimeraCaja.getAscent());
        String esquinaIzquierdaSuperior = "Esquina izquierda superior: (" + xPrimeraCaja + ", " + yPrimeraCaja + ")";
        g.drawString(esquinaIzquierdaSuperior, xPrimeraCaja + paddingPrimeraCaja, coordenadasY + paddingPrimeraCaja * 2 + metricsPrimeraCaja.getHeight());
        String esquinaDerechaSuperior = "Esquina derecha superior: (" + (xPrimeraCaja + widthPrimeraCaja) + ", " + yPrimeraCaja + ")";
        g.drawString(esquinaDerechaSuperior, xPrimeraCaja + paddingPrimeraCaja, coordenadasY + paddingPrimeraCaja * 3 + metricsPrimeraCaja.getHeight() * 2);
        String esquinaIzquierdaInferior = "Esquina izquierda inferior: (" + xPrimeraCaja + ", " + (yPrimeraCaja + heightPrimeraCaja) + ")";
        g.drawString(esquinaIzquierdaInferior, xPrimeraCaja + paddingPrimeraCaja, coordenadasY + paddingPrimeraCaja * 4 + metricsPrimeraCaja.getHeight() * 3);
        String esquinaDerechaInferior = "Esquina derecha inferior: (" + (xPrimeraCaja + widthPrimeraCaja) + ", " + (yPrimeraCaja + heightPrimeraCaja) + ")";
        g.drawString(esquinaDerechaInferior, xPrimeraCaja + paddingPrimeraCaja, coordenadasY + paddingPrimeraCaja * 5 + metricsPrimeraCaja.getHeight() * 4);
        String centro = "Centro: (" + (xPrimeraCaja + widthPrimeraCaja / 2) + ", " + (yPrimeraCaja + heightPrimeraCaja / 2) + ")";
        g.drawString(centro, xPrimeraCaja + paddingPrimeraCaja, coordenadasY + paddingPrimeraCaja * 6 + metricsPrimeraCaja.getHeight() * 5);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Cajas con Texto y Coordenadas Ajustadas en Canvas");
        Canvas canvas = new MultiLineBoxesWithTextAndAdjustedCoordinatesOnCanvas();
        frame.add(canvas);
        frame.setSize(400, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}