package com.example.varios.canvas;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JFrame;

public class DynamicBoxWithTextOnCanvas extends Canvas {

    private String texto = "Texto en la caja ......................../n......./n";

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Establecer la fuente y el color del texto
        g.setFont(new Font("Arial", Font.PLAIN, 14));
        g.setColor(Color.BLACK);

        // Obtener las métricas de la fuente para el texto
        FontMetrics metrics = g.getFontMetrics();
        int stringWidth = metrics.stringWidth(texto);
        int stringHeight = metrics.getHeight();

        // Calcular las dimensiones de la caja basadas en el texto
        int padding = 10; // Espacio adicional alrededor del texto
        int x = 50;
        int y = 50;
        int width = stringWidth + 2 * padding;
        int height = stringHeight + 2 * padding;

        // Dibujar la caja
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, width, height);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, width, height);

        // Calcular la posición para centrar el texto en la caja
        int xTexto = x + padding;
        int yTexto = y + padding + metrics.getAscent();

        // Dibujar el texto en la caja
        g.drawString(texto, xTexto, yTexto);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Caja con Texto Ajustable en Canvas");
        Canvas canvas = new DynamicBoxWithTextOnCanvas();
        frame.add(canvas);
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}