package com.example.varios.canvas;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JFrame;

public class BoxWithTextOnCanvas extends Canvas {

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Definir las coordenadas y dimensiones de la caja
        int x = 50;
        int y = 50;
        int width = 200;
        int height = 100;

        // Dibujar la caja
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, width, height);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, width, height);

        // Definir el texto a mostrar
        String texto = "Texto en la caja";

        // Definir la fuente y el color del texto
        g.setFont(new Font("Arial", Font.PLAIN, 14));
        g.setColor(Color.BLACK);

        // Calcular la posición para centrar el texto en la caja
        int stringWidth = g.getFontMetrics().stringWidth(texto);
        int stringHeight = g.getFontMetrics().getHeight();
        int xTexto = x + (width - stringWidth) / 2;
        int yTexto = y + (height - stringHeight) / 2 + g.getFontMetrics().getAscent();

        // Dibujar el texto en la caja
        g.drawString(texto, xTexto, yTexto);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Caja con Texto en Canvas");
        Canvas canvas = new BoxWithTextOnCanvas();
        frame.add(canvas);
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
