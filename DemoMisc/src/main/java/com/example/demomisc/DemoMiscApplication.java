package com.example.demomisc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMiscApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMiscApplication.class, args);
    }

}
