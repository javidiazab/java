package com.example.demomisc.services;

public interface UsersService {

    public String authenticateUser(String userName, String password) throws Exception;
}
