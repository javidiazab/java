package com.example.demomisc.controllers;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.tomcat.jni.Local;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class DemoController {

    @GetMapping("/public/hello")
    public String publicHello() {
        return "Public Hello";
    }

    @GetMapping("/public/date")
    public DemoValue publicDate() {
        return new DemoValue(LocalDateTime.now());
    }

    @GetMapping("/private/hello")
    public String privateHello() {
        return "Private Hello";
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/user")
    public String isUser(){
        return getUserName() + " is an user";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin")
    public String isAdmin(){
        return getUserName() + " is an admin";
    }

    private String getUserName() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return securityContext.getAuthentication().getName();
    }

    @Data
    @AllArgsConstructor
    private static class DemoValue {

        @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
        private LocalDateTime dateTime;
    }
}
