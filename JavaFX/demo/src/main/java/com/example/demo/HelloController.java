package com.example.demo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HelloController {
    @FXML
    private Label welcomeText;

    protected void onHelloButtonClick() {

        welcomeText.setText("Welcome to JavaFX Application!");
    }

    public void a(ActionEvent e) {
        System.out.println("Hola");
        System.out.println(e.getEventType());
        System.out.println(e.getTarget());
    }
}