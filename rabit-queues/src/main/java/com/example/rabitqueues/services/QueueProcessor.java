package com.example.rabitqueues.services;

import com.example.rabitqueues.messagin.output.SourceMessagesHandler;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
@Slf4j
public class QueueProcessor {
    private final SourceMessagesHandler sourceMessagesHandler;

    public void processInput1 (String inputMsg) {
        sourceMessagesHandler.routeToDestination2(inputMsg);
    }

    public Message<String> processInput2 (String inputMsg) {
        return sourceMessagesHandler.routeToDestination3(inputMsg);
    }

    @SneakyThrows
    public void processInput3 (String inputMsg) {
        log.info("This message goes to input3.Dlq");
        throw new Exception("Exception on " + inputMsg);
    }
}
