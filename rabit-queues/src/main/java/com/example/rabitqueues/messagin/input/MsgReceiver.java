package com.example.rabitqueues.messagin.input;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface MsgReceiver {

    String INPUT1 = "demoInput1";
    String INPUT2 = "demoInput2";
    String INPUT3 = "demoInput3";

    @Input(INPUT1)
    SubscribableChannel input1();

    @Input(INPUT2)
    SubscribableChannel input2();

    @Input(INPUT3)
    SubscribableChannel input3();
}
