package com.example.rabitqueues.messagin.output;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.messaging.support.MessageBuilder.withPayload;

@Component
@Slf4j
@RequiredArgsConstructor
public class SourceMessagesHandler {
    @Autowired
    private final MsgSender msgSender;
    private static final String OUTPUT_ROUTING_KEY_2 = "jjj.input2";
    private static final String OUTPUT_ROUTING_KEY_3 = "ggg.input3";

   //@SendTo(Processor.OUTPUT)
   //public Message<String> routeToDestination2(String mtMsg) {
   public void routeToDestination2(String mtMsg) {
       log.info("Routing to INPUT2: {}", mtMsg);
        Message<String> message = MessageBuilder.withPayload(mtMsg)
                .setHeader("myRoutingKey", OUTPUT_ROUTING_KEY_2)
                .setHeader("type", "toINPUT2")
                .build();
        log.info("header: " + message.getHeaders());
        msgSender.output().send(message);
        //return message;
    }

    public Message<String> routeToDestination3(String mtMsg) {
        log.info("Routing to destination3: {}", mtMsg);
        return buildMessage(mtMsg, OUTPUT_ROUTING_KEY_3);
        //msgSender.output().send(buildMessage(mtMsg, OUTPUT_ROUTING_KEY_3));
    }

    private Message<String> buildMessage(String message, String routingKey) {
        return withPayload(message)
                .setHeader("myRoutingKey", routingKey)
                .setHeader("type", "toINPUT3")
                .build();
    }
}

