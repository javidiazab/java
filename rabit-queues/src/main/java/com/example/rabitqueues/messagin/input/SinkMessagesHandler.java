package com.example.rabitqueues.messagin.input;


import com.example.rabitqueues.messagin.output.MsgSender;
import com.example.rabitqueues.services.QueueProcessor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;


@Component
@Slf4j
@AllArgsConstructor
public class SinkMessagesHandler {
    private final QueueProcessor queueProcessor;

    @StreamListener(target = MsgReceiver.INPUT1)
    public void handleSystemMsg1(@Payload String inputMsg) {
        log.info("Handling queue INPUT1: {}", inputMsg);
        queueProcessor.processInput1(inputMsg);
    }

    @StreamListener(MsgReceiver.INPUT2)
    @SendTo(MsgSender.OUTPUT1)
    //public void handleSystemMsg2(@Payload String inputMsg) {
    public Message<String> handleSystemMsg2(@Payload String inputMsg) {
        log.info("Handling queue INPUT2: {}", inputMsg);
        return queueProcessor.processInput2(inputMsg);
    }

    @StreamListener(target = MsgReceiver.INPUT3, condition = "headers['type']=='toINPUT3'")
    //@StreamListener(target = MsgReceiver.INPUT3, condition = "payload == 'myPayloadCondition3'")
    public void handleSystemMsg3(@Payload String inputMsg) {
        log.info("Handling queue INPUT3: {}", inputMsg);
        queueProcessor.processInput3(inputMsg);
    }


//Otro ejemplo
//    @StreamListener(Processor.INPUT)
//    @SendTo(Processor.OUTPUT)
//    public Flux convert(Flux input) {
//        return input.map(String::toUppercase);
//    }
}

