package com.example.rabitqueues.messagin.output;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface MsgSender {
    String OUTPUT1 = "demoOutput1";

    @Output(OUTPUT1)
    MessageChannel output();
}
