package com.example.rabitqueues;

import com.example.rabitqueues.messagin.input.MsgReceiver;
import com.example.rabitqueues.messagin.output.MsgSender;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({
		MsgReceiver.class,
		MsgSender.class
})
public class RabitQueuesApplication {
	public static void main(String[] args) {
		SpringApplication.run(RabitQueuesApplication.class, args);
	}

}
