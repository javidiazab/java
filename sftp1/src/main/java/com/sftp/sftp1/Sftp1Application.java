package com.sftp.sftp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
public class Sftp1Application {

    public static void main(String[] args) {
        SpringApplication.run(Sftp1Application.class, args);
    }

}
