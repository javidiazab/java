package com.example.moviesapi.application;

import com.example.moviesapi.domain.MoviesRepository;
import com.example.moviesapi.objectmothers.MoviesObjectMother;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@SpringBootTest
class GetMoviesInListHandlerTest {

    MoviesRepository repository;
    GetMoviesInListHandler handler;

    @BeforeEach
    public void setUp()  {
        repository = mock(MoviesRepository.class);
        handler = new GetMoviesInListHandler(repository);
    }

    @AfterEach
    public void cleanUp(){
        handler = null;
        repository = null;
    }

    @Test
    void handleEmptyListReturnEmptyList() {

        val ids = new ArrayList<String>();

        val result = handler.handle(new GetMoviesInListQuery(ids));

        assertNotNull(result);
        assertTrue(result.isEmpty());
        then(repository).should(times(0)).getInList(any());
    }

    @Test
    void handleInvalidIdsReturnEmptyResult() {

        val ids = List.of("1", "2");
        when(repository.getInList(ids)).thenReturn(new ArrayList<>());

        val result = handler.handle(new GetMoviesInListQuery(ids));

        assertNotNull(result);
        assertTrue(result.isEmpty());
        then(repository).should(times(1)).getInList(ids);
    }

    @Test
    void handleReturnExistingResult() {

        val items = List.of(MoviesObjectMother.getRandomDto(), MoviesObjectMother.getRandomDto());
        val ids = List.of(items.get(0).getId(), items.get(1).getId());
        when(repository.getInList(ids)).thenReturn(items);

        val result = handler.handle(new GetMoviesInListQuery(ids));

        assertNotNull(result);
        assertEquals(items.size(), result.size());
        items.forEach(x -> assertTrue(result.stream().anyMatch(x::equals)));
        then(repository).should(times(1)).getInList(ids);
    }

}