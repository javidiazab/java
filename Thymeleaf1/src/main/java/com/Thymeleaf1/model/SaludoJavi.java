package com.Thymeleaf1.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SaludoJavi {
   String idioma;
   String saludo;
   String nombre;
}
