package com.Thymeleaf1.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseJavi {
    String status;
    String details1;
    String details2;
    String details3;
    String createdBy;
}
