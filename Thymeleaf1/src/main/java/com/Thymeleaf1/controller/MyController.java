package com.Thymeleaf1.controller;

import com.Thymeleaf1.model.ResponseJavi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class MyController {
    @GetMapping("/")
    public String controller1() {
        log.info("En Controller 1");
        return "index";
    }

    @GetMapping("/saludo2/{name}")
    public String controller2(@PathVariable String name, Model model) {
        log.info("En Controller 2");
        model.addAttribute("nombre", name);
        return "index";
    }

    @GetMapping("/saludo3")
    public String controller3(@RequestParam(required = false, defaultValue = "Pedro") String name,
                              @RequestParam(required = false, defaultValue = "Pi") String apellido,
                              Model model) {
        log.info("En Controller 3");
        model.addAttribute("saludo", "Hello");
        model.addAttribute("nombre", name + " " + apellido);
        return "index";
    }

    @ResponseBody   //devuelve un json
    @RequestMapping(path = "/saludo4", produces = "application/json; charset=UTF-8")
    public ResponseJavi controller4(@RequestParam(required = false, defaultValue = "Pedro") String name,
                                    @RequestParam(required = false, defaultValue = "Pi") String apellido) {
        log.info("En Controller 4");
        return ResponseJavi.builder().details1("name").details2("apellido").build();
//        SaludoJavi.builder()
//                .idioma("ES")
//                .saludo("Hola")
//                .nombre(name + apellido)
//                .build();
    }
}