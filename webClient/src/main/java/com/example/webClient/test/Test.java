package com.example.webClient.test;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) {
        //https://howtodoinjava.com/spring-webflux/webclient-get-post-example/
        https://www.baeldung.com/spring-5-webclient
        //https://www.baeldung.com/spring-5-webclient#4-preparing-a-request---define-the-url

        //Creating a WebClient Instance
        //1
        WebClient client1 = WebClient.create();

        //2
        WebClient client2 = WebClient.create("http://localhost:8080");
        WebClient client3 = WebClient.create("http://localhost:8080");

        //3
        WebClient client4 = WebClient.builder()
                .baseUrl("http://localhost:8080")
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", "http://localhost:8080"))
                .build();

        //Creating a WebClient Instance with Timeouts
        HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
                .responseTimeout(Duration.ofMillis(5000))
                .doOnConnected(conn ->
                        conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
                                .addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));

        WebClient client = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();

        //Preparing a Request – Define the Method
        WebClient.UriSpec<WebClient.RequestBodySpec> uriSpec2 = client2.method(HttpMethod.POST);
        WebClient.UriSpec<WebClient.RequestBodySpec> uriSpec3 = client3.post();
        WebClient.UriSpec<WebClient.RequestBodySpec> uriSpec4 = client4.post();

        //Preparing a Request – Define the URL
        WebClient.RequestBodySpec bodySpec2 = uriSpec2.uri("/resource");
        WebClient.RequestBodySpec bodySpec3 = uriSpec2.uri("/resource");
        WebClient.RequestBodySpec bodySpec4 = uriSpec3.uri(uriBuilder -> uriBuilder.pathSegment("/resource").build());
        WebClient.RequestBodySpec bodySpec5 = uriSpec4.uri(URI.create("/resource"));

        //Preparing a Request – Define the Body
        WebClient.RequestHeadersSpec<?> headersSpec = bodySpec2.bodyValue("data");

        WebClient.RequestHeadersSpec<?> headersSpec2 = bodySpec3.body(
                Mono.just(new String("name")), String.class);

        WebClient.RequestHeadersSpec<?> headersSpec3 = bodySpec4.body(
                BodyInserters.fromValue("data"));

        WebClient.RequestHeadersSpec headersSpec4 = bodySpec5.body(
                BodyInserters.fromPublisher(Mono.just("data"), String.class));

        //Preparing a Request – Define the Headers
        WebClient.ResponseSpec responseSpec = headersSpec.header(
                        HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve();

        //-------------------------------------------------
        WebClient webClient = WebClient.create("http://localhost:3000");

        Employee empl = new Employee();
        Employee createdEmployee = webClient.post()
                .uri("/employees")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(empl), Employee.class)
                .retrieve()
                .bodyToMono(Employee.class)
                .block();


        Employee block = webClient.get()
                .uri("/employees/1")
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(Employee.class))
                .block();

        Mono<Employee> employeeMono = webClient.put()
                .uri("/employees/" + empl.getId())
                .body(Mono.just(empl), Employee.class)
                .retrieve()
                .bodyToMono(Employee.class);
        boolean response1 = true;
        ----------------------------------------------------------

                //Getting a Response
                //The final stage is sending the request and receiving a response. We can achieve this by using either the exchangeToMono/exchangeToFlux or the retrieve method.
                //The exchangeToMono and exchangeToFlux methods allow access to the ClientResponse along with its status and headers:
        Mono < String > response1 = headersSpec.exchangeToMono(response -> {
            if (response.statusCode().equals(HttpStatus.OK)) {
                return response.bodyToMono(String.class);
            } else if (response.statusCode().is4xxClientError()) {
                return Mono.just("Error response");
            } else {
                return response.createException()
                        .flatMap(Mono::error);
            }
        });

        //While the retrieve method is the shortest path to fetching a body directly:
        Mono<String> response = headersSpec.retrieve()
                .bodyToMono(String.class);

    }


    private static class Employee {
        int id;

        public int getId() {
            return id;
        }
    }
}
