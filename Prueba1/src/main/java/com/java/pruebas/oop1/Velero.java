package com.java.pruebas.oop1;

public class Velero extends EmbarcacionMotorizada implements Vela {
    String nombre;

    public Velero(String nombre) {
        super(nombre, "");
    }

    @Override
    public void arriarVelas() {
        System.out.println("Embarcacion: Arriar Velas");
    }

    @Override
    public void izarVelas() {
        System.out.println("Embarcacion: Izar Velas");
    }
}
