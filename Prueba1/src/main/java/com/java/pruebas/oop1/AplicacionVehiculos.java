package com.java.pruebas.oop1;

public class AplicacionVehiculos {

    public static void main(String[] args) {
        Coche celica = new Coche("toyota", "gt", "azul", "M9579ST");
        Coche peugeot = new Coche("peugeot", "206", "gris", "M7097ZC");
        Moto vespa = new Moto("vespa", "px", "azul", "8798BPB");
        Carro carro = new Carro(2, "rojo", "madera");
        Velero aitana = new Velero("aitana");
        EmbarcacionMotorizada barco1 = new EmbarcacionMotorizada("barco1", "V1 765");
        celica.arrancar();
        celica.circular();
        celica.apagar();
        celica.setColor("amarillo");
        vespa.arrancar();
        vespa.circular();
        vespa.apagar();
        carro.circular();
        carro.setColor("azul");
        aitana.zarpar();
        aitana.izarVelas();
        aitana.arriarVelas();
        aitana.atracar();
        barco1.navegar();
    }
}
