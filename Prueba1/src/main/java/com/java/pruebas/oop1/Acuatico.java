package com.java.pruebas.oop1;

public interface Acuatico {

    public void navegar();

    public void undirse();

    public void atracar();

    public void zarpar();
}
