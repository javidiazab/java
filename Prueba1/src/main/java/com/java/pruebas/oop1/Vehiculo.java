package com.java.pruebas.oop1;

public abstract class Vehiculo {
    private int asientos;
    private String color;
    private String material;

    public String getColor() {
        return this.color;
    }

    private void setColor(String color) {
        this.color = color;
    }
}
