package com.java.pruebas.oop1;

public class EmbarcacionMotorizada extends Embarcacion {
    private Motor motor;
    private String matricula;

    public EmbarcacionMotorizada(String nombre, String matricula) {
        super(nombre);
    }

    //TODO setear motor
    public Motor getMotor() {
        return motor;
    }

    public String getMatricula() {
        return matricula;
    }
}
