package com.java.pruebas.oop1;

public abstract class Embarcacion extends Vehiculo implements Acuatico {
    String nombre;

    public Embarcacion(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void navegar() {
        System.out.println("Embarcacion: puede navegar");
    }

    public void fondear() {
        System.out.println("Embarcacion: puede fondear");
    }

    @Override
    public void undirse() {
        System.out.println("Embarcacion: puede undirse");
    }

    @Override
    public void atracar() {
        System.out.println("Embarcacion: puede atracar");
    }

    @Override
    public void zarpar() {
        System.out.println("Embarcacion: puede zarpar");
    }

    public String getNombre() {
        return nombre;
    }
}
