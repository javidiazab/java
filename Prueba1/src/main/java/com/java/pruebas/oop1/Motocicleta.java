package com.java.pruebas.oop1;

public class Motocicleta extends VehiculoTerrestre {
    private String color;
    private int cantidadRuedas;
    private String material;

    public Motocicleta() {
        super();
        this.cantidadRuedas = 4;
        this.setColor("Dorado");
        this.material = "Hierro";
    }

    public Motocicleta(String color, String material) {
        this.cantidadRuedas = 4;
        this.setColor(color);
        this.material = material;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCantidadRuedas() {
        return cantidadRuedas;
    }

    public void setCantidadRuedas(int cantidadRuedas) {
        this.cantidadRuedas = cantidadRuedas;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
}
