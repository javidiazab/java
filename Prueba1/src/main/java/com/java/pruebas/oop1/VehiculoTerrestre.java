package com.java.pruebas.oop1;

public abstract class VehiculoTerrestre extends Vehiculo implements Terrestre {
    public VehiculoTerrestre() {
        super();
    }

    @Override
    public void derrapar() {
        System.out.println("Puede derrapar");
    }

    @Override
    public void pinchar() {
        System.out.println("Puede pinchar");
    }

    public void circular() {
        System.out.println("Puede circular");
    }
}
