package com.java.pruebas.oop1;

public class Moto extends VehiculoTerrestreMotorizado {
    private int cantidadRuedas;
    private String color;
    private String material;
    private String marca;
    private String modelo;
    private String matricula;
    private Motor motor;


    public Moto(String marca, String modelo, String color, String matricula) {
        super();
        this.setCantidadRuedas(4);
        this.setMarca(marca);
        this.setModelo(modelo);
        this.setColor(color);
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public int getCantidadRuedas() {
        return cantidadRuedas;
    }

    public String getColor() {
        return color;
    }

    public String getMaterial() {
        return material;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setColor(String color) {
        this.color = color;
    }

    private void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    private void setCantidadRuedas(int cantidadRuedas) {
        this.cantidadRuedas = cantidadRuedas;
    }

    private void setMarca(String marca) {
        this.marca = marca;
    }

    private void setModelo(String modelo) {
        this.modelo = modelo;
    }

    private void setMaterial(String material) {
        this.material = material;
    }
}
