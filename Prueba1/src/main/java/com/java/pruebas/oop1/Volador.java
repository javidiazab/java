package com.java.pruebas.oop1;

public interface Volador {

    public void despegar();

    public void volar();

    public void aterrizar();
}
