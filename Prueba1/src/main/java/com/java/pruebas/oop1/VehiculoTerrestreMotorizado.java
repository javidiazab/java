package com.java.pruebas.oop1;

public abstract class VehiculoTerrestreMotorizado extends VehiculoTerrestre {
    private Motor motor;
    private String matricula;

    public VehiculoTerrestreMotorizado() {
        super();
    }

    public VehiculoTerrestreMotorizado(int cantidadRuedas, Motor motor, String matricula) {
        super();
        this.motor = motor;
        this.matricula = matricula;
    }

    public void arrancar() {
        System.out.println("Vehiculo: Arrancado");
    }

    public void apagar() {
        System.out.println("Vehiculo: Apagado");
    }

    public void acelerar() {
        System.out.println("Vehiculo: Acelerando");
    }

    public void decelerar() {
        System.out.println("Vehiculo: Decelerando");
    }
}
