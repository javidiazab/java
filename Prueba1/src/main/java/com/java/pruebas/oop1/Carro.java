package com.java.pruebas.oop1;

public class Carro extends VehiculoTerrestre {
    private String color;
    private int cantidadRuedas;
    private String material;

    public Carro() {
        this.cantidadRuedas = 4;
    }

    public Carro(int cantidadRuedas, String color, String material) {
        super();
        this.setColor(color);
        this.material = material;
    }


    public String getColor() {
        return color;
    }

    public int getCantidadRuedas() {
        return cantidadRuedas;
    }

    public String getMaterial() {
        return material;
    }

    public void setColor(String color) {
        this.color = color;
    }

    private void setCantidadRuedas(int cantidadRuedas) {
        this.cantidadRuedas = cantidadRuedas;
    }

    private void setMaterial(String material) {
        this.material = material;
    }
}
