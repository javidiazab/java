package com.java.pruebas.oop1;

public class Bicicleta extends Vehiculo {
    private String color;
    private int cantidadRuedas;
    private String material;

    public Bicicleta() {
        this.cantidadRuedas = 2;
        this.material = "Aluminio";
        this.setColor("Cromado");
    }

    public Bicicleta(String color) {
        this.cantidadRuedas = 2;
        this.material = "Aluminio";
        this.setColor(color);
    }

    public void setColor(String color) {
        this.color = color;
    }
}
