package com.java.pruebas.sort.services;

import com.java.pruebas.sort.exceptions.FichaException;
import com.java.pruebas.sort.model.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.java.pruebas.sort.Constants.*;
import static com.java.pruebas.sort.InOutRecTypes.*;

class FichaService {
    final FichasSortDto fichasSortDto = new FichasSortDto();

    public FichaService(String ficha) throws FichaException {

        Optional<String[]> fichas = cargarFichas(ficha);

        if (fichas.isPresent()) {
            fichasSortDto.setFichaInRec(loadInRec(fichas.get()).orElse(null));
            fichasSortDto.setFichaInclude(loadIncludeCond(fichas.get()).orElse(null));
            fichasSortDto.setFichaOmit(loadOmitCond(fichas.get()).orElse(null));
            fichasSortDto.setFichaSortFields(loadSortFields(fichas.get()).orElse(null));
            fichasSortDto.setFichaSum(loadSum(fichas.get()).orElse(null));
            fichasSortDto.setFichaOutRec(loadOutRec(fichas.get()).orElse(null));
//            log.debug("Fichas: {}",fichasSortDto.toString());
        }
    }

    public FichasSortDto getFichasSortDto() {
        return fichasSortDto;
    }

    private Optional<String[]> cargarFichas(String fichaJCL) {
        try {
            return Optional.of(Files.lines(Paths.get(fichaJCL)).toArray(String[]::new));
        } catch (IOException e) {
            e.printStackTrace();
            return Optional.empty();
        }
//        } catch (IOException e) {
//            throw new FichaException("Error cargando Fichas: " + e.getMessage());
//        }
    }

    public Optional<List<InOutRecFieldsDto>> loadInRec(String[] fichas) {
        return Arrays.stream(fichas).filter(a -> a.matches(FORMATO_IN_REC_FIELDS))
                .map(this::getFichaItself)
                .findFirst()
                .map(this::cargarFichaInOutRec);
    }

    public Optional<List<SortFieldsDto>> loadSortFields(String[] fichas) {
        return Arrays.stream(fichas).filter(a -> a.matches(FORMATO_SORT_FIELDS))
                .map(this::getFichaItself)
                .findFirst()
                .map(this::cargaFichaSortFields);
    }

    private List<SortFieldsDto> cargaFichaSortFields(String fichaSortFields) {
        ArrayList<SortFieldsDto> fichas = new ArrayList<>();
        String[] elemsOrden = fichaSortFields.split(COMMA);
        for (int i = 0; i < elemsOrden.length; i += 3) {
            fichas.add(SortFieldsDto.builder()
                    .inicio(Integer.valueOf(elemsOrden[i]))
                    .longitud(Integer.valueOf(elemsOrden[i + 1]))
                    .orden(elemsOrden[i + 2])
                    .build());
        }
        return fichas;
    }

    public Optional<List<IncludeOmitCondDto>> loadIncludeCond(String[] fichas) {
        return Arrays.stream(fichas).filter(a -> a.matches(FORMATO_INCLUDE_COND))
                .map(this::getFichaItself)
                .findFirst()
                .map(this::cargarFichaIncludeCond);
    }

    private List<IncludeOmitCondDto> cargarFichaIncludeCond(String fichaIncludeCond) {
        ArrayList<IncludeOmitCondDto> fichas = new ArrayList<>();
        String[] elems = fichaIncludeCond.split(COMMA);
        for (int i = 0; i < elems.length; ) {
            if (elems[i].matches("(AND|OR|and|or)")) {
                fichas.add(IncludeOmitCondDto.builder()
                        .inicio(Integer.valueOf(elems[i + 1]))
                        .longitud(Integer.valueOf(elems[i + 2]))
                        .tipo(elems[i + 3])
                        .comparador(elems[i + 4])
                        .chars(extractChars(elems[i + 5]))
                        .operadorLogico(elems[i])
                        .build());
                i += 6;
            } else {
                fichas.add(IncludeOmitCondDto.builder()
                        .inicio(Integer.valueOf(elems[i]))
                        .longitud(Integer.valueOf(elems[i + 1]))
                        .tipo(elems[i + 2])
                        .comparador(elems[i + 3])
                        .chars(extractChars(elems[i + 4]))
                        .operadorLogico("")
                        .build());
                i += 5;
            }
        }
        return fichas;
    }

    private Optional<List<IncludeOmitCondDto>> loadOmitCond(String[] fichas) {
        return Arrays.stream(fichas).filter(a -> a.matches(FORMATO_OMIT_COND))
                .map(this::getFichaItself)
                .findFirst()
                .map(this::cargarFichaOmitCond);
    }

    private List<IncludeOmitCondDto> cargarFichaOmitCond(String fichaOmitCond) {
        ArrayList<IncludeOmitCondDto> fichas = new ArrayList<>();
        String[] elems = fichaOmitCond.split(COMMA);
        for (int i = 0; i < elems.length; ) {
            if (elems[i].matches("(AND|OR|and|or)")) {
                fichas.add(IncludeOmitCondDto.builder()
                        .inicio(Integer.valueOf(elems[i + 1]))
                        .longitud(Integer.valueOf(elems[i + 2]))
                        .tipo(elems[i + 3])
                        .comparador(elems[i + 4])
                        .chars(extractChars(elems[i + 5]))
                        .operadorLogico(elems[i])
                        .build());
                i += 6;
            } else {
                fichas.add(IncludeOmitCondDto.builder()
                        .inicio(Integer.valueOf(elems[i]))
                        .longitud(Integer.valueOf(elems[i + 1]))
                        .tipo(elems[i + 2])
                        .comparador(elems[i + 3])
                        .chars(extractChars(extractChars(elems[i + 4])))
                        .operadorLogico("")
                        .build());
                i += 5;
            }
        }
        return fichas;
    }

    public Optional<List<InOutRecFieldsDto>> loadOutRec(String[] fichas) {
        return Arrays.stream(fichas).filter(a -> a.matches(FORMATO_OUT_REC_FIELDS))
                .map(this::getFichaItself)
                .findFirst()
                .map(this::cargarFichaInOutRec);
    }

    private List<InOutRecFieldsDto> cargarFichaInOutRec(String fichaOutRec) {
        ArrayList<InOutRecFieldsDto> fichas = new ArrayList<>();
        String[] elems = fichaOutRec.split(COMMA);
        for (int i = 0; i < elems.length; ) {
            if (elems[i].matches("\\d*[cC]?'.+'")) {
                fichas.add(InOutRecFieldsDto.builder()
                        .type(CHARS)
                        .chars(extractChars(elems[i]))
                        .charMultiplier(extractMultiplier(elems[i]))
                        .build());
                i++;
            } else if (elems[i].matches("\\d*[xX]$")) {
                fichas.add(InOutRecFieldsDto.builder()
                        .type(SPACES)
                        .chars(SPACE)
                        .charMultiplier(Integer.parseInt(elems[i].toUpperCase().replace("X", "")))
                        .build());
                i++;
            } else if (elems[i].contains(COLON)) {
                String[] outrecParts = elems[i].split(COLON);
                fichas.add(InOutRecFieldsDto.builder()
                        .type(SELECT_OUTPUT_POSITION)
                        .posOutputFile(Integer.parseInt(outrecParts[0]))
                        .posInputFile(Integer.parseInt(outrecParts[1]))
                        .longInputFile(Integer.parseInt(elems[i + 1]))
                        .chars(SPACE)
                        .build());
                i += 2;
            } else {
                fichas.add(InOutRecFieldsDto.builder()
                        .type(NEXT_OUTPUT_POSITION)
                        .posInputFile(Integer.parseInt(elems[i]))
                        .longInputFile(Integer.parseInt(elems[i + 1]))
                        .build());
                i += 2;
            }
        }
        return fichas;
    }

    public Optional<List<SumDto>> loadSum(String[] fichas) {
        return Arrays.stream(fichas).filter(a -> a.matches(FORMATO_SUM_FIELDS))
                .map(this::getFichaItself)
                .findFirst()
                .map(this::cargaFichaSumFields);
    }

    private List<SumDto> cargaFichaSumFields(String fichaSumFields) {
        ArrayList<SumDto> fichas = new ArrayList<>();
        String[] elems = fichaSumFields.split(COMMA);
        for (int i = 0; i < elems.length; i += 2) {
            fichas.add(SumDto.builder()
                    .inicio(Integer.valueOf(elems[i]))
                    .longitud(Integer.valueOf(elems[i + 1]))
                    .build());
        }
        return fichas;
    }

    private String getFichaItself(String linea) {
        return linea.substring(linea.indexOf("(") + 1, linea.length() - 1);
    }

    private Integer extractMultiplier(String cadena) {
        String[] elems = cadena.split(QUOTATION_MARK);
        if (elems.length == 2) {
            String multiplier = elems[0].toUpperCase().replace("C", "");
            if (multiplier.length() > 0) {
                return Integer.parseInt(multiplier);
            }
        }
        return 1;
    }

    private String extractChars(String cadena) {
        String[] elems = cadena.split(QUOTATION_MARK);
        return elems.length == 2 ? elems[1] : elems[0];
    }

    public static void main(String[] args) {
        try {
            new FichaService("src/main/java/com/java/pruebas/sort/resources/Fichas.txt");
        } catch (FichaException exception) {
            exception.printStackTrace();
        }
    }
}