package com.java.pruebas.sort.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class SortFieldsDto {
    Integer inicio;
    Integer longitud;
    String orden;
}

