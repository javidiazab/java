package com.java.pruebas.sort.model;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FichasSortDto {
    private List<InOutRecFieldsDto> fichaInRec;
    private List<IncludeOmitCondDto> fichaInclude;
    private List<IncludeOmitCondDto> fichaOmit;
    private List<SortFieldsDto> fichaSortFields;
    private List<SumDto> fichaSum;
    private List<InOutRecFieldsDto> fichaOutRec;
}
