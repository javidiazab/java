package com.java.pruebas.sort.exceptions;

public class FichaException extends Exception {
    public FichaException(String msg) {
        super(msg);
    }
}
