package com.java.pruebas.sort.exceptions;

public class SortException extends Exception {
    public SortException(String msg) {
        super(msg);
    }
}
