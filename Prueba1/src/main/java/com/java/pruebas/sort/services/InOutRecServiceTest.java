package com.java.pruebas.sort.services;

import com.java.pruebas.sort.model.InOutRecFieldsDto;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.java.pruebas.sort.InOutRecTypes.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class InOutRecServiceTest {

    @Test
    void shouldSelectRightInput_whenCHARS() {
        //Given
        final InOutRecFieldsDto inOutRecFieldsDto = InOutRecFieldsDto.builder()
                .type(CHARS)
                .chars("Hola")
                .charMultiplier(2)
                .build();
        String[] file = {"0123456789aaabbbcccdddeeefffggghhh"};
        final InOutRecService inRecService = new InOutRecService(Arrays.asList(inOutRecFieldsDto));
        String[] expectedfile = {"HolaHola"};

        //When
        final String[] actualRecord = inRecService.applyCard(file);

        //Then
        assertEquals(expectedfile[0], actualRecord[0]);
    }

    @Test
    void shouldSelectRightInput_whenSELECT_OUTPUT_POSITION() {
        //Given
        final InOutRecFieldsDto inOutRecFieldsDto = InOutRecFieldsDto.builder()
                .type(SELECT_OUTPUT_POSITION)
                .posOutputFile(5)
                .posInputFile(3)
                .longInputFile(22)
                .chars(" ")
                .charMultiplier(0)
                .build();
        String[] file = {"0123456789aaabbbcccdddeeefffggghhh"};
        final InOutRecService inRecService = new InOutRecService(Arrays.asList(inOutRecFieldsDto));
        String[] expectedfile = {"     23456789aaabbbcccdddee"};

        //When
        final String[] actualRecord = inRecService.applyCard(file);

        //Then
        assertEquals(expectedfile[0], actualRecord[0]);
    }

    @Test
    void shouldSelectRightInput_whenNEXT_OUTPUT_POSITION() {
        //Given
        final InOutRecFieldsDto inOutRecFieldsDto = InOutRecFieldsDto.builder()
                .type(NEXT_OUTPUT_POSITION)
                .posInputFile(4)
                .longInputFile(3)
                .build();
        String[] expectedFile = {"A J"};
        final InOutRecService inRecService = new InOutRecService(Arrays.asList(inOutRecFieldsDto));
        String[] file = {"HOLA JAVI"};

        //When
        final String[] actualFile = inRecService.applyCard(file);

        //Then
        assertEquals(expectedFile[0], actualFile[0]);
    }

    @Test
    void shouldSelectRightInput_whenSeveralCards() {
        //Given

        String[] expectedFile = {"345   xxx      aaab"};
        final InOutRecService inRecService = new InOutRecService(ValidInOutRecFieldsDto());
        String[] file = {"0123456789aaabbbcccdddeeefffggghhh"};

        //When
        final String[] actualFile = inRecService.applyCard(file);

        //Then
        assertEquals(expectedFile[0], actualFile[0]);
    }

    @Test
    void shouldSelectRightInput_whenSeveraCardsAndLines() {
        //Given
        String[] file = {"0123456789aaabbbcccdddeeefffggghhh", "0123456789cccdddeeefffgggfffggghhh"};
        String[] expectedFile = {"345   xxx      aaab", "345   xxx      cccd"};
        final InOutRecService inRecService = new InOutRecService(ValidInOutRecFieldsDto());

        //When
        final String[] actualFile = inRecService.applyCard(file);

        //Then
        assertEquals(expectedFile[0], actualFile[0]);
    }

    private List<InOutRecFieldsDto> ValidInOutRecFieldsDto() {
        final InOutRecFieldsDto inOutRecFieldsDto1 = InOutRecFieldsDto.builder()
                .type(NEXT_OUTPUT_POSITION)
                .posInputFile(4)
                .longInputFile(3)
                .build();
        final InOutRecFieldsDto inOutRecFieldsDto2 = InOutRecFieldsDto.builder()
                .type(SPACES)
                .chars(" ")
                .charMultiplier(3)
                .build();
        final InOutRecFieldsDto inOutRecFieldsDto3 = InOutRecFieldsDto.builder()
                .type(CHARS)
                .chars("xxx")
                .charMultiplier(1)
                .build();
        final InOutRecFieldsDto inOutRecFieldsDto4 = InOutRecFieldsDto.builder()
                .type(SELECT_OUTPUT_POSITION)
                .posOutputFile(15)
                .posInputFile(11)
                .longInputFile(4)
                .chars(" ")
                .build();

        return Arrays.asList(inOutRecFieldsDto1, inOutRecFieldsDto2, inOutRecFieldsDto3, inOutRecFieldsDto4);
    }
}