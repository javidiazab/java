package com.java.pruebas.sort.services;

import com.java.pruebas.sort.exceptions.FichaException;
import com.java.pruebas.sort.model.InOutRecFieldsDto;
import com.java.pruebas.sort.model.RecordDto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class Sort {
    public static final String SPACE = " ";
    private Stream<String> fileCargado = null;
    private FichaService fichaService = null;
    private Boolean hayOrdenDescendente = false;
    private String orden = "";
    private String fileE = "";
    private String fileS = "";
    private String fileFichas = "";
    private Map<Integer, RecordDto> records = new HashMap<>();

    private Integer numRecord = 0;

    public Sort(String fileE, String fileS, String fileFichas) {
        this.fileE = fileE;
        this.fileS = fileS;
        this.fileFichas = fileFichas;
        try {
            this.fichaService = new FichaService(fileFichas);
        } catch (FichaException e) {
            e.printStackTrace();
        }
        this.fileCargado = cargaFile(fileE);
        applyIncludeCond();
        applyOmmitCond();
        Stream<String> processingFile = applyInCards();
        applySort();
        applyOutCards();
        //records.forEach((k,v) -> System.out.println("Key: " + k + ": Value: " + v));
    }

    private Stream<String> cargaFile(String file) {
        Stream<String> stream = Stream.empty();

        try {
            stream = Files.lines(Paths.get(file));
            stream.map(linea -> records.put(numRecord++, new RecordDto(numRecord++, "xxx", linea)))
                    .forEach(System.out::println);
            //.map(Ordenador::buildFromArray)
            //.mapToDouble(o -> o.getPrecio())
            //.onClose(() -> System.out.println("termino"))
            //.reduce(Double::sum)
            //.ifPresent(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    private void applyIncludeCond() {
    }

    private void applyOmmitCond() {
    }

    private Stream<String> applyInCards() {
        if (Objects.isNull(this.fileCargado) || Objects.isNull(this.fichaService)) {
            return null;
        }
        return fileCargado.map(this::in);
    }

    private String in(String lineaFile) {
        List<InOutRecFieldsDto> fichaInRec = this.fichaService.getFichasSortDto().getFichaInRec();
        String outLine = "";
        for (int i = 0; i < fichaInRec.size(); i++) {
            InOutRecFieldsDto inOutRecFields = fichaInRec.get(i);
            switch (inOutRecFields.getType()) {
                case CHARS:
                case SPACES:
                    outLine += inOutRecFields.getChars().repeat(inOutRecFields.getCharMultiplier());
                    break;
                case SELECT_OUTPUT_POSITION:
                    outLine += fillWithSpaces(outLine, inOutRecFields.getPosOutputFile());
                case NEXT_OUTPUT_POSITION:
                    outLine += lineaFile.substring(inOutRecFields.getPosInputFile() - 1, inOutRecFields.getPosInputFile() + inOutRecFields.getLongInputFile() - 2);
                    break;
                default:
                    break;
            }
        }
        return outLine;
    }

    private String fillWithSpaces(String outLine, int posOutputFile) {
        return SPACE.repeat(posOutputFile - outLine.length());
    }

    private void applySort() {
    }

    private void applyOutCards() {
    }

    private void ordenar(Integer indice, String clave) {
    }

    public static void main(String[] args) {
        String ficheroE = "src/main/java/com/java/pruebas/sort/resources/EPrueba.txt";
        String ficheroS = "src/main/java/com/java/pruebas/sort/resources/SPrueba.txt";
        String fileFichas = "src/main/java/com/java/pruebas/sort/resources/FichasPrueba1.txt";
        new Sort(ficheroE, ficheroS, fileFichas);
    }
}
