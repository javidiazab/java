package com.java.pruebas.sort;

public enum InOutRecTypes {
    CHARS("C'xxx'"),
    SPACES("3X"),
    SELECT_OUTPUT_POSITION("4:3,5"),
    NEXT_OUTPUT_POSITION("3,5");

    private String description;

    InOutRecTypes(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
