package com.java.pruebas.sort.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class IncludeOmitCondDto {
    Integer inicio;
    Integer longitud;
    String tipo;
    String comparador;
    String chars;
    String operadorLogico;
}

