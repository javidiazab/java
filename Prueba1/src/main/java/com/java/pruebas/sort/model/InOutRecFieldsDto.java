package com.java.pruebas.sort.model;

import com.java.pruebas.sort.InOutRecTypes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class InOutRecFieldsDto {
    InOutRecTypes type;
    Integer posOutputFile;
    Integer posInputFile;
    Integer longInputFile;
    String chars;
    Integer charMultiplier;
}

