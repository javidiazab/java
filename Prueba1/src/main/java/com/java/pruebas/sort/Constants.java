package com.java.pruebas.sort;

public class Constants {
    public static final String QUOTATION_MARK = "'";
    public static final String COMMA = ",";
    public static final String COLON = ":";
    public static final String SPACE = " ";

    public static final String COMMON_IN_OUT_RECORDS = "\\((((,?\\d+:\\d+,\\d+)|(,?\\d*[cC]?\\'.+\\')|(,?\\d+[xX])|(,?\\d+,\\d+))+)\\)[ ]*";
    public static final String COMMON_INCLUDE_OMIT_COND = "\\(((,(AND|and|OR|or),)?\\d+,\\d+,(CH|ch|BI|bi|PD|pd|ZD|zd),(GE|GT|EQ|LE|LT|ge|gt|eq|le|lt),[cC]?'.+')+\\)[ ]*";

    public static final String FORMATO_IN_REC_FIELDS = "[ ]*(inrec|INREC)[ ]*(fields|FIELDS)[ ]*=[ ]*" + COMMON_IN_OUT_RECORDS;
    public static final String FORMATO_INCLUDE_COND = "[ ]*(include|INCLUDE)[ ]*(cond|COND)[ ]*=[ ]*" + COMMON_INCLUDE_OMIT_COND;
    public static final String FORMATO_OMIT_COND = "[ ]*(omit|OMIT)[ ]*(cond|COND)[ ]*=[ ]*" + COMMON_INCLUDE_OMIT_COND;
    public static final String FORMATO_SORT_FIELDS = "[ ]*(sort|SORT)[ ]*(fields|FIELDS)[ ]*=[ ]*(COPY|copy|\\((,?\\d+,\\d+,[adAD])+\\))[ ]*";
    public static final String FORMATO_SUM_FIELDS = "[ ]*(sum|SUM)[ ]*(fields|FIELDS)[ ]*=[ ]*(NONE|none|\\((,?\\d+,\\d+)+\\))[ ]*";
    public static final String FORMATO_OUT_REC_FIELDS = "[ ]*(outrec|OUTREC)[ ]*(fields|FIELDS)[ ]*=[ ]*" + COMMON_IN_OUT_RECORDS;
}
