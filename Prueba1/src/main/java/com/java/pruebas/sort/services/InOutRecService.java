package com.java.pruebas.sort.services;

import com.java.pruebas.sort.model.InOutRecFieldsDto;

import java.util.ArrayList;
import java.util.List;

public class InOutRecService {
    private List<InOutRecFieldsDto> inOutRecFieldsDtoList;

    public InOutRecService(List<InOutRecFieldsDto> inOutRecFieldsDtoList) {
        this.inOutRecFieldsDtoList = inOutRecFieldsDtoList;
    }

    public String[] applyCard(String[] file) {
        ArrayList<String> outputFile = new ArrayList<>();
        String[] returnFile;
        String sOut = "";

        for (String linea : file) {
            for (InOutRecFieldsDto iR : inOutRecFieldsDtoList) {
                final Integer posInputFile = iR.getPosInputFile();
                switch (iR.getType()) {
                    case CHARS:
                    case SPACES:
                        sOut = sOut.concat(iR.getChars().repeat(iR.getCharMultiplier()));
                        break;
                    case SELECT_OUTPUT_POSITION:
                        //TODO check that sOut.lenght is !> posOututFile
                        sOut = sOut.concat(iR.getChars().repeat(iR.getPosOutputFile() - sOut.length()));
                        sOut = sOut.concat(linea.substring(posInputFile - 1, posInputFile + iR.getLongInputFile() - 1));
                        break;
                    case NEXT_OUTPUT_POSITION:
                        sOut = sOut.concat(linea.substring(posInputFile - 1, posInputFile + iR.getLongInputFile() - 1));
                        break;
                    default:
                        break;
                }
            }
            outputFile.add(sOut);
            sOut = "";
        }
        returnFile = new String[outputFile.size()];
        return outputFile.toArray(returnFile);
    }
}
