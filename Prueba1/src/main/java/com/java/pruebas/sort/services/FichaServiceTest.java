package com.java.pruebas.sort.services;

import com.java.pruebas.sort.exceptions.FichaException;
import com.java.pruebas.sort.model.InOutRecFieldsDto;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.java.pruebas.sort.InOutRecTypes.CHARS;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FichaServiceTest {

    @Test
    void shouldLoadInRec_whenCharWithCAndMultiplier() throws FichaException {
        //Given
        final InOutRecFieldsDto expectedInOutRecFieldsDto = InOutRecFieldsDto.builder()
                .type(CHARS)
                .chars("Hola")
                .charMultiplier(5)
                .build();
        String givenFicha = "java/com/java/pruebas/sort/resources/FichasInRecTest.txt";

        final InOutRecService inRecService = new InOutRecService(Arrays.asList(expectedInOutRecFieldsDto));

        //When

        FichaService fichaService = new FichaService(givenFicha);

        //Then
        assertEquals(expectedInOutRecFieldsDto.getType(), fichaService.getFichasSortDto().getFichaInRec().get(0).getType());
        assertEquals(expectedInOutRecFieldsDto.getLongInputFile(), fichaService.getFichasSortDto().getFichaInRec().get(0).getLongInputFile());
        assertEquals(expectedInOutRecFieldsDto.getPosInputFile(), fichaService.getFichasSortDto().getFichaInRec().get(0).getPosInputFile());
        assertEquals(expectedInOutRecFieldsDto.getPosOutputFile(), fichaService.getFichasSortDto().getFichaInRec().get(0).getPosOutputFile());
        assertEquals(expectedInOutRecFieldsDto.getChars(), fichaService.getFichasSortDto().getFichaInRec().get(0).getChars());
        assertEquals(expectedInOutRecFieldsDto.getCharMultiplier(), fichaService.getFichasSortDto().getFichaInRec().get(0).getCharMultiplier());
    }

    @Test
    void shouldLoadSortFields() {
        //Given
        //When
        //Then
    }

    @Test
    void shouldLoadIncludeCond() {
        //Given
        //When
        //Then
    }

    @Test
    void shouldLoadOmmitCond() {
        //Given
        //When
        //Then
    }

    @Test
    void shouldLoadOutRec() throws FichaException {
        //Given
        final InOutRecFieldsDto expectedInOutRecFieldsDto = InOutRecFieldsDto.builder()
                .type(CHARS)
                .chars("Hola")
                .charMultiplier(5)
                .build();
        String givenFicha = "java/com/java/pruebas/sort/resources/FichasInRecTest.txt";

        final InOutRecService inRecService = new InOutRecService(Arrays.asList(expectedInOutRecFieldsDto));

        //When

        FichaService fichaService = new FichaService(givenFicha);

        //Then
        assertEquals(expectedInOutRecFieldsDto.getType(), fichaService.getFichasSortDto().getFichaInRec().get(0).getType());
        assertEquals(expectedInOutRecFieldsDto.getLongInputFile(), fichaService.getFichasSortDto().getFichaInRec().get(0).getLongInputFile());
        assertEquals(expectedInOutRecFieldsDto.getPosInputFile(), fichaService.getFichasSortDto().getFichaInRec().get(0).getPosInputFile());
        assertEquals(expectedInOutRecFieldsDto.getPosOutputFile(), fichaService.getFichasSortDto().getFichaInRec().get(0).getPosOutputFile());
        assertEquals(expectedInOutRecFieldsDto.getChars(), fichaService.getFichasSortDto().getFichaInRec().get(0).getChars());
        assertEquals(expectedInOutRecFieldsDto.getCharMultiplier(), fichaService.getFichasSortDto().getFichaInRec().get(0).getCharMultiplier());
    }

    @Test
    void shouldLoadSum() {
        //Given
        //When
        //Then
    }

}