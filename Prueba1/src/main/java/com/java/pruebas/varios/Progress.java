package com.java.pruebas.varios;

import java.awt.*;
//import java.awt.event.*;
import javax.swing.*;

public class Progress extends JFrame {
    JProgressBar current;
    JTextArea out;
    JButton find;
    Thread runner;
    int num = 0;
    int total = 0;

    public Progress(int total) {
        super("Progress");

        this.total = total;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel pane = new JPanel();
        pane.setLayout(new FlowLayout());
        current = new JProgressBar(0, total);
        current.setValue(0);
        current.setStringPainted(true);
        pane.add(current);
        setContentPane(pane);
    }

    public void setValue(int value) {
        current.setValue(value);
        if (value == total) dispose();
    }

    public void iterate() {
        while (num < total) {
            current.setValue(num);
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
            }
            num += 1;
        }
    }

    public static void main(String[] arguments) {
        Progress frame = new Progress(2000);
        frame.pack();
        frame.setVisible(true);
        //frame.iterate();
        for (int i = 0; i <= 2000; i++) {
            frame.setValue(i);
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
            }
        }
        //frame.dispose();
    }
}


