package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;
import javax.swing.*;

//Matching de dos ficheros
//si en la longitud de la clave se introduce un ';' u otro caracter no toma longitud sino hasta que encuentre el caracter introducido

public class Matching {
    //Como mejora que valide la entrada del formato del fichero de salida

    private String nbPGM = "Matching";
    int tipoMatching = 0;
    int driverClaveDesde = 0;
    int driverClaveLong = 0;
    int matchingClaveDesde = 0;
    int matchingClaveLong = 0;

    String driverCaracterDivision = "";
    int driverClaveColumna = 0;
    String matchingCaracterDivision = "";
    int matchingClaveColumna = 0;


    private String directorio;
    String lineMatching = null;
    String lineDriver = null;
    String fmtoSalida = null;
    //para que ignore o no las mayusculas en la comparacion de la clave
    boolean ignoreCase = true;

    String maxValor = "}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}";


    String dirResultados = "\\Resultados " + nbPGM;
    File fileSeleccionado;
    String fechaS = UtilsJavi.getFechaHora();
    String FileMatchingIguales = "FileMatchingIguales " + fechaS + ".txt";
    String FileMatchingDriver = "FileMatchingDriver " + fechaS + ".txt";
    String FileMatchingMatchi = "FileMatchingMatchi " + fechaS + ".txt";
    String FileMatchingDistint = "FileMatchingDistint " + fechaS + ".txt";
    String analisisJCLS = "AnalisisJCLs " + fechaS + ".txt";

    int nivelLog = 0; //0 no sale nada, 5 sale todo;

    public Matching() {
        //MODO:
        //0=SOLO LOS QUE CONCIDAN EN EL DRIVER Y EN EL MATCHING (n)
        //1=TODO EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING (n)
        //2=SOLO LOS QUE CONCIDAN EN EL DRIVER Y EN EL MATCHING (solo 1)
        //3=TODO EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING (solo 1)
        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);
        //UtilsJavi.escribirLog(fileXMLATratar + ", " + nivelDesdeDondeSacar + ", " + soloEtiquetaConNombre, 1, nivelLog);
        //***************************************************************************************************
        //Pruebas
        //driverCaracterDivision = "";
        //matchingCaracterDivision = ";";
        //salida ("D,1,2,3,M,1,2,3", "1;2;3", "4;5;6");
        //salida ("D,1,2,4,2,M,2,2", "123456", "789");  //resultado 124589
        //salida ("D,1,2,4,2,M,2,3", "123456", "7;8;9");  //resultado 1245;8;9


        // ********************************  DRIVER  *************************************
        //Seleccionamos el tipo de matching
        String valor = JOptionPane.showInputDialog(
                "0=SOLO LOS QUE CONCIDAN EN EL DRIVER Y EN EL MATCHING (n)\n" +
                        "1=TODO EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING (n)\n" +
                        "2=SOLO LOS QUE CONCIDAN EN EL DRIVER Y EN EL MATCHING (solo 1)\n" +
                        "3=TODO EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING (solo 1)\n\n" +
                        "Tipo de Matching: 0, 1, 2, 3");
        if (!valor.equals("") && valor != null)
            tipoMatching = Integer.parseInt(valor);

        //Seleccionamos el driver
        fileSeleccionado = UtilsJavi.seleccionarFicheroDesc("C:\\DATOS\\Prueba", "", "", 2, "Driver");
        if (Objects.isNull(fileSeleccionado))
            return;
        File fileDriver = new File(fileSeleccionado.getAbsolutePath());
        directorio = UtilsJavi.getPath(fileDriver);

        //Seleccionamos el caracter de division del driver si hay
        valor = JOptionPane.showInputDialog("Driver caracter de divisi�n");
        if (valor.equals("") || valor == null) {
            //Seleccionamos la clave desde del driver
            valor = JOptionPane.showInputDialog("Driver clave desde");
            if (!valor.equals("") && valor != null)
                driverClaveDesde = Integer.parseInt(valor);

            //Seleccionamos la clave longitud del driver
            valor = JOptionPane.showInputDialog("Driver longitud de clave");
            if (!valor.equals("") && valor != null)
                driverClaveLong = Integer.parseInt(valor);

        } else {
            driverCaracterDivision = valor;

            //Seleccionamos la columna de clave del driver
            valor = JOptionPane.showInputDialog("Driver columna clave");
            if (!valor.equals("") && valor != null)
                driverClaveColumna = Integer.parseInt(valor);
        }


        // **********************************  MATCHING  *************************************
        //Seleccionamos el matching
        fileSeleccionado = UtilsJavi.seleccionarFicheroDesc("C:\\DATOS\\Prueba", "", "", 2, "Matching");
        if (Objects.isNull(fileSeleccionado))
            return;
        File fileMatching = new File(fileSeleccionado.getAbsolutePath());

        //Seleccionamos el caracter de division del matching si hay
        valor = JOptionPane.showInputDialog("Matching caracter de divisi�n");
        if (valor.equals("") || valor == null) {
            //Seleccionamos la clave desde del matching
            valor = JOptionPane.showInputDialog("Matching clave desde");
            if (!valor.equals("") && valor != null)
                matchingClaveDesde = Integer.parseInt(valor);

            //Seleccionamos la clave longitud del matching
            valor = JOptionPane.showInputDialog("Matching longitud de clave");
            if (!valor.equals("") && valor != null)
                matchingClaveLong = Integer.parseInt(valor);

        } else {
            matchingCaracterDivision = valor;

            //Seleccionamos la columna de clave del Matching
            valor = JOptionPane.showInputDialog("Matching columna clave");
            if (!valor.equals("") && valor != null)
                matchingClaveColumna = Integer.parseInt(valor);
        }

        // **********************************  SALIDA  *************************************

        valor = JOptionPane.showInputDialog(
                "Seleccionamos como vamos a montar la salida del fichero\n" +
                        "D(Driver)/M(Matching),pos,long: D,1,5,M,2,5\n" +
                        "D(Driver)/M(Matching),col: D,1,M,2\n" +
                        "se puede poner 'xxxx'  ej: D,1,2,3,'xxx',M,1,2,3\n\n" +
                        "Salida D(Driver)/M(Matching),pos,long: D,1,5,M,2,5");
        if (!valor.equals("") && valor != null)
            fmtoSalida = valor;


        //Creamos directorio resultado en el directorio donde esta el fichero driver y comenzamos lectura
        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        String claveDriver = "";
        String claveMatching = "";
        String claveDriverAnt = "";
        String claveMatchingAnt = "";

        try {
            FileSys driver = new FileSys(fileDriver.toString());
            lineDriver = driver.readLine();

            try {
                FileSys matching = new FileSys(fileMatching.toString());
                lineMatching = matching.readLine();
                while (lineDriver != null || lineMatching != null) { //Mientras no sea fin de ficheros

                    //claveDriverAnt = claveDriver;
                    //claveMatchingAnt = claveMatching;

                    if (!claveDriver.equals(maxValor)) {
                        if (!driverCaracterDivision.equals(""))   //obtenemos la clave (por caracter o por posiciones)
                            claveDriver = obtenerClave(lineDriver, driverCaracterDivision, driverClaveColumna);
                        else
                            claveDriver = obtenerClave(lineDriver, driverClaveDesde, driverClaveLong);
                    }

                    if (!claveMatching.equals(maxValor)) {
                        if (!matchingCaracterDivision.equals(""))
                            claveMatching = obtenerClave(lineMatching, matchingCaracterDivision, matchingClaveColumna);
                        else
                            claveMatching = obtenerClave(lineMatching, matchingClaveDesde, matchingClaveLong);
                    }

                    UtilsJavi.escribirLog("driver: " + claveDriver, 1, nivelLog);
                    UtilsJavi.escribirLog("matchi: " + claveMatching, 1, nivelLog);
                    UtilsJavi.escribirLog("driverAnt: " + claveDriverAnt, 1, nivelLog);
                    UtilsJavi.escribirLog("matchiAnt: " + claveMatchingAnt, 1, nivelLog);
                    UtilsJavi.escribirLog("**********************************", 1, nivelLog);


                    //0=SOLO LOS QUE CONCIDAN EN EL DRIVER Y TODOS LOS QUE COINCIDAN EN EL MATCHING (1:N)
                    //1,2,3,4,5 --salida----> 11111,3,4
                    if (tipoMatching == 0) {
                        if (comparaClaves(claveDriver, claveMatching) == 0) {
                            if (lineMatching != null) {
                                CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                                        claveDriver + ";" + lineMatching);
                                lineMatching = matching.readLine();
                                if (lineMatching == null) claveMatching = maxValor;
                            } else claveMatching = maxValor;
                            UtilsJavi.print("igual " + claveDriver + " " + claveMatching);
                        }
                        if (comparaClaves(claveDriver, claveMatching) > 0) {
                            if (lineMatching != null) {
                                lineMatching = matching.readLine();
                                if (lineMatching == null) claveMatching = maxValor;
                            } else {
                                claveMatching = maxValor;
                            }
                            UtilsJavi.print("mayor " + claveDriver + " " + claveMatching);
                        }
                        if (comparaClaves(claveDriver, claveMatching) < 0) {
                            if (lineDriver != null) {
                                lineDriver = driver.readLine();
                                if (lineDriver == null) claveDriver = maxValor;
                            } else claveDriver = maxValor;
                            UtilsJavi.print("menor " + claveDriver + " " + claveMatching);
                        }
                    }
                    //1=TODO EL DRIVER Y TODOS LOS QUE COINCIDAN EN EL MATCHING  (1:N)
                    //1,2,3,4,5 --salida---> 111,2,3,4,5555
                    if (tipoMatching == 1) {
                        if (comparaClaves(claveDriver, claveMatching) == 0) {
                            if (lineMatching != null) {
                                CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                                        salida(fmtoSalida, lineDriver, lineMatching));
                                claveMatchingAnt = claveMatching;
                                claveDriverAnt = claveDriver;
                                lineMatching = matching.readLine();
                                if (lineMatching == null) claveMatching = maxValor;
                                UtilsJavi.escribirLog("lineMatching: " + lineMatching, 1, nivelLog);
                            }
                            UtilsJavi.escribirLog("igual " + claveDriver + " " + claveMatching, 1, nivelLog);
                        }
                        if (comparaClaves(claveDriver, claveMatching) > 0) {
                            if (lineMatching != null) {
                                claveMatchingAnt = claveMatching;
                                lineMatching = matching.readLine();
                                if (lineMatching == null) claveMatching = maxValor;
                                UtilsJavi.escribirLog("lineMatching: " + lineMatching, 1, nivelLog);
                            }
                            UtilsJavi.escribirLog("mayor " + claveDriver + " " + claveMatching, 1, nivelLog);
                        }
                        if (comparaClaves(claveDriver, claveMatching) < 0) {
                            if (lineDriver != null) {
                                if (comparaClaves(claveDriver, claveDriverAnt) != 0) {
                                    CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                                            salida(fmtoSalida, lineDriver, ""));
                                }
                                claveDriverAnt = claveDriver;
                                lineDriver = driver.readLine();
                                if (lineDriver == null) claveDriver = maxValor;
                                UtilsJavi.escribirLog("lineDriver: " + lineDriver, 1, nivelLog);
                            }
                            UtilsJavi.escribirLog("menor " + claveDriver + " " + claveMatching, 1, nivelLog);
                        }
                        if (lineDriver != null) {
                            boolean a = true;
                            UtilsJavi.escribirLog("Driver EOF " + a, 1, nivelLog);
                        }
                        if (lineMatching != null) {
                            boolean a = true;
                            UtilsJavi.escribirLog("Matching EOF " + a, 1, nivelLog);
                        }
                        if (lineDriver != null || lineMatching != null)
                            UtilsJavi.escribirLog("No debe salir", 1, nivelLog);
                    }
                    //2=EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING, PERO SOLO UNO DEL MATCHING (1:1)
                    if (tipoMatching == 2) {

                    }
                }  //WHILE

            } catch (java.io.IOException exception) {
            }
            ;
            //System.out.println("Procesando: " + list[i]);
        } catch (java.io.IOException exception) {
        }
        ;
        //System.out.println("Procesando: " + list[i]);

        UtilsJavi.escribirLog("Proceso Terminado Correctamente", 1, nivelLog);
        System.out.println("Proceso Terminado Correctamente");
    } //public Matching (String directorio) {


    private String obtenerClave(String linea, String caracter, int columna) {
        String cadena = linea;
        boolean procesado = false;
        for (int i = 0; i < columna - 1; i++) {
            procesado = true;
            cadena = cadena.substring(cadena.indexOf(caracter) + 1);
        }
        if (procesado) cadena = cadena.substring(0, cadena.indexOf(caracter));
        return cadena;
    }

    private String obtenerClave(String linea, int inicioClave, int longitud) {
        return linea.substring(inicioClave - 1, inicioClave + longitud - 1);
    }

    private int comparaClaves(String driver, String matching) {
        int comparacion;
        if (ignoreCase) comparacion = driver.compareToIgnoreCase(matching);
        else comparacion = driver.compareTo(matching);

        if (comparacion == 0) return 0;
        else if (comparacion < 0) return -1;
        else return 1;
    }

    private String salida(String fmtoSalida, String driver, String matching) {
        //controlar
        //fmtoSalida: D=Driver M=Matching
        String fmtoSalidaTrabajo = fmtoSalida;
        String fichero = "";
        String salida = "";
        String[] parts = null;
        String[] partsDriver = null;
        String[] partsMatching = null;

        parts = fmtoSalidaTrabajo.split(",");
        if (parts.length < 2) return "ERROR 01";
        if (!parts[0].equals("D") && !parts[0].equals("M")) return "ERROR 02";

        if (!driverCaracterDivision.equals("")) partsDriver = driver.split(driverCaracterDivision);
        if (!matchingCaracterDivision.equals("")) partsMatching = matching.split(matchingCaracterDivision);

        for (int i = 0; i < parts.length; i++) {
            if (parts[i].equals("D") || parts[i].equals("M"))
                fichero = parts[i];
            else {
                if (fichero.equals("D") && !driverCaracterDivision.equals("") && !driver.equals("")) {   //Tiene caracter de division
                    if (salida.length() > 0 && !salida.endsWith(driverCaracterDivision))
                        salida = salida + driverCaracterDivision;
                    if (parts[i].startsWith("\'")) salida = salida + parts[i];
                    else salida = salida + partsDriver[Integer.parseInt(parts[i]) - 1] + ";";
                }
                if (fichero.equals("M") && !matchingCaracterDivision.equals("") && !matching.equals("")) { //Tiene caracter de division
                    if (salida.length() > 0 && !salida.endsWith(matchingCaracterDivision))
                        salida = salida + matchingCaracterDivision;
                    if (parts[i].startsWith("\'")) salida = salida + parts[i];
                    else salida = salida + partsMatching[Integer.parseInt(parts[i]) - 1] + ";";
                }
                if (fichero.equals("D") && driverCaracterDivision.equals("") && !driver.equals("")) {  //NO Tiene caracter de division
                    if (parts[i].startsWith("\'")) salida = salida + parts[i];
                    else
                        salida = salida + obtenerClave(driver, Integer.parseInt(parts[i]), Integer.parseInt(parts[i + 1]));
                    i++;
                }
                if (fichero.equals("M") && matchingCaracterDivision.equals("") && !matching.equals("")) { //NO Tiene caracter de division
                    if (parts[i].startsWith("\'")) salida = salida + parts[i];
                    else
                        salida = salida + obtenerClave(matching, Integer.parseInt(parts[i]), Integer.parseInt(parts[i + 1]));
                    i++;
                }
            }
        }  // for
        return salida;
    }


    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Ap�ndice de m�todo generado autom�ticamente
        new Matching();

    }

}
