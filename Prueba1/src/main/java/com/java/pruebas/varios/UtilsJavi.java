package com.java.pruebas.varios;

import java.io.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.Random;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.JFrame;

public class UtilsJavi {

    public static String removeExtension(File file) {
        /*
         * Get the file name of a file with no extention.
         */
        String nbFile = null;
        String s = file.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            nbFile = s.substring(0, i).toUpperCase();
        }
        return nbFile;
    }


    public static String getDateModifiedFile(File fichero) {
        long ms = fichero.lastModified();
        Date d = new Date(ms);
        Calendar c = new GregorianCalendar();
        c.setTime(d);
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString((c.get(Calendar.MONTH) + 1));
        String annio = Integer.toString(c.get(Calendar.YEAR));
        String hora = Integer.toString(c.get(Calendar.HOUR_OF_DAY));
        String minuto = Integer.toString(c.get(Calendar.MINUTE));
        String segundo = Integer.toString(c.get(Calendar.SECOND));
        return annio + "-" + mes + "-" + dia + ";" + hora + ":" + minuto + ":" + segundo;
    }


    public static int Aleatorio(int desde, int hasta) {
        //generar enteros al azar entre dos l�mites DESDE , HASTA, ambos incluidos:
        //rnd.nextInt(HASTA-DESDE+1)+DESDE
        Random rnd = new Random();
        return (int) rnd.nextInt(hasta - desde + 1) + desde;
    }

    public static String removeExtension(String file) {
        /*
         * Get the file name of a file with no extention.
         */
        String nbFile = null;
        int i = file.lastIndexOf('.');

        if (i > 0 && i < file.length() - 1) {
            nbFile = file.substring(0, i).toUpperCase();
        }
        return nbFile;
    }


    public static String getArchivo(String file) {
        /*
         * dado un path + file obiene el path.
         */
        File nbFile = new File(file);
        return nbFile.getName();
    }

    public static String getPath(File file) {
        /*
         * dado un String de una ruta de un archivo obiene el path
         */
        return file.getPath();
    }

    public static String getPath(String file) {
        /*
         * dado un String de una ruta de un archivo obiene el path
         */
        return file.substring(0, file.lastIndexOf('\\'));
    }

    public static String getFechaHora() {
        Calendar fecha = Calendar.getInstance();
        return (fecha.get(Calendar.YEAR) + "-" + fecha.get(Calendar.MONTH) + "-" +
                fecha.get(Calendar.DAY_OF_MONTH) + " " + fecha.get(Calendar.HOUR) + "." + fecha.get(Calendar.MINUTE) + "." + fecha.get(Calendar.SECOND));
    }

    public static String getFecha() {
        Calendar fecha = Calendar.getInstance();
        return (fecha.get(Calendar.YEAR) + "-" + fecha.get(Calendar.MONTH) + "-" +
                fecha.get(Calendar.DAY_OF_MONTH));
    }

    public static String getHora() {
        Calendar fecha = Calendar.getInstance();
        return (fecha.get(Calendar.HOUR) + "." + fecha.get(Calendar.MINUTE) + "." + fecha.get(Calendar.SECOND));
    }


    public static String getExtension(File file) {
        /*
         * Get the extension of a file.
         */
        String ext = null;
        String s = file.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public static String getExtension(String file) {
        /*
         * Get the extension of a file.
         */
        File nbFile = null;
        String ext = null;
        nbFile = new File(file);
        String s = nbFile.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }


    public static boolean CrearDirectorio(String directorio) {
        // Creamos el nombre de un directorio mediante un objeto File
        // El directorio raiz debe de existir -- mkdir --
        File dir = new File(directorio);

        return dir.mkdir();
        //if (dir.mkdir())
        //System.out.println("Se ha creado directorio");
        //else
        //System.out.println("No se ha podido crear el directorio");

        // El directorio raiz no tiene pq existir -- mkdirs    --
        //File directorio2 = new File("c:\\temp\\directorio\\lineadecodigo\\java_io\\hola");
        //if (directorio2.mkdirs())
        // System.out.println("Se ha creado directorio");
        //else
        // System.out.println("No se ha podido crear el directorio");

    }


    public static void print(int i) {
        System.out.println(i);
    }

    public static void print(String cadena) {
        System.out.println(cadena);
    }

    public static int isIn(String aBuscar, String dondeBuscar) {
//		  frame.setFrameIcon(new javax.swing.ImageIcon("error.gif"));
        //int index;
        return dondeBuscar.indexOf(aBuscar);
    }

    public static void dialog(String txt) {
        javax.swing.JOptionPane.showMessageDialog(null,
                txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }

    public static void dialog(java.awt.Container ventana, String txt) {
        boolean salir = false;

        java.awt.Container container = ventana;
        for (int i = 0; i < 10 && salir == false; i++) {
            if (container instanceof java.awt.Frame) {
                javax.swing.JOptionPane.showMessageDialog(((java.awt.Frame) container),
                        txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                salir = true;
            } else if (container instanceof java.awt.Dialog) {
                javax.swing.JOptionPane.showMessageDialog(((java.awt.Dialog) container),
                        txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                salir = true;
            }
            container = container.getParent();
        }
        if (salir) return;
        else javax.swing.JOptionPane.showMessageDialog(null,
                txt, "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }

    public static File seleccionarFichero() {
        //Selecciona ficheros y directorios
        javax.swing.JFileChooser selectorArchivo = new javax.swing.JFileChooser();
        selectorArchivo.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        File file = null;
        if (selectorArchivo.showDialog(null, "Seleccionar") == javax.swing.JFileChooser.APPROVE_OPTION) {
            file = selectorArchivo.getSelectedFile();
        }
        return file;
    }

    public static File seleccionarFichero(String path, String extension, String extensionDescripcion, int directorioArchivo) {
        // NO se le puede introducir un texto que muestre en el titulo
        //Se puede seleccionar el tipo de fichero:
        //	0 ficheros y directorios
        //	1 directorios solo
        //	2 ficheros solo
        if (!Optional.ofNullable(path).isPresent() || Optional.of(path).isEmpty()) {
            path = System.getProperties().getProperty("user.home");
        }
        JFrame frame = new JFrame("Show Message Dialog");
        frame.setAlwaysOnTop(true);

        javax.swing.JFileChooser chooser = new javax.swing.JFileChooser(path);

        if (!extensionDescripcion.equals("") && !extension.equals("")) {
            javax.swing.filechooser.FileNameExtensionFilter filter = new javax.swing.filechooser.FileNameExtensionFilter(extensionDescripcion, extension);
            chooser.addChoosableFileFilter(filter);
        }

        chooser.setDialogTitle("Seleccion de directorio/archivo a tratar");
        chooser.setApproveButtonText("Seleccionar");

        if (directorioArchivo == 0) chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        if (directorioArchivo == 1) chooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
        if (directorioArchivo == 2) chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);

        //Para establecer el directorio por defecto
        chooser.setCurrentDirectory(new File(path));

        int xrVal = chooser.showDialog(frame, "Procesar");

        if (xrVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            return new File(chooser.getCurrentDirectory().toString() +
                    System.getProperty("file.separator") +
                    chooser.getSelectedFile().getName());
            //JOptionPane.showMessageDialog(frame,fileXML); // Muestra un dialogo
        }
        return null;
    }

    public static File seleccionarFicheroDesc(String path, String extension, String extensionDescripcion, int directorioArchivo, String texto) {
        //se le puede introducir un texto que muestre en el titulo
        //Se puede seleccionar el tipo de fichero:
        //	0 ficheros y directorios
        //	1 directorios solo
        //	2 ficheros solo
        JFrame frame;
        frame = new JFrame("Show Message Dialog");

        javax.swing.JFileChooser chooser =
                new javax.swing.JFileChooser("C:");

        //Esto era antes ahora con el 1.6 ya no hace falta
        //FilterJavi filter = new FilterJavi();
        //filter.setDescription(extensionDescripcion);   //ej: "*.xml (Extensible Markup Language)"
        ///filter.add(extension);  //ej: "xml"

        if (!extensionDescripcion.equals("") && !extension.equals("")) {
            javax.swing.filechooser.FileNameExtensionFilter filter = new javax.swing.filechooser.FileNameExtensionFilter(extensionDescripcion, extension);
            chooser.addChoosableFileFilter(filter);
        }

        chooser.setDialogTitle(texto);
        chooser.setApproveButtonText("Seleccionar");

        if (directorioArchivo == 0) chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        if (directorioArchivo == 1) chooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
        if (directorioArchivo == 2) chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);

        //Para establecer el directorio por defecto
        chooser.setCurrentDirectory(new File(path));

        int xrVal = chooser.showDialog(frame, "Procesar");

        if (xrVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            return new File(chooser.getCurrentDirectory().toString() +
                    System.getProperty("file.separator") +
                    chooser.getSelectedFile().getName());
            //JOptionPane.showMessageDialog(frame,fileXML); // Muestra un dialogo
        }
        return null;
    }

    public static boolean isNumber1(String string) {
        try {
            Long.parseLong(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean isNumber2(String string) {
        return string.matches("[0-9]*");
    }


    public static void ejecucionComando(String comando) {
        try {
            Process process = Runtime.getRuntime().exec(comando);
            //esto si queremos ver el resultado de lo que devuelve el comando de ejecucion
            InputStream inputstream = process.getInputStream();
            BufferedInputStream bufferedinputstream = new BufferedInputStream(inputstream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void copyFile(String fileFrom, String dirTo) {
        try {
            String comando = "cmd /c xcopy \"" + fileFrom + "\" \"" + dirTo + "\\\"";
            Process process = Runtime.getRuntime().exec(comando);

            //esto si queremos ver el resultado de lo que devuelve el comando de ejecucion
            InputStream inputstream = process.getInputStream();
            BufferedInputStream bufferedinputstream = new BufferedInputStream(inputstream);


//	        java.io.BufferedReader cmd_input = 
//	        new java.io.BufferedReader(new java.io.InputStreamReader(process.getInputStream()));
//	        while ((output = cmd_input.readLine()) != null) {
//	            System.out.println(output);
//	        }
//	        cmd_input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void escribirLog(String rutaArchivo, String mensaje) {

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {
            fh = new FileHandler(rutaArchivo, true);
            logger.addHandler(fh);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info(mensaje);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //este hay que quitarle hasta que se ponga un bbuen log. es para que no peten los pgms qe lo usaban
    public static void escribirLog(String rutaArchivo, String mensaje, int a, int b) {

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {
            fh = new FileHandler(rutaArchivo, true);
            logger.addHandler(fh);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info(mensaje);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //este hay que quitarle hasta que se ponga un bbuen log. es para que no peten los pgms qe lo usaban
    public static void escribirLog(String mensaje, int a, int b) {

		/*
		SEVERE
		WARNING
		INFO
		CONFIG
		FINE
		FINER
		FINEST
	*/

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {

            fh = new FileHandler("c:\\Datos", true);
            logger.addHandler(fh);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info(mensaje);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void burbuja(int[] A) {
        int i, j, aux;
        for (i = 0; i < A.length - 1; i++)
            for (j = 0; j < A.length - i - 1; j++)
                if (A[j + 1] < A[j]) {
                    aux = A[j + 1];
                    A[j + 1] = A[j];
                    A[j] = aux;
                }
    }


    //Ordenacion QuickSort *******************************************************
    public static void rapido(int[] v) {
        rapirec(v, 0, v.length - 1);
    }

    private static void rapirec(int[] v, int iz, int de) {
        int m;
        if (de > iz) {
            m = particion(v, iz, de);
            rapirec(v, iz, m - 1);
            rapirec(v, m + 1, de);
        }
    }

    private static int particion(int[] v, int iz, int de) {
        int i, pivote;
        permuta(v, (iz + de) / 2, iz);
        // el pivote es el de centro y se cambia con el primero
        pivote = v[iz];
        i = iz;
        for (int s = iz + 1; s <= de; s++)
            if (v[s] <= pivote) {
                i++;
                permuta(v, i, s);
            }
        permuta(v, iz, i);// se restituye el pivote donde debe estar
        return i; // retorna la posicion en que queda el pivote
    }

    private static void permuta(int[] a, int i, int j) {
        int t;
        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
    //@Test
    //public void testSeleccion() {
    //    int[] comprueba = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    //    int[] ordenados = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    //    int[] inverso = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
    //    int[] random = { 1, 0, 4, 3, 5, 10, 8, 9, 2, 6, 7 };
    //    o.seleccion(ordenados);
    //    assertArrayEquals(comprueba, ordenados);
    //    o.seleccion(inverso);
    //    assertArrayEquals(comprueba, inverso);
    //    o.seleccion(random);
    //    assertArrayEquals(comprueba, random);
    //}
    //FIN Ordenacion QuickSort *******************************************************


    public static void main(String[] args) {
        //UtilsJavi.dialog("" + UtilsJavi.isIn("Hola", "Hola  javier diaz"));

    }
}
