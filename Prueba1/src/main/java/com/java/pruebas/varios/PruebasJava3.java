package com.java.pruebas.varios;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PruebasJava3 {
    public static final String DATE_TIME_FORMAT_YYYYMMDD = "yyyyMMdd";

    public PruebasJava3() {
        LocalDate localDate = LocalDate.parse(LocalDate.now().toString(),
                DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDD));
        System.out.println("Fecha: " + localDate.toString());
    }

    public static void main(String[] args) {
        new PruebasJava3();
        System.out.println("fffff");
    }
}
