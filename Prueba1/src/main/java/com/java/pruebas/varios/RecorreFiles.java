package com.java.pruebas.varios;

//Recorre todos los directorios y saca un listado de objetos con la siguiente info
// ruta y nombre + "file"/"dir" + length + DateModifiedFile + lastModified

import java.io.File;
import java.io.FileWriter;
import java.util.Objects;

public class RecorreFiles {

    private String nbPGM = "RecorreFiles";    // lo deja en C:\DATOS
    private String path = "C:\\DATOS\\";
    private int nivelLog = 5; // 0 no sale nada, 5 sale todo;
    private int nivelASacar = 0;   //no lo esta utilizando
    private File fileSeleccionado;
    private long totalDir;
    private String line;
    private String nbResultado = nbPGM + ".txt";
    private String resultadoEn = path + nbResultado;
    private FileWriter fichero = null;
    static long totalGeneral = 0L;
    static long totalCarpetas = 0L;
    static long totalFicheros = 0L;


    public RecorreFiles(CreaFile salida) {
        fileSeleccionado = UtilsJavi.seleccionarFichero(path, "", "", 1);
        if (Objects.isNull(fileSeleccionado))
            return;
        Procesar(fileSeleccionado.getAbsolutePath(), salida);
    }

    public RecorreFiles(String directorio, CreaFile salida) {
        Procesar(directorio, salida);
    }

    void Procesar(String directorio, CreaFile salida) {

        File path = new File(directorio);
        String[] list;
        list = path.list();
        Boolean sacarResultadoAFichero = true;

        if (salida == null)
            sacarResultadoAFichero = false;

        for (int i = 0; i < list.length; i++) {
            File recurso = new File(path.getAbsolutePath() + "\\" + list[i]);
            if (recurso.isDirectory()) {
                line = path.getAbsolutePath() + "\\" + list[i] + ";" + list[i] + ";" + "dir" + ";" + recurso.length() + ";" +
                        UtilsJavi.getDateModifiedFile(recurso) + ";" + recurso.lastModified();
                if (sacarResultadoAFichero) {
                    salida.writeLine(line);
                }
                totalDir = 0;
                totalDir += recurso.length();
                totalCarpetas++;
                new RecorreFiles(path.getAbsolutePath() + "\\" + list[i], salida);
            }
            if (recurso.isFile()) {
                line = path.getAbsolutePath() + "\\" + list[i] + ";" + list[i] + ";" + "file" + ";" + recurso.length() + ";" +
                        UtilsJavi.getDateModifiedFile(recurso) + ";" + recurso.lastModified() + " fin";
                if (sacarResultadoAFichero) {
                    salida.writeLine(line);
                }
                totalDir += recurso.length();
                totalGeneral += recurso.length();
                totalFicheros++;
            } //if (f1.isFile()) {
            System.out.println(list[i]);
        } //	for(int i = 0; i < list.length; i++) {

    } //public procesar

    public long getTamagno() {
        return totalGeneral;
    }

    public long getCarpetas() {
        return totalCarpetas;
    }

    public long getFicheros() {
        return totalFicheros;
    }

    public static void main(String[] args) {
        //CreaFile salida = new CreaFile("C:\\DATOS\\Ficheros sacar 20200206.txt");
        //RecorreFiles recorreFiles = new RecorreFiles(salida);

        RecorreFiles recorreFiles = new RecorreFiles(null);
        //salida.cierraFile();
        System.out.println("Proceso terminado.");
        //System.out.println("salida en " + salida.getPath());
        System.out.println("Carpetas: " + recorreFiles.getCarpetas() + " Archivos: " + recorreFiles.getFicheros());
        System.out.println("Tamaño: " + recorreFiles.getTamagno() + " / " +
                recorreFiles.getTamagno() / 1024 + " Kb / " + recorreFiles.getTamagno() / 1048576 + " Mb / " +
                recorreFiles.getTamagno() / 1073741824 + " Gb");
    }

}
