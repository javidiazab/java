package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;

// pgm que lee un txt con un pgm cobol y extrae las selects

public class LeerTI {

    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    private String directorio;
    FileSys fileSys = null;
    int index = 0;
    boolean EOF = false;
    String dirResultadosTI = "\\Resultados TI";
    String fechaS = UtilsJavi.getFechaHora();
    String[] mes = new String[200];
    String[] e = new String[200];
    String[] s = new String[200];
    String[] o = new String[200];
    String[] r = new String[200];

    public LeerTI(String directorio) {
        ProcesarTI(directorio);
    }

    public LeerTI() {
        File dirSeleccionado = UtilsJavi.seleccionarFichero(
                "C:\\",
                "", "", 1);
        if (Objects.isNull(dirSeleccionado))
            return;
        ProcesarTI(dirSeleccionado.getAbsolutePath());
    }

    public void ProcesarTI(String directorio) {
        this.directorio = directorio;
        File path = new File(directorio);
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultadosTI);

        for (int i = 0; i < list.length; i++) {
            File fileTRX = new File(directorio + "\\" + list[i]);
            if (fileTRX.isFile()) {
                try {
                    //Inicializar();
                    fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        procesaTI(line, UtilsJavi.removeExtension(list[i]) + ".txt");
                        if (line != null)
                            line = fileSys.readLine();
                    } //while (line != null);
                } catch (IOException exception) {
                }
                ;

            } //if (f1.isFile()) {
            System.out.println("Proceso Terminado Correctamente");
        } //	for(int i = 0; i < list.length; i++) {
    } //public RenameFiles (String directorio) {


    public void procesaTI(String line, String pgm) {
        //UtilsJavi.print("line; " + line);
        //genera fichero con Pantallas. Lo hacemos aqui porque todavia no se ha cambiado nada
        if (line.contains(" MES-")) {
            UtilsJavi.print(line);
            if (line.contains(" MOVE ")) {
                CreaFileStatic.log(directorio + dirResultadosTI + "\\" + pgm, line);
                // 	  {

                //CreaFileStatic.log(directorio + dirResultadosTI + "\\"  + pgm, line.substring(7,72));
                //		 CreaFileStatic.log(directorio + dirResultadosTI + "\\"  + pgm, "");
                //		 CreaFileStatic.log(directorio + dirResultadosTI + "\\"  + pgm, "");
            } else {
                CreaFileStatic.log(directorio + dirResultadosTI + "\\" + pgm, "no Move" + line);
            } //if ((line.contains(" MOVE ") {

        } //if (line.contains(" MES- ")) {
    } //procesaTI(String line, String pgm) {

    public void Inicializar() {
        for (int i = 0; i < 200; i++) {
            mes[i] = "";
            e[i] = "";
            s[i] = "";
            o[i] = "";
            r[i] = "";
        }
    }

    public static void main(String[] args) {
        new LeerTI();
    }
}
