/*
 * Creado el 26-ago-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;
import javax.swing.*;

public class LeerXML {
    //Modificaion desde 1
    //Si queremos un determinado valor de una etiqueta llamamos de la siguiente manera:
    //LeerXML x = new LeerXML("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201\\prueba.xml", 1, "<valorAConsultar>");
    //valorRequerido = x.fileXML.toString();

    //Podiamos exportar un logico en ROSE y del XML generado explotarlo con este pgm

    private String nbPGM = "LeerXML";
    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    File fileXML = null;
    String ValorSolicitado = "";


    public LeerXML() {
        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);
        int nivelASacar = 0;

        File XMLSeleccionado = UtilsJavi.seleccionarFichero("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201", "xml", "*.xml (Extensible Markup Language)", 2);
        if (Objects.isNull(XMLSeleccionado))
            return;

        String nivelIntroducido = JOptionPane.showInputDialog("Nivel de datos a sacar");
        if (!nivelIntroducido.equals("") && nivelIntroducido != null)
            nivelASacar = Integer.parseInt(nivelIntroducido);

        String soloEtiqueta = JOptionPane.showInputDialog("Obtener solo etiquetas con nombre ...");

        UtilsJavi.dialog(ProcesaXML(XMLSeleccionado.getAbsolutePath(), nivelASacar, soloEtiqueta, true));
    }

    public LeerXML(String fileXMLATratar, int nivelDesdeDondeSacar, String soloEtiquetaConNombre) {
        UtilsJavi.escribirLog(fileXMLATratar + ", " + nivelDesdeDondeSacar + ", " + soloEtiquetaConNombre, 1, nivelLog);
        ProcesaXML(fileXMLATratar, nivelDesdeDondeSacar, soloEtiquetaConNombre, true);
    }

    public LeerXML(String fileXMLATratar, int nivelDesdeDondeSacar, String soloEtiquetaConNombre, Boolean sacarAFichero) {
        // sacarAFichero hace que saque o no por fichero, ya que si queremos obtener solo un valor no queremos ficheros
        UtilsJavi.escribirLog(fileXMLATratar + ", " + nivelDesdeDondeSacar + ", " + soloEtiquetaConNombre, 1, nivelLog);
        ProcesaXML(fileXMLATratar, nivelDesdeDondeSacar, soloEtiquetaConNombre, sacarAFichero);
    }

    public LeerXML(String fileXMLATratar) {
        UtilsJavi.escribirLog(fileXMLATratar, 1, nivelLog);
        ProcesaXML(fileXMLATratar, 0, "", true);
    }


    String ProcesaXML(String fileXMLATratar, int nivelDesdeDondeSacar, String soloEtiquetaConNombre, Boolean sacarAFichero) {
        // sacarAFichero hace que saque o no por fichero, ya que si queremos obtener solo un valor no queremos ficheros
        UtilsJavi.escribirLog(fileXMLATratar + ", " + nivelDesdeDondeSacar + ", " + soloEtiquetaConNombre, 1, nivelLog);

        String fileTODO = " TODO " + UtilsJavi.getFechaHora() + ".txt";

        fileXML = new File(fileXMLATratar);

        String line = null;
        char[] charLine = null;

        if ((fileXML).exists()) {
            try {
                String datoEtiqueta = "";
                String datoValor = "";
                boolean valor = false;
                boolean esDato = false;
                boolean esEtiqueta = false;
                boolean etiquetaInicio = false;
                boolean etiquetaFin = false;
                boolean etiqVacia = false;
                boolean XMLFormat = false;
                boolean comentario = false;
                FileSys fileSys = new FileSys(fileXML.toString());
                String[][] cposAbiertos = new String[500][2];
                int j = 0;
                line = fileSys.readLine();  //Lee la primera vez del XML

                while (line != null) { //Mientras no sea fin de fichero XML
                    charLine = line.toCharArray();
                    for (int i = 0; i < charLine.length; i++) { //recorremos la linea leida
                        //lineDebug = lineDebug + charLine[i];
                        if (charLine[i] == '<') {
                            if (datoValor != "" && j > 0 && datoValor != null) {
                                cposAbiertos[j - 1][1] = datoValor.trim();
                                UtilsJavi.escribirLog("datoValor: " + datoValor, 3, nivelLog); //System.out.println("datoValor: " + datoValor);
                            }
                            datoValor = "";
                            datoEtiqueta = "";
                            XMLFormat = false;
                            etiquetaFin = false;  // '>'
                            etiquetaInicio = true; // '>'
                            etiqVacia = false;
                        }  // '/>'

                        if ((charLine[i] == '?' && charLine[i - 1] == '<') ||
                                (charLine[i] == '?' && charLine[i - 1] == '>'))
                            XMLFormat = true;

                        //if ((charLine[i] == '!' && charLine[i-1] == '-' && charLine[i-2] == '-') ||
                        //	(charLine[i] == '-' && charLine[i-1] == '-' && charLine[i-2] == '>'))
                        //	comentario = true;

                        if ((charLine[i] != '>' && charLine[i] != '<' &&
                                !(charLine[i] == '?' && XMLFormat)))
                            if (charLine[i] == '/' && etiquetaInicio) {
                                if (charLine[i + 1] == '>') etiqVacia = true;
                                esDato = false;
                            } else
                                esDato = true;
                        else
                            esDato = false;

                        if (esDato && etiquetaInicio)
                            esEtiqueta = true;
                        else esEtiqueta = false;

                        if (charLine[i] == '>' && !valor) {
                            UtilsJavi.escribirLog("datoEtiqueta: " + datoEtiqueta, 3, nivelLog); //System.out.println("datoEtiqueta: " + datoEtiqueta);
                            etiquetaFin = true;
                            etiquetaInicio = false;
                        }

                        if (esDato && etiquetaFin) valor = true;
                        else valor = false;

                        if (esEtiqueta) {
                            datoEtiqueta = datoEtiqueta + charLine[i];
                        }
                        if (valor) {
                            datoValor = datoValor + charLine[i];
                        }
//***************************************************************************
//********************* Control de etiquetas abiertas y cerradas
                        if (etiquetaFin && !esDato && !XMLFormat && !etiqVacia) {
                            String etiquetaProcesada = procesarAtributos(datoEtiqueta);
                            String nombreEtiqueta = etiquetaProcesada.substring(0, etiquetaProcesada.indexOf(';'));
                            //String datosEtiqueta = etiquetaProcesada.substring(1 + etiquetaProcesada.indexOf(';'));
                            if (j == 0) {
                                UtilsJavi.escribirLog("A�adimos " + j + ":" + nombreEtiqueta, 3, nivelLog); //System.out.println("A�adimos " + j +":" + nombreEtiqueta);
                                cposAbiertos[j][0] = nombreEtiqueta;
                                j++;
                                //cposAbiertos[j++][1] = datosEtiqueta;
                            } else {
                                if (!cposAbiertos[j - 1][0].equals(nombreEtiqueta)) {
                                    UtilsJavi.escribirLog("A�adimos " + j + ":" + nombreEtiqueta, 3, nivelLog); //System.out.println("A�adimos " + j +":" + nombreEtiqueta);
                                    cposAbiertos[j][0] = nombreEtiqueta;
                                    j++;
                                    //cposAbiertos[j++][1] = datosEtiqueta;
                                } else {
                                    j--;
                                    UtilsJavi.escribirLog("Eliminamos " + j + ":" + nombreEtiqueta + "=" + cposAbiertos[j][0], 3, nivelLog); //System.out.println("Eliminamos " + j + ":" + nombreEtiqueta + "=" + cposAbiertos[j][0]);
                                    if (j >= nivelDesdeDondeSacar) {
                                        if (soloEtiquetaConNombre.equals(cposAbiertos[j][0]) || soloEtiquetaConNombre.equals("")) {
                                            String datos = UneDatos(nivelDesdeDondeSacar, j, cposAbiertos);
                                            if (sacarAFichero) CreaFileStatic.log(fileXML.toString() + fileTODO, datos);
                                            if (soloEtiquetaConNombre.equals(cposAbiertos[j][0]))
                                                return ValorSolicitado = cposAbiertos[j][1];
                                        } //if (soloEtiquetaConNombre.equals(cposAbiertos[j][0]) || soloEtiquetaConNombre.equals("")) {
                                    } //if (j >= nivelDesdeDondeSacar) {
                                    cposAbiertos[j][0] = "";
                                    cposAbiertos[j][1] = "";
                                } //if (soloEtiqueta.equals(cposAbiertos[j][0])) {
                            } //if (j == 0) {
                        }  //if (etiquetaFin && !dato && !XMLFormat) {
                        if (etiqVacia && etiquetaFin) {
                            String etiquetaProcesada = procesarAtributos(datoEtiqueta);
                            String nombreEtiqueta = etiquetaProcesada.substring(0, etiquetaProcesada.indexOf(';'));
                            String datosEtiqueta = etiquetaProcesada.substring(1 + etiquetaProcesada.indexOf(';'));
                            //if (cposAbiertos[j][0] != nombreEtiqueta) {
                            UtilsJavi.escribirLog("Ni Eliminamos ni anadimos " + j + ":" + nombreEtiqueta, 3, nivelLog); //System.out.println("Ni Eliminamos ni anadimos " + j + ":" + nombreEtiqueta);
                            etiqVacia = false;
                            if (j >= nivelDesdeDondeSacar) {
                                if (soloEtiquetaConNombre.equals(cposAbiertos[j][0]) || soloEtiquetaConNombre.equals("")) {
                                    String datos = UneDatos(nivelDesdeDondeSacar, j - 1, cposAbiertos) + nombreEtiqueta;
                                    if (sacarAFichero)
                                        CreaFileStatic.log(fileXML.toString() + fileTODO, datos + ';' + datosEtiqueta);
                                } //if (soloEtiqueta.equals(cposAbiertos[j][0])) {

                            } //if (j >= nivelASacar) {
                            //} //if (cposAbiertos[j][0] == datoEtiqueta) {
                        } //if (etiqVacia) {

                        if (etiquetaFin && XMLFormat)
                            XMLFormat = false;

                        if (etiquetaFin && comentario)
                            comentario = false;
//******************************************************************
                    } //for (int i=0; i < charLine.length; i++) {
                    line = fileSys.readLine(); //Lee siguiente linea del fichero
                }
                ;  //while (line != null)
                //UtilsJavi.dialog("Resultado de la ejecuci�n: " + j + "\n" + "Salida: " + fileXML.toString() + fileTODO);
            } catch (IOException exception) {
            }
            ;
        } //if ((new File(fileXML)).exists()) {
        return fileXML.toString() + fileTODO;
    } //public ProcesaXMLSinFichero() {


    String procesarAtributos(String datoEtiqueta) {
        //Procesa los atributos de una etiqueta (cuando tiene varios ej: <EstadoSelector nombre="cajaOcta" id="49476E7500BB" posicionX="878" posicionY="1524" discriminador="cajaOcta">
        String cadena = "";
        String nombreEtiqueta = "";
        String XLSdatoEtiqueta = "";

        datoEtiqueta = datoEtiqueta.trim();
        datoEtiqueta = datoEtiqueta.replace(';', ',');

        for (; datoEtiqueta.indexOf("=\"") > -1; ) {  //comprobamos si tiene ="
            cadena = datoEtiqueta.substring(0, datoEtiqueta.indexOf("=\"")).trim();
            if (cadena.indexOf(' ') > -1) { //comprobamos si hay dos campos ej: 'AsociacionEvento nombreEvento="cta"'
                if (nombreEtiqueta == "") {
                    nombreEtiqueta = cadena.substring(0, cadena.indexOf(' ')).trim();
                    XLSdatoEtiqueta = XLSdatoEtiqueta + nombreEtiqueta;
                } else {
                    UtilsJavi.escribirLog("Error de etiqueta: " + datoEtiqueta, 3, nivelLog); //UtilsJavi.print("Error de etiqueta: " + datoEtiqueta);
                } //if (nombreEtiqueta == "") {
                datoEtiqueta = datoEtiqueta.substring(1 + datoEtiqueta.indexOf(' ')).trim();
            } //if (cadena.indexOf(' ') > -1)

            if (nombreEtiqueta == "")
                nombreEtiqueta = datoEtiqueta.substring(0, datoEtiqueta.indexOf("=\"")).trim();

            XLSdatoEtiqueta = XLSdatoEtiqueta + ';' + datoEtiqueta.substring(0, datoEtiqueta.indexOf("=\""));
            datoEtiqueta = datoEtiqueta.substring(2 + datoEtiqueta.indexOf("=\"")).trim();
            XLSdatoEtiqueta = XLSdatoEtiqueta + ';' + datoEtiqueta.substring(0, datoEtiqueta.indexOf("\""));
            datoEtiqueta = datoEtiqueta.substring(1 + datoEtiqueta.indexOf("\"")).trim();
        } //for (;datoEtiqueta.indexOf("=\"") > -1;) {

        for (; datoEtiqueta.indexOf("=\'") > -1; ) {  //comprobamos si tiene ="
            cadena = datoEtiqueta.substring(0, datoEtiqueta.indexOf("=\'")).trim();
            if (cadena.indexOf(' ') > -1) { //comprobamos si hay dos campos ej: 'AsociacionEvento nombreEvento="cta"'
                if (nombreEtiqueta == "") {
                    nombreEtiqueta = cadena.substring(0, cadena.indexOf(' ')).trim();
                    XLSdatoEtiqueta = XLSdatoEtiqueta + nombreEtiqueta;
                } else {
                    UtilsJavi.escribirLog("Error de etiqueta: " + datoEtiqueta, 3, nivelLog); //UtilsJavi.print("Error de etiqueta: " + datoEtiqueta);
                } //if (nombreEtiqueta == "") {
                datoEtiqueta = datoEtiqueta.substring(1 + datoEtiqueta.indexOf(' ')).trim();
            } //if (cadena.indexOf(' ') > -1)

            if (nombreEtiqueta == "")
                nombreEtiqueta = datoEtiqueta.substring(0, datoEtiqueta.indexOf("=\'")).trim();

            XLSdatoEtiqueta = XLSdatoEtiqueta + ';' + datoEtiqueta.substring(0, datoEtiqueta.indexOf("=\'"));
            datoEtiqueta = datoEtiqueta.substring(2 + datoEtiqueta.indexOf("=\'")).trim();
            XLSdatoEtiqueta = XLSdatoEtiqueta + ';' + datoEtiqueta.substring(0, datoEtiqueta.indexOf("\'"));
            datoEtiqueta = datoEtiqueta.substring(1 + datoEtiqueta.indexOf("\'")).trim();
        } //for (;datoEtiqueta.indexOf("=\"") > -1;) {
        //UtilsJavi.print("xls: " + XLSdatoEtiqueta);

        if (nombreEtiqueta == "")
            return datoEtiqueta + ';';
        else
            return XLSdatoEtiqueta + ';';
    }

    public String UneDatos(int inicio, int fin, String[][] cposAbiertos) {
        String cadena = "";
        for (; inicio <= fin; inicio++) {
            cadena = cadena + cposAbiertos[inicio][0] + ';' + (cposAbiertos[inicio][1] != null ? cposAbiertos[inicio][1] : "");
        }
        return cadena;
    }


    //<?xml
    public static void main(String[] args) {
        //new LeerXML("D:\\wsad_workspace_S1\\javi\\Paquete_Bhtcs_Default.xml");
        //new LeerXML("C:\\DATOS\\_JAVI\\javi SRC\\prueba2.xml");
        //new LeerXML("C:\\DATOS\\WORKSPACE_P1\\MODELO_SANT_ENS\\build.xml"); //---> Tiene comentarios
        //new LeerXML("C:\\DATOS\\_JAVI\\_ISBAN\\LAB Blanqueo\\Nuevos Desarrollos LAB\\Pendiente\\200805 - Migracion Blanca I BKS\\ML\\BlancaIv10.xml");
        new LeerXML();
        //new LeerXML("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201\\prueba2.xml");
        //new LeerXML("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201\\prueba.xml", 1, "");
        //new LeerXML("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201\\prueba.xml", 1, "name");

        //LeerXML x = new LeerXML("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201\\prueba.xml", 0, "size", false); //false porque no queremos fichero, solo que devuelva el dato requerido
        //UtilsJavi.print("esto es lo que busco -> " + x.ValorSolicitado);  //--> crear nbFile que devuelva el nb del file

        //LeerXML a = new LeerXML("C:\\DATOS\\_Javi\\z_Javi\\Pgms Java\\Java\\Java 201201\\book.xml", 0, "", true); //false porque no queremos fichero, solo que devuelva el dato requerido
        //UtilsJavi.print("esto es lo que busco -> " + a.ValorSolicitado);  //--> crear nbFile que devuelva el nb del file
    }
}

