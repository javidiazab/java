/*
 * Creado el 11-ago-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * Programa de prueba para probar cosas
 */
package com.java.pruebas.varios;

import java.util.*;
import java.util.Random;

import javax.swing.JOptionPane;

public class Programa {

    private void calculaDiasEntreFechas() {
        String fechaDesde = JOptionPane.showInputDialog("Fecha Desde");
        if (fechaDesde != "" && fechaDesde != null)
            System.out.println("Error");

        String fechaHasta = JOptionPane.showInputDialog("Fecha Hasta");
        if (fechaHasta != "" && fechaHasta != null)
            System.out.println("Error");

        char[] fecha1 = fechaDesde.toCharArray();
        char[] fecha2 = fechaHasta.toCharArray();

        //Creo las dos instancias de fecha
        GregorianCalendar gc = new GregorianCalendar(2000, 11, 20);
        GregorianCalendar gc1 = new GregorianCalendar(2000, 11, 25);
        //Obtengo los objetos Date para cada una de ellas
        Date fec1 = gc.getTime();
        Date fec2 = gc1.getTime();
        //Realizo la operaci�n
        long time = fec2.getTime() - fec1.getTime();
        //Muestro el resultado en d�as
        System.out.println(time / (3600 * 24 * 1000));
    }

    public static void main(String[] args) {
        CreaFile creaFile = new CreaFile(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "aprueba.txt");
        UtilsJavi.print(System.getProperty("user.home"));
        UtilsJavi.print(System.getProperty("java.io.tmpdir"));
        UtilsJavi.print(System.getProperty("java.library.path"));
        UtilsJavi.print(System.getProperty("user.dir"));
        //Genera numeros aleatorios con math
        for (int i = 1; i <= 5; i++)
            System.out.println((int) (Math.random() * 6 + 1));

        //Genera numeros aleatorios. mas flexible (de 0 a 7)
        Random rnd = new Random();
        for (int i = 1; i <= 5; i++)
            System.out.println(rnd.nextInt(7));

        //generar enteros al azar entre dos l�mites DESDE , HASTA, ambos incluidos:
        //rnd.nextInt(HASTA-DESDE+1)+DESDE
        rnd = new Random();
        for (int i = 1; i <= 5; i++)
            System.out.println(rnd.nextInt(20 - 10 + 1) + 10);

    }
}
