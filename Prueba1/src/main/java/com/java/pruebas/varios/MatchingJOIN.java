package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;
import javax.swing.*;

//Detalles de como funciona JOIN de HOST al final de la clase

//Este pgm se deberia de hacer tb en VB

//pendiente:
//ObtenemosSalida();    // Este metodo devuelve un array con los elementos a montar en la salida
//Hacer una pantalla de formulario donde se elijan todos los datos a la vez. O un fichero que lo haga.
//permitir que en formato de salida salgan todas los ficheros que se indiquen pe: SALF1: 
//en las claves por posiciones que se permita que se pongan varias posiciones y con varias longitudes
//incluir condiciones de salida (para que saque en las salidas por fichero unos datos u otros
//Hacer que la primera vez se elija la ruta pero que las siguientes veces la recuerde
//hacer que sace 1:1 o 1:n
//que valide que el formato de la salida es correcto
//que valide que si las posiciones del formato de salida se salen del fichero que casque


//Matching de dos ficheros
//Este matching funciona como un JOINKEYS de HOST 3270
//si en la longitud de la clave se introduce un ';' u otro caracter no toma longitud sino hasta que encuentre el caracter introducido


//ERROR 05: Se ha especificado una columna en la salida que no esta en la entrada

public class MatchingJOIN {
    //Como mejora que valide la entrada del formato del fichero de salida

    private String tipoMatching = "";
    private String driverClave = "";
    private String matchingClave = "";
    private String driverCaracterDivision = "";
    //private String driverClaveColumnas = "";
    private String matchingCaracterDivision = "";
    private String lineMatching = null;
    //private String matchingClaveColumnas = "";
    private String lineDriver = null;
    private String lineMatchingAnt = null;
    private String lineDriverAnt = null;
    private String fmtoSalida = null;
    //para que ignore o no las mayusculas en la comparacion de la clave
    private boolean ignoreCase = true;
    private String maxValor = "}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}";
    private File fileSeleccionado;
    private String fechaS = UtilsJavi.getFechaHora();
    private String FileMatchingIguales = "FileMatchingIguales " + fechaS + ".txt";
    private String FileMatchingDriver = "FileMatchingDriver " + fechaS + ".txt";
    private String FileMatchingMatchi = "FileMatchingMatchi " + fechaS + ".txt";
    private String FileMatchingDistint = "FileMatchingDistint " + fechaS + ".txt";
    private int nivelLog = 5; //0 no sale nada, 5 sale todo;
    private int nivelLogEstablecido = 0; // igual o mayor que este lo saca por el log
    private FileSys matching = null;
    private FileSys driver = null;
    private String claveDriver = "";
    private String claveMatching = "";
    private String claveDriverAnt = "";
    private String claveMatchingAnt = "";
    private String claveDriverGrabado = "";
    private String claveMatchingGrabado = "";
    private boolean paired = false;
    private boolean unpaired = false;
    private boolean f1 = false;
    private boolean f2 = false;
    private boolean only = false;
    boolean procesandoFicheros = false;
    private File fileDriver;
    private File fileMatching;
    private String nbPGM = "Matching";
    private String dirResultados = "\\Resultados " + nbPGM;
    private String directorio;

    public MatchingJOIN() {
        //MODO:
        //0=SOLO LOS QUE CONCIDAN EN EL DRIVER Y EN EL MATCHING (n)
        //1=TODO EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING (n)
        //2=SOLO LOS QUE CONCIDAN EN EL DRIVER Y EN EL MATCHING (solo 1)
        //3=TODO EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING (solo 1)
        UtilsJavi.escribirLog("\n**************************\n*\n*\n" + nbPGM + "() nivelLog=" + "nivelLog", 1, nivelLogEstablecido);
        //UtilsJavi.escribirLog(fileXMLATratar + ", " + nivelDesdeDondeSacar + ", " + soloEtiquetaConNombre, 1, nivelLog);
        //***************************************************************************************************
        //Pruebas
        //driverCaracterDivision = "";
        //matchingCaracterDivision = ";";
        //salida ("D,1,2,3,M,1,2,3", "1;2;3", "4;5;6");
        //salida ("D,1,2,4,2,M,2,2", "123456", "789");  //resultado 124589
        //salida ("D,1,2,4,2,M,2,3", "123456", "7;8;9");  //resultado 1245;8;9

        ObtenemosTipoMatching();
        ObtenemosDriver();
        ObtenemosMatching();
        fmtoSalida = ObtenemosFmtSalida();    // Este metodo devuelve una cadena con los elementos a montar en la salida

        //Creamos directorio resultado en el directorio donde esta el fichero driver y comenzamos lectura
        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        driver = abrirFich(fileDriver.toString(), driverCaracterDivision, driverClave);
        lineDriverAnt = lineDriver;  //????????????????????? quitar de aqui.
        lineDriver = leerFich(driver);
        claveDriver = driver.getClave();


        matching = abrirFich(fileMatching.toString(), matchingCaracterDivision, matchingClave);
        lineMatchingAnt = lineMatching;  //????????????????????? quitar de aqui.
        lineMatching = leerFich(matching);
        claveMatching = matching.getClave();

        while (lineDriver != null || lineMatching != null) { //Mientras no sea fin de ficheros
            proceso();
        }


        UtilsJavi.escribirLog("Proceso Terminado Correctamente", 1, nivelLog);
        UtilsJavi.dialog("Resultados: " + directorio + dirResultados + "\\" + FileMatchingIguales + "\n" +
                "Log1: ");
        System.out.println("Proceso Terminado Correctamente");
    } //public Matching (String directorio) {

    private void proceso() {
        procesandoFicheros = true;
        //if (!claveDriver.equals(maxValor))
        //		claveDriver = obtenerClave(lineDriver, driverCaracterDivision , driverClave);

        //if (!claveMatching.equals(maxValor))
        //		claveMatching = obtenerClave(lineMatching, matchingCaracterDivision , matchingClave);

        UtilsJavi.escribirLog("driver: " + claveDriver, 1, nivelLogEstablecido);
        UtilsJavi.escribirLog("matchi: " + claveMatching, 1, nivelLogEstablecido);
        UtilsJavi.escribirLog("driverAnt: " + claveDriverAnt, 1, nivelLogEstablecido);
        UtilsJavi.escribirLog("matchiAnt: " + claveMatchingAnt, 1, nivelLogEstablecido);
        UtilsJavi.escribirLog("**********************************", 1, nivelLogEstablecido);


//Proceso Iguales
        if (!f1 && !f2) {
            f1 = true;
            f2 = true;
        }

        if (comparaClaves(claveDriver, claveMatching) == 0) {

            if (paired) {
                UtilsJavi.print("paired iguales:" + claveDriver + "-" + claveMatching);
                //if (claveDriverGrabado != claveDriver) {   // esto si no se quiere que se saquen bastantes de una misma clave
                CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                        componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                                matchingCaracterDivision));   //con los campos que se requieran tanto de f1 como de f2

                claveDriverGrabado = claveDriver;
                claveMatchingGrabado = claveMatching;
                //}
            }
            if (unpaired) {
                if (f1 && !only) {
                    UtilsJavi.print("unpaired f1 iguales:" + claveDriver + "-" + claveMatching);
                    //if (claveDriverGrabado != claveDriver) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                            componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                                    matchingCaracterDivision));   //con los campos que se requieran tanto de f1 como de f2

                    claveDriverGrabado = claveDriver;
                    claveMatchingGrabado = claveMatching;
                    //}
                }
                if (f2 && !only) {
                    UtilsJavi.print("unpaired f2 iguales:" + claveDriver + "-" + claveMatching);
                    //if (claveMatchingGrabado != claveMatching) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                            componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                                    matchingCaracterDivision));   //con los campos que se requieran tanto de f1 como de f2

                    claveDriverGrabado = claveDriver;
                    claveMatchingGrabado = claveMatching;
                    //}
                }
                if (only) {
                    UtilsJavi.print("unpaired iguales only:" + claveDriver + "-" + claveMatching);
                    // si es only no hace nada si son iguales
                }
            }
            //else claveMatching = maxValor;
            claveMatchingAnt = claveMatching;
            lineMatching = leerFich(matching);
            claveMatching = matching.getClave();
        }
//proceso mayor
        if (comparaClaves(claveDriver, claveMatching) > 0) {

            if (paired) {
                UtilsJavi.print("paired mayor " + claveDriver + " - " + claveMatching);
            }
            if (unpaired) {
                if (f1) {
                    UtilsJavi.print("unpaired f1 mayor:" + claveDriver + "-" + claveMatching);
                }
                if (f2) {
                    UtilsJavi.print("unpaired f2 mayor:" + claveDriver + "-" + claveMatching);
                    //if (claveMatchingGrabado != claveMatching) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                            componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                                    matchingCaracterDivision));   //con los campos que se requieran tanto de f1 como de f2

                    claveDriverGrabado = claveDriver;
                    claveMatchingGrabado = claveMatching;
                    //}
                }
                if (only) {
                    UtilsJavi.print("unpaired f1 mayor only:" + claveDriver + "-" + claveMatching);

                    //parece que no haria falta en este punto
                }
            }
            claveMatchingAnt = claveMatching;
            lineMatching = leerFich(matching);
            claveMatching = matching.getClave();
        }
//proceso menor
        if (comparaClaves(claveDriver, claveMatching) < 0) {
            if (paired) {
                UtilsJavi.print("paired menor:" + claveDriver + "-" + claveMatching);
            }
            if (unpaired) {
                if (f1) {
                    UtilsJavi.print("unpaired f1 menor:" + claveDriver + "-" + claveMatching);
                    if (claveDriverGrabado != claveDriver) {
                        CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                                componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                                        matchingCaracterDivision));   //con los campos que se requieran tanto de f1 como de f2

                        claveDriverGrabado = claveDriver;
                        claveMatchingGrabado = claveMatching;
                    }
                    //puede que ya lo haya sacado anteriormente
                }
                if (f2) {
                    UtilsJavi.print("unpaired f2 menor:" + claveDriver + "-" + claveMatching);
                    if (claveMatchingGrabado != claveMatching) {
                        CreaFileStatic.log(directorio + dirResultados + "\\" + FileMatchingIguales,
                                componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                                        matchingCaracterDivision));   //con los campos que se requieran tanto de f1 como de f2

                        claveDriverGrabado = claveDriver;
                        claveMatchingGrabado = claveMatching;
                    }
                }
                if (only) {
                    UtilsJavi.print("unpaired f2 menor only:" + claveDriver + "-" + claveMatching);
                }
            }
            claveDriverAnt = claveDriver;
            lineDriver = leerFich(driver);
            claveDriver = driver.getClave();
        }
//1=TODO EL DRIVER Y TODOS LOS QUE COINCIDAN EN EL MATCHING  (1:N)
//1,2,3,4,5 --salida---> 111,2,3,4,5555
        if (lineDriver != null) {
            boolean a = true;
            UtilsJavi.escribirLog("Driver EOF " + a, 1, nivelLog);
        }
        if (lineMatching != null) {
            boolean a = true;
            UtilsJavi.escribirLog("Matching EOF " + a, 1, nivelLog);
        }
        if (lineDriver != null || lineMatching != null) UtilsJavi.escribirLog("No debe salir", 1, nivelLog);
//2=EL DRIVER CON LA INFO DE LOS QUE CONCIDAN EN EL EL MATCHING, PERO SOLO UNO DEL MATCHING (1:1)
        if (tipoMatching == "") {

        }
    }

    private void ObtenemosTipoMatching() {
        // **********************************  DRIVER  F1 *************************************
        //Seleccionamos el tipo de matching
        boolean tipoMatchingOK = false;
        while (!tipoMatchingOK) {
            tipoMatching = JOptionPane.showInputDialog(
                    "�	Si no se indica nada hace un inner Join (PAIRED)\n" +
                            "�	UNPAIRED,F1,ONLY Registros del fichero 1 no coincidentes.\n" +
                            "�	UNPAIRED,F2,ONLY Registros del fichero 2 no coincidentes.\n" +
                            "�	UNPAIRED,F1 Registros coincidentes y registros del fichero 1 no coincidentes (left outer join)\n" +
                            "�	UNPAIRED,F2 Registros coincidentes y registros del fichero 2 no coincidentes (right outer join)\n" +
                            "�	UNPAIRED,F1,F2 o UNPAIRED Registros coincidentes, reg no \n" +
                            "            coincidentes del fich 1 y reg no coincidentes del fich 2 (full outer join)\n" +
                            "�	UNPAIRED,F1,F2 ONLY o UNPAIRED ONLY Registros no coincidentes del fichero 1 y registros no coincidentes del fichero 2\n");

            if (tipoMatching == null || tipoMatching.equals("")) tipoMatching = "PAIRED";

            tipoMatchingOK = validarTipoMatching(tipoMatching.toUpperCase());
        } //while
    }

    private boolean validarTipoMatching(String tipo) {
        String[] tipoMatchingSeparado = tipo.split(",");
        for (int i = 0; i < tipoMatchingSeparado.length; i++) {
            if (tipoMatchingSeparado[i].equals("PAIRED"))
                paired = true;
            if (tipoMatchingSeparado[i].equals("UNPAIRED"))
                unpaired = true;
            if (tipoMatchingSeparado[i].equals("F1"))
                f1 = true;
            if (tipoMatchingSeparado[i].equals("F2"))
                f2 = true;
            if (tipoMatchingSeparado[i].equals("ONLY"))
                only = true;
            if (!tipoMatchingSeparado[i].equals("PAIRED") && !tipoMatchingSeparado[i].equals("UNPAIRED") &&
                    !tipoMatchingSeparado[i].equals("F1") && !tipoMatchingSeparado[i].equals("F2") &&
                    !tipoMatchingSeparado[i].equals("ONLY")) {
                UtilsJavi.dialog("Error al introducir el tipo de matching. " + tipoMatchingSeparado[i]);
                return false;
            }
        }    //for
        return true;
    }

    private void ObtenemosDriver() {
        //Seleccionamos el driver
        fileSeleccionado = UtilsJavi.seleccionarFicheroDesc("C:\\DATOS\\Prueba\\Pruebas Matching", "", "", 2, "Seleccionar el fichero Driver");
        if (Objects.isNull(fileSeleccionado))
            return;
        fileDriver = new File(fileSeleccionado.getAbsolutePath());
        directorio = UtilsJavi.getPath(fileDriver);

        //Seleccionamos el caracter de division del driver si hay
        String division = JOptionPane.showInputDialog("Driver: Caracter de divisi�n");
        if (division.equals("") || division == null) {  //no han seleccionado caracter de division
            //Seleccionamos las posiciones de la clave del driver (formato: pos,long,pos,long ... etc
            division = JOptionPane.showInputDialog("Driver: Posiciones de la clave: pos,long,pos,long,...");
            if (!division.equals("") && division != null)
                driverClave = division;
        } else {
            driverCaracterDivision = division;
            //Seleccionamos la/s columna/s de clave del driver
            division = JOptionPane.showInputDialog("Driver: Columnas clave. Separadas por coma");
            if (!division.equals("") && division != null)
                driverClave = division;
        }

    }

    private void ObtenemosMatching() {
        // **********************************  MATCHING  *************************************
        //Seleccionamos el matching
        fileSeleccionado = UtilsJavi.seleccionarFicheroDesc("C:\\DATOS\\Prueba\\Pruebas Matching", "", "", 2, "Seleccionar el fichero Matching");
        if (Objects.isNull(fileSeleccionado))
            return;
        fileMatching = new File(fileSeleccionado.getAbsolutePath());

        //Seleccionamos el caracter de division del matching si hay
        String division = JOptionPane.showInputDialog("Matching: Caracter de divisi�n");
        if (division.equals("") || division == null) {
            //Seleccionamos las posiciones de la clave del matching (formato: pos,long,pos,long ... etc
            division = JOptionPane.showInputDialog("Matching: Posiciones de la clave: pos,long,pos,long,...");
            if (!division.equals("") && division != null)
                matchingClave = division;

        } else {
            matchingCaracterDivision = division;

            //Seleccionamos la/s columna/s de clave del Matching
            division = JOptionPane.showInputDialog("Matching: Columnas clave. Separadas por coma");
            if (!division.equals("") && division != null)
                matchingClave = division;
        }
    }

    private int comparaClaves(String driver, String matching) {
        int comparacion;
        if (ignoreCase) comparacion = driver.compareToIgnoreCase(matching);
        else comparacion = driver.compareTo(matching);

        if (comparacion == 0) return 0;
        else if (comparacion < 0) return -1;
        else return 1;
    }

    private String ObtenemosFmtSalida() {
        // **********************************  SALIDA  *************************************
        // ------------ESTO LO TENEMOS QUE CAMBIAR PARA QUE COJA TODAS LAS SALIDAS (ficheros) QUE SE ESPECIFIQUEN ----
        //	OUTFIL FNAMES=F1SOLO,INCLUDE=(21,1,CH,EQ,C'1'),
        //	BUILD=(1,10)
        //	OUTFIL FNAMES=F2SOLO,INCLUDE=(21,1,CH,EQ,C'2'),
        //	BUILD=(11,10)
        //	OUTFIL FNAMES=AMBOS,INCLUDE=(21,1,CH,EQ,C'B'),
        //	BUILD=(1,10,14,7)

        //String FichFmtoSalida [] = null;   esto cuando procesemos mas de una salida y mas de un formato
        String FichFmtoSalida = null;
        String valor = "";
        //for (int i = 0; valor != ""; i++) {    esto cuando procesemos mas de una salida y mas de un formato
        valor = JOptionPane.showInputDialog(
                "Seleccionamos como vamos a montar la salida del fichero\n" +
                        "D(Driver)/M(Matching),pos,long: SALF1:D,1,5,M,2,5\n" +   //lo del SALF1 en el futuro. de momento solo el D o M
                        "D(Driver)/M(Matching),col: SALF1:D,1,M,2\n" +
                        "Se puede poner C'xxxx'  ej: SALF1:D,1,2,3,C'xxx',M,1,2,3 o D,1,2,3\n" +
                        "Se puede poner <n>X para que rellene con espacios ej: 56X\n" +
                        "Se puede poner la pos. de inicio ej: 15:8C'0' para que rellene con el caracter que se quiera\n\n" +  //esto de momento no lo hacemos
                        "Ej: Salida D(Driver)/M(Matching),pos,long: SALF1:D,1,5,M,2,5\n");
        if (!valor.equals("") && valor != null)
            FichFmtoSalida = valor.toUpperCase();
        else
            FichFmtoSalida = "D";   // por defecto todo el driver
        // aqui se puede validar que el formato sea correcto
        return FichFmtoSalida;
        //		FichFmtoSalida[i] = valor;     esto cuando procesemos mas de una salida y mas de un formato
        //} for     esto cuando procesemos mas de una salida y mas de un formato
    }

    //Desarrollar este metodo
    private String escribirSalida(String fmtoSalida, String regDriver, String divisionDriver, String regMatching, String divisionMatching) {
        componerSalida(fmtoSalida, lineDriver, driverCaracterDivision, lineMatching,
                matchingCaracterDivision);   //con los campos que se requieran tanto de f1 como de f2
        return "";
    }

    private String componerSalida(String fmtoSalida, String regDriver, String divisionDriver, String regMatching, String divisionMatching) {
        String identDriver = "D";
        String identMatching = "M";
        //controlar
        //fmtoSalida: D=Driver M=Matching
        String fmtoSalidaTrabajo = fmtoSalida;
        String fichero = "";
        String salida = "";
        String nbFichSalida = "";
        String[] partsFmtSalida = null;
        String[] partsDriver = null;
        String[] partsMatching = null;
        //boolean tratado = false;

        //aqui comprobamos que si viene un nb de fichero lo
        //guardamos y lo quitamos de la cadena del fmt de salida para que no moleste
        //en el futuro partiremos todo con , y entonces trataremos las partes, si una tiene el : entendemos que es un nb de fichero
        String[] partsFichSalida = fmtoSalidaTrabajo.split(":");
        if (partsFichSalida.length == 2) {
            nbFichSalida = partsFichSalida[0];
            UtilsJavi.print("nbFichSalida: " + nbFichSalida);
            fmtoSalidaTrabajo = partsFichSalida[1];
        } else {
            if (partsFichSalida.length > 2) return "ERROR 01";
        }

        //dividimos por la coma
        partsFmtSalida = fmtoSalidaTrabajo.split(",");
        if (partsFmtSalida.length < 1) return "ERROR 02";
        if (!partsFmtSalida[0].equals(identDriver) && !partsFmtSalida[0].equals(identMatching)
                && !partsFmtSalida[0].startsWith("C\'") && !partsFmtSalida[0].endsWith("X"))
            return "ERROR 03";

        //esto seria bueno que no estuviera aqui y que se tratara independentemente en vbles propias del metodo y que se pasara por parametros

        //dividimos por la coma
        if (!divisionDriver.equals("")) partsDriver = regDriver.split(divisionDriver);
        if (!divisionMatching.equals("")) partsMatching = regMatching.split(divisionMatching);

        UtilsJavi.escribirLog("Longitud fmt salida: " + partsFmtSalida.length, 1, nivelLogEstablecido);
        for (int i = 0; i < partsFmtSalida.length; i++) {   //bucle hasta que hayamos procesado las distintas posiciones/columnas del formateo de salida
            if (partsFmtSalida[i].equals(identDriver) || partsFmtSalida[i].equals(identMatching))
                fichero = partsFmtSalida[i];
            else {
                if (partsFmtSalida[i].startsWith("C\'") || partsFmtSalida[i].endsWith("X"))
                    salida = salida + tratarCaracteresRelleno(partsFmtSalida[i]);
                else {
                    //Es D y tiene caracter de division ------------------------------
                    if (fichero.equals(identDriver) && !divisionDriver.equals("") && !driver.equals("")) {
                        if (partsDriver.length < Integer.parseInt(partsFmtSalida[i]) - 1)
                            UtilsJavi.escribirLog("ERROR 05: Driver=" + partsDriver.length + " cols y en salida se pide la " + partsFmtSalida[i], 1, nivelLogEstablecido);
                        else
                            salida = salida + partsDriver[Integer.parseInt(partsFmtSalida[i]) - 1];  //el -1 para que la columna que indican del fichero sea la correcta del array (que empieza en 0)
                        UtilsJavi.escribirLog("D y division: " + salida, 1, nivelLogEstablecido);
                    }
                    //Es M y tiene caracter de division ---------------------------------
                    if (fichero.equals(identMatching) && !divisionMatching.equals("") && !matching.equals("")) {
                        if (partsMatching.length < Integer.parseInt(partsFmtSalida[i]) - 1)
                            UtilsJavi.escribirLog("ERROR 05: Matching=" + partsMatching.length + " cols y en salida se pide la " + partsFmtSalida[i], 1, nivelLogEstablecido);
                        else
                            salida = salida + partsMatching[Integer.parseInt(partsFmtSalida[i]) - 1];
                        UtilsJavi.escribirLog("M y division: " + salida, 1, nivelLogEstablecido);
                    }
                    //Es D y NO Tiene caracter de division -----------------------------------------
                    if (fichero.equals(identDriver) && divisionDriver.equals("") && !driver.equals("")) {
                        //if (Integer.parseInt(partsFmtSalida[i+1])) > // hacer que no falle si se pasa de la longitud total del fichero
                        salida = salida + regDriver.substring(Integer.parseInt(partsFmtSalida[i]) - 1, Integer.parseInt(partsFmtSalida[i]) - 1
                                + Integer.parseInt(partsFmtSalida[i + 1]));
                        i++; //incrementamos 1 extra aqui para que pase los 2 no solo uno. La posicion y la longitud
                        UtilsJavi.escribirLog("D y No division: " + salida, 1, nivelLogEstablecido);
                    }
                    //Es M y NO Tiene caracter de division --------------------------------------------
                    if (fichero.equals(identMatching) && divisionMatching.equals("") && !matching.equals("")) {
                        salida = salida + regMatching.substring(Integer.parseInt(partsFmtSalida[i]) - 1, Integer.parseInt(partsFmtSalida[i]) - 1
                                + Integer.parseInt(partsFmtSalida[i + 1]));
                        i++; //incrementamos 1 extra aqui para que pase los 2 no solo uno. La posicion y la longitud
                        UtilsJavi.escribirLog("M y No division: " + salida, 1, nivelLogEstablecido);
                    }
                    UtilsJavi.escribirLog("Salida: " + salida, 1, nivelLogEstablecido);
                } //if (partsFmtSalida[i].startsWith("C\'") || partsFmtSalida[i].endsWith("X"))
            } //if (partsFmtSalida[i].equals(identDriver) || partsFmtSalida[i].equals(identMatching))
        }  // for
        UtilsJavi.escribirLog("Salida final: " + salida, 1, nivelLogEstablecido);
        return salida;
    }

    private String tratarCaracteresRelleno(String cadena) {
        String salida = "";
        if (cadena.startsWith("C\'") && cadena.endsWith("\'")) {
            salida = cadena.substring(2, cadena.length() - 1);
        }
        if (cadena.endsWith("X")) {
            salida = rellenarCadenaSpaces(" ", Integer.parseInt(cadena.substring(
                    0, cadena.indexOf("X")).toString())); //Esto es cuando queremos caracteres de relleno
        }
        // if (cadena.xxxx(xxx)) { 	//esto es para si se pone pe 15:8C'0' para que rellene con el caracter que se quiera
        return salida;
    }

    private String rellenarCadenaSpaces(String caracter, int longitud) {
        String cadena = "";
        for (int i = 0; i < longitud; i++) {
            cadena = cadena + caracter;
        }
        return cadena;
    }

    private FileSys abrirFich(String nbFichero, String caracterDivision, String clave) {
        FileSys fichero = null;
        try {
            fichero = new FileSys(nbFichero, caracterDivision, clave);
        } catch (java.io.IOException exception) {
        }
        ;

        return fichero;
    }

    private String leerFich(FileSys driver) {
        String linea = "";
        try {
            linea = driver.readLine();
        } catch (java.io.IOException exception) {
        }
        ;

        return linea;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Ap�ndice de m�todo generado autom�ticamente
        new MatchingJOIN();

    }

}

/*
 Explicacion 1
MATCH en JCL 
A continuaci�n se presenta un ejemplo de c�mo realizar un MATCH entre 2 ficheros.

//MATCH JOB  (,,,),CLASS=F,MSGCLASS=X
//*
//PASO01   EXEC PGM=ICEMAN,REGION=4M
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SORTIN01 DD *
333AAA
444BBB
222CCC
333DDD
111EEE
333FFF
999GGG
333HHH
//SORTIN02 DD *
777III
555JJJ
666KKK
333LLL
000MMM
//JNF1CNTL DD *
  OMIT COND=(1,6,SS,EQ,C'9')
//JNF2CNTL DD *
  INCLUDE COND=(1,3,CH,NE,C'000')
//SORTOUT  DD SYSOUT=*
//SOLOENF1 DD SYSOUT=* 
//SOLOENF2 DD SYSOUT=*   
//ENLOSDOS DD SYSOUT=* 
//SYSIN DD *
  JOINKEYS F1=SORTIN01,FIELDS=(1,3,A)
  JOINKEYS F2=SORTIN02,FIELDS=(1,3,A),SORTED,NOSEQCK
  REFORMAT FIELDS=(F1:1,6,F2:1,6,?) 
  OPTION COPY  
  JOIN UNPAIRED
  OUTFIL FNAMES=SOLOENF1,INCLUDE=(13,1,CH,EQ,C'1'),BUILD=(1,6)
  OUTFIL FNAMES=SOLOENF2,INCLUDE=(13,1,CH,EQ,C'2'),BUILD=(7,6)
  OUTFIL FNAMES=ENLOSDOS,INCLUDE=(13,1,CH,EQ,C'B'),BUILD=(1,12)

SORTIN01 y SORTIN02: Primer y segundo fichero de entrada, respectivamente. De ahora en adelante, F1 y F2.

JNF1CNTL: Afecta solo a F1. Incluye una condici�n por la cual se omitir�n del MATCH aquellos registros que tengan un 9 en las primeras 6 posiciones. Se pueden incluir m�s condiciones con la sentencias INREC, INCLUDE, etc.

JNF1CNTL: Afecta solo a F2. Incluye una condici�n por la cual solo participar�n en el MATCH aquellos registros que no tengan �000� en las 3 primeras posiciones. Se pueden incluir m�s condiciones con la sentencias INREC, INCLUDE, etc.

SORTOUT, SOLOENF1, SOLOENF2 y ENLOSDOS: Ficheros de salida.

La sentencia JOINKEYS describe cuales son los campos por los que se va a realizar el MATCH. 
Se pueden especificar varios campos. El campo puede estar ordenado en orden ascendente A o descendenete D. 
La opci�n SORTED indica que el fichero de entrada ya est� ordenado por la clave. 
A pesar de todo, se verifica si el fichero esta ordenado devolviendo un error si no lo est�. 
Con la opcion SORTED se puede especificar NOSEQCK indicando que no se verifique el orden del fichero. 
Si no se indica la opci�n SORTED el fichero se ordenar�.  
                                                                             
Sentencia JOIN:                                               
                                                                 
 Si no se indica JOIN     -> INNER JOIN   
 JOIN UNPAIRED,F1         -> LEFT OUTER JOIN 
 JOIN UNPAIRED,F2         -> RIGHT OUTER JOIN 
 JOIN UNPAIRED            -> FULL OUTER JOIN 
 JOIN UNPAIRED,F1,F2      -> FULL OUTER JOIN
 JOIN UNPAIRED,F1,ONLY    -> Solo registros de F1 no emparejados 
 JOIN UNPAIRED,F2,ONLY    -> Solo registros de F2 no emparejados 
 JOIN UNPAIRED,ONLY       -> Registros de F1 y F2 no emparejados 
 JOIN UNPAIRED,F1,F2,ONLY -> Registros de F1 y F2 no emparejados

La sentencian REFORMAT indica que posiciones se van a incluir en el fichero de salida. En el ejemplo, las 6 primeras posiciones de cada fichero de entrada. Por �ltimo, con ? se incluye en la �ltima posici�n un 1 si el registro solo tiene informaci�n de F1. Si se indica 2, solo tiene informaci�n de F2 y si indica B es que tienen informaci�n de F1 y F2.

Cuando se realiza el JOIN, se genera el fichero SORTOUT. 
A partir del mismo y con la sentencia OUTFIL se pueden crear diferentes ficheros de salida. 
En el ejemplo se crean 3. Con la opci�n BUILD se indica las posiciones a tomar el fichero SORTOUT.

Por �ltimo, cabe destacar que el rendimiento de esta utilidad no tiene comparaci�n con 
cualquier otro programa escrito en COBOL, PL/1, REXX, etc, ya compilado o interpretado.


Explicacion 2

Cruce de ficheros en un JCL III
Posted on 18/05/2012 by fguerreroga � 5 comentarios 
Vamos a introducir dos nuevas opciones de la aplicaci�n JOINKEYS:
SORTED: Indica que el fichero de entrada se encuentra ordenado y no es necesario el proceso de ordenaci�n.
JOIN:  Permite indicar el tipo de cruce que queremos realizar. Por defecto se realiza el producto cartesiano (inner join), cuyo resultado son los registros con clave coincidente. Las otras opciones son:
�	Si no se indica nada hace un inner Join
�	UNPAIRED,F1,ONLY: Registros del fichero 1 no coincidentes.
�	UNPAIRED,F2,ONLY: Registros del fichero 2 no coincidentes.
�	UNPAIRED,F1: Registros coincidentes y registros del fichero 1 no coincidentes (left outer join)
�	UNPAIRED,F2: Registros coincidentes y registros del fichero 2 no coincidentes (right outer join)
�	UNPAIRED,F1,F2 o UNPAIRED: Registros coincidentes, registros no coincidentes del fichero 1 y registros no coincidentes del fichero 2 (full outer join)
�	UNPAIRED,F1,F2 ONLY o UNPAIRED ONLY: Registros no coincidentes del fichero 1 y registros no coincidentes del fichero 2
Para ver un caso de utilizaci�n de estas nuevas opciones vamos a incluirlas en el ejemplo del post anterior sobre enfrentamiento. Recordamos que se trataba de dos ficheros de entrada de 10 posiciones con la clave en las 3 primeras. El primero de ellos conten�a nombres de personas y el segundo tel�fonos. Cuando las claves de los ficheros 1 y 2 coincid�an quer�amos escribir en salida un registro con la clave, el nombre y el tel�fono.
Sabemos que los ficheros de entrada est�n ordenados, por lo que usamos la opci�n SORTED, que evita la ordenaci�n inicial. En salida queremos obtener los registros que est�n s�lo en el fichero 1 y s�lo en el fichero 2, adem�s de los que cruzan. Para ello empleamos la opci�n UNPAIRED,F1,F2.
/S1 EXEC PGM=SORT
//SYSOUT DD SYSOUT=*
//SORTJNF1 DD DSN=ENTRADA1,DISP=SHR
//SORTJNF2 DD DSN=ENTRADA2,DISP=SHR
//F1SOLO   DD DSN=SALIDA1,
//            DISP=(,CATLG,DELETE),SPACE=(TRK,(1,1),RLSE),
//            DCB=(RECFM=FB,LRECL=10,BLKSIZE=0)
//F2SOLO   DD DSN=SALIDA2,
//            DISP=(,CATLG,DELETE),SPACE=(TRK,(1,1),RLSE),
//            DCB=(RECFM=FB,LRECL=10,BLKSIZE=0)
//AMBOS    DD DSN=SALIDA3,
//            DISP=(,CATLG,DELETE),SPACE=(TRK,(1,1),RLSE),
//            DCB=(RECFM=FB,LRECL=17,BLKSIZE=0)
//SYSIN DD *
JOINKEYS FILE=F1,FIELDS=(1,3,A),SORTED
JOINKEYS FILE=F2,FIELDS=(1,3,A),SORTED
JOIN UNPAIRED,F1,F2
REFORMAT FIELDS=(F1:1,10,F2:1,10,?)
* �?� indica que a�adimos un car�cter adicional para poder distinguir  
* los registros de salida
OPTION COPY
OUTFIL FNAMES=F1SOLO,INCLUDE=(21,1,CH,EQ,C'1'),
BUILD=(1,10)
OUTFIL FNAMES=F2SOLO,INCLUDE=(21,1,CH,EQ,C'2'),
BUILD=(11,10)
OUTFIL FNAMES=AMBOS,INCLUDE=(21,1,CH,EQ,C'B'),
BUILD=(1,10,14,7)


*/
