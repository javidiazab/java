package com.java.pruebas.varios;

import java.io.*;

// Crea fichero. Se utiliza tambien para crear un log con una vble estatica

//nivelLog es el nivel de log de la linea a loguear
//nivelLogEstablecido es el nivel de log a partir del que se va a loguear
//int nivelLog = 0; 0 no sale nada, 5 sale todo, 6 sale por la consola


public class CreaFileStatic {

    private String fileNb = "nbs.log";
    static BufferedWriter bw;
    static PrintWriter salida; // = new PrintWriter(bw);

    public CreaFileStatic(String fileNb) {
        this.fileNb = fileNb;
        creaFile(this.fileNb);
    }

    public CreaFileStatic() {
        creaFile(fileNb);
    }

    public static void creaFile(String fileNb) {
        File file = new File(fileNb);
        System.out.println(file.getName() + " --> " + file.getAbsolutePath());
        if (!(new File(fileNb)).exists()) {
//			System.out.println("Abre fichero");
            try {
                file.createNewFile();
                bw = new BufferedWriter(new java.io.FileWriter(fileNb, true));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//				System.out.println("No Abre fichero"); } //if (!(new File(fileNb)).exists()) {
            try {
                bw = new BufferedWriter(
                        new java.io.FileWriter(fileNb, true));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } //lo ponemos nuevo al quitar el system
    }

    public static void cierraFile() {
        if (bw != null)
            try {
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static void writeLine(String line) {
        if (bw != null)
            try {
                bw.write(line);
                bw.newLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static void log(String fich, String line) {
        creaFile(fich);
        writeLine(line);
        cierraFile();
    }

    public static void log(String line, int nivelLog, int nivelLogEstablecido) {
        //El fichero de log va a ir a terminar a C:\DATOS\BKSPlatform\workspace1\Java\nbs.log
        //nivelLog es el nivel de log de la linea a loguear
        //nivelLogEstablecido es el nivel de log a partir del que se va a loguear
        //int nivelLog = 0; //0 no sale nada, 5 sale todo, 6 sale por la consola
        if (nivelLog == 6) {

            String fechaS = UtilsJavi.getFechaHora();
            System.out.println(fechaS + " - " + line);
        } else if (nivelLog >= nivelLogEstablecido) {
            String fechaS = UtilsJavi.getFechaHora();
            creaFile("nbs.log");
            writeLine(fechaS + " - " + line);
            cierraFile();
        }
    }


    public static void main(String[] args) {
        CreaFileStatic a = new CreaFileStatic("C:\\DATOS\\javid.jda");
        writeLine("hola1");
        writeLine("hola2");
        cierraFile();
    }

}

