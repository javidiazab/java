package com.java.pruebas.varios;

import java.util.Scanner;

public class Scan {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int numero = 0;

        while (numero >= 0) {
            System.out.println("Dame un numero: ");
            numero = lector.nextInt();
        }
    }
}
