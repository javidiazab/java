package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;

//A a partir de los datos de una tabla de CM genera tantos txt como JCLS con sus datos de CM
//Ejemplo: EXW.GRISB.SCHEDULE.FX$0E.txt ----> EXW.GRISB.SCHEDULE(FX$0E)
//Ejemplo: TES.CTM.SCHEDULE(HX$AIS01) en San
//Se puede ejecutar luego LeerCMRelaciones para que cree el diagrama de dependencias

public class LeerCM {

    private String nbPGM = "LeerCM";
    private String directorio;
    int index = 0;
    String dirResultados = "\\Resultados " + nbPGM;
    File fileSeleccionado;

    String fechaS = UtilsJavi.getFechaHora();
    //String fileJCLs  = "JCLs " + fechaS + ".txt";
    String fileJCLs = "JCLs" + ".txt";
    String fileDSNs = "DSNs " + fechaS + ".txt";
    //String fileTODO  = "TODOs " + fechaS + ".txt";
    String fileTODO = "TODOs" + ".txt";
    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    String JCL = null;
    Boolean JCLSI = false;
    Boolean InSI = false;
    Boolean OutSI = false;
    String Periodo = "";
    int OrdenCM = 1;


    public LeerCM() {

        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);

        fileSeleccionado = UtilsJavi.seleccionarFichero("C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\JCLS", "", "", 2);
        if (Objects.isNull(fileSeleccionado))
            return;

        File fileCM = new File(fileSeleccionado.getAbsolutePath());
        directorio = UtilsJavi.getPath(fileCM);

        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        try {
            FileSys fileSys = new FileSys(fileCM.toString());
            String line = null;
            line = fileSys.readLine();
            while (line != null) {
                procesaLine(line);
                line = fileSys.readLine();
            } //while (line != null);
        } catch (IOException exception) {
        }
        ;
//				System.out.println("Procesando: " + list[i]);
        System.out.println("Proceso Terminado Correctamente");
    } //public LeerCM (String directorio) {


    public void procesaLine(String line) {

        if (line.startsWith("DCONTROLM")) {
            Periodo = line.substring(13, 20);
        }
        if (line.startsWith("M")) {
            JCL = line.substring(1, 9);
            if (JCL.equals("NADA    "))
                JCLSI = false;
            if (!JCL.equals("NADA    ")) {
                JCLSI = true;
                CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs, line.substring(9, 14) + ";" + JCL);
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<CM>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Nombre>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", JCL);
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Nombre>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Periodo>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", Periodo);
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Periodo>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<TablaCM>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(9, 14));
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</TablaCM>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<OrdenCM>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "" + OrdenCM++);
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</OrdenCM>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
            }
        }
        if (line.startsWith("H") && JCLSI) {
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Descripcion>");
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(1));
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Descripcion>");
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
        }
        if (line.startsWith("L") && JCLSI) {
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Libreria>");
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(1));
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Libreria>");
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
        }
        if (line.startsWith("I") && JCLSI) {
            if (!InSI) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<CondicionesIn>");
                InSI = true;
            }
            if (line.substring(1, 2).equals("|")) {
                if (line.length() > 1) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<In>");
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(2, 25));
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</In>");
                }
                if (line.length() > 25) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<In>");
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(26, 49));
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</In>");
                }
                if (line.length() > 49) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<In>");
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(50));
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</In>");
                }
            } else {
                if (line.length() > 1) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<In>");
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(1, 25));
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</In>");
                }
                if (line.length() > 25) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<In>");
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(25, 49));
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</In>");
                }
                if (line.length() > 49) {
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<In>");
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(49));
                    CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</In>");
                }
            }
        }
        if (line.startsWith("O") && JCLSI) {
            if (InSI) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</CondicionesIn>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
                InSI = false;
            }
            if (!OutSI) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<CondicionesOut>");
                OutSI = true;
            }
            if (line.length() > 1) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Out>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(1, 26));
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Out>");
            }
            if (line.length() > 26) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Out>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(26, 50));
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Out>");
            }
            if (line.length() > 51) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "<Out>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", line.substring(51));
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</Out>");
            }
        }

        if (line.startsWith("R") && JCLSI) {
            if (InSI) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</CondicionesIn>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
                InSI = false;
            }
            if (OutSI) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</CondicionesOut>");
                CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "");
                OutSI = false;
            }
            CreaFileStatic.log(directorio + dirResultados + "\\" + JCL + ".txt", "</CM>");
        }

        CreaFileStatic.log(directorio + dirResultados + "\\" + fileTODO, JCL + ";" + line);
    }

    public static void main(String[] args) {
        new LeerCM();
    }
}

