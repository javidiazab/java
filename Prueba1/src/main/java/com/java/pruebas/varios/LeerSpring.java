package com.java.pruebas.varios;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

// pgm que lee un txt con un pgm cobol y extrae las selects

public class LeerSpring {

    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    private String directorio;
    FileSys fileSys = null;
    int index = 0;
    boolean EOF = false;
    String dirResultado = "\\Resultado";
    String fechaS = UtilsJavi.getFechaHora();

    public LeerSpring(String directorio) {
        ProcesarSpring(directorio);
    }

    public LeerSpring() {
        File dirSeleccionado = UtilsJavi.seleccionarFichero(
                "C:\\",
                "", "", 1);
        if (Objects.isNull(dirSeleccionado))
            return;
        ProcesarSpring(dirSeleccionado.getAbsolutePath());
    }

    public void ProcesarSpring(String directorio) {
        this.directorio = directorio;
        File path = new File(directorio);
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultado);

        for (int i = 0; i < list.length; i++) {
            File fileTRX = new File(directorio + "\\" + list[i]);
            if (fileTRX.isFile()) {
                try {
                    fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        proceso(line, UtilsJavi.removeExtension(list[i]) + ".txt");
                        if (line != null)
                            line = fileSys.readLine();
                    } //while (line != null);
                } catch (IOException exception) {
                }
                ;

            } //if (f1.isFile()) {
            System.out.println("Proceso Terminado Correctamente");
        } //	for(int i = 0; i < list.length; i++) {
    } //public RenameFiles (String directorio) {


    public void proceso(String line, String pgm) {
        if (line.contains(" EXEC SQL ") && line.contains("END-SQL")) UtilsJavi.print(line);
        if ((line.contains(" EXEC SQL ") || line.contains(" EXEC  SQL ") || line.contains(" EXEC  SQL "))
                && !line.substring(6, 7).equals("*")) {
            while (!line.contains("END-EXEC") && !line.substring(6, 7).equals("*") && line != null) {
                CreaFileStatic.log(directorio + dirResultado + "\\" + pgm, line.substring(7, 72));

                try {
                    line = fileSys.readLine();
                    if (line.contains("END-EXEC")) {
                        CreaFileStatic.log(directorio + dirResultado + "\\" + pgm, line.substring(7, 72));
                        CreaFileStatic.log(directorio + dirResultado + "\\" + pgm, "");
                        CreaFileStatic.log(directorio + dirResultado + "\\" + pgm, "");
                    }
                } catch (IOException exception) {
                }
                ;
            }
        }
    }


    public static void main(String[] args) {
        new LeerSpring();
    }
}
