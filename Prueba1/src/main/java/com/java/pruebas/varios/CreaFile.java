/*
 * Creado el 13-jul-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author jdiazaba
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

//Crea fichero. No utiliza vble estatica


import java.io.*;
import java.io.FileWriter;


public class CreaFile {

    private BufferedWriter bw;
    private PrintWriter salida; // = new PrintWriter(bw);
    private File file;


    public CreaFile() {
        file = Crea("fileNb.txt");
    }

    public CreaFile(String fileNb) {
        file = Crea(fileNb);
    }

    public CreaFile(String fileNb, Boolean append) {
        file = Crea(fileNb, append);
    }

    private File Crea(String fileNb) {
        if (!(new File(fileNb)).exists()) {
            //		System.out.println("Abre fichero");
            this.file = new File(fileNb);
            try {
                file.createNewFile();
                bw = new BufferedWriter(new java.io.FileWriter(fileNb));
                return file;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("no abre fichero " + fileNb);
        }  //if (!(new File(fileNb)).exists()) {
        return null;
    } //public CreaFile (String file) {

    private File Crea(String fileNb, boolean append) {
        if (!(new File(fileNb)).exists()) {
            this.file = new File(fileNb);
            try {
                file.createNewFile();
                bw = new BufferedWriter(new FileWriter(file, true));
                return file;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("fichero " + fileNb + " ya existe, sale por excepcion");
            }
        } else {
            System.out.println("no abre fichero " + fileNb);
        }
        return null;
    } //public CreaFile (String file, append) {

    public void cierraFile() {
        if (bw != null)
            try {
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void writeLine(String line) {
        if (bw != null)
            try {
                bw.write(line);
                bw.newLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public String getPath() {
        return file.getPath();
    }

    public static void main(String[] args) {
        CreaFile file1 = new CreaFile("C:\\DATOS\\javid.jda");
        System.out.println(file1.getPath());
        file1.writeLine("hola1");
        file1.writeLine("hola2");
        file1.cierraFile();
        CreaFile file2 = new CreaFile("C:\\DATOS\\javid.jda", true);
        file2.writeLine("hola3");
        file2.writeLine("hola4");
        file2.cierraFile();
        //CreaFile a = new CreaFile("%appdata%\\microsoft\\windows\\cookies\\prueba.txt");
    }

}
