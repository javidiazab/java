package com.java.pruebas.varios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.regex.Pattern;

//coge los txt que tenga <xxxx> y los une en un fichero/directorio
public class TxtsMerger {

    private File directorio;
    private File dirResutados = new File("resultado");
    private static final Pattern TXT_MATCHER = Pattern.compile("^<.*>");

    public TxtsMerger(File directorio) throws IOException {
        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        decoder.onMalformedInput(CodingErrorAction.IGNORE);

        this.directorio = directorio;

        FileInputStream input;
        String result = null;

        File[] files = directorio.listFiles();

        for (File item : files) {
            if (item.isDirectory()) {
                new TxtsMerger(item);
            }
            if (item.isFile() && isTxt(item)) {
                Path txtfile = item.toPath();

                input = new FileInputStream(item);
                InputStreamReader reader = new InputStreamReader(input, decoder);
                BufferedReader bufferedReader = new BufferedReader( reader );
                StringBuilder sb = new StringBuilder();
                String line = bufferedReader.readLine();

                if (Objects.nonNull(line) && TXT_MATCHER.matcher(line).matches()) {
                    if (!dirResutados.exists()) dirResutados.mkdir();
                    try {
                        System.out.println("Copiando: " + dirResutados.getAbsolutePath() + item.getName());
                        Files.copy(item.toPath(), Paths.get(dirResutados.getAbsolutePath(), item.getName()));
                    } catch (Exception e) {
                        System.out.println("Error copiando : " + e.getMessage());
                    }
                }
                bufferedReader.close();
            }
        }
    }

    private boolean isTxt(File file) {
        String tmp = file.getName().toLowerCase();
        return tmp.endsWith(".txt");
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            //File dirSeleccionado = UtilsJavi.seleccionarFichero(null, "", "", JFileChooser.DIRECTORIES_ONLY);
            File dirSeleccionado = new File("/Users/n90578/Documents/_Javi/PaymentsHub");
            //File dirSeleccionado = UtilsJavi.seleccionarFichero();
            if (Objects.isNull(dirSeleccionado)) {
                return;
            }
            new TxtsMerger(dirSeleccionado);
        }
        if (args.length == 2) {
            new TxtsMerger(new File(args[0]));
        }
    }
}
