package com.java.pruebas.varios;

import java.io.File;
import java.util.Objects;

//Ver si esta clase sirve para algo, si no quitarla. Existe ya un LeerCM
// parece que coge ficeros sacados ya de tratar un cm (tienen ya formato xml) y no se sabe lo qeu hace

public class TratarCM {

    private String nbPGM = "TratarCM";
    private String directorio;
    int index = 0;
    String dirResultados = "\\Resultados" + nbPGM;

    String fechaS = UtilsJavi.getFechaHora();
    String fileJCLs = "JCLs " + fechaS + ".txt";
    String fileDSNs = "DSNs " + fechaS + ".txt";
    String filePGMs = "PGMs " + fechaS + ".txt";
    String filePGMDINDB2s = "PGMSINDB2s " + fechaS + ".txt";
    String fileInterfaces = "Interfaces " + fechaS + ".txt";
    String fileComentarios = "Comentarios " + fechaS + ".txt";
    String PSOGen = "";   //paso general paa que todos lo procesitos lo usen
    String DDGen = "";   //DD general paa que todos lo procesitos lo usen
    int nivelLog = 0; //0 no sale nada, 5 sale todo;

    public TratarCM(String directorio) {
        ProcesarCM(directorio);
    }

    public TratarCM() {
        File dirSeleccionado = UtilsJavi.seleccionarFichero(
                "C:\\",
                "", "", 1);
        if (Objects.isNull(dirSeleccionado))
            return;
        ProcesarCM(dirSeleccionado.getAbsolutePath());
    }

    public void ProcesarCM(String directorio) {

        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);

        this.directorio = directorio;
        File path = new File(directorio);
        //File[] archivos = path.listFiles(); //verificar porque parece que no se usa
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        //if (f.exists()) f.delete();
        //f = new File(directorio + "\\JCL-OPs " + fecha + ".txt");
        //if (f.exists()) f.delete();

        for (int i = 0; i < list.length; i++) {
            File fileJCL = new File(directorio + "\\" + list[i]);
            if (fileJCL.isFile() && !fileJCL.getName().equals(fileJCLs)) {
                try {
                    FileSys fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    String lineFinal = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        if (line.startsWith("//*")) {
                            procesaComentario(line, UtilsJavi.removeExtension(list[i]));
                        } else {
                            lineFinal = UtilsJavi.removeExtension(list[i]) + ";" + line;
                            procesaLine(lineFinal);
                        }
                        //CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs, lineFinal);
                        line = fileSys.readLine();
                    } //while (line != null);
                } catch (java.io.IOException exception) {
                }
                ;
//				System.out.println("Procesando: " + list[i]);
                CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs,
                        UtilsJavi.removeExtension(list[i]));
            } //if (f1.isFile()) {
        } //	for(int i = 0; i < list.length; i++) {
        System.out.println("Proceso Terminado Correctamente");
    } //public RenameFiles (String directorio) {

    public void procesaComentario(String line, String file) {
//		busca COMENTARIOS ***********************************
        CreaFileStatic.log(directorio + dirResultados + "\\" + fileComentarios,
                file + ";" + line);
    }

    public void procesaLine(String line) {
//		busca PASOS ***********************************
        String PSO;
        int indexEXEC = line.indexOf(" EXEC ");
        if (indexEXEC > -1) {
            PSO = "";
            char[] charLinePSO = line.toCharArray();
            boolean finPSO = false;

            for (int indexPSO = 8 + 3; indexPSO < charLinePSO.length && !finPSO; indexPSO++) {
                if (charLinePSO[indexPSO] == ' ')
                    finPSO = true;
                else {
                    PSO = PSO + charLinePSO[indexPSO];
                }
            }
            PSOGen = PSO;
            //UtilsJavi.print("PSO " + indexEXEC +" "+ PSO);
        }
    }

    public static void main(String[] args) {
        new TratarCM("C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\JCLS\\Tablas CM 20130125\\Banesto\\Resultados");
        //new TratarCM();
    }

}
