package com.java.pruebas.varios;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Utils2 {

    public static void escribirLog(String rutaArchivo, String mensaje, int a, int b) {

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {

            fh = new FileHandler(rutaArchivo, true);
            logger.addHandler(fh);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info(mensaje);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void escribirLog(String rutaArchivo, String mensaje) {

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {

            fh = new FileHandler(rutaArchivo, true);
            logger.addHandler(fh);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info(mensaje);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //este hay que quitarle hasta que se ponga un bbuen log. es para que no peten los pgms qe lo usaban
    public static void escribirLog(String mensaje, int a, int b) {

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {

            fh = new FileHandler("c:\\Datos", true);
            logger.addHandler(fh);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info(mensaje);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String... args) {
        for (int i = 1; i < 6; i++) {
            escribirLog("C:\\Datos\\rutaLog\\archivo.log", "MensajePrueba" + i);
            escribirLog("C:\\Datos\\rutaLog\\archivo.log", "MensajePrueba2" + i);
            escribirLog("C:\\Datos\\rutaLog\\archivo.log", "MensajePrueba3" + i);
            escribirLog("C:\\Datos\\rutaLog\\archivo.log", "MensajePrueba4" + i);
            escribirLog("C:\\Datos\\rutaLog\\archivo.log", "MensajePrueba5" + i);
        }
    }
}
