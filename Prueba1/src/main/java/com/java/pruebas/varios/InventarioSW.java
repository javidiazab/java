/* Creado el 09-sep-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

import java.io.*;

//Pasa a Excel el analisis de ARBV de TSO BTMENUGS
//Despues se puede ejecutar el InventarioPGMSDet para obtener info detallada por cada PGM


public class InventarioSW {
    private String nbPGM = "InventarioSW";
    private String DIR_RESULTADOS = "\\Resultados " + nbPGM;
    private String sua = "";
    private String fechaS = UtilsJavi.getFechaHora();
    private String cabTipoObj = "";
    private String cabObj = "";
    private String perim = "";
    private String nivel0 = "000";
    private String nivel1 = "001";
    private String fileMSJs = "MSJs " + fechaS + ".txt";
    private String filePGMs = "PGMs " + fechaS + ".txt";
    private String fileSERs = "SERs " + fechaS + ".txt";
    private String fileTRXs = "TRXs " + fechaS + ".txt";
    private String fileTODO = "TODO " + fechaS + ".txt";

    public InventarioSW(String directorio, String fichero) {
        ProcesaSW(directorio, fichero);
    }

    public InventarioSW() {
        File ficheroSWSeleccionado = UtilsJavi.seleccionarFichero();
        if (ficheroSWSeleccionado == null)
            return;

        //String subApp = JOptionPane.showInputDialog("Subaplicacion");

        ProcesaSW(UtilsJavi.getPath(ficheroSWSeleccionado), ficheroSWSeleccionado.getName());
    }

    private String ProcesaSW(String dirSW, String fileSW) {

        UtilsJavi.CrearDirectorio(dirSW + DIR_RESULTADOS);

        try {
            FileSys fileSys = new FileSys(dirSW + "\\" + fileSW);
            String line = null;
            line = fileSys.readLine();
            while (line != null) {
                if (!line.startsWith("====") && !line.startsWith("----") &&
                        !line.equals("") && !line.startsWith("SUA-MSJ") && line.length() > 50) {
                    procesaLine(line, dirSW);
                }
                line = fileSys.readLine();
                //System.out.println("Procesando: " + line);
            } //while (line != null);
        } catch (IOException exception) {
        }
        ;

        System.out.println("Proceso Terminado Correctamente");
        return "";
    } //public InventarioSW (String directorio) {


    public void procesaLine(String line, String directorio) {
//		busca Tipo y objeto ***********************************

        if (line.startsWith("SUA -->")) {
            sua = line.substring(7, 15); //SUA
            cabTipoObj = line.substring(24, 27); //tipo de objeto
            cabObj = line.substring(16, 24);     // objeto
            perim = line.substring(38, 40);     // en Perimetro o no
            //System.out.println("Obj: " + cabTipoObj + " " + cabObj);
        }

        if (!line.startsWith("SUA -->")) {
//			SACA TODOelfichero a fichero
            CreaFileStatic.log(directorio + DIR_RESULTADOS + "\\" + sua + " " + fileTODO,
                    cabObj + ";" + cabTipoObj + ";" + perim + ";" +
                            trataMSJ(line) + trataSER(line) + trataTRX(line) + trataPGM(line));
        }

//		busca datos de MSJ ***********************************				
        if (!line.startsWith("SUA -->") && cabTipoObj.equals("MSJ")) {
            if ((line.substring(0, 8).equals(sua) &&   //si la parte del MSJ esta informada
                    (line.substring(116, 119).equals(nivel0) ||
                            line.substring(116, 119).equals(nivel1))) ||

                    (line.substring(72, 80).equals(sua) &&  //si la parte del pgm esta informada
                            (line.substring(116, 119).equals(nivel0) ||
                                    line.substring(116, 119).equals(nivel1)))
            ) {
                CreaFileStatic.log(directorio + DIR_RESULTADOS + "\\" +
                        sua + " " + fileMSJs, cabObj + ";" + cabTipoObj + ";" + perim + ";" +
                        trataMSJ(line) + trataSER(line) + trataPGM(line));
            }  //if (line.substring(72, 80).equals(subApp) &&
        }  //if (!line.startsWith("SUA -->") && tipoObj.equals("PGM")) {

//		busca datos de PGM ***********************************		
        if (!line.startsWith("SUA -->") && cabTipoObj.equals("PGM")) {
            if (line.substring(72, 80).equals(sua) &&
                    (line.substring(116, 119).equals(nivel0) ||
                            line.substring(116, 119).equals(nivel1))) {
                CreaFileStatic.log(directorio + DIR_RESULTADOS + "\\" + sua + " " + filePGMs,
                        cabObj + ";" + cabTipoObj + ";" + perim + ";" + trataPGM(line));
            }  //if (line.substring(72, 80).equals(subApp) &&
        }  //if (!line.startsWith("SUA -->") && tipoObj.equals("PGM")) {

//		busca datos de SERVICIO ***********************************		
        if (!line.startsWith("SUA -->") && cabTipoObj.equals("SER")) {
            if (line.substring(20, 28).equals(sua) &&
                    (line.substring(116, 119).equals(nivel0) ||
                            line.substring(116, 119).equals(nivel1))) {
                CreaFileStatic.log(directorio + DIR_RESULTADOS + "\\" + sua + " " + fileSERs,
                        cabObj + ";" + cabTipoObj + ";" + perim + ";" + trataSER(line) + trataPGM(line));
            }  //if (line.substring(72, 80).equals(subApp) &&
        }  //if (!line.startsWith("SUA -->") && tipoObj.equals("PGM")) {

//		busca datos de TRX ***********************************		
        if (!line.startsWith("SUA -->") && cabTipoObj.equals("TRX")) {
            if ((line.substring(40, 48).equals(sua) &&   //si la parte de la trx esta informada
                    (line.substring(116, 119).equals(nivel0))) ||

                    (line.substring(72, 80).equals(sua) &&  //si la parte del pgm esta informada
                            (line.substring(116, 119).equals(nivel0) ||
                                    line.substring(116, 119).equals(nivel1)))
            ) {
                CreaFileStatic.log(directorio + DIR_RESULTADOS + "\\" + sua + " " + fileTRXs,
                        cabObj + ";" + cabTipoObj + ";" + perim + ";" + trataTRX(line) + trataPGM(line));
            }  //if (line.substring(72, 80).equals(subApp) &&
        }  //if (!line.startsWith("SUA -->") && tipoObj.equals("PGM")) {
    }  //public void procesaLine(String line) {

    private static String trataMSJ(String line) {
        //String suaMsj = line.substring(0, 8) + ";" +
        //String msj = line.substring(9, 17) + ";" +
        //String msjPerimetro = line.substring(18, 19) + ";" +
        return
                line.substring(0, 8) + ";" +
                        line.substring(9, 17) + ";" +
                        line.substring(18, 19) + ";";
    }

    private static String trataSER(String line) {
        //String suaServicio = line.substring(20, 28) + ";" +
        //String servicio = line.substring(29, 37) + ";" +
        //String serPerimetro = line.substring(38, 39);
        return
                line.substring(20, 28) + ";" +
                        line.substring(29, 37) + ";" +
                        line.substring(38, 39) + ";";
    }

    private static String trataPGM(String line) {
        ////pgm
        //String suaPgm = line.substring(72, 80);
        //String pgm = line.substring(81, 89);
        //String pgmPerimetro = line.substring(90, 91);
        //Obj
        //String suaObj = line.substring(92, 100);
        //String tipoObj = line.substring(101, 104);
        //String Obj = line.substring(105, 113);
        //String ObjPerimetro = line.substring(114, 115);
        //String nivel = line.substring(116, 119);
        //String entidad = line.substring(120, 145);
        //String entorno = line.substring(146, 148);
        return
                //pgm
                line.substring(72, 80) + ";" +
                        line.substring(81, 89) + ";" +
                        line.substring(90, 91) + ";" +
                        //Obj
                        line.substring(92, 100) + ";" +
                        line.substring(101, 104) + ";" +
                        line.substring(105, 113) + ";" +
                        line.substring(114, 115) + ";" +
                        line.substring(116, 119) + ";" +
                        line.substring(120, 145) + ";" +
                        line.substring(146, 148);
    }

    private static String trataTRX(String line) {
        //String suaTrx1 = line.substring(40, 48);
        //String trx1 = line.substring(49, 53);
        //String trx1Perimetro = line.substring(54, 55);
        //String suaTrx2 = line.substring(56, 64);
        //String trx2 = line.substring(65, 69);
        //String trx2Perimetro = line.substring(70, 71);
        return
                line.substring(40, 48) + ";" +
                        line.substring(49, 53) + ";" +
                        line.substring(54, 55) + ";" +
                        line.substring(56, 64) + ";" +
                        line.substring(65, 69) + ";" +
                        line.substring(70, 71) + ";";
    }

    public static void main(String[] args) {
        //= new InventarioSW("D:\\_ISBAN\\LAB Blanqueo\\Inventarios\\JCLS\\Procesos\\GSW\\SubApp 472",
        //					"DES.P0000.DESFUE.ARB.D100505.H095512.txt");
        //new InventarioSW("D:\\_ISBAN\\LAB Blanqueo\\Inventarios\\JCLS\\Procesos\\GSW\\SubApp 1274",
        //						"DES.P0000.DESABA.ARB.D100518.H141813.txt");
        //new InventarioSW("C:\\DATOS\\_JAVI\\_ISBAN\\Delete\\472","ARBOL1.TXT","00000472");
        new InventarioSW();
    }
}


