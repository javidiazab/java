/*
 * Creado el 09-sep-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

public class DefColTRX {

    static int colTRX = 0;
    static int colOP = 1;
    static int colPGM = 2;
    static int colPANT = 3;
    static int colPF = 4;
    static int colLIT = 5;
    static int colPETI = 6;
    static int colPGMEJ = 7;
    static int colPGMEJEPREV = 8;
    static int colSIGTRX = 9;
    static int colSIGOP = 10;
    static int colMSG = 11;
    static int colINI = 12;

    static String[] colDescriptions = {"colTRX", "colOP",
            "colPGM", "colPANT", "colPF", "colLIT",
            "colPETI", "colPGMEJ", "colPGMEJEPREV",
            "colSIGTRX", "colSIGOP", "colMSG", "colINI"};

    static int nCols = colDescriptions.length;

    public boolean colValid(int col) {
        if (col < 0 || col > nCols) return false;
        else return true;
    }

    public int getIndexTRX() {
        return colTRX;
    }

    public int getIndexOP() {
        return colOP;
    }

    public int getIndexPGM() {
        return colPGM;
    }

    public int getIndexPANT() {
        return colPANT;
    }

    public int getIndexPF() {
        return colPF;
    }

    public int getIndexLIT() {
        return colLIT;
    }

    public int getIndexPETI() {
        return colPETI;
    }

    public int getIndexPGMEJ() {
        return colPGMEJ;
    }

    public int getIndexPGMEJEPREV() {
        return colPGMEJEPREV;
    }

    public int getIndeSIGTRX() {
        return colSIGTRX;
    }

    public int getIndexSIGOP() {
        return colSIGOP;
    }

    public int getIndexMSG() {
        return colMSG;
    }

    public int getIndexINI() {
        return colINI;
    }


    public static void main(String[] args) {
    }
}
