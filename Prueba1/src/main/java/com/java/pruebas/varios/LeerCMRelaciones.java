package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;
//Saca las relacione de dependencias entre JCLS
//El directorio de entrada debe ser el de salida del analisis del CM. Se debe ejecutar por tanto despues de LeerCM


public class LeerCMRelaciones {

    private String nbPGM = "LeerCMRelaciones";
    private String directorio;
    String dirResultados = "\\Resultados" + nbPGM;

    String fechaS = UtilsJavi.getFechaHora();
    String fileJCLs = "JCLs.txt";
    String fileTODOs = "TODOs.txt";
    String fileDEPENDENCIAS = "DEPENDENCIAS.txt";
    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    boolean varIn = false;
    String dependencias[][] = new String[1000][100];
    int lastDependencia = 0;

    public LeerCMRelaciones(String directorio) {
        ProcesarRelaciones(directorio);
    }

    public LeerCMRelaciones() {

        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);

        File dirSeleccionado = UtilsJavi.seleccionarFichero(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\JCLS\\Tablas CM 20130604\\Santander",
                "", "", 1);
        //String dirSeleccionado = UtilsJavi.seleccionarFichero(
        //		"C:\\DATOS\\borrar",
        //		"",	"",	1);

        if (Objects.isNull(dirSeleccionado))
            return;

        ProcesarRelaciones(dirSeleccionado.getAbsolutePath());
    }

    public void ProcesarRelaciones(String directorio) {

        this.directorio = directorio;
        File path = new File(directorio);
        //File[] archivos = path.listFiles(); //verificar porque parece que no se usa
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        //if (f.exists()) f.delete();
        //f = new File(directorio + "\\JCL-OPs " + fecha + ".txt");
        //if (f.exists()) f.delete();

        for (int i = 0; i < list.length; i++) {
            File fileJCL = new File(directorio + "\\" + list[i]);
            if (fileJCL.isFile() && !fileJCL.getName().equals(fileJCLs)
                    && !fileJCL.getName().equals(fileJCLs)) {
                try {
                    FileSys fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    String cadena = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        cadena = UtilsJavi.removeExtension(list[i]);
                        procesaLine(cadena, line);
                        //CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs, lineFinal);
                        line = fileSys.readLine();
                    } //while (line != null);
                } catch (IOException exception) {
                }
                ;
//				System.out.println("Procesando: " + list[i]);

                //graba tambien los jcls que no tienen dependencias o no dependen de ellos nadie
                int padre = buscaCadenaTodo(UtilsJavi.removeExtension(list[i]));
                if (padre < 0) {  //no existe en la tabla de dependencias.
                    dependencias[lastDependencia][0] = UtilsJavi.removeExtension(list[i]);
                    lastDependencia++;
                }

                CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs,
                        UtilsJavi.removeExtension(list[i]));
            } //if (f1.isFile()) {
        } //	for(int i = 0; i < list.length; i++) {
        //imprimeDependencias();
        grabaDependencias();
        System.out.println("Proceso Terminado Correctamente");
    } //public LeerCMRelaciones (String directorio) {


    public void procesaLine(String cadena, String line) {
        if (line.equals("</In>"))
            varIn = false;

        if (varIn) {
            String dependencia = dejarDependencia(line);
            int padre = buscaCadena(dependencia);
            if (padre < 0) {  //no existe en la tabla de dependencias.
                dependencias[lastDependencia][0] = dependencia;
                dependencias[lastDependencia][1] = cadena;
                lastDependencia++;
            } else {   //existe en la tabla de dependencias.
                int ultimoHijo = buscarUltimaOcurrencia(padre);
                dependencias[padre][ultimoHijo + 1] = cadena;
            }
        }

        if (line.equals("<In>"))
            varIn = true;

    } //public void procesaLine(String line)


    public String dejarDependencia(String line) {
        boolean listo = false;
        String dependencia = "";
        for (int i = 0; i < line.length() && !listo; i++) {
            if (line.charAt(i) == '.' || line.charAt(i) == ' ') {
                listo = true;
            } else {
                dependencia = dependencia + line.charAt(i);
            }
        }
        return dependencia;
    }


    public int buscaCadena(String dependencia) {
        int posicion = 0;
        boolean encontrado = false;

        for (int i = 0; i < dependencias.length && !encontrado && dependencias[i][0] != null; i++) {
            if (dependencias[i][0].equals(dependencia)) {
                posicion = i;
                encontrado = true;
            }
        } // for (int i = 0; ; i++) {
        if (encontrado) {
            return posicion;
        }
        return -1;
    } //public int buscaCadena(String cadena) {

    public int buscaCadenaTodo(String dependencia) {
        int posicion = 0;
        boolean encontrado = false;

        for (int padre = 0; padre < dependencias.length && !encontrado && dependencias[padre][0] != null; padre++) {
            for (int hijos = 0; hijos < dependencias[padre].length && !encontrado && dependencias[padre][hijos] != null; hijos++) {
                if (dependencias[padre][hijos].equals(dependencia)) {
                    posicion = padre;
                    encontrado = true;
                }
            }
        } // for (int i = 0; ; i++) {
        if (encontrado) {
            return posicion;
        }
        return -1;
    } //public int buscaCadenaTodo(String cadena) {

    public int buscarUltimaOcurrencia(int posicion) {
        boolean encontrado = false;
        int i = 1;
        for (; i < dependencias[posicion].length && !encontrado && dependencias[posicion][i] != null; i++) ;
        return i - 1;
    }

    public void imprimeDependencias() {
        for (int padre = 0; padre < lastDependencia; padre++) {
            UtilsJavi.print("padre" + padre);
            UtilsJavi.print(dependencias[padre][0]);
            for (int hijos = 1; dependencias[padre][hijos] != null; hijos++) {
                UtilsJavi.print("hijos " + hijos);
                UtilsJavi.print(dependencias[padre][hijos]);
            }
        }
    } //public void imprimeDependencias() {

    public void grabaDependencias() {
        String fila = "";
        for (int padre = 0; padre < lastDependencia; padre++) {
            fila = dependencias[padre][0];
            for (int hijos = 1; dependencias[padre][hijos] != null; hijos++) {
                fila = fila + ';' + dependencias[padre][hijos];
            }
            CreaFileStatic.log(directorio + dirResultados + "\\" + fileDEPENDENCIAS, fila);
        }
    } //public void imprimeDependencias() {

    public static void main(String[] args) {
        new LeerCMRelaciones();
        //new LeerCMRelaciones("C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\JCLS\\Tablas CM 20130604\\Santander\\Resultados");
    } //public static void main(String[] args) {
} //public class LeerCMRelaciones {

