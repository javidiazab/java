package com.java.pruebas.varios;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LeerXsd {
    private String nbPGM = "XsdProcesado";
    private String DIR_RESULTADOS = "\\Resultados " + nbPGM;
    private String sua = "";
    private String fechaS = UtilsJavi.getFechaHora();
    private String cabTipoObj = "";
    private String cabObj = "";
    private String perim = "";
    private String nivel0 = "000";
    private String nivel1 = "001";
    private String fileMSJs = "MSJs " + fechaS + ".txt";
    private String filePGMs = "PGMs " + fechaS + ".txt";
    private String fileSERs = "SERs " + fechaS + ".txt";
    private String fileTRXs = "TRXs " + fechaS + ".txt";
    private String fileTODO = "TODO " + fechaS + ".txt";
    private List<String> xsd;
    private List<Dato> datos;

    public class Dato {
        private String tipo;
        private String nb;

        public Dato(String tipo, String nb) {
            this.nb = nb;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public void setNb(String nb) {
            this.nb = nb;
        }
    }

    public LeerXsd(String directorio, String fichero) {
        processXsd(directorio, fichero);
    }

    public LeerXsd() {
        File ficheroSWSeleccionado = UtilsJavi.seleccionarFichero();
        if (ficheroSWSeleccionado == null)
            return;
        processXsd(UtilsJavi.getPath(ficheroSWSeleccionado), ficheroSWSeleccionado.getName());
    }

    private void processXsd(String dirXsd, String file) {
        loadXsd(dirXsd, file);
        loadAttributes(dirXsd, file);

        System.out.println("Proceso Terminado Correctamente");
    }

    private void loadXsd(String dirXsd, String name) {
        try {
            FileSys fileSys = new FileSys(dirXsd + "\\" + name);
            String line = null;
            line = fileSys.readLine();
            while (line != null) {
                xsd.add(line);
                line = fileSys.readLine();
            }
        } catch (IOException exception) {
        }
        ;
        System.out.println("Cargado XSD");
    }

    private void loadAttributes(String dirXsd, String name) {
        for (String linea : xsd) {

        }
        System.out.println("Cargados atributos");
    }

    public void procesaLine(String line, String directorio) {
        if (line.contains("xs:element")) {
            sua = line.substring(7, 15); //SUA
            cabTipoObj = line.substring(24, 27); //tipo de objeto
            cabObj = line.substring(16, 24);     // objeto
            perim = line.substring(38, 40);     // en Perimetro o no
            //System.out.println("Obj: " + cabTipoObj + " " + cabObj);
        }
        if (line.contains("/xs:element")) {
            sua = line.substring(7, 15); //SUA
            cabTipoObj = line.substring(24, 27); //tipo de objeto
            cabObj = line.substring(16, 24);     // objeto
            perim = line.substring(38, 40);     // en Perimetro o no
            //System.out.println("Obj: " + cabTipoObj + " " + cabObj);
        }
        if (line.contains("xs:complexType")) {
            sua = line.substring(7, 15); //SUA
            cabTipoObj = line.substring(24, 27); //tipo de objeto
            cabObj = line.substring(16, 24);     // objeto
            perim = line.substring(38, 40);     // en Perimetro o no
            //System.out.println("Obj: " + cabTipoObj + " " + cabObj);
        }
        if (line.contains("/xs:complexType")) {
            sua = line.substring(7, 15); //SUA
            cabTipoObj = line.substring(24, 27); //tipo de objeto
            cabObj = line.substring(16, 24);     // objeto
            perim = line.substring(38, 40);     // en Perimetro o no
            //System.out.println("Obj: " + cabTipoObj + " " + cabObj);
        }
    }

    public static void main(String[] args) {
        new LeerXsd("C:\\Users\\N90578.000\\OneDrive - Santander Office 365\\Documents\\Borrar",
                "pacs.008.001.08.xsd");
//         new LeerXsd("D:\\_ISBAN\\LAB Blanqueo\\Inventarios\\JCLS\\Procesos\\GSW\\SubApp 1274",
//        						"DES.P0000.DESABA.ARB.D100518.H141813.txt");
//         new LeerXsd("C:\\DATOS\\_JAVI\\_ISBAN\\Delete\\472","ARBOL1.TXT","00000472");
//        new LeerXsd();
    }
}
