
 /* Creado el 09-sep-09
  *
  * Para cambiar la plantilla para este archivo generado vaya a
  * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
  */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
 package com.java.pruebas.varios;

 import java.io.*;
 import java.util.Objects;


 public class LeerTRX {

//	ESTRUCTURA DE LA TABLA
//	"colTRX", "colOP",
//	"colPGM", "colPANT", "colPF", "colLIT", 
//	"colPETI", "colPGMEJ", "colPGMEJEPREV",
//	"colSIGTRX", "colSIGOP", "colMSG", "colINI"

     int nivelLog = 0; //0 no sale nada, 5 sale todo;
     private String directorio;
     String[][] tablaLoad = new String[2000][13];
     int index = 0;
     DefColTRX col = new DefColTRX();
     boolean EOF = false;
     String dirResultados = "\\Resultados";
     String fechaS = UtilsJavi.getFechaHora();
     String fileTRXs = "TRXs " + fechaS + ".txt";
     String fileTRXsExcell = "TRXsExcell " + fechaS + ".txt";
     String fileTRXOP = "TRXOPs " + fechaS + ".txt";
     String filePGMs = "PGMs " + fechaS + ".txt";
     String filePants = "Pants " + fechaS + ".txt";
     String OPAnterior = null;
     String PantAnterior = null;

     public LeerTRX(String directorio) {
         ProcesarTRXs(directorio);
     }

     public LeerTRX() {
         File dirSeleccionado = UtilsJavi.seleccionarFichero(
                 "C:\\",
                 "", "", 1);
         if (Objects.isNull(dirSeleccionado))
             return;
         ProcesarTRXs(dirSeleccionado.getAbsolutePath());
     }

     public void ProcesarTRXs(String directorio) {
         this.directorio = directorio;
         File path = new File(directorio);
         File[] archivos = path.listFiles(); //verificar
         String[] list;
         list = path.list();

         UtilsJavi.CrearDirectorio(directorio + "\\Resultados");

         //if (f.exists()) f.delete();
         //f = new File(directorio + "\\TRX-OPs " + fecha + ".txt");
         //if (f.exists()) f.delete();

         int indTabla = 0;
         for (int i = 0; i < list.length; i++) {
             File fileTRX = new File(directorio + "\\" + list[i]);
             if (fileTRX.isFile()) {
                 try {
                     FileSys fileSys = new FileSys(directorio + "\\" + list[i]);
                     String line = null;
                     String lineFinal = null;
                     line = fileSys.readLine();
                     while (line != null) {
                         lineFinal = UtilsJavi.removeExtension(list[i]) + " " + line;
                         procesaLine(lineFinal, indTabla++);
                         //CreaFileStatic.log(directorio + dirResultados + "\\" + fileTRXs, lineFinal);
                         line = fileSys.readLine();
                     } //while (line != null);
                 } catch (IOException exception) {
                 }
                 ;

             } //if (f1.isFile()) {
             System.out.println("Procesando: " + list[i]);
         } //	for(int i = 0; i < list.length; i++) {
		

		/*for (int i = 0; i < tablaLoad.length && tablaLoad[i][0] != null; i ++) {
			System.out.println(tablaLoad[i][0]+tablaLoad[i][1]+
			tablaLoad[i][2]+tablaLoad[i][3]+
			tablaLoad[i][4]+tablaLoad[i][5]+
			tablaLoad[i][6]+tablaLoad[i][7]+
			tablaLoad[i][8]+tablaLoad[i][9]+
			tablaLoad[i][10]+tablaLoad[i][11]+tablaLoad[i][12]);}*/
     } //public RenameFiles (String directorio) {

     public void procesaLine(String line, int nlinea) {
         UtilsJavi.escribirLog("0,4" + line.substring(0, 4), 3, nivelLog);
         UtilsJavi.escribirLog("5,6" + line.substring(5, 6), 3, nivelLog);
         UtilsJavi.escribirLog("13,21" + line.substring(13, 21), 3, nivelLog);
         UtilsJavi.escribirLog("26,34" + line.substring(26, 34), 3, nivelLog);
         UtilsJavi.escribirLog("40,43" + line.substring(40, 43), 3, nivelLog);
         UtilsJavi.escribirLog("44,52" + line.substring(44, 52), 3, nivelLog);
         UtilsJavi.escribirLog("53,56" + line.substring(53, 56), 3, nivelLog);
         UtilsJavi.escribirLog("57,58" + line.substring(57, 58), 3, nivelLog);
         UtilsJavi.escribirLog("61,62" + line.substring(61, 62), 3, nivelLog);
         UtilsJavi.escribirLog("65,69" + line.substring(65, 69), 3, nivelLog);
         UtilsJavi.escribirLog("70,71" + line.substring(70, 71), 3, nivelLog);
         UtilsJavi.escribirLog("77,78" + line.substring(77, 78), 3, nivelLog);
         UtilsJavi.escribirLog("79,80" + line.substring(79, 80), 3, nivelLog);


         //genera fichero con Pantallas. Lo hacemos aqui porque todavia no se ha cambiado nada
         if (!line.substring(26, 34).equals("        ")) {
             CreaFileStatic.log(directorio + dirResultados + "\\" + filePants, line);
         }

         // genera fichero con trx/ops. Lo hacemos aqui porque todavia no se ha cambiado nada
         if (!line.substring(5, 6).equals(" ")) {
             CreaFileStatic.log(directorio + dirResultados + "\\" + fileTRXOP, line);
         }

         //si es una op nueva nos quedamos con la pantalla, tenga o no
         if (!line.substring(5, 6).equals(" ")) //&& line.substring(23,34).equals("        "))
             PantAnterior = line.substring(23, 34);

         // Si no hay pant usamos la que habia en la op, hubiera o no
         if (!line.substring(23, 34).equals("        "))
             line = line.substring(0, 23) + PantAnterior + line.substring(34);

         //Nos guardamos la op cuando venga para rellenar luego cuando no venga
         if (!line.substring(5, 6).equals(" ")) {
             OPAnterior = line.substring(5, 6);
         } else {
             line = line.substring(0, 5) + OPAnterior + line.substring(6);
         }

         // cargamos la tabla con los datos que hemos leido y hemos modificado
         tablaLoad[nlinea][DefColTRX.colTRX] = line.substring(0, 4);
         tablaLoad[nlinea][DefColTRX.colOP] = line.substring(5, 6);
         tablaLoad[nlinea][DefColTRX.colPGM] = line.substring(13, 21);
         tablaLoad[nlinea][DefColTRX.colPANT] = line.substring(26, 34);
         tablaLoad[nlinea][DefColTRX.colPF] = line.substring(40, 43);
         tablaLoad[nlinea][DefColTRX.colLIT] = line.substring(44, 52);
         tablaLoad[nlinea][DefColTRX.colPETI] = line.substring(53, 56);
         tablaLoad[nlinea][DefColTRX.colPGMEJ] = line.substring(57, 58);
         tablaLoad[nlinea][DefColTRX.colPGMEJEPREV] = line.substring(61, 62);
         tablaLoad[nlinea][DefColTRX.colSIGTRX] = line.substring(65, 69);
         tablaLoad[nlinea][DefColTRX.colSIGOP] = line.substring(70, 71);
         tablaLoad[nlinea][DefColTRX.colMSG] = line.substring(77, 78);
         tablaLoad[nlinea][DefColTRX.colINI] = line.substring(79, 80);

         // genera fichero con todos los ficheros de trxs
         CreaFileStatic.log(directorio + dirResultados + "\\" + fileTRXs, line);

//		genera fichero con todos los ficheros de trxs para excel
         CreaFileStatic.log(directorio + dirResultados + "\\" + fileTRXsExcell,
                 line.substring(0, 4) + line.substring(5, 6) + line.substring(13, 21) +
                         line.substring(26, 34) + line.substring(40, 43) +
                         line.substring(44, 52) + line.substring(53, 56) +
                         line.substring(57, 58) + line.substring(61, 62) +
                         line.substring(65, 69) + line.substring(70, 71) +
                         line.substring(77, 78) + line.substring(79, 80));

         // genera fichero con Pgms
         if (!line.substring(13, 21).equals("        "))
             CreaFileStatic.log(directorio + dirResultados + "\\" + filePGMs, line);
     }


     public void printTabla() {
         for (int f = 0; f < tablaLoad.length && tablaLoad[f][0] != null; f++) {
             UtilsJavi.print("");
             for (int c = 0; c < DefColTRX.nCols; c++) {
                 UtilsJavi.print(tablaLoad[f][c]);
             }
         }
     }


     public String[][] desdeDonde() {
         // desde donde se llama a una trx/op
         return tablaLoad;
     }

     public String[][] cargarTrx() {
         return tablaLoad;
     }


     public static void main(String[] args) {
         //LeerTRX a = new LeerTRX("E:\\201101 - Migracion Sensible DCPBC\\TRXs");
         //LeerTRX a = new LeerTRX("C:\\DATOS\\_JAVI\\_ISBAN\\LAB Blanqueo\\Nuevos Desarrollos LAB\\Completado\\200912 - Migracion Contabilidad a BKS\\TRX");
         //new LeerTRX("C:\\DATOS\\_JAVI\\_ISBAN\\LAB Blanqueo\\Nuevos Desarrollos LAB\\Pendiente\\200805 - Migracion Blanca I BKS\\20110208 Modificacion a multi\\TRX");
         new LeerTRX();
     }
 }
