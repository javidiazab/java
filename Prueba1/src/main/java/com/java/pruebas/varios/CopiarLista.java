package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;
//import static java.nio.file.StandardCopyOption.*;   // Files.copy(source, target, REPLACE_EXISTING);
//copia los ficheros que vienen en un txt a una carpeta deseada.


public class CopiarLista {
    int nivelLog = 0; //0 no sale nada, 5 sale everything;
    String file;
    String dir;
    FileSys fileSys = null;
    int index = 0;
    boolean EOF = false;


    public CopiarLista(String file, String dir) {
        Proceso(file, dir);
    }

    public CopiarLista() {
        File fileSeleccionado = UtilsJavi.seleccionarFicheroDesc(
                "C:\\",
                "", "", 2, "Fichero a tratar");
        if (Objects.isNull(fileSeleccionado))
            return;
        File dirSeleccionado = UtilsJavi.seleccionarFicheroDesc(
                fileSeleccionado.getAbsolutePath(),
                "", "", 1, "Seleccione destino");
        if (Objects.isNull(fileSeleccionado))
            return;
        Proceso(fileSeleccionado.getAbsolutePath(), dirSeleccionado.getAbsolutePath());
    }

    public void Proceso(String file, String dirInicial) {
        this.file = file;
        this.dir = dirInicial;
        String[] cadenaSeparada = null;
        File fileACopiar = null;

        try {
            fileSys = new FileSys(file);
            String line = fileSys.readLine();
            while (line != null) {
                cadenaSeparada = line.split(";");
                if (cadenaSeparada[2].equals("file")) {
                    fileACopiar = new File(cadenaSeparada[0]);
                    //hacemos esperar un poco para que le de tiempo a crearlo para que se vea en el .exists()
                    //Thread.sleep(4000);

                    if (fileACopiar.exists()) {
                        String archivoDestino = cadenaSeparada[0].substring(2).equals("C:") ?
                                cadenaSeparada[0].substring(2) : cadenaSeparada[0].substring(2);
                        UtilsJavi.copyFile(cadenaSeparada[0], dirInicial + UtilsJavi.getPath(archivoDestino));
                    } else {
                        System.out.println("No se puede copiar: " + cadenaSeparada[0]);

                    }
                }
                if (line != null) line = fileSys.readLine();
            } //while (line != null);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        System.out.println("*** Proceso Terminado Correctamente ***");
    } //public CopiarLista (String directorio) {


    public static void main(String[] args) {
        //new CopiarLista("C:\\DATOS\\Prueba\\Pruebas copiar files\\copiar.txt", "C:\\DATOS\\Prueba\\Pruebas copiar files\\Resultados");
        //new CopiarLista("C:\\DATOS\\_Javi\\Prueba\\Pruebas copiar files\\Prueba1.txt", "C:\\DATOS\\_Javi\\Prueba\\Pruebas copiar files\\Resultados");
        new CopiarLista();
    }
}

