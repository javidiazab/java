package com.java.pruebas.varios;

import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;

/*
The levels in descending order are:
        SEVERE (highest value)
        WARNING
        INFO
        CONFIG
        FINE
        FINER
        FINEST (lowest value)
*/

public class LoggerPrueba {

    public static void main(String[] args) {

        Logger log2 = Logger.getLogger("Log");
        FileHandler fhand;

        try {

            fhand = new FileHandler("c:\\Datos\\ArchivoLog.log", true);
            log2.addHandler(fhand);
            log2.setLevel(Level.ALL);
            SimpleFormatter formatter = new SimpleFormatter();
            fhand.setFormatter(formatter);
            //log2.log(Level.OFF, "My 7th log");
            log2.log(Level.SEVERE, "My 1 log");
            log2.log(Level.WARNING, "My 2 log");
            log2.log(Level.INFO, "My 3 log");
            log2.log(Level.CONFIG, "My 4 log");
            log2.log(Level.FINE, "My 5 log");
            log2.log(Level.FINER, "My 6 log");
            log2.log(Level.FINEST, "My 7 log");

            //log2.log(Level.OFF, "My 7th log");


        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}