package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;

public class LeerES {
    // pgm que lee un txt con un pgm cobol y extrae las selects
    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    private String directorio;
    FileSys fileSys = null;
    int index = 0;
    boolean EOF = false;
    String dirResultadosES = "\\Resultados ES";
    String fechaS = UtilsJavi.getFechaHora();

    public LeerES(String directorio) {
        ProcesarCOBOL(directorio);
    }

    public LeerES() {
        File dirSeleccionado = UtilsJavi.seleccionarFichero(
                "C:\\",
                "", "", 1);
        if (Objects.isNull(dirSeleccionado))
            return;
        ProcesarCOBOL(dirSeleccionado.getAbsolutePath());
    }

    public void ProcesarCOBOL(String directorio) {
        this.directorio = directorio;
        File path = new File(directorio);
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultadosES);

        for (int i = 0; i < list.length; i++) {
            File fileTRX = new File(directorio + "\\" + list[i]);
            if (fileTRX.isFile()) {
                try {
                    fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        procesaCOPY(line, UtilsJavi.removeExtension(list[i]) + ".txt");
                        if (line != null)
                            line = fileSys.readLine();
                    } //while (line != null);
                } catch (IOException exception) {
                }
                ;

            } //if (f1.isFile()) {
            System.out.println("Proceso Terminado Correctamente");
        } //	for(int i = 0; i < list.length; i++) {
    } //public RenameFiles (String directorio) {

    public void procesaCOPY(String line, String pgm) {
        if (line.contains(" COPY ") && !line.substring(6, 7).equals("*")) {
            CreaFileStatic.log(directorio + dirResultadosES + "\\" + pgm, line.substring(7, 72).replace(".", " ").trim());
        }
    }


    public static void main(String[] args) {
        new LeerES();
    }
}
