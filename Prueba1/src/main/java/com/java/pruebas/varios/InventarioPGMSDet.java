package com.java.pruebas.varios;

import java.io.*;


//A partir del fichero TODO generado por el InventarioSW obtiene tantos ficheros txt como programas existentes. En ellos se especifica
//las rutinas, entidades, itfs, ites y sqls a las que se accede. Si se introduce un fichero con descripciones de aplicaciones, 
//obsoletos, pgms en desarrollo, de descripciones de pgms y de sqls y tambien las incorpora.

public class InventarioPGMSDet {
    private String nbPGM = "InventarioPGMSDet";

    String DIR_RESULTADOS = "\\DetallePgms " + nbPGM;
    String sua = "";
    String fechaS = UtilsJavi.getFechaHora();
    String nivel0 = "000";
    String nivel1 = "001";
    String pgmTratando = "";
    String pgmAnterior = "";

    String[][] PGMSDescripciones = new String[2000][3];
    String[][] PGMSObsoletos = new String[1000][3];
    String[][] PGMSDesarrollo = new String[1000][3];
    String[][] APISUAS = new String[500][4];
    String[][] TablaEnt = new String[20][2];
    String[][] TablaPgms = new String[20][2];
    String[][] TablaITFs = new String[20][2];
    String[][] TablaITEs = new String[20][2];
    File[] TablaSQLs = new File[1000];
    int indENT = 0;
    int indPGM = 0;
    int indITF = 0;
    int indITE = 0;
    int indSQL = 0;

    public InventarioPGMSDet(String directorio, String fichero) {
        ProcesaSW(directorio, fichero);
    }

    public InventarioPGMSDet() {
        File ficheroSWSeleccionado = UtilsJavi.seleccionarFicheroDesc(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472\\Resultados",
                "", "", 2, "Seleccionar fichero a tratar");
        if (ficheroSWSeleccionado == null)
            return;
        File ficheroSel = new File(ficheroSWSeleccionado.getAbsolutePath());
//      procesamos el fichero despues de cargar las tablas de descripciones y aplicaciones

        File ficheroDescripciones = UtilsJavi.seleccionarFicheroDesc(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472",
                "", "", 2, "Seleccionar fichero con Descripciones de PGMS");
        if (ficheroDescripciones != null) {
            File ficheroDesc = new File(ficheroDescripciones.getAbsolutePath());
            CargaDescripciones(UtilsJavi.getPath(ficheroDesc), ficheroDesc.getName());
        }


        File ficheroAplicaciones = UtilsJavi.seleccionarFicheroDesc(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472",
                "", "", 2, "Seleccionar fichero con descripcion de APIS y SUAS");
        if (ficheroAplicaciones != null) {
            File ficheroAPIs = new File(ficheroAplicaciones.getAbsolutePath());
            CargaAplicaciones(UtilsJavi.getPath(ficheroAPIs), ficheroAPIs.getName());
        }

        File directorioSQLs = UtilsJavi.seleccionarFicheroDesc(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472",
                "", "", 1, "Seleccionar directorio de SQLs");
        if (directorioSQLs != null) {
            CargaSQLs(directorioSQLs.getAbsolutePath());
        }

        File ficheroObsoletos = UtilsJavi.seleccionarFicheroDesc(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472",
                "", "", 2, "Seleccionar fichero con PGMS Obsoletos");
        if (ficheroObsoletos != null) {
            File ficheroObs = new File(ficheroObsoletos.getAbsolutePath());
            CargaObsoletos(UtilsJavi.getPath(ficheroObs), ficheroObs.getName());
        }

        File ficheroDesarrollo = UtilsJavi.seleccionarFicheroDesc(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472",
                "", "", 2, "Seleccionar fichero con PGMS en Desarrollo");
        if (ficheroDesarrollo != null) {
            File ficheroDesa = new File(ficheroDesarrollo.getAbsolutePath());
            CargaDesarrollo(UtilsJavi.getPath(ficheroDesa), ficheroDesa.getName());
        }

        ProcesaSW(UtilsJavi.getPath(ficheroSel), ficheroSel.getName());
    }

    String ProcesaSW(String dirSW, String fileSW) {

        UtilsJavi.CrearDirectorio(dirSW + DIR_RESULTADOS);


        // tratar linea final?????


        try {
            FileSys fileSys = new FileSys(dirSW + "\\" + fileSW);
            String line = null;
            line = fileSys.readLine();
            while (line != null) {
                if (line.substring(9, 12).equals("PGM") && line.substring(132, 135).equals("000")) {
                    pgmTratando = line.substring(0, 8).trim();
                    procesaLine(line, dirSW);
                }
                line = fileSys.readLine();
                //System.out.println("Procesando: " + line);
            } //while (line != null);
        } catch (IOException exception) {
        }
        ;

        System.out.println("Proceso Terminado Correctamente");
        return "";
    } //public InventarioSW (String directorio) {


    public void procesaLine(String line, String directorio) {
        if (!pgmTratando.equals(pgmAnterior)) {
            if (!pgmAnterior.equals("")) {
                String ficheroAnterior = directorio + DIR_RESULTADOS + "\\" + pgmAnterior + ".txt";

                CreaFileStatic.log(ficheroAnterior, "<Rutinas accedidas>");
                if (TablaPgms[0][0].equals("")) {
                    CreaFileStatic.log(ficheroAnterior, "N/A");
                } else {  //(TablaPgms[0][0].equals("")) {
                    for (indPGM = 0; indPGM < TablaPgms.length && !TablaPgms[indPGM][0].equals(""); indPGM++) {
                        CreaFileStatic.log(ficheroAnterior, TablaPgms[indPGM][0].trim() + buscaAPI(TablaPgms[indPGM][1]));
                        //	" (de la SUA " + TablaPgms[indPGM][1] + ")");
                    } //(indPGM = 0; indPGM < TablaPgms; indPGM++) {
                } //(TablaPgms[0][0].equals("")) {
                CreaFileStatic.log(ficheroAnterior, "</Rutinas accedidas>");

                CreaFileStatic.log(ficheroAnterior, "");
                CreaFileStatic.log(ficheroAnterior, "<Entidades accedidas>");
                if (TablaEnt[0][0].equals("")) {
                    CreaFileStatic.log(ficheroAnterior, "N/A");
                } else {  //(TablaEnt[0][0].equals("")) {
                    for (indENT = 0; indENT < TablaEnt.length && !TablaEnt[indENT][0].equals(""); indENT++) {
                        CreaFileStatic.log(ficheroAnterior, TablaEnt[indENT][0].trim() + buscaAPI(TablaEnt[indENT][1]));
                        //	" (de la SUA " + TablaEnt[indENT][1] + ")");
                    } //(indENT = 0; indENT < TablaEnt.length; indENT++) {
                } //(TablaEnt[0][0].equals("")) {
                CreaFileStatic.log(ficheroAnterior, "</Entidades accedidas>");

                CreaFileStatic.log(ficheroAnterior, "");
                CreaFileStatic.log(ficheroAnterior, "<Sentencias DB2>");
                for (indSQL = 0; indSQL < TablaSQLs.length && TablaSQLs[indSQL].toString() != null; indSQL++) {
                    //CreaFileStatic.log(ficheroAnterior, TablaSQLs[indSQL].trim() + buscaSQL(TablaSQLs[indSQL]));
                    CreaFileStatic.log(ficheroAnterior, "HAY SQLS");
                    //			" (de la SUA " + TablaSQLs[indSQL][1] + ")");
                } //(indSQL = 0; indSQL < TablaSQLs; indSQL++) {
                CreaFileStatic.log(ficheroAnterior, "</Sentencias DB2>");

                CreaFileStatic.log(ficheroAnterior, "");
                CreaFileStatic.log(ficheroAnterior, "<ITFs accedidas>");
                if (TablaITFs[0][0].equals("")) {
                    CreaFileStatic.log(ficheroAnterior, "N/A");
                } else {  //(TablaITFs[0][0].equals("")) {
                    for (indITF = 0; indITF < TablaITFs.length && !TablaITFs[indITF][0].equals(""); indITF++) {
                        CreaFileStatic.log(ficheroAnterior, TablaITFs[indITF][0].trim() + buscaAPI(TablaITFs[indITF][1]));
                        //	" (de la SUA " + TablaITFs[indITF][1] + ")");
                    } //(indITE = 0; indITF < TablaITFs; indITF++) {
                } //(TablaITFs[0][0].equals("")) {
                CreaFileStatic.log(ficheroAnterior, "</ITFs accedidas>");

                CreaFileStatic.log(ficheroAnterior, "");
                CreaFileStatic.log(ficheroAnterior, "<ITEs accedidas>");
                if (TablaITEs[0][0].equals("")) {
                    CreaFileStatic.log(ficheroAnterior, "N/A");
                } else {  //(TablaITEs[0][0].equals("")) {
                    for (indITE = 0; indITE < TablaITEs.length && !TablaITEs[indITE][0].equals(""); indITE++) {
                        CreaFileStatic.log(ficheroAnterior, TablaITEs[indITE][0].trim() + buscaAPI(TablaITEs[indITE][1]));
                        //			" (de la SUA " + TablaITEs[indITE][1] + ")");
                    } //(indENT = 0; indENT < TablaITEs; indENT++) {
                } //(TablaITEs[0][0].equals("")) {
                CreaFileStatic.log(ficheroAnterior, "</ITEs accedidas>");

                CreaFileStatic.log(ficheroAnterior, "");
                CreaFileStatic.log(ficheroAnterior, "<Comentarios>");
                CreaFileStatic.log(ficheroAnterior, "N/A");
                CreaFileStatic.log(ficheroAnterior, "</Comentarios>");
            }

            pgmAnterior = pgmTratando;
            //imprimir lo anterior
            limpiarDatos();

            String fichero = directorio + DIR_RESULTADOS + "\\" + pgmTratando + ".txt";

            CreaFileStatic.log(fichero, "<Nombre F�sico>");
            CreaFileStatic.log(fichero, pgmTratando);
            CreaFileStatic.log(fichero, "</Nombre F�sico>");
            CreaFileStatic.log(fichero, "");
            CreaFileStatic.log(fichero, "<Tipo>");
            if (line.substring(2, 3).equals("B") || line.substring(2, 3).equals("C"))
                CreaFileStatic.log(fichero, "Batch");
            if (line.substring(2, 3).equals("D") || line.substring(2, 3).equals("Z"))
                CreaFileStatic.log(fichero, "Online");
            CreaFileStatic.log(fichero, "</Tipo>");
            CreaFileStatic.log(fichero, "");
            CreaFileStatic.log(fichero, "<Descripcion>");
            if (buscaObsoletos(pgmTratando).equals("")) {
            }
            if (buscaDesarrollo(pgmTratando).equals("")) {
            }
            CreaFileStatic.log(fichero, buscaDescripcion(pgmTratando));
            CreaFileStatic.log(fichero, "</Descripcion>");
            CreaFileStatic.log(fichero, "");
            CreaFileStatic.log(fichero, "<Entrada>");
            CreaFileStatic.log(fichero, "N/A");
            CreaFileStatic.log(fichero, "</Entrada>");
            CreaFileStatic.log(fichero, "");
            CreaFileStatic.log(fichero, "<Salida>");
            CreaFileStatic.log(fichero, "N/A");
            CreaFileStatic.log(fichero, "</Salida>");
            CreaFileStatic.log(fichero, "");

            tratarObjetos(line);

        } else { //(pgmTratando.equals(pgmAnterior) {
            tratarObjetos(line);
        }  //(!pgmTratando.equals(pgmAnterior) {


    }  //public void procesaLine(String line) {

    public void CargaSQLs(String directorio) {
        //Directorio con un txt por cada programa con sentencias SQL
        File path = new File(directorio);
        String[] list;
        list = path.list();
        int indTabla = 0;
        for (int i = 0; i < list.length; i++) {
            File fileSQL = new File(directorio + "\\" + list[i]);
            if (fileSQL.isFile()) {
                TablaSQLs[indTabla++] = fileSQL;
            } //if (f1.isFile()) {
        } //	for(int i = 0; i < list.length; i++) {
    } //public


    public void CargaDescripciones(String dirSW, String file) {
        //ITE;ITE01685;OBTENCI�N DE DATOS PARA EL BANCO DE ESPA
        //MSJ;005422;MANTENIMIENTO DE LIMITES PARA LOS DISTIN
        //MSJ;005426;LISTA DE LA TABLA DE LIMITES DE SELECCIO
        try {
            FileSys fileSys = new FileSys(dirSW + "\\" + file);
            String line = null;
            line = fileSys.readLine();
            int ind = 0;
            while (line != null) {
                PGMSDescripciones[ind++] = line.split(";");
                line = fileSys.readLine();
            } //while (line != null);
        } catch (IOException exception) {
            UtilsJavi.print("Error leyendo fichero de descripciones");
        }
        ;
    }   //public void CargaDescripciones(String dirSW, String fileDesc){

    public void CargaAplicaciones(String dirSW, String file) {
        //SUAS ANALIZ   DESCRIPCION SUA     COD-APL    DESCRIP. APLICACION
        //----------- -------------------- ---------- ---------------------
        //  00000472  BLANQUEO DE DINERO    00000357   C.P.B.C
        //
        //SUAS DEPEND   DESCRIPCION SUA     COD-APL    DESCRIP. APLICACION
        //----------- -------------------- ---------- ---------------------
        //  00000051  CONSOLIDAC. FINANC.   00000010   CONSOLIDACION
        //  00000100  LIQUIDACION CUENTAS   00000021   LIQUIDACIONES
        try {
            FileSys fileSys = new FileSys(dirSW + "\\" + file);
            String line = null;
            line = fileSys.readLine();
            int ind = 0;
            while (line != null) {
                if (line.startsWith("  ")) {
                    APISUAS[ind][0] = line.substring(2, 10);
                    APISUAS[ind][1] = line.substring(12, 32);
                    APISUAS[ind][2] = line.substring(35, 43);
                    APISUAS[ind][3] = line.substring(45, line.length());
                    ind++;
                }
                line = fileSys.readLine();
            } //while (line != null);
        } catch (IOException exception) {
            UtilsJavi.print("Error leyendo fichero de aplicaciones");
        }
        ;

    }   //public void CargaAplicaciones(String dirSW, String fileAPIs){


    public void CargaObsoletos(String dirSW, String file) {
        try {
            FileSys fileSys = new FileSys(dirSW + "\\" + file);
            String line = null;
            line = fileSys.readLine();
            int ind = 0;
            while (line != null) {
                PGMSObsoletos[ind++] = line.split(";");
                line = fileSys.readLine();
            } //while (line != null);
        } catch (IOException exception) {
            UtilsJavi.print("Error leyendo fichero de Obsoletos");
        }
        ;
    }   //public void CargaOBsoletos(String dirSW, String fileDesc){


    public void CargaDesarrollo(String dirSW, String file) {
        try {
            FileSys fileSys = new FileSys(dirSW + "\\" + file);
            String line = null;
            line = fileSys.readLine();
            int ind = 0;
            while (line != null) {
                PGMSDesarrollo[ind++] = line.split(";");
                line = fileSys.readLine();
            } //while (line != null);
        } catch (IOException exception) {
            UtilsJavi.print("Error leyendo fichero de Pgms en Desarrollo");
        }
        ;
    }   //public void CargaDesarrollo(String dirSW, String fileDesc){


    public void limpiarDatos() {
        for (indENT = 0; indENT < TablaEnt.length; indENT++) {
            TablaEnt[indENT][0] = "";
            TablaEnt[indENT][1] = "";
        }
        for (indPGM = 0; indPGM < TablaPgms.length; indPGM++) {
            TablaPgms[indPGM][0] = "";
            TablaPgms[indPGM][1] = "";
        }
        for (indITF = 0; indITF < TablaITFs.length; indITF++) {
            TablaITFs[indITF][0] = "";
            TablaITFs[indITF][1] = "";
        }
        for (indITE = 0; indITE < TablaITEs.length; indITE++) {
            TablaITEs[indITE][0] = "";
            TablaITEs[indITE][1] = "";
        }
        indENT = 0;
        indPGM = 0;
        indITF = 0;
        indITE = 0;

    } //public void limpiarDatos() {

    public void tratarObjetos(String line) {
        if (line.substring(117, 120).equals("ENT")) {
            TablaEnt[indENT][0] = line.substring(136, 160);
            TablaEnt[indENT][1] = line.substring(108, 116);
            indENT++;
        }
        if (line.substring(117, 120).equals("PGM")) {
            TablaPgms[indPGM][0] = line.substring(121, 129);
            TablaPgms[indPGM][1] = line.substring(108, 116);
            indPGM++;
        }
        if (line.substring(117, 120).equals("ITF")) {
            TablaITFs[indITF][0] = line.substring(121, 129);
            TablaITFs[indITF][1] = line.substring(108, 116);
            indITF++;
        }
        if (line.substring(117, 120).equals("ITE")) {
            TablaITEs[indITE][0] = line.substring(121, 129);
            TablaITEs[indITE][1] = line.substring(108, 116);
            indITE++;
        }
    } //public void tratarObjetos() {

    public String buscaDescripcion(String pgm) {
        if (!pgm.equals("") && pgm != null) {
            for (int ind = 0; ind < PGMSDescripciones.length && PGMSDescripciones[ind][1] != null; ind++) {
                if (PGMSDescripciones[ind][1].equals(pgm)) {
                    return PGMSDescripciones[ind][2];
                } //if (PGMSDescripciones[ind][1].equals(pgm
            } //for (int ind = 0; ind < PGMSDescripciones.length && !PGMSDes
        }
        return "";
    }

    public String buscaAPI(String sua) {
        if (!sua.equals("") && sua != null) {
            for (int ind = 0; ind < APISUAS.length && APISUAS[ind][0] != null; ind++) {
                if (APISUAS[ind][0].equals(sua)) {
                    return " de la SUA: " + APISUAS[ind][0].trim() + " " + APISUAS[ind][1].trim() + " (Aplicacion " +
                            APISUAS[ind][2].trim() + " " + APISUAS[ind][3].trim() + ")";
                } //if (APISUAS[ind][0].equals(su
            } //for (int ind = 0; ind < API
        } //if (!sua.equals("") && su
        return "";

    }

    public String buscaObsoletos(String pgm) {
        if (!pgm.equals("") && pgm != null) {
            for (int ind = 0; ind < PGMSObsoletos.length && PGMSObsoletos[ind][1] != null; ind++) {
                if (PGMSObsoletos[ind][1].equals(pgm)) {
                    return PGMSObsoletos[ind][2];
                } //if (PGMSObsoletos[ind][1].equals(pgm
            } //for (int ind = 0; ind < PGMSObsoletos && !PGMSDes
        }
        return "";
    }

    public String buscaDesarrollo(String pgm) {
        if (!pgm.equals("") && pgm != null) {
            for (int ind = 0; ind < PGMSDesarrollo.length && PGMSDesarrollo[ind][1] != null; ind++) {
                if (PGMSDesarrollo[ind][1].equals(pgm)) {
                    return PGMSDesarrollo[ind][2];
                } //if (PGMSDesarrollo[ind][1].equals(pgm
            } //for (int ind = 0; ind < PGMSDesarrollo && !PGMSDes
        }
        return "";
    }


    public static void main(String[] args) {
        new InventarioPGMSDet();
    }
}


