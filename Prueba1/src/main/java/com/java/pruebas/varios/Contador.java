/*
 * Creado el 02-jun-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author jdiazaba
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

public class Contador {
    static int contadorFile;
    static int contadorDirectory;


    public static String addFile(int valor) {
        contadorFile = contadorFile + valor;
        return pad(contadorFile);
    }

    public static String getFile() {
        return pad(contadorFile);
    }

    public static String addDirectory(int valor) {
        contadorDirectory = contadorDirectory + valor;
        return pad(contadorFile);
    }

    public static String getDirectory() {
        return pad(contadorDirectory);
    }

//	public static String padRight(String s, int n) {
//		 return String.format("%1$-" + n + "s", s);  
//	}
//
//	public static String padLeft(String s, int n) {
//			return String.format("%1$#" + n + "s", s);
//	}

    public static String pad(int n) {
        String s = "";
        if (n < 10) s = "00000" + n;
        if (n > 9 && n < 100) s = "0000" + n;
        if (n > 99 && n < 1000) s = "000" + n;
        if (n > 999 && n < 10000) s = "00" + n;
        if (n > 9999 && n < 100000) s = "0" + n;
        if (n > 99999 && n < 1000000) s = "" + n;
        return s;
    }

    public static void main(String[] args) {
    }
}
