 /*
  * Creado el 28-may-09
  *
  * Para cambiar la plantilla para este archivo generado vaya a
  * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
  */

/**
 * @author jdiazaba
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
 package com.java.pruebas.varios;

 import java.io.*;

 public class RenameFiles {

     int nivelLog = 0; //0 no sale nada, 5 sale todo;
     private String directorio;
     private String dirPrefix = "AA";
     private String nbFile = "Aero000";

     private String chequeaExt(String fileCheck) {
         String fileChecka = "";
         fileChecka = fileCheck;
         String ext = "";

         if (fileChecka.endsWith(".jpg"))
             fileChecka = nbFile + Contador.getFile() + ".dl1";
         else if (fileChecka.endsWith(".dl1"))
             fileChecka = nbFile + Contador.getFile() + ".jpg";
         else if (fileChecka.endsWith(".bmp"))
             fileChecka = nbFile + Contador.getFile() + ".dl2";
         else if (fileChecka.endsWith(".dl2"))
             fileChecka = nbFile + Contador.getFile() + ".bmp";
         else if (fileChecka.endsWith(".txt"))
             fileChecka = nbFile + Contador.getFile() + ".txs";
         else if (fileChecka.endsWith(".txs"))
             fileChecka = nbFile + Contador.getFile() + ".txt";
         return fileChecka;
     }

     private boolean rename(String viejo, String nuevo) {
         File oldFileName = new File(viejo);
         File newFileName = new File(nuevo);
         boolean resultado = oldFileName.renameTo(newFileName);
         return resultado;
     }

     private boolean rename(File viejo, File nuevo) {
         boolean resultado = viejo.renameTo(nuevo);
         return resultado;
     }

     public RenameFiles() {
         new RenameFiles(".");
     }

     public RenameFiles(String directorio) {
         this.directorio = directorio;
         File path = new File(directorio);
         File[] archivos = path.listFiles(); //verificar
         String[] list;
         list = path.list();

         for (int i = 0; i < list.length; i++) {
             File old = new File(path.getAbsolutePath() + "\\" + list[i]);
             if (old.isDirectory()) {
                 String nbDirectorio = dirPrefix + Contador.getDirectory();
                 rename(path.getAbsolutePath() + "\\" + list[i], path.getAbsolutePath() + "\\" + nbDirectorio);
                 Contador.addDirectory(1);
                 System.out.println(path.getAbsolutePath() + "\\" + list[i] + "; " + path.getAbsolutePath() + "\\" + nbDirectorio);
                 CreaFileStatic.log(path.getAbsolutePath() + "\\" + list[i] + "; " + path.getAbsolutePath() + "\\" + nbDirectorio, 1, nivelLog);
                 new RenameFiles(path.getAbsolutePath() + "\\" + nbDirectorio);
             }
             if (old.isFile()) {
                 rename(path.getAbsolutePath() + "\\" + list[i], path.getAbsolutePath() + "\\" + chequeaExt(list[i]));
                 System.out.println(path.getAbsolutePath() + "\\" + list[i] + "; " + path.getAbsolutePath() + "\\" + chequeaExt(list[i]));
                 CreaFileStatic.log(path.getAbsolutePath() + "\\" + list[i] + "; " + path.getAbsolutePath() + "\\" + chequeaExt(list[i]), 1, nivelLog);
                 Contador.addFile(1);
             } //if (f1.isFile()) {
             System.out.println(list[i]);
         } //	for(int i = 0; i < list.length; i++) {


     } //public RenameFiles (String directorio) {


     public static void main(String[] args) {
         if (args.length == 0) {
             //RenameFiles a = new RenameFiles(".\\Pruebas");
             File path = UtilsJavi.seleccionarFichero();
             String directorio = path.getAbsolutePath();
             new RenameFiles(directorio);
         }
         if (args.length == 1) {
             new RenameFiles(args[0]);
         }

//****************************************************************

//******************************************************************
/*  try{
	FileSys a = new FileSys("PruebaFileSys.txt");
	System.out.println(a.readLine());
	System.out.println(a.readLine());
	System.out.println(a.readLine());
	a.close();
	String [] tabla = null;
	tabla = a.read();
	System.out.println(tabla[1]);
  } catch (java.io.IOException exception){};
*/
//********************************************************************
//	LeerXML a = new LeerXML("D:\\wsad_workspace_S1\\javi\\a.xml");
//	********************************************************************
         //LeerTRX a = new LeerTRX("D:\\_ISBAN\\TRX");
         //LeerTRX a = new LeerTRX("I:\\Fotos\\Bruno");
         //LeerTRX a = new LeerTRX("D:\\_ISBAN\\Delete\\NAVEGACION modificado 20091128");
//	********************************************************************

     } //main
 } //RenameFiles