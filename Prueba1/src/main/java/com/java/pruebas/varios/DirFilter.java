/*
 * Creado el 28-may-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author jdiazaba
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

import java.io.*;

//Ya no se usa a partir de la 1.6 ahora se usa
//javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
//javax.swing.filechooser.FileNameExtensionFilter filter = new javax.swing.filechooser.FileNameExtensionFilter(extensionDescripcion, extension);
//chooser.addChoosableFileFilter(filter);


import java.util.regex.Pattern;

public class DirFilter {
    private Pattern pattern;

    public DirFilter(String regex) {
        pattern = Pattern.compile(regex);
    }

    public boolean accept(File dir, String name) {
//	   Strip path information, search for regex:
        return pattern.matcher(
                new File(name).getName()).matches();
    }
}
