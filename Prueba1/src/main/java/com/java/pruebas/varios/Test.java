package com.java.pruebas.varios;

public class Test {
    public static void main(String args[]) {


        String linea = "123456789abcd";
        String caracter = "";
        String columnasClave = "1,2,3";
        String clave = "";
        String[] colsClave = columnasClave.split(",");

        for (int i = 0; i < colsClave.length; i = i + 2) {
            if (i < colsClave.length + 2) {  // para comprobar que podemos ir 2 pos mas alla debido al formato pos ini, longitud
                if (Integer.parseInt(colsClave[i]) - 1 + Integer.parseInt(colsClave[i + 1]) <= linea.length()) { //para comprobar que no nos vamos mas alla de la linea leida
                    UtilsJavi.print("Integer.parseInt(colsClave[i]) - 1: " + (Integer.parseInt(colsClave[i]) - 1));
                    clave = clave + linea.substring(Integer.parseInt(colsClave[i]) - 1,
                            Integer.parseInt(colsClave[i]) - 1 + Integer.parseInt(colsClave[i + 1]));
                }
            }
        }

        UtilsJavi.print("Clave: " + clave);

        //*******************************************************

        UtilsJavi.print("\n");
        String cadena2 = "C'12345678'";
        UtilsJavi.print(cadena2.substring(2, cadena2.length() - 1));
        //*************************************************
        UtilsJavi.print("\n");
        String cadena1 = "C\'holaguapi\'";
        UtilsJavi.print(cadena1);
        UtilsJavi.print(cadena1.length());
        UtilsJavi.print(cadena1.substring(2, cadena1.length() - 1));
        UtilsJavi.print(cadena1.length() - 3);

        //********************************************
        UtilsJavi.print("\n");
        String cadena = "cadena";
        for (int i = 0; i < 5; i++) {
            cadena = cadena + "-";
        }
        System.out.print(cadena);

        //*****************************************
        String a = "1X";
        System.out.print("\n");
        System.out.print(a.substring(0, a.indexOf("X")));


        //************************************
        boolean listo = false;
        String line = "FXCCHK0    ODQATE        ";
        String dependencia = "";
        for (int i = 0; i < line.length() && !listo; i++) {
            if (line.charAt(i) == '.' || line.charAt(i) == ' ') {
                listo = true;
            } else {
                dependencia = dependencia + line.charAt(i);
            }
        }
        System.out.print("\nResultado: " + dependencia);
    }
}
