/* Creado el 09-sep-09
 *
 * Saca info de jcls como DDS, pgms etc
 * */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.java.pruebas.varios;

//Procesa JCLS y da toda la informacion de ellos

import java.io.*;
import java.util.Objects;

public class LeeJCL {

    private String nbPGM = "LeeJCL";
    private String directorio;
    int index = 0;
    String dirResultados = "\\Resultados" + nbPGM;

    String fechaS = UtilsJavi.getFechaHora();
    String fileJCLs = "JCLs " + fechaS + ".txt";
    String fileDSNs = "DSNs " + fechaS + ".txt";
    String filePGMs = "PGMs " + fechaS + ".txt";
    String analisisJCLS = "AnalisisJCLs " + fechaS + ".txt";
    String filePGMDINDB2s = "PGMSINDB2s " + fechaS + ".txt";
    String fileInterfaces = "Interfaces " + fechaS + ".txt";
    String fileComentarios = "Comentarios " + fechaS + ".txt";
    String PSOGen = "";   //paso general paa que todos lo procesitos lo usen
    String DDGen = "";   //DD general paa que todos lo procesitos lo usen
    int nivelLog = 0; //0 no sale nada, 5 sale todo;
    String pasoActual = "";

    //tablaDSN = compuesta del nb del fichero, luego dice si es E ntrada y la ultima col S alida
    private String tablaDSN[][] = new String[1000][3];
    private int xDSN = 0;
    private int yDSN = 0;


    public LeeJCL() {

        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);

        File dirSeleccionado = UtilsJavi.seleccionarFichero("C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\JCLS\\BAN 201004\\JCLS", "", "", 1);
        if (Objects.isNull(dirSeleccionado))
            return;

        //String subApp = JOptionPane.showInputDialog("Subaplicacion");

        directorio = dirSeleccionado.getAbsolutePath();
        File path = new File(directorio);
        //File[] archivos = path.listFiles(); //verificar porque parece que no se usa
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        //if (f.exists()) f.delete();
        //f = new File(directorio + "\\JCL-OPs " + fecha + ".txt");
        //if (f.exists()) f.delete();

        for (int i = 0; i < list.length; i++) {
            File fileJCL = new File(directorio + "\\" + list[i]);
            if (fileJCL.isFile() && !fileJCL.getName().equals(fileJCLs)) {
                try {
                    FileSys fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    String lineFinal = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        if (line.startsWith("//*")) {
                            procesaComentario(line, UtilsJavi.removeExtension(list[i]));
                        } else {
                            lineFinal = UtilsJavi.removeExtension(list[i]) + ";" + line;
                            //procesaLine(lineFinal);
                            analisis(lineFinal);
                            procesaDiagrama(lineFinal);
                        }
                        //CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs, lineFinal);
                        line = fileSys.readLine();
                        //System.out.println(line);
                    } //while (line != null);
                } catch (IOException exception) {
                }
                ;
//				System.out.println("Procesando: " + list[i]);
                CreaFileStatic.log(directorio + dirResultados + "\\" + fileJCLs,
                        UtilsJavi.removeExtension(list[i]));
            } //if (f1.isFile()) {
        } //	for(int i = 0; i < list.length; i++) {
        System.out.println("Proceso Terminado Correctamente");
        for (int i = 0; i < tablaDSN.length; i++)
            System.out.println(tablaDSN[i][0] + " " + tablaDSN[i][1] + tablaDSN[i][2]);
    } //public RenameFiles (String directorio) {

    public void procesaComentario(String line, String file) {
//		busca COMENTARIOS ***********************************
        CreaFileStatic.log(directorio + dirResultados + "\\" + fileComentarios,
                file + ";" + line);
    }

    public void procesaLine(String line) {
//		busca PASOS ***********************************
        //System.out.println(line);
        String PSO;
        int indexEXEC = line.indexOf(" EXEC ");
        if (indexEXEC > -1) {
            PSO = "";
            char[] charLinePSO = line.toCharArray();
            boolean finPSO = false;

            for (int indexPSO = line.indexOf(";") + 3; indexPSO < charLinePSO.length && !finPSO; indexPSO++) {
                if (charLinePSO[indexPSO] == ' ')
                    finPSO = true;
                else {
                    PSO = PSO + charLinePSO[indexPSO];
                }    //if
            } //for
            PSOGen = PSO;
            //UtilsJavi.print("PSO " + indexEXEC +" "+ PSO);
        } // if


// busca DSNs ***********************************
        String DSN = "";
        String DSN1 = "";
        char[] charLineDSN = null;

        int indexDSN = line.indexOf("DSN=");
        if (indexDSN > -1) {
            charLineDSN = line.toCharArray();
            boolean finDSN = false;
            for (indexDSN += 4; indexDSN < charLineDSN.length && !finDSN; indexDSN++) {
                if (charLineDSN[indexDSN] == ',' || charLineDSN[indexDSN] == ' ')
                    finDSN = true;
                else DSN = DSN + charLineDSN[indexDSN];
            }
            // AHORA VAMOS A OBTENER EL NB DE PASO si viene algo. Puede que haya varios fich
            // por DD.
            String DD = "";
            char[] charLineDD = line.toCharArray();
            if (charLineDD[line.indexOf(";") + 3] != ' ') {
                boolean finDD = false;
                for (int indexDD = line.indexOf(";") + 3; indexDD < charLineDD.length && !finDD; indexDD++) {
                    if (charLineDD[indexDD] == ' ')
                        finDD = true;
                    else {
                        DD = DD + charLineDD[indexDD];
                        DDGen = DD;
                    }
                } //for (int indexDD = line.indexOf(";//") + 3; indexDD < charLineDD.l
            } //if (charLineDD[7+3] != "")
            //UtilsJavi.print(indexDSN + " " + DSN);

            //Nos quedamos con el primer cualificador
            for (int indexDSN1 = 0; DSN.charAt(indexDSN1) != '.'; indexDSN1++)
                DSN1 = DSN1 + DSN.charAt(indexDSN1);
            //genera fichero con DSNS
            if (!DSN1.equals("CTM") && !DSN1.equals("DBE1") && !DSN1.equals("TCPIPSIS") &&
                    !DSN1.equals("TCPIP") && !DSN1.equals("PRUL") && !DSN1.equals("DB2"))
                CreaFileStatic.log(directorio + dirResultados + "\\" + fileDSNs,
                        line.substring(0, line.indexOf(";")) + ";" + PSOGen + ";" + DDGen + ";" + DSN);
            //genera fichero con interfaces
            if (!DSN1.equals("EXP7") && !DSN1.equals("EXW7") && !DSN1.equals("EXF7") &&
                    !DSN1.equals("CTM") && !DSN1.equals("DBE1") && !DSN1.equals("TCPIPSIS") &&
                    !DSN1.equals("TCPIP") && !DSN1.equals("PRUL") && !DSN1.equals("DB2"))
                CreaFileStatic.log(directorio + dirResultados + "\\" + fileInterfaces,
                        line.substring(0, line.indexOf(";")) + ";" + PSOGen + ";" + DDGen + ";" + DSN);

        } //if (indexDSN > -1) {

//		busca PGMS SIN DB2 *******************************************
        String PGM = "";
        char[] charLinePGM = null;

        int indexPGM = line.indexOf("PGM=");
        if (indexPGM > -1) {
            charLinePGM = line.toCharArray();
            boolean finPGM = false;
            for (indexPGM += 4; indexPGM < charLinePGM.length && !finPGM; indexPGM++) {
                if (charLinePGM[indexPGM] == ',' || charLinePGM[indexPGM] == ' ')
                    finPGM = true;
                else
                    PGM = PGM + charLinePGM[indexPGM];
            }

            //Genera fichero con PGMS
            if (!PGM.equals("IKJEFT01") && !PGM.equals("IDCAMS") &&
                    !PGM.equals("SORT") && !PGM.equals("FTP") &&
                    !PGM.equals("IKJEFT1B") && !PGM.equals("IEBGENER") &&
                    !PGM.equals("ADUUMAIN") && !PGM.equals("SYSCLEAN") &&
                    !PGM.equals("IEBCOPY") && !PGM.equals("ICETOOL") &&
                    !PGM.equals("XX0MEMO") && !PGM.equals("ICEGENER") &&
                    !PGM.equals("LIBSEDIT") && !PGM.equals("VERFIC") &&
                    !PGM.equals("IEFBR14") && !PGM.equals("@CNTFCV9") &&
                    !PGM.equals("XL0MQ") && !PGM.equals("VERFICS") &&
                    !PGM.equals("") && !PGM.equals("DSNUTILB") &&
                    !PGM.equals(" NABUDBX")) {
                CreaFileStatic.log(directorio + dirResultados + "\\" + filePGMs,
                        line.substring(0, line.indexOf(";")) + ";" + PSOGen + ";" + PGM);
                CreaFileStatic.log(directorio + dirResultados + "\\" + filePGMDINDB2s,
                        line.substring(0, line.indexOf(";")) + ";" + PSOGen + ";" + PGM);
            }
            //UtilsJavi.print("PGM " + PGM);
        }

//		busca PGMS CON DB2 *******************************************
        String PGMDB2 = "";
        char[] charLinePGMDB2 = null;

        int indexPGMDB2 = line.indexOf("PROGRAM(");
        if (indexPGMDB2 > -1) {
            charLinePGMDB2 = line.toCharArray();
            boolean finPGMDB2 = false;
            for (indexPGMDB2 += 8; indexPGMDB2 < charLinePGMDB2.length
                    && !finPGMDB2; indexPGMDB2++) {
                if (charLinePGMDB2[indexPGMDB2] == ')')
                    finPGMDB2 = true;
                else
                    PGMDB2 = PGMDB2 + charLinePGMDB2[indexPGMDB2];
            }
            //Genera ficheros con PGMS DB2
            if (!PGMDB2.equals("ARB010") && !PGM.equals("DSNUTILB") &&
                    !PGM.equals(" NABUDBX"))
                //UtilsJavi.print("PGMDB2 " + PGMDB2);
                CreaFileStatic.log(directorio + dirResultados + "\\" + filePGMs,
                        line.substring(0, line.indexOf(";")) + ";" + PSOGen + ";" + PGMDB2);
        }

    }

    public void analisis(String line) {
        String PSO;
        String PGM = "";
        char[] charLinePGM = null;
        String tipoFichero = "";

        int indexEXEC = line.indexOf(" EXEC ");
        if (indexEXEC > -1) {
            PSO = "";
            char[] charLinePSO = line.toCharArray();
            boolean finPSO = false;

            for (int indexPSO = line.indexOf(";") + 3; indexPSO < charLinePSO.length && !finPSO; indexPSO++) {
                if (charLinePSO[indexPSO] == ' ')
                    finPSO = true;
                else {
                    PSO = PSO + charLinePSO[indexPSO];
                }     //if
            } //for
            PSOGen = PSO;
            //UtilsJavi.print("PSO " + indexEXEC +" "+ PSO);
            int indexPGM = line.indexOf("PGM=");
            if (indexPGM > -1) {
                charLinePGM = line.toCharArray();
                boolean finPGM = false;
                for (indexPGM += 4; indexPGM < charLinePGM.length && !finPGM; indexPGM++) {
                    if (charLinePGM[indexPGM] == ',' || charLinePGM[indexPGM] == ' ')
                        finPGM = true;
                    else {
                        PGM = PGM + charLinePGM[indexPGM];
                        pasoActual = PGM;
                    }
                }

                //Genera fichero con PGMS
                if (!PGM.equals("IKJEFT01")) {

                }
                if (!PGM.equals("IDCAMS")) {

                }
                if (!PGM.equals("SORT")) {

                }
                if (!PGM.equals("FTP")) {

                }
                if (!PGM.equals("IKJEFT1B")) {

                }
                if (!PGM.equals("IEBGENER")) {

                }
                if (!PGM.equals("ADUUMAIN")) {

                }
                if (!PGM.equals("SYSCLEAN")) {

                }
                if (!PGM.equals("IEBCOPY")) {

                }
                if (!PGM.equals("ICETOOL")) {

                }
                if (!PGM.equals("XX0MEMO")) {

                }
                if (!PGM.equals("ICEGENER")) {

                }
                if (!PGM.equals("LIBSEDIT")) {

                }
                if (!PGM.equals("VERFIC")) {

                }
                if (!PGM.equals("IEFBR14")) {

                }
                if (!PGM.equals("@CNTFCV9")) {

                }
                if (!PGM.equals("XL0MQ")) {

                }
                if (!PGM.equals("VERFICS")) {

                }
                if (!PGM.equals("DSNUTILB")) {

                }
                if (!PGM.equals("NABUDBX")) {

                }
                if (!PGM.equals("SYSCLEAN")) {

                }
            }
        } //if

        String DSN = "";
        String DSN1 = "";

        int indexDSN = line.indexOf("DSN=");
        if (indexDSN > -1) {

            char[] charLineDSN = null;

            charLineDSN = line.toCharArray();
            boolean finDSN = false;
            for (indexDSN += 4; indexDSN < charLineDSN.length && !finDSN; indexDSN++) {
                if (charLineDSN[indexDSN] == ',' || charLineDSN[indexDSN] == ' ')
                    finDSN = true;
                else DSN = DSN + charLineDSN[indexDSN];
            }
        }

        if (line.indexOf("DISP=") > -1) {
            if (line.indexOf("SHR") > -1) {
                tipoFichero = "E";
            } else {
                tipoFichero = "S";
            }
            analizaDSN(DSN, tipoFichero);
            CreaFileStatic.log(directorio + dirResultados + "\\" + analisisJCLS,
                    line.substring(0, line.indexOf(";")) + ";" + DSN + ";" + tipoFichero);
        }


        int indexPGM = line.indexOf("PGM=");
        if (indexPGM > -1) {

        }

        int indexPGMDB2 = line.indexOf("PROGRAM(");
        if (indexPGMDB2 > -1) {

        }
    }

    private void analizaDSN(String fileDSN, String tipoES) {
        int indexFindDSN = find(fileDSN);
        if (indexFindDSN > -1) {             //---------------> por aqui
            if (tipoES.equals("E")) tablaDSN[indexFindDSN][1] = tipoES;
            else tablaDSN[indexFindDSN][2] = tipoES;
        } else {
            tablaDSN[indexFindDSN + 1][0] = fileDSN;
            if (tipoES.equals("E")) tablaDSN[indexFindDSN + 1][1] = tipoES;
            else tablaDSN[indexFindDSN + 1][2] = tipoES;
        }
    }

    private int find(String file) {
        for (int i = 0; i < tablaDSN.length && tablaDSN[i][0] != null; i++)
            if (tablaDSN[i][0].equals(file)) return i;
        return -1;
    }

    public void procesaDiagrama(String line) {
    }

    public static void main(String[] args) {
        //new LeeJCL("D:\\_ISBAN\\LAB Blanqueo\\JCLS\\SANT\\JCLS");
        new LeeJCL();
        //new LeeJCL("D:\\_ISBAN\\Delete\\jcls");
    }
}

