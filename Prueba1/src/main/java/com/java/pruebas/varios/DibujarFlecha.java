package com.java.pruebas.varios;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class DibujarFlecha extends JFrame {
    private JPanel contentPane;

    /**
     * Create the frame.
     */
    public DibujarFlecha() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        setBounds(0, 0, 800, 600);
    }

    public void paint(Graphics g) {
        super.paint(g);

        double ang = 0.0, angSep = 0.0;
        double tx, ty;
        int dist = 0;
        Point punto1 = null, punto2 = null;

        //defino dos puntos extremos
        punto1 = new Point(15, 30);
        punto2 = new Point(120, 200);

        //tama�o de la punta de la flecha
        dist = 15;

	     /* (la coordenadas de la ventana es al revez)
	         calculo de la variacion de "x" y "y" para hallar el angulo
	      **/

        ty = -(punto1.y - punto2.y) * 1.0;
        tx = (punto1.x - punto2.x) * 1.0;
        //angulo
        ang = Math.atan(ty / tx);

        if (tx < 0) {// si tx es negativo aumentar 180 grados
            ang += Math.PI;
        }

        //puntos de control para la punta
        //p1 y p2 son los puntos de salida
        Point p1 = new Point(), p2 = new Point(), punto = punto2;

        //angulo de separacion
        angSep = 25.0;

        p1.x = (int) (punto.x + dist * Math.cos(ang - Math.toRadians(angSep)));
        p1.y = (int) (punto.y - dist * Math.sin(ang - Math.toRadians(angSep)));
        p2.x = (int) (punto.x + dist * Math.cos(ang + Math.toRadians(angSep)));
        p2.y = (int) (punto.y - dist * Math.sin(ang + Math.toRadians(angSep)));

        Graphics2D g2D = (Graphics2D) g;

        //dale color a la linea
        g.setColor(Color.BLUE);
        // grosor de la linea
        g2D.setStroke(new BasicStroke(1.2f));
        //dibuja la linea de extremo a extremo
        g.drawLine(punto1.x, punto1.y, punto.x, punto.y);
        //dibujar la punta
        g.drawLine(p1.x, p1.y, punto.x, punto.y);
        g.drawLine(p2.x, p2.y, punto.x, punto.y);

    }


    //Este no lo utilizamos
    public void drawArrow(Graphics g, int x0, int y0, int x1, int y1) {
        double alfa = Math.atan2(y1 - y0, x1 - x0);
        g.drawLine(x0, y0, x1, y1);
        int k = 5;
        int xa = (int) (x1 - k * Math.cos(alfa + 1));
        int ya = (int) (y1 - k * Math.sin(alfa + 1));
        // Se dibuja un extremo de la direcci�n de la flecha.
        g.drawLine(xa, ya, x1, y1);
        xa = (int) (x1 - k * Math.cos(alfa - 1));
        ya = (int) (y1 - k * Math.sin(alfa - 1));
        // Se dibuja el otro extremo de la direcci�n de la flecha.
        g.drawLine(xa, ya, x1, y1);
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    DibujarFlecha frame = new DibujarFlecha();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


}



