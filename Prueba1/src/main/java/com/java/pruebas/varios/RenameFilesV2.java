package com.java.pruebas.varios;

import javax.swing.*;
import java.io.File;
import java.util.Base64;
import java.util.Objects;

//renombra ficheros y directorios
public class RenameFilesV2 {
    public static final String CODE = "CODE";
    public static final String DECODE = "DECODE";

    private File directorio;

    public RenameFilesV2() {
        new RenameFilesV2(directorio, CODE);
    }

    public RenameFilesV2(File directorio, String codeType) {
        this.directorio = directorio;
        File[] files = directorio.listFiles();

        for (File item : files) {
            if (item.isDirectory()) {
                item = rename(item, codeType);
                System.out.println("entrando en: " + item.getAbsolutePath());
                new RenameFilesV2(item, codeType);
            }
            if (item.isFile()) {
                if (!item.getName().startsWith(".")) {
                    rename(item, codeType);
                }
            }
        }
    }

    private File rename(File oldFileName, String codeType) {
        String newName = getNameCoded(oldFileName.getName(), codeType);
        File newFileName = new File(oldFileName.getParent() + System.getProperty("file.separator") +
                newName);
        oldFileName.renameTo(newFileName);
        System.out.println("file: " + oldFileName.getAbsoluteFile() + " -> " + newName);
        return newFileName;
    }

    private String getNameCoded(String string, String codeType) {
        if (codeType.equals(CODE)) {
            return Base64.getEncoder().encodeToString(string.getBytes());
        } else {
            return new String(Base64.getDecoder().decode(string));
        }
    }

    public static void main(String[] args) {
        if (args.length == 0) {
             File dirSeleccionado = UtilsJavi.seleccionarFichero(null, "", "", JFileChooser.DIRECTORIES_ONLY);
         //   File dirSeleccionado = new File("/Users/n90578/Documents/_Javi/Personal/Prueba/prueba rename");
            //File dirSeleccionado = UtilsJavi.seleccionarFichero();
            if (Objects.isNull(dirSeleccionado)) {
                return;
            }
            new RenameFilesV2(dirSeleccionado, CODE);
        }
        if (args.length == 2) {
            new RenameFilesV2(new File(args[0]), args[1]);
        }
    }
}
