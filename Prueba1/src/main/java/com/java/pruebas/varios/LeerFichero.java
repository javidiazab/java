package com.java.pruebas.varios;

import java.io.*;


//Seria conveniente terminarlo. de momento se usa FileSys

public class LeerFichero {

    boolean lectura = false;
    boolean escritura = false;
    String file = "";

    static BufferedWriter bw;
    static PrintWriter salida; // = new PrintWriter(bw);

    public LeerFichero(String file, char entradaSalida) {
        if (entradaSalida == 'E') lectura = true;
        else escritura = true;
        this.file = file;
    }

    public LeerFichero(String file) {
        lectura = true;
        this.file = file;
    }

    public void abrirFile(String fileNb) {
        File file = new File(fileNb);
        //System.out.println(file.getName() + " --> " + file.getAbsolutePath());
        if (!(new File(fileNb)).exists()) {
//			System.out.println("Abre fichero");
            try {
                file.createNewFile();
//				java.io.BufferedWriter bw = new java.io.BufferedWriter(
//							new java.io.FileWriter(fileNb));
                if (lectura) {

                }
                if (escritura) {
                    bw = new BufferedWriter(
                            new java.io.FileWriter(fileNb, true));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//				System.out.println("No Abre fichero"); } //if (!(new File(fileNb)).exists()) {
            try {
                if (lectura) {

                }
                if (escritura) {
                    bw = new BufferedWriter(
                            new java.io.FileWriter(fileNb, true));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } //lo ponemos nuevo al quitar el system
    }

    public void cierraFile() {
        if (bw != null)
            try {
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void writeLine(String line) {
        if (bw != null)
            try {
                bw.write(line);
                bw.newLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void log(String fich, String line) {
        abrirFile(fich);
        writeLine(line);
        cierraFile();
    }

    public void log(String line, int nivelLog, int nivelLogEstablecido) {
        //nivelLog es el nivel de log de la linea a loguear
        //nivelLogEstablecido es el nivel de log a partir del que se va a loguear
        if (nivelLog >= nivelLogEstablecido) {
            String fechaS = UtilsJavi.getFechaHora();
            abrirFile("nbs.log");
            writeLine(fechaS + " - " + line);
            cierraFile();
        }
    }

    public void main(String[] args) {
        //CreaFileStatic a = new CreaFileStatic("javid.jda");
        writeLine("hola1");
        writeLine("hola2");
        cierraFile();
    }

}


