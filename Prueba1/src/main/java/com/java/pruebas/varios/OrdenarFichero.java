package com.java.pruebas.varios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;

/**
 * @author Nessy *
 */
public class OrdenarFichero {
    /**
     * @param args Ficheros de entrada y de salida
     */
    public static void main(String[] args) {
        File f_entrada;
        File f_salida;
        File fileAOrdenar;
        File fileOrdenado;
        if (args.length != 2) {
            fileAOrdenar = UtilsJavi.seleccionarFicheroDesc("C:\\DATOS\\", "", "", 2, "Fichero a ordenar:");
            if (Objects.isNull(fileAOrdenar))
                return;

            fileOrdenado = UtilsJavi.seleccionarFicheroDesc("C:\\DATOS\\", "", "", 2, "Fichero a ordenar:");
            if (Objects.isNull(fileOrdenado))
                return;

            f_entrada = new File(fileAOrdenar.getAbsolutePath());
            f_salida = new File(fileOrdenado.getAbsolutePath());

//System.err.println("Numero de argumentos incorrecto");
//System.exit(1);
        } else {
            f_entrada = new File(args[0]);
            f_salida = new File(args[1]);
        }
        LinkedList<String> lista = new LinkedList<String>();
        try {
/* Creamos un FileReader para leer del fichero en forma de
Bytes */
            FileReader fr = new FileReader(f_entrada);
/* Creamos un BufferedReader a partir del FileReader para poder
leer caracteres */
            BufferedReader br = new BufferedReader(fr);
/* Creamos un FileWriter para escribir en el fichero en forma
de Bytes */
            FileWriter fw = new FileWriter(f_salida);
/* Creamos un PrintWriter a partir del FileWriter para poder
escribir caracteres */
            PrintWriter pw = new PrintWriter(fw);
/* Vamos leyendo linea a linea hasta llegar al final del
fichero (null) */
            String linea = null;
            while ((linea = br.readLine()) != null) {
                lista.add(linea);
            }
/* Ordenamos la lista con el metodo sort de la clase
Collections */
            Collections.sort(lista);
            /* Escribimos en el fichero de salida */
            Iterator iter = lista.iterator();
            String cadena;
            while (iter.hasNext()) {
                cadena = (String) iter.next();
                pw.println(cadena);
//System.out.println(cadena);
            }
            /* Cerramos los flujos de los ficheros */
            br.close();
            fr.close();
            pw.close();
            fw.close();
        } catch (FileNotFoundException e) {
//e.printStackTrace();
            System.err.println("No se ha encontrado el fichero");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Proceso terminado");
        }
    }
}

