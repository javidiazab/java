package com.java.pruebas.varios;

import java.io.*;
import java.util.Objects;

//Crea info para meter directamente en ROSE
//Creo que obtiene la info de InventarioPGMSRose


public class InventarioPGMSRose {
    private String nbPGM = "InventarioPGMSRose";
    private String directorio = "";
    int index = 0;
    int incremento = 10000;
    String dirResultados = "\\Resultados " + nbPGM;
    String ficheroResultados = "";

    String fechaS = UtilsJavi.getFechaHora();
    String fileRose = "FileRose " + fechaS + ".txt";
    int nivelLog = 0; //0 no sale nada, 5 sale todo;


    public InventarioPGMSRose() {

        UtilsJavi.escribirLog(nbPGM + "()", 1, nivelLog);

        File dirSeleccionado = UtilsJavi.seleccionarFichero(
                "C:\\DATOS\\_Javi\\Gestion de Configuracion\\Catalogo de objetos\\Inventarios\\TSO BTMENUGS\\Subapp 472\\Resultados\\DetallePgms",
                "", "", 1);
        if (Objects.isNull(dirSeleccionado))
            return;

        //String subApp = JOptionPane.showInputDialog("Subaplicacion");

        directorio = dirSeleccionado.getAbsolutePath();
        File path = new File(directorio);
        //File[] archivos = path.listFiles(); //verificar porque parece que no se usa
        String[] list;
        list = path.list();

        UtilsJavi.CrearDirectorio(directorio + dirResultados);

        ficheroResultados = directorio + dirResultados + "\\" + fileRose;

        for (int i = 0; i < list.length; i++) {
            File file = new File(directorio + "\\" + list[i]);
            if (file.isFile()) {
                // hay que montar lo siguiente ...				);
                //(object Operation "OperacionB"
                //attributes 	(list Attribute_Set
                //	    (object Attribute
                //		tool       	"Seguridad"
                //		name       	"Sin Seguridad"
                //		value      	("SiNo" 1))
                //	    (object Attribute
                //		tool       	"Seguridad"
                //		name       	"Bola Seg. Corta"
                //		value      	("SiNo" 2))
                //	    (object Attribute
                //		tool       	"Seguridad"
                //		name       	"Bola Seg. Larga"
                //		value      	("SiNo" 2))
                //	    (object Attribute
                //		tool       	"Seguridad"
                //		name       	"No Asignado"
                //		value      	("SiNo" 2))
                //	    (object Attribute
                //		tool       	"Seguridad"
                //		name       	"Sin Estudio"
                //		value      	("SiNo" 2))
                //	    (object Attribute
                //		tool       	"Seguridad"
                //		name       	"Otro"
                //		value      	""))
                //		quid       	"510F94DF00F5"
                //		documentation 	"Descipcion operacionB"
                //		concurrency 	"Sequential"
                //		opExportControl 	"Public"
                //		uid        	0)
                CreaFileStatic.log(ficheroResultados, "(object Operation \"" + UtilsJavi.removeExtension(list[i]) + "\"");
                CreaFileStatic.log(ficheroResultados, "attributes 	(list Attribute_Set");
                CreaFileStatic.log(ficheroResultados, "(object Attribute");
                CreaFileStatic.log(ficheroResultados, "tool       	\"Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "name       	\"Sin Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "value      	(\"SiNo\" 1))");
                CreaFileStatic.log(ficheroResultados, "(object Attribute");
                CreaFileStatic.log(ficheroResultados, "tool       	\"Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "name       	\"Bola Seg. Corta\"");
                CreaFileStatic.log(ficheroResultados, "value      	(\"SiNo\" 2))");
                CreaFileStatic.log(ficheroResultados, "(object Attribute");
                CreaFileStatic.log(ficheroResultados, "tool       	\"Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "name       	\"Bola Seg. Larga\"");
                CreaFileStatic.log(ficheroResultados, "value      	(\"SiNo\" 2))");
                CreaFileStatic.log(ficheroResultados, "(object Attribute");
                CreaFileStatic.log(ficheroResultados, "tool       	\"Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "name       	\"No Asignado\"");
                CreaFileStatic.log(ficheroResultados, "value      	(\"SiNo\" 2))");
                CreaFileStatic.log(ficheroResultados, "(object Attribute");
                CreaFileStatic.log(ficheroResultados, "tool       	\"Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "name       	\"Sin Estudio\"");
                CreaFileStatic.log(ficheroResultados, "value      	(\"SiNo\" 2))");
                CreaFileStatic.log(ficheroResultados, "(object Attribute");
                CreaFileStatic.log(ficheroResultados, "tool       	\"Seguridad\"");
                CreaFileStatic.log(ficheroResultados, "name       	\"Otro\"");
                CreaFileStatic.log(ficheroResultados, "value      	\"\"))");
                CreaFileStatic.log(ficheroResultados, "quid       	 \"" + "510F94D" + incremento + "\"");
                CreaFileStatic.log(ficheroResultados, "documentation");

                try {
                    FileSys fileSys = new FileSys(directorio + "\\" + list[i]);
                    String line = null;
                    line = fileSys.readLine();
                    while (line != null) {
                        procesaLine(line);
                        line = fileSys.readLine();
                    } //while (line != null);
                } catch (IOException exception) {
                }
                ;
                CreaFileStatic.log(ficheroResultados, "concurrency     \"Sequential\"");
                CreaFileStatic.log(ficheroResultados, "opExportControl     \"Public\"");
                CreaFileStatic.log(ficheroResultados, "uid        	0)");
            } //if (f1.isFile()) {
        } //	for(int i = 0; i < list.length; i++) {
        System.out.println("Proceso Terminado Correctamente");
    } //public RenameFiles (String directorio) {

    public void procesaLine(String line) {
        if (!line.startsWith("</")) {
            if (line.equals("<Nombre F�sico>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Nombre F�sico:");
            }
            if (line.equals("<Tipo>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Tipo:");
            }
            if (line.equals("<Descripcion>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Descripcion:");
            }
            if (line.equals("<Entrada>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Entrada:");
            }
            if (line.equals("<Salida>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Salida:");
            }
            if (line.equals("<Rutinas accedidas>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Rutinas accedidas:");
            }
            if (line.equals("<Entidades accedidas>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Entidades accedidas:");
            }
            if (line.equals("<ITFs accedidas>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "ITFs accedidas:");
            }
            if (line.equals("<ITEs accedidas>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "ITEs accedidas:");
            }
            if (line.equals("<Comentarios>")) {
                CreaFileStatic.log(ficheroResultados, "|" + "Comentarios:");
            }
            if (!line.startsWith("<")) {
                CreaFileStatic.log(ficheroResultados, "|" + line);
            }
        } //if (!line.startsWith("</")) {
    }

    public static void main(String[] args) {
        new InventarioPGMSRose();
    }
}


