package com.java.pruebas.varios;

import java.awt.print.*;
//junto con la clase ObjetoAImprimir

//clase p�blica que se ejecuta donde debe de estar el main que llama a la
//otra clase.

public class Imprime {
    public static void main(String[] args) {
        // Creamos un objeto de impresi�n.
        PrinterJob job = PrinterJob.getPrinterJob();

        // Hacemos imprimible el objeto ObjetoAImprimir
        job.setPrintable(new ObjetoAImprimir());
        //Pondr� algo tipo Informaci�n job: sun.awt.windows.WPrinterJob@4a5ab2
        System.out.println("Informaci�n job: " + job.toString());

        //Abre el cuadro de di�logo de la impresora, si queremos que imprima
        //directamente sin cuadro de di�logo quitamos el if...
        if (job.printDialog()) {
            //Imprime, llama a la funci�n print del objeto a imprimir
            //en nuestro caso el Objeto ObjetoAImprimir
            try {
                job.print();
            } catch (PrinterException e) {
                System.out.println("Error de impresi�n: " + e);
            }
        }
    }
}
