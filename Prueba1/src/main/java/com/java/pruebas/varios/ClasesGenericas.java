package com.java.pruebas.varios;

public class ClasesGenericas<T> {
    T d;

    public ClasesGenericas(T d) {

        this.d = d;
    }

    public T getD() {

        return d;
    }

    public static void main(String[] args) {
        ClasesGenericas<Integer> numero = new ClasesGenericas<Integer>(3);
        System.out.println(numero.getD());
        ClasesGenericas<String> palabra = new ClasesGenericas<String>("Hola");
        System.out.println(palabra.getD());
    }
}