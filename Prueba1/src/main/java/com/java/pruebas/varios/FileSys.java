/*
 * Creado el 30-jul-09
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 * @author Javito Corleone
 * lee el fichero .sys para cargar la configuracion
 * <p>
 * Clase usada en BD y en Configurar para leer el fichero
 */
/*
 * FileSys.java
 *
 * Created on 23 de abril de 2004, 23:06
 */

/**
 *
 * @author Javito Corleone
 * lee el fichero .sys para cargar la configuracion
 *
 * Clase usada en BD y en Configurar para leer el fichero
 *
 *
 */
package com.java.pruebas.varios;

public class FileSys {
    String file;
    java.io.BufferedReader linea = null;
    String lineaLeida = "";
    String lineaAnt = "";
    String charDivision = "";
    String columnasClave = "";
    String clave = "";
    String claveAnt = "";
    long ctLeidas = 0;  //lineas/registros leidos

    String maxValor = "}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}";

    /** Creates a new instance of FileSys */
    public FileSys(String file) throws java.io.IOException {
        this.file = file;
//			 String[] tablaConfiguracion = read(file);
        try {
            StringBuffer sb = new StringBuffer();
            linea = new java.io.BufferedReader(
                    new java.io.FileReader(file));
//			linea.readLine();
//			linea.close();
        } catch (java.io.IOException exception) {
            exception.printStackTrace();
        }
    }

    /** Creates a new instance of FileSys */
    // este es para que se le indique al crearlo la clave (separada por caracter o por posiciones) y los caracteres de division si tiene
    public FileSys(String file, String charDivision, String columnasClave) throws java.io.IOException {
        this.charDivision = charDivision;
        this.columnasClave = columnasClave;
        this.file = file;

//			 String[] tablaConfiguracion = read(file);
        try {
            StringBuffer sb = new StringBuffer();
            linea = new java.io.BufferedReader(
                    new java.io.FileReader(file));
//			linea.readLine();
//			linea.close();
        } catch (java.io.IOException exception) {
            exception.printStackTrace();
        }
    }


    public FileSys() throws java.io.IOException {
        lineaAnt = linea.toString();

        this.file = System.getProperty("app.fileConf");
//			  String[] tablaConfiguracion = read("Tomas.sys");
    }

    public String readLine() throws java.io.IOException {
        try {
            lineaAnt = lineaLeida;
            lineaLeida = linea.readLine();

            //UtilsJavi.print("FileSys - lineaLeida: " + lineaLeida);
            if (lineaLeida == null) clave = maxValor;
            else ctLeidas++;

            clave = obtenerClave(lineaLeida, charDivision, columnasClave);
            return lineaLeida;

        } catch (java.io.IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public long getLeidas() {
        return ctLeidas;
    }

    public String getClave() {
        return clave;
    }

    public String getClaveAnt() {
        return claveAnt;
    }

    public String getLinea() {
        return lineaLeida;
    }

    public String getLineaAnt() {
        return lineaAnt;
    }

    public String getCharDivision() {
        return charDivision;
    }

    public String getColumnasClave() {
        return columnasClave;
    }

    public void close() throws java.io.IOException {
        try {
            linea.close();
        } catch (java.io.IOException exception) {
            exception.printStackTrace();
        }
    }

    public String[] read(String fileName) throws java.io.IOException {
        lineaAnt = linea.toString();
        try {
//				  File.separatorChar
//				  String curDir = System.getProperty("user.dir");
//				  System.out.println(curDir);
            StringBuffer sb = new StringBuffer();
            java.io.BufferedReader in =
                    new java.io.BufferedReader(new java.io.FileReader(fileName));
//				  System.out.println(fileName);
            String[] tabla = new String[5000]; // <-------- Optimizar
            int i = 0;
            while ((tabla[i] = in.readLine()) != null) {
//					  System.out.println(tabla[i]);
                sb.append(tabla[i]);
                i++;
                ctLeidas++;
            }

            in.close();
            String[] tablaAux = new String[i - 1];
            for (int index = 0; index < tablaAux.length; index++) {
                tablaAux[index] = tabla[index];
            }
            return tablaAux;
        } catch (java.io.IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public String[] read() throws java.io.IOException {
        try {
//				  File.separatorChar
//				  String curDir = System.getProperty("user.dir");
//				  System.out.println(curDir);
            StringBuffer sb = new StringBuffer();
            java.io.BufferedReader in =
                    new java.io.BufferedReader(new java.io.FileReader(file));
//				  System.out.println(fileName);
            String[] tabla = new String[5000]; // <-------- Optimizar
            int i = 0;
            while ((tabla[i] = in.readLine()) != null) {
//					  System.out.println(tabla[i]);
                sb.append(tabla[i]);
                i++;
                ctLeidas++;
            }
            in.close();
            String[] tablaAux = new String[i + 1];
            for (int index = 0; index < tablaAux.length; index++) {
                tablaAux[index] = tabla[index];
            }
            return tablaAux;
        } catch (java.io.IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }


    public String obtenerClave(String linea, String caracter, String columnasClave) {
        String clave = "";

        //if (!linea.equals("")) return "";
        if (linea == null) return maxValor;
        if (caracter.equals("") && columnasClave.equals("")) return "";
        if (!caracter.equals("") && columnasClave.equals("")) return "";

        String[] colsClave = columnasClave.split(",");

        if (caracter.equals("")) {
            for (int i = 0; i < colsClave.length; i = i + 2) {
                if (i < colsClave.length + 2) {  // para comprobar que podemos ir 2 pos mas alla debido al formato pos ini, longitud
                    if (Integer.parseInt(colsClave[i]) - 1 + Integer.parseInt(colsClave[i + 1]) <= linea.length()) { //para comprobar que no nos vamos mas alla de la linea leida
                        clave = clave + linea.substring(Integer.parseInt(colsClave[i]) - 1,
                                Integer.parseInt(colsClave[i]) - 1 + Integer.parseInt(colsClave[i + 1]));
                    } else {
                        clave = "";
                        UtilsJavi.dialog("Error en obtencion de clave pos: " + linea + "," + caracter + "," + columnasClave);
                    }
                }
            }
        } else {
            String[] colsLinea = linea.split(caracter);
            for (int i = 0; i < colsClave.length; i++) {
                if ((Integer.parseInt(colsClave[i]) - 1) <= colsLinea.length)
                    clave = clave + colsLinea[Integer.parseInt(colsClave[i]) - 1];
                else {
                    clave = "";
                    UtilsJavi.dialog("Error en obtencion de clave cols: " + linea + "-" + caracter + "-" + columnasClave);
                }
            }
        }
        return clave;
    }

    public static void main(String[] args) {
        //UtilsJavi.setJaviProperties();
        String[] tabla = null;
        try {
            FileSys fileSys = new FileSys(System.getProperty("nbs.log"));
            tabla = fileSys.read(System.getProperty("nbs.log")); // <----- Optimizar
//					  + file.separator + "Javi" + file.separator + "Java" + file.separator + "pgms" + file.separator + "Tomas" + file.separator + "Tomas.sys");
//					  FileSys a = new FileSys();
//					  tabla = a.read("Tomas.sys");
            for (int i = 0; i < tabla.length; i++) {
                System.out.println(tabla[i]);
            }
        } catch (java.io.IOException exception) {
        }
        ;
		
	/*  try{
			FileSys a = new FileSys("PruebaFileSys.txt");
			System.out.println(a.readLine());
			System.out.println(a.readLine());
			System.out.println(a.readLine());
			a.close();
			String [] tabla = null;
			tabla = a.read();
			System.out.println(tabla[1]);
  		} catch (java.io.IOException exception){};
    */
    }

}
