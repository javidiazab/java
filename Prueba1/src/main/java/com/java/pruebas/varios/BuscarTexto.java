
 /* Creado el 09-sep-09
  *
  * Para cambiar la plantilla para este archivo generado vaya a
  * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
  */

/**
 * @author xIS06021
 * <p>
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
 package com.java.pruebas.varios;

 import java.io.*;
 import java.util.Objects;


 public class BuscarTexto {

     int nivelLog = 0; //0 no sale nada, 5 sale todo;
     private String directorio;
     String[][] tablaLoad = new String[2000][13];
     int index = 0;
     DefColTRX col = new DefColTRX();
     boolean EOF = false;
     String dirResultados = "\\Resultados";
     String fechaS = UtilsJavi.getFechaHora();
     String fileTRXs = "TRXs " + fechaS + ".txt";
     String fileTRXsExcell = "TRXsExcell " + fechaS + ".txt";
     String fileTRXOP = "TRXOPs " + fechaS + ".txt";
     String filePGMs = "PGMs " + fechaS + ".txt";
     String filePants = "Pants " + fechaS + ".txt";
     String OPAnterior = null;
     String PantAnterior = null;

     public BuscarTexto(String directorio) {
         ProcesarTRXs(directorio);
     }

     public BuscarTexto() {
         File dirSeleccionado = UtilsJavi.seleccionarFichero(
                 "C:\\",
                 "", "", 1);
         if (Objects.isNull(dirSeleccionado))
             return;
         ProcesarTRXs(dirSeleccionado.getAbsolutePath());
     }

     public void ProcesarTRXs(String directorio) {
         this.directorio = directorio;
         File path = new File(directorio);
         File[] archivos = path.listFiles(); //verificar
         String[] list;
         list = path.list();

         UtilsJavi.CrearDirectorio(directorio + "\\Resultados");

         //if (f.exists()) f.delete();
         //f = new File(directorio + "\\TRX-OPs " + fecha + ".txt");
         //if (f.exists()) f.delete();

         int indTabla = 0;
         for (int i = 0; i < list.length; i++) {
             File fileTRX = new File(directorio + "\\" + list[i]);
             if (fileTRX.isFile()) {
                 try {
                     FileSys fileSys = new FileSys(directorio + "\\" + list[i]);
                     String line = null;
                     String lineFinal = null;
                     line = fileSys.readLine();
                     while (line != null) {
                         lineFinal = UtilsJavi.removeExtension(list[i]) + " " + line;
                         procesaLine(lineFinal, indTabla++);
                         //CreaFileStatic.log(directorio + dirResultados + "\\" + fileTRXs, lineFinal);
                         line = fileSys.readLine();
                     } //while (line != null);
                 } catch (IOException exception) {
                 }
                 ;

             } //if (f1.isFile()) {
             if (fileTRX.isDirectory()) {
                 new BuscarTexto(path.getAbsolutePath() + "\\" + directorio);
             } //if isDirectory
             System.out.println("Procesando: " + list[i]);
         } //	for(int i = 0; i < list.length; i++) {
		

		/*for (int i = 0; i < tablaLoad.length && tablaLoad[i][0] != null; i ++) {
			System.out.println(tablaLoad[i][0]+tablaLoad[i][1]+
			tablaLoad[i][2]+tablaLoad[i][3]+
			tablaLoad[i][4]+tablaLoad[i][5]+
			tablaLoad[i][6]+tablaLoad[i][7]+
			tablaLoad[i][8]+tablaLoad[i][9]+
			tablaLoad[i][10]+tablaLoad[i][11]+tablaLoad[i][12]);}*/
     } //public BuscarFichero (String directorio) {

     public void procesaLine(String line, int nlinea) {

         //genera fichero con Pantallas. Lo hacemos aqui porque todavia no se ha cambiado nada
         if (!line.contains("JAVI1")) {
             UtilsJavi.dialog("Encontrado!!!");
             //CreaFileStatic.log(directorio  + dirResultados + "\\"  + filePants, line);
         }

     }


     public void printTabla() {
         for (int f = 0; f < tablaLoad.length && tablaLoad[f][0] != null; f++) {
             UtilsJavi.print("");
             for (int c = 0; c < DefColTRX.nCols; c++) {
                 UtilsJavi.print(tablaLoad[f][c]);
             }
         }
     }


     public static void main(String[] args) {
         //LeerTRX a = new LeerTRX("E:\\201101 - Migracion Sensible DCPBC\\TRXs");
         //LeerTRX a = new LeerTRX("C:\\DATOS\\_JAVI\\_ISBAN\\LAB Blanqueo\\Nuevos Desarrollos LAB\\Completado\\200912 - Migracion Contabilidad a BKS\\TRX");
         //new LeerTRX("C:\\DATOS\\_JAVI\\_ISBAN\\LAB Blanqueo\\Nuevos Desarrollos LAB\\Pendiente\\200805 - Migracion Blanca I BKS\\20110208 Modificacion a multi\\TRX");
         new BuscarTexto();
     }
 }