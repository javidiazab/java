package com.java.pruebas.varios;

import java.io.BufferedWriter;
import java.io.File;
import java.io.PrintWriter;


//esta clase no vale de nada


// Crea fichero. Se utiliza tambien para crear un log con una vble estatica

//nivelLog es el nivel de log de la linea a loguear
//nivelLogEstablecido es el nivel de log a partir del que se va a loguear
//int nivelLog = 0; 0 no sale nada, 5 sale todo, 6 sale por la consola


public class Log {

    private BufferedWriter bw;
    private PrintWriter salida; // = new PrintWriter(bw);

    public Log(String fileNb) {
        File file = new File(fileNb);
        //System.out.println(file.getName() + " --> " + file.getAbsolutePath());
        if (!(new File(fileNb)).exists()) {
//			System.out.println("Abre fichero");
            try {
                file.createNewFile();
//				java.io.BufferedWriter bw = new java.io.BufferedWriter(
//							new java.io.FileWriter(fileNb));
                bw = new BufferedWriter(
                        new java.io.FileWriter(fileNb, true));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//				System.out.println("No Abre fichero"); } //if (!(new File(fileNb)).exists()) {
            try {
                bw = new BufferedWriter(
                        new java.io.FileWriter(fileNb, true));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } //lo ponemos nuevo al quitar el system
    }

    public void cierraFile() {
        if (bw != null)
            try {
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void writeLine(String line) {
        if (bw != null)
            try {
                bw.write(line);
                bw.newLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void log(String fich, String line) {
        new Log(fich);
        writeLine(line);
        cierraFile();
    }

    public void log(String line, int nivelLog, int nivelLogEstablecido) {
        //El fichero de log va a ir a terminar a C:\DATOS\BKSPlatform\workspace1\Java\nbs.log
        //nivelLog es el nivel de log de la linea a loguear
        //nivelLogEstablecido es el nivel de log a partir del que se va a loguear
        //int nivelLog = 0; //0 no sale nada, 5 sale todo, 6 sale por la consola
        if (nivelLog == 6) {

            String fechaS = UtilsJavi.getFechaHora();
            System.out.println(fechaS + " - " + line);
        } else if (nivelLog >= nivelLogEstablecido) {
            String fechaS = UtilsJavi.getFechaHora();
            new Log("nbs.log");
            writeLine(fechaS + " - " + line);
            cierraFile();
        }
    }


    public static void main(String[] args) {
        CreaFileStatic a = new CreaFileStatic("javid.jda");
        a.writeLine("hola1");
        a.writeLine("hola2");
        a.cierraFile();
    }

}

