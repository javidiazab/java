package com.java.pruebas.oop2;

public class App {

    public static void main(String[] args) {
        InterfacePadre i1 = new ClaseA();
        InterfacePadre i2 = new ClaseB();
        i1.a();
        i1.a();
        i2.b();
        i2.b();
        ClasePadre c1 = new ClaseHija1();
        ClasePadre c2 = new ClaseHija2();
        ClaseHija1 c1a = (ClaseHija1) c1;
        ClaseHija2 c2a = (ClaseHija2) c2;
        c1a.b();
        c2a.d();
    }
}
