package com.java.pruebas.oop2;

public class ClaseA implements InterfacePadre {
    @Override
    public void a() {
        System.out.println("ClaseA: a");
    }

    @Override
    public void b() {
        System.out.println("ClaseA: b");
    }

    public void propioClaseA1() {
        System.out.println("ClaseA: propioClaseA1");
    }

}
