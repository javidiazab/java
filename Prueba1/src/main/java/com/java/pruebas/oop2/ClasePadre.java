package com.java.pruebas.oop2;

public class ClasePadre {

    public ClasePadre() {
    }

    public void a() {
        System.out.println("ClasePadre: a");
    }

    public void b() {
        System.out.println("ClasePadre: b");
    }
}
