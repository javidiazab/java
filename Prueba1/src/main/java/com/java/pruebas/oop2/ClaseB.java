package com.java.pruebas.oop2;

public class ClaseB implements InterfacePadre {
    @Override
    public void a() {
        System.out.println("ClaseB: a");
    }

    @Override
    public void b() {
        System.out.println("ClaseB: b");
    }

    public void propioClaseB1() {
        System.out.println("ClaseA: propioClaseB1");
    }
}
