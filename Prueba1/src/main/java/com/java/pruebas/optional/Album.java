package com.java.pruebas.optional;

import java.util.Optional;

public class Album {
    private final String valor;

    public Album(String valor) {
        this.valor = valor;
    }

    public Optional<String> getValor(int numero) {
        return (numero % 2 == 0) ? Optional.of(valor) : Optional.empty();
        //return Optional.ofNullable(valor);
    }
}
