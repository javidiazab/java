package com.java.pruebas.optional;

import java.util.Optional;

public class OptionalJavi {

    public static void main(String[] args) {
        System.out.println("\n------------------------------ prueba1");
        Optional<String> empty1 = Optional.empty();
        if (empty1.isPresent()) System.out.println("present");
        else System.out.println("not present");
        if (empty1.isEmpty()) System.out.println("empty");
        else System.out.println("not empty");

        System.out.println("\n------------------------------ prueba2");
        String name = "name";
        Optional<String> empty2 = Optional.of(name);
        if (empty2.isPresent()) System.out.println("present");
        else System.out.println("not present");
        if (empty2.isEmpty()) System.out.println("empty");
        else System.out.println("not empty");

        System.out.println("\n------------------------------ prueba3");
        //Optional<Album> a = Optional.ofNullable(new Album("uno"));
        //Optional<Album> a = Optional.of(new Album("uno"));


        Optional<Album> c = Optional.empty();
        System.out.println("\n---------------- 10");
        if (c.get().getValor(10).isPresent()) System.out.println("Present");
        else System.out.println("not present");
        if (c.get().getValor(10).isEmpty()) System.out.println("Empty");
        else System.out.println("not empty");
        System.out.println("\n---------------- 11");
        if (c.get().getValor(11).isPresent()) System.out.println("Present");
        else System.out.println("not present");
        if (c.get().getValor(11).isEmpty()) System.out.println("Empty");
        else System.out.println("not empty");
        System.out.println(c.isPresent());
        c.orElse(new Album("dos"));
        c = null;
        System.out.println(c.isPresent());
        c.orElse(new Album("cuatro"));

        System.out.println("\n------------------------------ prueba4");
        Optional<String> b = null;
        System.out.println(b.isEmpty());

        Optional<String> d = null;
        System.out.println(d.orElseGet(() -> Optional.of("orElseGet").toString()));

    }
/*

Album album;
    Optional<Album> albumOptional = getAlbum("Random Memory Access");
if(albumOptional.isPresent()){
        album = albumOptional.get();
    }else{
        // Avisar al usuario de que no se ha encontrado el album
    }


    private static double getDurationOfAlbumWithName(String name) {
        return getAlbum(name)
                .flatMap((album) -> getAlbumTracks(album.getName()))
                .map((tracks) -> getTracksDuration(tracks))
                .orElse(0.0);
    }

    Optional<Double> getDurationOfAlbumWithName(String name) {
        Optional<Double> duration = getAlbum(name)
                .flatMap((album) -> getAlbumTracks(album.getName()))
                .map((tracks) -> getTracksDuration(tracks));
        return duration;
    }

*/

}
