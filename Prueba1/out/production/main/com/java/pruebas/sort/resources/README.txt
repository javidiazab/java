Formato entrada

The INCLUDE, OMIT and INREC statements refer to fields as they appear in the input records.
The SORT and OUTREC statements refer to fields as they appear in the reformatted INREC records.
The OUTFIL statements refer to fields as they appear in the reformatted OUTREC records.

    // INREC FIELDS = (10:2,5)
    // INCLUDE COND=(39,8,CH,EQ,C'AUTONOMO',AND,55,1,CH,EQ,C'6')
    // INCLUDE COND=(39,8,CH,EQ,C'AUTONOMO',AND,
    //         (55,5,BI,GE,X'600000000C',AND,55,5,BI,LT,X'700000000C'))
    // SUM
    // SORT FIELDS= Copy
    //T – Tipo de dato del campo que se quiere sumar:
    //CH - Alfanumérico o numérico normal(sin COMP)
    //ZD - Numérico normal(sin COMP)
    //BI - Hexadecimal (campos COMP)
    //PD - Empaquetado con o sin signo(campos COMP-3)


Ejemplo:

inrec fields = (c'dddd',1:2,2,1:2,2,c'ffff',1,100,100X)
SORT FIELDS = (1,2,A,3,4,D,5,6,A)
INCLUDE COND = (1,2,CH,EQ,C'aaa',OR,1,2,CH,EQ,'bbb')
omit COND = (1,2,CH,EQ,'sss',AND,3,2,CH,EQ,C'444')
outrec fields = (c'dddd',1:2,2,1:2,2,c'ffff',1,100,100X)
sum fields = (1,2,100,3)