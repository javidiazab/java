package com.example.activemq5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Activemq5Application {

	public static void main(String[] args) {
		SpringApplication.run(Activemq5Application.class, args);
	}

}
