package com.javi.MatchingJOIN;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchingJoinApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchingJoinApplication.class, args);
	}

}
