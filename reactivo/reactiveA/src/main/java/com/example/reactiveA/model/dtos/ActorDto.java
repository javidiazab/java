package com.example.reactiveA.model.dtos;

import com.example.reactiveA.model.entities.ActorEntity;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActorDto {

    private String id;
    private String name;
    private LocalDate birthdate;

    public ActorEntity toActorEntity() {
        ActorEntity entity = new ActorEntity();
        BeanUtils.copyProperties(this, entity);
        return entity;
    }
}