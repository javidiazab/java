package com.example.reactiveA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveAApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveAApplication.class, args);
	}

}
