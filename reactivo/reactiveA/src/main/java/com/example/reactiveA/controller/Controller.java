package com.example.reactiveA.controller;

import com.example.reactiveA.model.dtos.ActorDto;
import com.example.reactiveA.model.dtos.MovieDto;
import com.example.reactiveA.services.ActorService;
import com.example.reactiveA.services.MovieService;
import com.example.reactiveA.services.VariosService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reactiveA")
public class Controller {

    private final MovieService movieService;
    private final ActorService actorService;
    private final VariosService variosService;

    @GetMapping("/")
    public Mono<String> getMono() {
        return Mono.just("String1");
    }

    @GetMapping("1")
    public Flux<List<Integer>> getFlux1() {
        return Flux.just(List.of(2, 3, 4, 5, 6, 1, 7));
    }

    // CRUD para Movies

    @PostMapping("movies")
    public Mono<MovieDto> addMovie(@RequestBody MovieDto movie) {
        return movieService.saveMovie(movie);
    }

    @GetMapping("movies")
    public Flux<MovieDto> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping(value = "movies-slowly", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<MovieDto> getAllMovies_slowly() {
        return movieService.getAllMovies();
    }

    @GetMapping("movies/{id}")
    public Mono<MovieDto> getMovieById(@PathVariable String id) {
        return movieService.getMovieById(id);
    }

    @PutMapping("movies/{id}")
    public Mono<MovieDto> updateMovie(@PathVariable String id, @RequestBody MovieDto movieDetails) {
        return movieService.updateMovie(id, movieDetails);
    }

    @DeleteMapping("movies/{id}")
    public Mono<Void> deleteMovie(@PathVariable String id) {
        return movieService.deleteMovie(id);
    }

    // CRUD para Actors
    @PostMapping("actors")
    public Mono<ActorDto> addActor(@RequestBody ActorDto actor) {
        return actorService.saveActor(actor);
    }

    @GetMapping("actors")
    public Flux<ActorDto> getAllActors() {
        return actorService.getAllActors();
    }

    @GetMapping(value = "actors-slowly", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<ActorDto> getAllAlctors_slowly() {
        return actorService.getAllActors();
    }

    @GetMapping("actors/{id}")
    public Mono<ActorDto> getActorById(@PathVariable String id) {
        return actorService.getActorById(id);
    }

    @PutMapping("actors/{id}")
    public Mono<ActorDto> updateActor(@PathVariable String id, @RequestBody ActorDto actorDetails) {
        return actorService.updateActor(id, actorDetails);
    }

    @DeleteMapping("actors/{id}")
    public Mono<Void> deleteActor(@PathVariable String id) {
        return actorService.deleteActor(id);
    }

    // CRUD para Varios
    @GetMapping("varios")
    public Flux<String> varios1() {
        return variosService.varios1();
    }

}
