package com.example.reactiveA.services;

import com.example.reactiveA.model.dtos.MovieDto;
import com.example.reactiveA.model.entities.MovieEntity;
import com.example.reactiveA.model.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class MovieService {

    private final MovieRepository moviesRepository;

    public Mono<MovieDto> saveMovie(MovieDto movie) {
        log.info("saveMovie");
        return moviesRepository.save(movie.toMovieEntity())
            .map(MovieEntity::toMovieDto);
    }

    public Flux<MovieDto> getAllMovies() {
        log.info("getAllMovies");
        return moviesRepository.findAll().map(MovieEntity::toMovieDto);
    }

    public Mono<MovieDto> getMovieById(String id) {
        log.info("getMovieById");
        return moviesRepository.findById(id)
            .map(MovieEntity::toMovieDto);
    }

    public Mono<MovieDto> updateMovie(String id, MovieDto movieDto) {
        log.info("updateMovie");
        return moviesRepository.save(movieDto.toMovieEntity())
            .map(MovieEntity::toMovieDto);
    }

    public Mono<Void> deleteMovie(String id) {
        log.info("deleteMovie");
        return moviesRepository.deleteById(id);
    }
}
