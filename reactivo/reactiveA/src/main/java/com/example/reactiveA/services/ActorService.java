package com.example.reactiveA.services;

import com.example.reactiveA.model.dtos.ActorDto;
import com.example.reactiveA.model.entities.ActorEntity;
import com.example.reactiveA.model.repository.ActorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Slf4j
public class ActorService {

    private final ActorRepository actorRepository;

    public Mono<ActorDto> saveActor(ActorDto actor) {
        log.info("saveActor");
        return actorRepository.save(actor.toActorEntity())
            .map(ActorEntity::toActorDto);
    }

    public Flux<ActorDto> getAllActors() {
        log.info("getAllActors");
        return actorRepository.findAll().map(ActorEntity::toActorDto);
    }

    public Mono<ActorDto> getActorById(String id) {
        log.info("getActorById");
        return actorRepository.findById(id).map(ActorEntity::toActorDto);
    }

    public Mono<ActorDto> updateActor(String id, ActorDto actorDto) {
        log.info("updateActor");
        return actorRepository.save(actorDto.toActorEntity())
            .map(ActorEntity::toActorDto);
    }

    public Mono<Void> deleteActor(String id) {
        log.info("deleteActor");
        return actorRepository.deleteById(id);
    }
}
