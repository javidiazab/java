package com.example.reactiveA.model.entities;

import com.example.reactiveA.model.dtos.ActorDto;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "actor")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActorEntity {

    @Id
    private String id;
    private String name;
    private LocalDate birthdate;

    public ActorDto toActorDto() {
        ActorDto actorDto = new ActorDto();
        BeanUtils.copyProperties(this, actorDto);
        return actorDto;
    }
}
