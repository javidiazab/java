package com.example.reactiveA.model.entities;

import com.example.reactiveA.model.dtos.MovieDto;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "movie")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovieEntity {

    @Id
    private String id;
    private String title;
    private Integer releaseYear;
    private String genre;
    private ActorEntity starring;

    public MovieDto toMovieDto() {
        MovieDto movieDto = new MovieDto();
        BeanUtils.copyProperties(this, movieDto);
        return movieDto;
    }
}