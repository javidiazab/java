package com.example.reactiveA.model.repository;

import com.example.reactiveA.model.entities.MovieEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends ReactiveCrudRepository<MovieEntity, String> {
}
