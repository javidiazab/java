package com.example.reactiveA.config;

import com.example.reactiveA.model.entities.ActorEntity;
import com.example.reactiveA.model.entities.MovieEntity;
import com.example.reactiveA.model.repository.ActorRepository;
import com.example.reactiveA.model.repository.MovieRepository;
import java.time.LocalDate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class SeedRunner implements CommandLineRunner {

    private final MovieRepository movieRepository;
    private final ActorRepository actorRepository;

    @Override
    public void run(String... args) throws Exception {
        log.info("EXECUTING : command line runner");
        if (movieRepository.count().block() == 0) {
            movieRepository.save(MovieEntity.builder().title("xx movie").build()).block();
            movieRepository.save(new MovieEntity(null, "javi movie", 2001, "Horror", new ActorEntity(null, "Javi", LocalDate.now()))).block();
            movieRepository.save(new MovieEntity(null, "sonia movie",2002, "thriller", new ActorEntity(null, "Javi", LocalDate.now()))).block();
            movieRepository.save(new MovieEntity(null, "admin movie", 2003, "Comedy", new ActorEntity(null, "Javi", LocalDate.now()))).block();
        }

        if (actorRepository.count().block() == 0) {
            actorRepository.save(new ActorEntity(null, "javi", LocalDate.now())).block();
            actorRepository.save(new ActorEntity(null, "sonia", LocalDate.now())).block();
            actorRepository.save(new ActorEntity(null, "admin", LocalDate.now())).block();
        }

        log.info("Movies: " + movieRepository.count().block());
        log.info("Actors: " + actorRepository.count().block());
    }
}
