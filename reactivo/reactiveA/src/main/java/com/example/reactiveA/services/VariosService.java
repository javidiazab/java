package com.example.reactiveA.services;

import com.example.reactiveA.model.entities.ActorEntity;
import com.example.reactiveA.model.entities.MovieEntity;
import com.example.reactiveA.model.repository.ActorRepository;
import com.example.reactiveA.model.repository.MovieRepository;
import java.time.Duration;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class VariosService {

    private final ActorRepository actorRepository;
    private final MovieRepository movieRepository;


    public Flux<String> varios1() {
        Flux<MovieEntity> movieEntityFlux = movieRepository.findAll().delayElements(Duration.ofMillis(1000L));
        Flux<ActorEntity> actorEntityFlux = actorRepository.findAll().delayElements(Duration.ofMillis(1000L));

//        movieEntityFlux.then(Mono.from(actorRepository.findAll().map(x -> MovieDto.builder().title(x.getName()).build()).take(1)));
//
//
//        Flux.concat(actorEntityFlux.map(x -> Mono.just(ActorEntity.builder().name(x.getName()).build())));
//        actorEntityFlux.concatWith(x -> Mono.just(ActorEntity.builder().name(x.getName()).build())));

        Mono<String> mono1 = Mono.just("Return value");
        Mono<String> mono2 = Mono.just("Ignored value");
        return mono1.concatWith(mono2::thenReturn); //mono1.concatWith(v -> mono2.thenReturn(v));
    }
}
