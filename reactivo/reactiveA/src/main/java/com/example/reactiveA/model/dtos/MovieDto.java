package com.example.reactiveA.model.dtos;

import com.example.reactiveA.model.entities.MovieEntity;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovieDto {

    private String id;
    private String title;
    private Integer releaseYear;
    private String genre;
    private ActorDto starring;

    public MovieEntity toMovieEntity() {
        MovieEntity entity = new MovieEntity();
        BeanUtils.copyProperties(this, entity);
        return entity;
    }
}