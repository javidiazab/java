package com.example.reactiveA.model.repository;

import com.example.reactiveA.model.entities.ActorEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends ReactiveCrudRepository<ActorEntity, String> {

}
