package com.example.reactiveA;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootTest
@EnableWebFlux
class ReactiveAApplicationTests {

	@Test
	void contextLoads() {
	}

}
