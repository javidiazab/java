package com.example.reactiveA;

import java.time.Duration;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.util.function.Tuple2;

public class Tests {


    private int min = 1;
    private int max = 5;
    Flux<Integer> evenNumbers = Flux
        .range(min, max)
        .filter(x -> x % 2 == 0); // i.e. 2, 4

    Flux<Integer> oddNumbers = Flux
        .range(min, max)
        .filter(x -> x % 2 > 0);  // ie. 1, 3, 5

    @Test
    public void givenFluxes_whenConcatIsInvoked_thenConcat() {
        Flux<Integer> fluxOfIntegers = Flux.concat(
            evenNumbers,
            oddNumbers);

        StepVerifier.create(fluxOfIntegers)
            .expectNext(2)
            .expectNext(4)
            .expectNext(1)
            .expectNext(3)
            .expectNext(5)
            .expectComplete()
            .verify();
    }

    @Test
    public void zip_une_dcha_e_iqda_en_tuplas() {
        Flux<String> flux1 = Flux.just("A", "B", "C");
        Flux<String> flux2 = flux1.flatMap(s ->
            Mono.just(s.toLowerCase()).delayElement(Duration.ofMillis(100))
        );

        Flux.zip(flux1, flux2, (upper, lower) -> upper + lower)  ////QUE ES ESTO y porque no muestra nada??
            .subscribe(System.out::println); // Emite AA, BB, CC

    }

    @Test
    public void zip() {
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<String> flux2 = Flux.just("a", "b", "c");

        Flux<Tuple2<Integer, String>> zippedFlux = Flux.zip(flux1, flux2);

        zippedFlux.subscribe(tuple -> {
            Integer number = tuple.getT1();
            String letter = tuple.getT2();
            System.out.println(number + ": " + letter);
        });
    }

    @Test
    public void merge1() {
        Flux<String> flux1 = Flux.just("A", "B", "C").delayElements(Duration.ofMillis(100));
        Flux<String> flux2 = Flux.just("1", "2", "3").delayElements(Duration.ofMillis(150));

        Flux.merge(flux1, flux2)
            .subscribe(System.out::println); // Emite los elementos en el orden en que se reciben

        sleep(1000);
    }

    @Test
    public void combineLast() {
        Flux<String> flux1 = Flux.just("A", "B", "C").delayElements(Duration.ofMillis(100));
        Flux<Integer> flux2 = Flux.just(1, 2, 3).delaySubscription(Duration.ofMillis(50));

        Flux.combineLatest(flux1, flux2, (string, integer) -> string.toLowerCase() + integer)
            .subscribe(System.out::println); //  emite A3, B3, C3


        sleep(1000);

    }

    @Test
    public void filter() {
        Flux<String> namesFlux = Flux.just("Ana", "Bob", "Carol")
            .filter(name -> name.startsWith("B"));

    }

    private static void sleep(int milis) {
        try {
            Thread.sleep(milis); // Espera 1 segundo para que los flujos terminen
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
