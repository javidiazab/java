import contracts.Observable;
import observables.MyObservable;
import observers.ObserverOne;
import observers.ObserverTwo;

import java.util.stream.Stream;

/**
 * Docs:
 * https://refactoring.guru/es/design-patterns/observer
 *
 * Difference with the publisher/susbscriber:
 * https://medium.com/building-the-open-data-stack/observer-versus-pub-sub-design-patterns-48b1dbc83916#:~:text=In%20the%20Observer%20pattern%2C%20the,the%20bridging%20component%2C%20the%20broker.
 *
 * Hollywood principle: https://deviq.com/principles/hollywood-principle
 */
public class ObserverPattern {
    public static void main(String[] args) {
        Observable observable = new MyObservable();
        observable.subscribe(new ObserverOne());
        observable.subscribe(new ObserverTwo());

        //Notify event

        Stream
                .of("One", "Two", "Three")
                        .forEach(s -> {
                            sleep();
                            System.out.println("...................................................");
                            System.out.println("Changing to state :" + s);
                            System.out.println("...................................................");
                            observable.changeState(s);
                        });
    }

    private static void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
