package observers;

import contracts.Observer;

public class ObserverTwo implements Observer {
    @Override
    public void update(String newState) {
        System.out.println("ObserveTwo changed to state: " + newState);
    }
}
