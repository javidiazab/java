package observers;

import contracts.Observer;

public class ObserverOne implements Observer {
    @Override
    public void update(String newState) {
        System.out.println("ObserverOne changed to state: " + newState);
    }
}
