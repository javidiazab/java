package contracts;

public interface Observable {
    void subscribe(Observer observer);
    void changeState(String newState);
}
