package contracts;

public interface Observer {
    void update(String newState);
}
