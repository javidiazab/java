CREATE TABLE if not exists movies(
     id bigserial NOT NULL,
     title varchar(255) ,
     CONSTRAINT pk_movies_id PRIMARY KEY (id)
);
