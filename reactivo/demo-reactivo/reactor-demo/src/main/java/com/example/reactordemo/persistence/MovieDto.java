package com.example.reactordemo.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovieDto {

    private Long id;
    private String title;

    public MovieEntity toMovieEntity(MovieEntity entity) {
        BeanUtils.copyProperties(this, entity);
        return entity;
    }
}
