package com.example.reactordemo.persistence;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface MoviesRepository extends ReactiveCrudRepository<MovieEntity, Long> {
}
