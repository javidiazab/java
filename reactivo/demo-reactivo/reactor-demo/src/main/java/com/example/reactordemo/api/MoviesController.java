package com.example.reactordemo.api;

import com.example.reactordemo.persistence.MovieEntity;
import com.example.reactordemo.persistence.MoviesRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.stream.IntStream;

@RestController
@RequiredArgsConstructor
@Slf4j
public class MoviesController {

    private final MoviesRepository moviesRepository;

    @GetMapping
    public Flux<MovieEntity> getMovies() {
        return moviesRepository
                .findAll()
                .doOnNext(x -> log.info("Returning id: {}", x.getId()));
    }

    @PostMapping
    public Mono<MovieEntity> addMovie(@RequestBody MovieEntity movie) {
        return Optional
                .ofNullable(movie)
                .map(m -> {
                    m.setId(null);
                    return m;
                })
                .map(m -> moviesRepository.save(movie))
                .orElseThrow();
    }

    @GetMapping("/fill")
    public void fill() {
        IntStream
                .rangeClosed(1, 10000)
                .forEach(x -> moviesRepository.save(MovieEntity.builder().title("Movie " + x).build()).subscribe());
    }

    //Important, check the results (what about specifications?)
    @GetMapping("/first")
    public Flux<MovieEntity> getFirst() {
        return moviesRepository
                .findAll()
                .doOnNext(x -> log.info("Returning id: {}", x.getId()))
                .take(1);
    }

    https://www.youtube.com/watch?v=2HevdEu3EMM&t=2569s
    //It's possible to return Mono<Void>
    //map:  transforma u objeto en otro
    //flatMap: aplana un flujo en objetos
    //sitchIfEmpty: cambia un flujo vacio por otro flujo
    //mergeWith: fusiona un flujo con otro
    //then(): retorna un Mono<Void> cuando el flujo se completa
    //Then(Mono<V>): retorna el Mono cuando el flujo se completa
    //justOrEmpty: crea un Mono<V>, si es null crea un Mono<Void>

    @PostMapping("/movie")
    public Mono<MovieDto> readMovie(@RequestBody String movieCode) {
        return this.moviesRepository.findById(movieCode)
                .switchIfEmpty(Mono.error(new NotFoundException("movie code (" + movieCode + ")")))
                .map(MovieDto::new);

    }

    @PostMapping("/movie1")
    public Mono<MovieDto> create(MovieEntity movie) {
        return assertIdAndTittleNotExist(movie.getId(), movie.getTittle()) //Mono<>
                .then(moviesRepository.save(new MovieEntity(movie))) //Flux<>
                .map(MovieDto::new);  //Mono<MovieEntity>
    }

    private Mono<Void> assertIdAndTittleNotExist(Long id, String tittle) {
        return this.moviesRepository.findById(id) //Mono<Movies>
                .mergeWith(this.moviesRepository.findByTittle(tittle)) //Flux<Movies>
                .flatmap(movie -> Mono.error(new Exception("error")
                )) //Flux<Oject>
                .then(); //Mono<Void>  ---> not quite necessary here

    }

    public Mono<MovieDto> update (Long id, MovieDto movieDto) {
        return this.moviesRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException("Non Existent id: " + id)))
                .flatMap(movieEntity ->
                        BeanUtils.copyProperties(movieDto, movieEntity);
                        return moviesRepository.save(movieEntity);
                )
                .map(MovieEntity::toMovieDto);
    }
}
