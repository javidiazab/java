package com.example.reactordemo.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("movies")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovieEntity {

    @Id
    private Long id;

    private String title;

    public MovieDto toMovieDto() {
        MovieDto movieDto = new MovieDto();
        BeanUtils.copyProperties(this, movieDto);
        return movieDto;
    }
}
