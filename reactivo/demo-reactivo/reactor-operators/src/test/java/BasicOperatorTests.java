import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.List;
import java.util.stream.Stream;

public class BasicOperatorTests {

    @Test
    void displayNumbersJust() {

        Flux
                .just(1, 2, 3, 4, 5)
                .subscribe(System.out::println);
    }

    @Test
    void displayNumbersRange() {

        Flux
                .range(1,5)
                .subscribe(System.out::println);
    }

    @Test
    void displayNumbersFromStream() {

        var stream = Stream.of(1,2,3,4,5);

        Flux
                .fromStream(stream)
                .subscribe(System.out::println);
    }

    @Test
    void displayNumbersFromIterable() {

        var iterable = List.of(1,2,3,4,5);

        Flux
                .fromIterable(iterable)
                .subscribe(System.out::println);
    }

    @Test
    @SneakyThrows
    void displayNumbersInterval() {

        Flux
                .interval(Duration.ofMillis(500))
                .subscribe(System.out::println);

        //Por que es necesario => porque sino no muestra nada pues termina el hilo
        //Ejemplo de stream infinito a diferencia de los anteriores casos
        Thread.sleep(5000);
    }

    @Test
    void displayNumbersFiltered() {

        Flux
                .range(1,10)
                .filter(x -> x % 2 == 0)
                .subscribe(System.out::println);
    }

    @Test
    void displayNumbersMap() {

        //Propagacion del cambio en el stream
        Flux
                .range(1,5)
                .map(x -> x*10)
                .subscribe(System.out::println);
    }

    @Test
    void displayNumbersCustomSubscribe() {

        Flux
                .range(1,5)
                .map(x -> x*10)
                .subscribe(x -> {
                    System.out.println("# = " + x);
                    System.out.println("Mas acciones");
                });
    }

    @Test
    void displayNumbersWithDo() {

        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x)) //Do no cambia el resultado, es un peek
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x))
                .subscribe(); //el subscribe no necesita un consumidor, pero es necesario
    }

    @Test
    @SneakyThrows
    void displayNumbersWithoutSubscribe() {

        //Que se muestra en pantalla?

        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x))
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x));

        Thread.sleep(2000);
    }

    @Test
    @SneakyThrows
    void displayNumbersDeclarative() {

        var flux = Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x))
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x));

        System.out.println("No se ha realizado una subscripcion todavia, asi que no se ve nada en pantalla");
        flux.subscribe();
        System.out.println("Repitiendo........................................");
        flux.subscribe();
    }

    @Test
    void displayNumbersPublishOn() {

        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x + " en el hilo " + Thread.currentThread().getName()))
                .publishOn(Schedulers.boundedElastic()) //Importa donde se ponga en la pipeline
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x + " en el hilo " + Thread.currentThread().getName()))
                .subscribe(x -> System.out.println("Subscribe en el hilo " + Thread.currentThread().getName()));
    }

    @Test
    void displayNumbersPublishOnCustomScheduler() {

        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x + " en el hilo " + Thread.currentThread().getName()))
                .publishOn(Schedulers.single()) //Importa donde se ponga en la pipeline
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x + " en el hilo " + Thread.currentThread().getName()))
                .subscribe(x -> System.out.println("Subscribe en el hilo " + Thread.currentThread().getName()));
    }

    @Test
    void displayNumbersSubscribeOn() {

        //Que se debe mostrar en pantalla?
        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x + " en el hilo " + Thread.currentThread().getName()))
                .subscribeOn(Schedulers.boundedElastic())
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x + " en el hilo " + Thread.currentThread().getName()))
                .subscribe(x -> System.out.println("Subscribe en el hilo " + Thread.currentThread().getName()));
    }

    @Test
    @SneakyThrows
    void displayNumbersSubscribeOn2() {

        //Que se debe mostrar en pantalla?
        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x + " en el hilo " + Thread.currentThread().getName()))
                .subscribeOn(Schedulers.boundedElastic()) //No importa donde se ponga en la pipeline
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x + " en el hilo " + Thread.currentThread().getName()))
                .subscribe(x -> System.out.println("Subscribe en el hilo " + Thread.currentThread().getName()));

        Thread.sleep(2000);
    }

    @Test
    @SneakyThrows
    void displayNumbersOnParallel() {

        Flux
                .range(1,5)
                .doOnNext(x -> System.out.println("Antes: " + x + " en el hilo " + Thread.currentThread().getName()))
                .parallel() //importa el orden donde se ponga
                .runOn(Schedulers.boundedElastic())
                .map(x -> x*10)
                .map(x -> x+1)
                .doOnNext(x -> System.out.println("Despues: " + x + " en el hilo " + Thread.currentThread().getName()))
                .sequential()
                .subscribe(x -> System.out.println("Subscribe en el hilo " + Thread.currentThread().getName()));

        Thread.sleep(2000);
    }

    @Test
    @SneakyThrows
    void displayNumbersMethodsDo() {

        Flux
                .range(1,2)
                .doOnNext(x -> System.out.println("Antes: " + x + " en el hilo " + Thread.currentThread().getName()))
                .map(x -> x*10)
                .doOnNext(x -> System.out.println("Despues: " + x + " en el hilo " + Thread.currentThread().getName()))
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .doOnComplete(() -> System.out.println("Accion cuando se complete"))
                .subscribe(x -> System.out.println("Subscribe en el hilo " + Thread.currentThread().getName()));

        Thread.sleep(2000);
    }

    @Test
    void fromCallable() {

        Mono
                .fromCallable(() -> {
                    //To use non-reactive calls
                    return 123;
                })
                .subscribe(System.out::println);
    }

    @Test
    void map(){
        convertToInteger(new Flux.of("1", "2", "3"));
    }

    @Test
    void flatMap(){
        convertToIntegerFlatMap(xnew Flux.of("1", "2", "3"));
    }

    public Flux<Integer> convertToInteger(Flux<String> flux) {
        return flux.map(Integer::valueOf);
    }


    public Flux<Integer> convertToIntegerFlatMap(Flux<String> flux) {
        return flux.flatMap(sttring -> Mono.just(Integer.valueOf(string)));
    }
}
