import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

public class OtherOperatorsTest {

    @Test
    void take() {
        Flux
                .just(1, 2, 3, 4, 5)
                .take(2)
                .subscribe(System.out::println);
    }

    @Test
    void takeLast() {
        Flux
                .just(1, 2, 3, 4, 5)
                .takeLast(2)
                .subscribe(System.out::println);
    }

    @Test
    void takeUntil() {
        Flux
                .just(1, 2, 3, 4, 5)
                .takeUntil(x -> x > 3)
                .subscribe(System.out::println);
    }

    @Test
    void takeWhile() {
        Flux
                .just(1, 2, 3, 4, 5)
                .takeWhile(x -> x != 3)
                .subscribe(System.out::println);
    }
}
