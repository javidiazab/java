import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class StepVerifierTest {

    @Test
    void example() {

        var flux = Flux.just(1,2,3);

        StepVerifier
                .create(flux)
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .verifyComplete();
    }

    @Test
    void example2() {

        var mono = Mono.error(new RuntimeException("test"));

        StepVerifier
                .create(mono)
                .expectError()
                .verify();
    }
}
