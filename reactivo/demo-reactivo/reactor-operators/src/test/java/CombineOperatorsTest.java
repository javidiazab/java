import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class CombineOperatorsTest {

    @Test
    @SneakyThrows
    void merge() {

        var flux1 = Flux.interval(Duration.ofMillis(200)).doOnNext(x -> System.out.println("Flux1 with value: " + x));
        var flux2 = Flux.interval(Duration.ofMillis(200)).doOnNext(x -> System.out.println("Flux2 with value: " + x));

        Flux
                .merge(flux1, flux2) //mezcla valores de ambos, acorde a cuando llegan
                .subscribe();

        Thread.sleep(2000);
    }

    @Test
    @SneakyThrows
    void concat() {

        var flux1 = Flux.interval(Duration.ofMillis(200)).doOnNext(x -> System.out.println("Flux1 with value: " + x));
        var flux2 = Flux.interval(Duration.ofMillis(200)).doOnNext(x -> System.out.println("Flux2 with value: " + x));

        Flux
                .concat(flux1, flux2) //Termina primero uno y despues empieza con el otro
                .subscribe();

        Thread.sleep(2000);
    }

    @Test
    @SneakyThrows
    void zipWith() {

        var flux1 = Flux.just(1).delayElements(Duration.ofMillis(300)).doOnNext(x -> System.out.println("Mono 1"));
        var flux2 = Flux.just(2).delayElements(Duration.ofMillis(100)).doOnNext(x -> System.out.println("Mono 2"));

        flux1
                .zipWith(flux2) //no espera por el resultado anterior
                .subscribe(System.out::println);

        Thread.sleep(2000);
    }

    @Test
    @SneakyThrows
    void zipWhen() {

        var flux1 = Flux.just(1).delayElements(Duration.ofMillis(300)).doOnNext(x -> System.out.println("Mono 1"));
        var flux2 = Flux.just(2).delayElements(Duration.ofMillis(100)).doOnNext(x -> System.out.println("Mono 2"));

        flux1.next()
                .zipWhen(x -> flux2.next()) //depende del resultado anterior
                .subscribe(System.out::println);

        Thread.sleep(2000);
    }
}
