import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.util.retry.RetryBackoffSpec;

import java.time.Duration;

public class ErrorsTest {

    //Normalmente el manejo de errores se hace a nivel de mono en nuestro codigo, del flux se
    //encarga webflux

    @Test
    void errorExample() {

        Flux
                .just(0, 1)
                .doOnNext(System.out::println)
                .map(x -> 12/x)
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe();
    }

    @Test
    void doOnError() {

        //Muestra info cuando se produce un error pero lo deja seguir
        Flux
                .just(0, 1)
                .doOnNext(System.out::println)
                .map(x -> 12/x)
                .doOnError(x -> System.out.println("Reportando error: " + x.getMessage()))
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe();
    }

    @Test
    void onErrorMap() {

        //Captura la exception y genera una propia
        Flux
                .just(0, 1)
                .doOnNext(System.out::println)
                .map(x -> 12/x)
                .onErrorMap(x -> new IllegalAccessException("my mensaje"))
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe();
    }

    @Test
    void onErrorResume() {

        //Si hay error sigue el flujo con un fallback
        Flux
                .just(0, 1)
                .doOnNext(System.out::println)
                .map(x -> 12/x)
//                .onErrorResume(x -> Mono.just(10)) //ignora el resto de los valores y usa solo este
                .onErrorResume(x -> Flux.just(10, 11)) //ignora el resto de los valores y usa solo este
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe(System.out::println);
    }

    @Test
    void onErrorContinue() {

        //Si hay error haz algo con el pero sigue el flujo
        Flux
                .just(0, 1)
                .doOnNext(System.out::println)
                .map(x -> 12/x)
                .onErrorContinue((error, value) -> System.out.println("Error: " + error.getMessage()))
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe(System.out::println);
    }

    @Test
    void retries() {

        Mono
                .just(0)
                .doOnNext(x -> System.out.println("Value is: " + x))
                .map(x -> 12/x)
                .retry(2)
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe(System.out::println);
    }

    @Test
    @SneakyThrows
    void retries2() {

        Mono
                .just(0)
                .doOnNext(x -> System.out.println("Value is: " + x))
                .map(x -> 12/x)
                .retryWhen(RetryBackoffSpec
                        .backoff(2, Duration.ofMillis(100))
                        .doBeforeRetry(x -> System.out.println("Retrying because " + x.failure().getMessage()+".........."))
                        .onRetryExhaustedThrow((x,y) -> new RuntimeException("Max of retries reached"))
                )
                .doFinally(signalType -> System.out.println("Completed with signal: " + signalType))
                .subscribe(System.out::println);

        Thread.sleep(5000); //este no es el metodo correcto, ver el siguiente test
    }

    @Test
    @SneakyThrows
    void retries3() {

        var mono = Mono
                .just(0)
                .doOnNext(x -> System.out.println("Value is: " + x))
                .map(x -> 12/x)
                .retryWhen(RetryBackoffSpec
                        .backoff(2, Duration.ofMillis(100))
                        .doBeforeRetry(x -> System.out.println("Retrying because " + x.failure().getMessage()+".........."))
                        .onRetryExhaustedThrow((x,y) -> new RuntimeException("Max of retries reached"))
                );

        StepVerifier
                .create(mono)
                .expectError(RuntimeException.class)
                .verify();
    }
}
