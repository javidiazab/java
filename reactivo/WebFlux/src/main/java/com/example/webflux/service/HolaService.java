package com.example.webflux.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class HolaService {
    public Mono<String> hola() {
        return Mono.just("hola asincrono").delayElement(Duration.ofSeconds(3));
    }

    public Mono<String> hola2() {
        return Mono.just("hola2 asincrono").delayElement(Duration.ofSeconds(3));
    }
}
