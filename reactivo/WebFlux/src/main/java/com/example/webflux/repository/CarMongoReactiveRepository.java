package com.example.webflux.repository;

import com.example.webflux.model.Car;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;

public interface CarMongoReactiveRepository extends ReactiveSortingRepository<Car, Long> {
}
