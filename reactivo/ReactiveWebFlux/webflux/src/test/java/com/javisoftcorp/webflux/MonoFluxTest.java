package com.javisoftcorp.webflux;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonoFluxTest {

    @Test
    void testMono() {
        Mono<?> javi_mono = Mono.just("Javi Mono")
               // .then(Mono.error(new RuntimeException("exception ocurred")))
                .log();
        javi_mono.subscribe(System.out::println);
    }

    @Test
    void testFlux() {
        Flux<String> javi_flux = Flux.just("Spring", "Sprong Boot", "Hibernate", "microservice")
                .concatWithValues("AWS")
              //  .concatWith(Flux.error(new RuntimeException("exception ocurred in flux")))
                .concatWithValues("Cloud")
                .log();

        javi_flux.subscribe(System.out::println);


    }
}
