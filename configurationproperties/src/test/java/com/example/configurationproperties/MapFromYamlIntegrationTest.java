package com.example.configurationproperties;

import com.example.configurationproperties.config.AdditionalClientProperties;
import com.example.configurationproperties.config.ClientProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
class MapFromYamlIntegrationTest {
    @Autowired
    private ClientProperties clientProperties;

    @Test
    void test1() {
        Map<String, AdditionalClientProperties> map1 = clientProperties.getClientPropertiesMap();
        System.out.println("Contains 4001: " + map1.containsKey("4001"));
        AdditionalClientProperties additionalClientProperties = map1.get("4001");
        List<String> bics = additionalClientProperties.getBics();
        bics.forEach(System.out::println);

        Map<String, String> map2 = clientProperties.getBicToClientId();
        System.out.println("Contains desc: " + map2.containsKey("desc"));
        System.out.println("desc: " + map2.get("desc"));

        Map<String, List<String>> map3 = clientProperties.getConfig();
        System.out.println("Contains ips: " + map3.containsKey("ips"));
        List<String> strings = map3.get("ips");
        strings.forEach(System.out::println);
    }
}