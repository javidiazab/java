package com.example.configurationproperties.config;

import java.util.List;

public class AdditionalClientProperties {
    private List<String> bics;

    public List<String> getBics() {
        return this.bics;
    }

    public void setBics(final List<String> bics) {
        this.bics = bics;
    }

    public AdditionalClientProperties() {
    }

    public AdditionalClientProperties(final List<String> bics) {
        this.bics = bics;
    }
}