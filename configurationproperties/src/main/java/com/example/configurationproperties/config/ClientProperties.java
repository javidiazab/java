package com.example.configurationproperties.config;

import java.util.List;
import java.util.Map;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Configuration
@ConfigurationProperties("sepa-sponsored-config")
public class ClientProperties {
    private List<String> useTlmEnrich;
    private List<String> useIberpayClearingHouse;
    private List<String> useOutFreezedFolder;
    private List<String> useSettlementFile;
    private List<String> useSepaServiceLevel;
    private List<String> avoidPagoNxtPriorityRoute;
    private List<String> avoidOrchestrator;
    private List<String> avoidReturnedEvent;
    private List<String> accountHasWordSaving;
    private Map<String, AdditionalClientProperties> clientPropertiesMap;
    private Map<String, String> bicToClientId;
    private Map<String, List<String>> config;
}
