package es.slaserybelleza.test1.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/ping")
@Slf4j
public class ServerTestController {

    @GetMapping
    public String pin(){
        log.info("Received call /ping");
        return "Conextion stablished " + LocalDateTime.now();
    }
}
